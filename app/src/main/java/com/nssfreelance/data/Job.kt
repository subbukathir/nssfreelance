package com.nssfreelance.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Job(
    val client_job_post_id:Int,
    val job_title:String,
    val job_starts_from:String?,
    val job_ends_on:String?,
    val job_highlights:String?,
    val job_overview:String?,
    val job_requirement:String?,
    val job_post_created_on:String?,
    val job_city_id:Int?,
    val city_name:String?,
    val job_type_id:Int?,
    val job_type:String?,
    val job_price:Long?,
    val bid_price:Int?,
    val is_job_assigned:Int? = 0,
    val client_user_name:String?,
    val client_user_id:Int?,
    var is_favourite:Int? = 0,
    val is_applied:Int = 0,
    val request_progress_id:Int?,
    val skills:String?,
    val state_name:String?="",
    val state_id:String?="",
    val country_name:String?="",
    val country_id:String?="",
    val favourite:Int?=0,
    val rejected:Int?=0,
    val applied:Int?=0,
    var isIcon:Boolean= false,
    var url:String?="",
    var address_line_1:String?="",
    var address_line_2:String?="",
    val first_name:String="",
    val last_name:String="",
    val is_completed:Int?=0,
    val is_freelancer_completed:Int?=0,
    val freelancer_user_id:Int?=0,
    val contract_final_amount:Int=0,
    val applied_date:String?="",
    val shortlisted_date:String?="",
    val accepted_date:String?="",
    val application_status:String?="",
    val contract_review_status:Boolean =false,
    val is_client_reviewed:Int?=0,
    val job_completed_on:String?=""

):Parcelable


