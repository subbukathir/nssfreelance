package com.nssfreelance.data

data class SearchJobFreelancer(
    val job_title:String,
    val job_overview:String?,
    val skill:String?,
    val client_job_post_id:String?,
    val started_on:String?,
    val job_highlights:String?,
    val job_requirement:String?,
    val job_price:Long?,
    val city_name:String?,
    val country_name:String?,
    val first_name:String?,
    val last_name:String?,
    val is_favourite:Int? = 0,
    val rating:String?,
    val posted_on:String?,
    val additional_param_2:String?="",
)
