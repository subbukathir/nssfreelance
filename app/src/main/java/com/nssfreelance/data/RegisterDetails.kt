package com.nssfreelance.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegisterDetails(
    val gender:List<Gender>,
    val skills:List<Skills>,
    val job_type:List<JobType>,
    val user_type:List<UserType>,
) :Parcelable{
    @Parcelize
    data class Gender(
        val gender_id:Int,
        val gender:String,
        val gender_code:String
    ):Parcelable {
        override fun toString(): String {
            return "Gender(gender_id=$gender_id, gender='$gender', gender_code='$gender_code')"
        }
    }

    @Parcelize
    data class Skills(
        val skill_id:Int,
        val skill:String,
        val skill_code:String,
        var isChecked:Boolean = false
    ):Parcelable

    @Parcelize
    data class JobType(
        val job_type_id:Int,
        val job_type:String,
        val job_type_code:String
    ):Parcelable

    @Parcelize
    data class UserType(
        val user_type_id:Int,
        val user_type:String,
        val user_type_code:String
    ):Parcelable
}


