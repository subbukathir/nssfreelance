package com.nssfreelance.data

data class SearchByCategory(
    val job_category_id:Int,
    var job_category:String,
    var asset_link:String,
    var isSelected:Boolean = false
)
