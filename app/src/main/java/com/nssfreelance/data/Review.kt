package com.nssfreelance.data

data class Review(
    val client_job_review_id: Int,
    val review: String,
    val client_job_post_id: Int,
    val job_title: String,
    val job_starts_from: String,
    val job_ends_on: String,
    val job_price: String,
    val contract_final_amount:Int=0,
    val reviewsubmitdate:String,
    val rating:Int?,
    val client_user_id:String,
    val name:String

)

