package com.nssfreelance.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class JobDetails(
    val client_job_post_id: String="",
    val job_title: String="",
    val job_price: Long=0,
    val job_starts_from: String="",
    val job_ends_on: String="",
    val job_post_created_on: String="",
    val job_requirement: String="",
    val skills: String?="",
    val location: String="",
    val job_overview: String="",
    val job_qualification: String="",
    val job_highlights: String="",
    val job_city_id: String="",
    val city_name: String="",
    val state_name:String="",
    val country_name:String="",
    val job_type_id: String="",
    val job_type: String="",
    val is_job_assigned: String?="",
    val client_user_name: String="",
    val client_user_id: String="",
    val is_favourite: Int? = 0,
    val is_applied: Int? = 0,
    val is_offered: Int? = 0,
    val is_shortlisted: Int? = 0,
    val is_rejected: Int? = 0,
    val request_progress_id: String="",
    val address_line_1:String?="",
    val address_line_2:String?="",
    val is_payment_settled:Boolean=false,
):Parcelable


