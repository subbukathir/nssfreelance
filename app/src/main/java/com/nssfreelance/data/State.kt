package com.nssfreelance.data

data class State(
    val state_id:Int,
    val state_name:String,
    val state_short_code:String,
    val country_name:String
)
