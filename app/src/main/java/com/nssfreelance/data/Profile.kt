package com.nssfreelance.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Profile(
    val name:String,
    val first_name:String?,
    val last_name:String?,
    val country_name:String?,
    val state_name:String?,
    val city_name:String?,
    val address:String?,
    val email_address:String,
    val mobile_no:String?,
    val about_me:String?,
    val profile_photo_link:String?,
    val registered_on:String?,
    val skills:String?,
    val total_earnings:Int,
    val amount_collected:Int,
    val amount_pending:Int,
    val application_submitted:Int,
    val review_count:Int,
    val rating_count:Int,
    //client
    val full_name:String,
    val gender:String,
    val date_of_birth:String,
    val qualification:String?,
    val designation:String?,
    val joined_date:String,
    val photo_profile_url:String?,
    val money_spent:String?,
    val open_job_count:Int = 0,
    val completed_job_count:Int = 0,
    val submission_count:Int=0,
    val posted_job_count:Int=0,
    val rating:Int?=0,
    val is_paid:Int = 0,
):Parcelable


