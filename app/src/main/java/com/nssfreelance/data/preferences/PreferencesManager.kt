package com.nssfreelance.data.preferences

import android.content.Context
import android.util.Log
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.createDataStore
import com.nssfreelance.util.Const
import com.nssfreelance.util.Const.PREFERENCE_NIGHT_MODE_DEF_VAL
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "PreferencesManager"
@Singleton
class PreferencesManager @Inject constructor(@ApplicationContext context: Context){
    private val datastore = context.createDataStore("freelance_preferences")

    val preferencesFlow = datastore.data
        .catch { exception ->
            if(exception is IOException) {
                Log.e(TAG, "Error reading preferences ", exception)
                emit(emptyPreferences())
            }else{
                throw exception
            }
        }
        .map { preferences ->
            val isSignInCompleted = preferences[PreferencesKeys.SIGNIN_COMPLETED]?:false
            val isSignupCompleted = preferences[PreferencesKeys.SIGNUP_COMPLETED] ?:false
            val isSkillsNotUpdated = preferences[PreferencesKeys.SIGNUP_SKILLS_NOT_UPDATED] ?:false
            val userId = preferences[PreferencesKeys.USER_ID] ?:""
            val userTypes = preferences[PreferencesKeys.USER_TYPES] ?:""
            val userToken = preferences[PreferencesKeys.USER_TOKEN] ?:""
            val userType = preferences[PreferencesKeys.USER_TYPE] ?:0
            val userDetails = preferences[PreferencesKeys.USER_DETAILS]?:""
            val location = preferences[PreferencesKeys.LOCATION]?:""
            val locality = preferences[PreferencesKeys.LOCALITY]?:""
            val username = preferences[PreferencesKeys.USERNAME]?:""
            val isDarkMode = preferences[PreferencesKeys.IS_DARK_MODE]?:PREFERENCE_NIGHT_MODE_DEF_VAL
            val userTypeCode = preferences[PreferencesKeys.USER_TYPE_CODE]?: Const.FREELANCER
            val fcmToken = preferences[PreferencesKeys.FCM_TOKEN]?: ""
            val profileUrl = preferences[PreferencesKeys.PROFILE_URL]?:""
            val isPaid = preferences[PreferencesKeys.IS_PAID]?:false
            val uid = preferences[PreferencesKeys.UID]?:""

            FreelancerPreferences(
                isSignInCompleted,
                isSignupCompleted,
                isSkillsNotUpdated,
                userId,
                userTypes,
                userToken = userToken,
                userType = userType,
                userTypeCode = userTypeCode,
                userDetails = userDetails,
                location = location,
                locality = locality,
                username,
                isDarkMode,
                fcmToken,
                profileUrl,
                isPaid,
                uid,
            )
        }

    suspend fun updateSignIn(signInCompleted: Boolean){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.SIGNIN_COMPLETED] = signInCompleted
        }
    }
    suspend fun updateSignUp(signUpCompleted: Boolean){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.SIGNUP_COMPLETED] = signUpCompleted
        }
    }

    suspend fun updateSkillsNotUpdated(skillsUpdated:Boolean){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.SIGNUP_SKILLS_NOT_UPDATED] = skillsUpdated
        }
    }

    suspend fun updateUserId(userId: String){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.USER_ID] = userId
        }
    }

    suspend fun updateUserType(userType: Int){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.USER_TYPE] = userType
        }
    }

    suspend fun updateUserTypeCode(userType: String){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.USER_TYPE_CODE] = userType
        }
    }

    suspend fun updateUserTypes(userTypes: String){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.USER_TYPES] = userTypes
        }
    }
    suspend fun updateLocation(location: String){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.LOCATION] = location
        }
    }
    suspend fun updateLocality(locality: String){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.LOCALITY] = locality
        }
    }
    suspend fun updateUsername(username: String){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.USERNAME] = username
        }
    }

    suspend fun updateUserToken(userToken: String){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.USER_TOKEN] = userToken
        }
    }

    //whole register details
    suspend fun updateUserDetails(userDetails: String){
        datastore.edit { preferences ->
            preferences[PreferencesKeys.USER_DETAILS] = userDetails
        }
    }

    suspend fun updateTheme(isDarkMode: Int){
        datastore.edit { preferences -> preferences[PreferencesKeys.IS_DARK_MODE] = isDarkMode }
    }

    suspend fun updateFcmToken(fcmToken: String){
        datastore.edit { preferences -> preferences[PreferencesKeys.FCM_TOKEN] =fcmToken }
    }

    suspend fun updateProfileUrl(profileUrl: String){
        datastore.edit { preferences -> preferences[PreferencesKeys.PROFILE_URL] = profileUrl }
    }

    suspend fun updateIsPaid(isPaid:Boolean){
        datastore.edit { preferences -> preferences[PreferencesKeys.IS_PAID] = isPaid }
    }

    suspend fun clearDatastore(){
        datastore.edit { preferences -> preferences.clear() }
    }

    suspend fun updateUID(uid: String) {
        datastore.edit { preferences -> preferences[PreferencesKeys.UID] = uid }
    }

    private object PreferencesKeys{
        val IS_PAID = booleanPreferencesKey("is_paid")
        val SIGNIN_COMPLETED = booleanPreferencesKey("signin_completed")
        val SIGNUP_COMPLETED = booleanPreferencesKey("signup_completed")
        val SIGNUP_SKILLS_NOT_UPDATED = booleanPreferencesKey("signup_skills_not_updated")
        val USER_ID = stringPreferencesKey("user_id")
        val USER_TYPES = stringPreferencesKey("user_types")
        val USER_TOKEN = stringPreferencesKey("user_token")
        val USER_TYPE = intPreferencesKey("user_type")
        val USER_TYPE_CODE = stringPreferencesKey("user_type_code")
        val USER_DETAILS = stringPreferencesKey("user_details")
        val LOCATION = stringPreferencesKey("location")
        val LOCALITY = stringPreferencesKey("locality")
        val USERNAME = stringPreferencesKey("username")
        val IS_DARK_MODE = intPreferencesKey("is_dark_theme")
        val FCM_TOKEN = stringPreferencesKey("fcm_token")
        val PROFILE_URL= stringPreferencesKey("profile_url")
        val UID= stringPreferencesKey("uid")
    }
}

data class FreelancerPreferences(
    val isSignInCompleted:Boolean,
    val isSignupCompleted:Boolean,
    val isSkillsNotUpdated:Boolean,
    val userId:String,
    val userTypes:String,
    val userToken:String,
    val userType:Int,
    val userTypeCode:String,
    val userDetails:String,
    val location:String,
    val locality:String,
    val firstname: String,
    val isDarkMode:Int,
    val fcmToken:String,
    val profileUrl:String,
    val isPaid:Boolean,
    val uid:String,
)