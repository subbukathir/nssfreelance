package com.nssfreelance.data

data class Notification(
    val notification_id:Int?,
    val notification_on:String?,
    val client_job_post_id:Int?,
    val client_user_id:Int?,
    val freelancer_name:String?,
    val client_name:String?,
    val freelancer_profile_url:String?,
    val client_profile_url:String?,
    val freelancer_user_id:Int?,
    val job_title:String?,
    val notification_status_code:String?,
    val job_status_code:String?,
    val descriptions:String?
)
