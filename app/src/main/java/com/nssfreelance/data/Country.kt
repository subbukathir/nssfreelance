package com.nssfreelance.data

data class Country(
    val country_id:Int,
    val country_name:String,
    val country_short_code:String,
    val country_code:String
)
