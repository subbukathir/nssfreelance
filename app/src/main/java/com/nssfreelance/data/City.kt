package com.nssfreelance.data

data class City(
    val city_id:Int,
    val city_name:String,
    val city_short_code:String,
    val state_name:String,
)
