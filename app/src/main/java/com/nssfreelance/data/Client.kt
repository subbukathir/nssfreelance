package com.nssfreelance.data

data class Client(
    val full_name:String?="",
    val first_name:String?="",
    val last_name:String?="",
    val email_address:String?="",
    val gender:String?="",
    val city_name:String?="",
    val state_name:String?="",
    val country_name:String?="",
    val date_of_birth:String?="",
    val mobile_no:String?="",
    val address:String?="",
    val qualification:String?="",
    val about_me:String?="",
    val designation:String?="",
    val photo_profile_url:String?="",
    val money_spent:Long?=0,
    val open_job_count:Int?=0,
    val completed_job_count:Int?=0,
    val submission_count:Int?=0,
    val rating:String?="",
    val is_paid:Int?=0,
    val joined_date:String?="",
    val profile_photo_link:String?="",
    val posted_job_count:Int?=0,
    val rating_count:Int?=0
)


