package com.nssfreelance.data

data class HomeClientJob(
    val client_job_post_id:Int,
    val job_title:String,
    val job_starts_from:String,
    val job_ends_on:String,
    val job_highlights:String,
    val job_overview:String,
    val job_requirement:String,
    val job_post_created_on:String,
    val job_price:String,
    val country_name:String,
    val country_id:String,
    val city_id:String,
    val city_name:String,
    val job_type_id:String,
    val job_type:String,
    val is_job_assigned:Int?=0,
    val address_line_1:String,
    val address_line_2:String,
    val is_payment_settled:Boolean=false,
    val url:String?,
    val request_progress_id:Int,
    val applied_count:Int,
    val rejected_count:Int,
    val shortlisted_count:Int,
    val skills:String?
)
