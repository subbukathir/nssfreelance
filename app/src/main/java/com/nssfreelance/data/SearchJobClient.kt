package com.nssfreelance.data

data class SearchJobClient(
    val user_id: Int,
    val full_name: String?,
    val taxonomy_name: String?,
    val photo_profile_url: String?,
    val city_name: String?,
    val state_name: String?,
    val country_name: String?,
    val hourly_price: String?,
    val joined_since: String?,
    val is_favourite:Int?=0,
    val completed_job_count:Int?=0,
    val rating:Int?=0,
    val rating_count:Int?=0,
    val skills:String?="",
    val bid_price:String?="",
    val client_job_post_id:Int=0,
    val country:String="",
    val is_invited:Int=0
)

