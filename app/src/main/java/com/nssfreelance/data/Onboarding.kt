package com.nssfreelance.data

data class Onboarding(
    val onboardingImg:Int,
    val onboardingMsg:String
)
