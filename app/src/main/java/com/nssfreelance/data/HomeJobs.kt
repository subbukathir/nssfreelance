package com.nssfreelance.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HomeJobs(
    val jobNameValue:String,
    val jobName:String,
    val jobs:List<Job>
):Parcelable
