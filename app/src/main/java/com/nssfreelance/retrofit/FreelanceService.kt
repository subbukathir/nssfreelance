package com.nssfreelance.retrofit

import com.nssfreelance.BuildConfig
import com.nssfreelance.retrofit.request.*
import com.nssfreelance.retrofit.response.*
import retrofit2.http.*

interface FreelanceService {
    companion object {
        const val BASE_URL =BuildConfig.BASE_URL
    }

    @POST("public/add/device_token")
    suspend fun updateFcmToken(
        @Body params: FcmTokenParams
    ): LoginResponse

    @GET("public/job/category")
    suspend fun searchCategoryList(): CategoryListResponse

    @GET("public/public_types")
    suspend fun registerDetails(): RegisterDetailsResponse

    @GET("public/country")
    suspend fun getCountry(): CountryResponse

    @POST("auth/register/{user_type}")
    suspend fun register(
        @Path("user_type") user_type: String,
        @Body params: RegisterParams
    ): LoginResponse

    @GET("public/state/{country_id}")
    suspend fun getState(
        @Path("country_id", encoded = true) country_id: String
    ): StateResponse

    @GET("public/city/{state_id}")
    suspend fun getCity(
        @Path("state_id", encoded = true) stateId: String
    ): CityResponse

    @POST("auth/login/{user_type}")
    suspend fun login(
        @Path("user_type") type: String,
        @Body params: LoginParams
    ): LoginResponse

    @FormUrlEncoded
    @POST("fb/register/{user_type}")
    suspend fun registerWithFb(
        @Path("user_type") userType: String,
        @FieldMap params: HashMap<String, String>
    ): LoginResponse

    //skills
    @POST("jobPosts/freelancer/AddSkills")
    suspend fun postSkills(
        @Body params: SkillsParams
    ): LoginResponse

    @FormUrlEncoded
    @POST("jobPosts/freelancer/profile")
    suspend fun profile(
        @Header("Authorization") token: String,
        @FieldMap params: HashMap<String, String>
    ): ProfileResponse

    @FormUrlEncoded
    @POST("jobPosts/freelancer/all_jobs")
    suspend fun allJobs(
        @Header("Authorization") token: String,
        @FieldMap params: HashMap<String, String>
    ): AllJobsResponse

    @FormUrlEncoded
    @POST("jobPosts/freelancer/recommended_jobs")
    suspend fun recommendedJobs(
        @Header("Authorization") token: String,
        @FieldMap params: HashMap<String, String>
    ): AllJobsResponse

    @FormUrlEncoded
    @POST("jobPosts/freelancer/trending_jobs")
    suspend fun trendingJobs(
        @Header("Authorization") token: String,
        @FieldMap params: HashMap<String, String>
    ): AllJobsResponse

    @FormUrlEncoded
    @POST("jobPosts/freelancer/nearby_jobs")
    suspend fun nearbyJobs(
        @Header("Authorization") token: String,
        @FieldMap params: HashMap<String, String>
    ): AllJobsResponse

    @FormUrlEncoded
    @POST("jobPosts/freelancer/recent_jobs")
    suspend fun latestJobs(
        @Header("Authorization") token: String,
        @FieldMap params: HashMap<String, String>
    ): AllJobsResponse

    @FormUrlEncoded
    @POST("jobPosts/freelancer/{end_url}")
    suspend fun getJobsByUrl(
        @Header("Authorization") token: String,
        @Path("end_url") url: String,
        @FieldMap params: HashMap<String, String>
    ): AllJobsResponse

    @POST("jobPosts/freelancer/job_details/{job_id}")
    suspend fun getJobDetails(
        @Header("Authorization") token: String,
        @Path("job_id") jobId: String
    ): JobDetailsResponse

    @POST("jobPosts/freelancer/favourite_or_apply_job")
    suspend fun applyFavouriteJob(
        @Header("Authorization") token: String,
        @Body params: ApplyJobFavouriteParams
    ): LoginResponse

    @POST("jobPosts/freelancer/complete_by_freelancer")
    suspend fun completeJob(
        @Header("Authorization") token: String,
        @Body params: CompleteFreelancerJobParams
    ): LoginResponse

    @GET("jobPosts/freelancer/search/{search_str}")
    suspend fun getSearchResult(
        @Header("Authorization") token: String,
        @Path("search_str") searchString: String
    ): SearchJobsResponse

    /*Client api's*/

    @FormUrlEncoded
    @POST("jobPosts/client/profile_view")
    suspend fun clientProfile(
        @Header("Authorization") token: String,
        @FieldMap params: HashMap<String, String>
    ): ProfileClientResponse

    @FormUrlEncoded
    @POST("jobPosts/client/posted_jobs")
    suspend fun getClientPostedJobs(
        @Header("Authorization") userToken: String,
        @FieldMap params: HashMap<String, String>
    ): AllJobsResponse

    @FormUrlEncoded
    @POST("jobPosts/freelancer/applied_jobs")
    suspend fun getClientHomeJobs(
        @Header("Authorization") token: String,
        @FieldMap params: HashMap<String, String>
    ): AllClientJobResponse

    @FormUrlEncoded
    @POST("jobPosts/client/specific_job/{end_url}")
    suspend fun getClientJobsByUrl(
        @Header("Authorization") token: String,
        @Path("end_url") url: String,
        @FieldMap params: HashMap<String, String>
    ): AllClientJobResponse

    @POST("jobPosts/client/create_jobPost")
    suspend fun createJobPost(
        @Header("Authorization") token: String,
        @Body params: CreateJobPostParams
    ): LoginResponse


    @GET("jobPosts/freelancer/skillsearch/{search_str}")
    suspend fun getSearchClientResult(
        @Header("Authorization") token: String,
        @Path("search_str") searchString: String
    ): SearchJobsClientResponse

    @FormUrlEncoded
    @POST("jobPosts/client/{end_url}")
    suspend fun getClientActiveJobs(
        @Header("Authorization") token: String,
        @Path("end_url") url: String,
        @FieldMap params: HashMap<String, String>
    ): AllClientJobResponse

    @FormUrlEncoded
    @POST("jobPosts/client/{end_url}")
    suspend fun getHomeClientJobs(
        @Header("Authorization") token: String,
        @Path("end_url") url: String,
        @FieldMap params: HashMap<String, String>
    ): HomeClientJobResponse


    @FormUrlEncoded
    @POST("jobPosts/client/{end_url}")
    suspend fun getClientJobDetail(
        @Header("Authorization") token: String,
        @Path("end_url") url: String,
        @FieldMap params: HashMap<String, String>
    ): ClientJobDetailReponse


    @POST("jobPosts/client/update/jobstatus")
    suspend fun updateJobStatus(
        @Header("Authorization") token: String,
        @Body params: UpdateJobStatusParams
    ): LoginResponse


    @POST("jobPosts/client/{end_url}")
    suspend fun updateFavouriteInvite(
        @Header("Authorization") token: String,
        @Path("end_url") url: String,
        @Body params: InviteFavouriteClientParams
    ): LoginResponse

    @POST("payments/create-paymentIntent")
    suspend fun getPaymentSecret(
        @Header("Authorization") token: String,
        @Body params: PaymentSecretParams
    ): PaymentSecretResponse


    @POST("payments/store-transaction")
    suspend fun storePaymentSuccess(
        @Header("Authorization") token: String,
        @Body params: StorePaymentParams
    ): LoginResponse

    @POST("admin/updateUser")
    suspend fun updateProfile(
        @Header("Authorization")
        token: String,
        @Body params: UpdateProfileParams): LoginResponse


    @FormUrlEncoded
    @POST("auth/otpverify")
    suspend fun verifyOtp(
        @FieldMap params: HashMap<String, String>
    ): LoginResponse

    @FormUrlEncoded
    @POST("auth/resendotp")
    suspend fun resendOtp(
        @FieldMap params: HashMap<String, String>
    ): LoginResponse

    @FormUrlEncoded
    @POST("admin/forget_password")
    suspend fun forgotPassord(
        @FieldMap params: HashMap<String, String>
    ): LoginResponse


    @POST("jobPosts/client/contract_details")
    suspend fun requestContract(
        @Header("Authorization")
        token: String,
        @Body params: ContractRequestParams): ContractDetailResponse

    @POST("jobPosts/client/addReview")
    suspend fun addReview(
        @Header("Authorization")
        token: String,
        @Body params: AddReviewParams): LoginResponse

    @POST("jobPosts/freelancer/addReview")
    suspend fun addReviewFreelancer(
        @Header("Authorization")
        token: String,
        @Body params: AddReviewParams): LoginResponse


    @POST("jobPosts/freelancer/addRating")
    suspend fun addRatingFreelancer(
        @Header("Authorization")
        token: String,
        @Body params: AddReviewParams): LoginResponse


    @POST("jobPosts/client/addRating")
    suspend fun addRatingClient(
        @Header("Authorization")
        token: String,
        @Body params: AddReviewParams): LoginResponse

    @GET("public/notification/{user_id}")
    suspend fun getNotifications(
        @Header("Authorization") token: String,
        @Path("user_id") searchString: String
    ): NotificationResponse
}