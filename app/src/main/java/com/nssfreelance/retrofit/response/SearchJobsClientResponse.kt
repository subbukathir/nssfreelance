package com.nssfreelance.retrofit.response

import com.nssfreelance.data.SearchJobClient

data class SearchJobsClientResponse(
    val status:Boolean,
    val message:String,
    val error:ErrorResponse,
    val data:List<SearchJobClient>
)
