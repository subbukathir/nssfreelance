package com.nssfreelance.retrofit.response

import com.nssfreelance.data.City
import com.nssfreelance.data.State

data class CityResponse(
    val status:Boolean,
    val message:String,
    val data:List<City>
)
