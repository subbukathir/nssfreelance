package com.nssfreelance.retrofit.response

import com.nssfreelance.data.RegisterDetails

data class RegisterDetailsResponse(
    val status:Boolean,
    val message:String,
    val data: RegisterDetails
)
