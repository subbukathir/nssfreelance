package com.nssfreelance.retrofit.response

import com.nssfreelance.data.ContractDetail

data class ContractDetailResponse(
    val status:Boolean,
    val message:String,
    val result:List<ContractDetail>
)
