package com.nssfreelance.retrofit.response

data class PaymentSecretResponse(
    val status:Boolean,
    val clientSecret:String,
    val message:String
)
