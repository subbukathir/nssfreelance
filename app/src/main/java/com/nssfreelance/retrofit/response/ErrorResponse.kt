package com.nssfreelance.retrofit.response

data class ErrorResponse(
    val name:String,
    val message:String
)
