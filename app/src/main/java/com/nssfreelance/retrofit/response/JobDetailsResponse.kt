package com.nssfreelance.retrofit.response

import com.nssfreelance.data.Client
import com.nssfreelance.data.Job
import com.nssfreelance.data.JobDetails
import com.nssfreelance.data.Review

data class JobDetailsResponse(
    val status:Boolean,
    val message:String,
    val data:JobDetailGroup
){
    data class JobDetailGroup(
        val job_details:JobDetails,
        val client_posted_jobs:List<Job>,
        val review_about_client:List<Review>,
        val about_client:List<Client>
    )
}