package com.nssfreelance.retrofit.response

import com.nssfreelance.data.SearchByCategory

data class CategoryListResponse(
    val status:Boolean,
    val data:List<SearchByCategory>
)
