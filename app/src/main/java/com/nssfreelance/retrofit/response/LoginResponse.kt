package com.nssfreelance.retrofit.response

data class LoginResponse(
    val status:Boolean,
    val message:String,
    val token:String,
    val user_id:Int,
    val uid:String,
)
