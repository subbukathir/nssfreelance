package com.nssfreelance.retrofit.response

import com.nssfreelance.data.Job

data class AllJobsResponse(
    val status:Boolean,
    val message:String,
    val error:ErrorResponse,
    val data:List<Job>
)
