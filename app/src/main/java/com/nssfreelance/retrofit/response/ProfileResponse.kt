package com.nssfreelance.retrofit.response

import com.nssfreelance.data.Profile
import com.nssfreelance.data.Review

data class ProfileResponse(
    val status:Boolean,
    val message:String,
    val data:ProfileData
){
    data class ProfileData(
        val profile:Profile,
        val review:List<Review>,
        val rating:Rating
    ){
        data class Rating(
            val freelancer_user_rating:Int
        )
    }
}
