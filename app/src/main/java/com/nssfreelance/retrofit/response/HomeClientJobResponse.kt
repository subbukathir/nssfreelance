package com.nssfreelance.retrofit.response

import com.nssfreelance.data.HomeClientJob

data class HomeClientJobResponse(
    val status:Boolean = false,
    val data:List<HomeClientJob>
)
