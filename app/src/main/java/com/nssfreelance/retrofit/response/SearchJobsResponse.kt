package com.nssfreelance.retrofit.response

import com.nssfreelance.data.SearchJobFreelancer

data class SearchJobsResponse(
    val status:Boolean,
    val message:String,
    val error:ErrorResponse,
    val data:List<SearchJobFreelancer>
)
