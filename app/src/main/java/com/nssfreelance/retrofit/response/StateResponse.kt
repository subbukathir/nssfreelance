package com.nssfreelance.retrofit.response

import com.nssfreelance.data.State

data class StateResponse(
    val status:Boolean,
    val message:String,
    val data:List<State>
)
