package com.nssfreelance.retrofit.response

import com.nssfreelance.data.Profile
import com.nssfreelance.data.Review

data class ProfileClientResponse(
    val status:Boolean,
    val message:String,
    val data:Profile,
    val review:List<Review>
)
