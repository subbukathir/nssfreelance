package com.nssfreelance.retrofit.response

import com.nssfreelance.data.JobDetails

data class ClientJobDetailReponse(
    val status:Boolean,
    val message:String,
    val data: JobDetails
)
