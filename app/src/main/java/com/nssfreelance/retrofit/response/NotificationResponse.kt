package com.nssfreelance.retrofit.response

import com.nssfreelance.data.Notification

data class NotificationResponse(
    val status:Boolean,
    val message:String,
    val data:List<Notification>
)
