package com.nssfreelance.retrofit.response

import com.nssfreelance.data.Country

data class CountryResponse(
    val status:Boolean,
    val message:String,
    val data:List<Country>
)
