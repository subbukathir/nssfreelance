package com.nssfreelance.retrofit.response

import com.nssfreelance.data.ClientJob

data class AllClientJobResponse(
    val status:Boolean = false,
    val data:List<ClientJob>
)
