package com.nssfreelance.retrofit.request

import com.stripe.android.model.PaymentMethod

data class StorePaymentParams(
    val client_user_id:String="",
    val client_job_post_id:String="",
    val data:PaymentResponse,
) {
    data class PaymentResponse(
        val amount:String?=null,
        val canceledAt:String?=null,
        val captureMethod:String?=null,
        val clientSecret:String?=null,
        val confirmationMethod:String?=null,
        val created:String?=null,
        val currency:String?=null,
        val id:String?=null,
        val isLiveMode:String?=null,
        val paymentMethod:PaymentMethods,
        val paymentMethodId:String,
        var paymentMethodTypes:Array<String>,
        val status:String,
    ) {
        data class PaymentMethods(
            val billingDetails:BillingDetail,
            val card:Card,
        ) {
            data class BillingDetail(
                val address:Address,
            ) {
                data class Address(
                    val postalCode:String="",
                )

            }

            data class Card(
                val brand:String,
                var checks: PaymentMethod.Card.Checks,
                val country:String,
                val expiryMonth:String,
                val expiryYear:String,
                val funding:String,
                val last4:String,
                val networks:Network,
                val threeDSecureUsage:ThreeDUsage,
                val created:String,
                val id:String,
                val liveMode:String,
                val type:String,
            ) {
                data class Network(
                    val available:Array<String>,
                    val selectionMandatory:String,
                )

                data class ThreeDUsage(
                    val isSupported:String
                )

            }

        }
    }

    override fun toString(): String {
        return "StorePaymentParams(client_user_id='$client_user_id', data=$data)"
    }

}
