package com.nssfreelance.retrofit.request

data class ContractRequestParams(
    val freelancer_user_id:Int,
    val client_user_id:Int,
    val client_job_post_id:Int,
)
