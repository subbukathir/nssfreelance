package com.nssfreelance.retrofit.request

data class ForgotPwdParams(
    var uid:String,
    var password:String
)
