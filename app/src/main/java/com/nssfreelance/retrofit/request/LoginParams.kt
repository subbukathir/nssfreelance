package com.nssfreelance.retrofit.request

data class LoginParams(
    val email:String,
    val password:String
)
