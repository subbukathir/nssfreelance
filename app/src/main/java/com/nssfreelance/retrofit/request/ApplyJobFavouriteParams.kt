package com.nssfreelance.retrofit.request

data class ApplyJobFavouriteParams(
    var request_progress_id:String="",
    var client_job_post_id:String="",
    var freelancer_user_id:String="",
    var is_applied:String="",
    var is_favourite:String="",
    var client_user_id:String="",
    var job_bid_price:String="0",
    var job_id:String="",
    var job_type_id:String="10"
)