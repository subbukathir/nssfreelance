package com.nssfreelance.retrofit.request

data class SkillsParams(
    var user_id:Int,
    var skills: List<SkillSet>
){
    data class SkillSet(
        val skill_id:Int
    )
}
