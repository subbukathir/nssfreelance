package com.nssfreelance.retrofit.request

data class UpdateJobStatusParams(
    var status_code: String = "",
    var client_user_id: Int = 0,
    var request_progress_id: Int = 0,
    var freelancer_user_id: Int = 0,
    var client_job_post_id: Int = 0,
    var contract_job_type_id: Int = 0,
    var contract_final_amount: Int = 0,
)
