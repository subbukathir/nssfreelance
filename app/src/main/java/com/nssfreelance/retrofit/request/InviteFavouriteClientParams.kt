package com.nssfreelance.retrofit.request

data class InviteFavouriteClientParams(
    var client_user_id:Int=0,
    var freelancer_user_id:Int=0,
    var client_job_post_id:Int=0,
)
