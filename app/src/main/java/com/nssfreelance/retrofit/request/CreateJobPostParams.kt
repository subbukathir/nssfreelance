package com.nssfreelance.retrofit.request

data class CreateJobPostParams(
    var job_title:String="",
    var job_city_id:String="",
    var job_starts_from:String="",
    var job_ends_on:String="",
    var job_highlights:String="",
    var job_overview:String="",
    var job_requirement:String="",
    var job_qualification:String="",
    var job_type_id:String="",
    var job_price:String="",
    var client_user_id:String="",
    var skills:List<String> = mutableListOf(),
    var job_category_id:String="",
    var address_line_1:String="",
    var address_line_2:String="",
)