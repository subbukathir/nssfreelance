package com.nssfreelance.retrofit.request

data class CompleteFreelancerJobParams(
    val user_id:String,
    val job_id:String,
    val is_completed:String
)