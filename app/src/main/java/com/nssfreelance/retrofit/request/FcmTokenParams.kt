package com.nssfreelance.retrofit.request

data class FcmTokenParams(
    var token:String,
    var user_id:Int,
    var device:String="ANDROID"
)
