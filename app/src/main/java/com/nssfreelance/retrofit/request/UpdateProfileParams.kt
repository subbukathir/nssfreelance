package com.nssfreelance.retrofit.request

data class UpdateProfileParams(
    var uid:String="",
    var user_id:String="",
    var first_name:String="",
    var last_name:String="",
    var mobile_number:String="",
    var email:String="",
    var emailVerified:Boolean=false,
    var profile_photo_link:String="",
    var image:String="",
    var about_me:String=""
)
