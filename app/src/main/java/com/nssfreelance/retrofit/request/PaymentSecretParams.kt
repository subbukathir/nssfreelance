package com.nssfreelance.retrofit.request

data class PaymentSecretParams(
    var items: PaymentParams
) {
    data class PaymentParams(
        var name: String = "",
        var amount: Int = 0
    )
}
