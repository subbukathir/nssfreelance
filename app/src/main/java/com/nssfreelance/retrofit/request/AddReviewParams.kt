package com.nssfreelance.retrofit.request

data class AddReviewParams(
    val freelancer_user_id:Int,
    val client_user_id:Int,
    val client_job_post_id:Int,
    val rating:Float,
    val review:String
)
