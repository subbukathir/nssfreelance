package com.nssfreelance.retrofit.request

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegisterParams(
    val firstName:String?,
    val lastName:String?,
    val email:String?,
    val password:String?,
    val dob:String?,
    val gender_id:String?,
    val city_id:String?,
    val county_id:String?,
    val mobile_no:String?
):Parcelable
