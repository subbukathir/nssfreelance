package com.nssfreelance.di

import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.FreelanceService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {

    @Singleton
    @Provides
    fun provideFreelanceRepository(
            freelanceService: FreelanceService
    ):Repository{
        return Repository(
                freelanceService = freelanceService
        )
    }
}