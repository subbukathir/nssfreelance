package com.nssfreelance.util

interface DTOMapper<DTO,DomainModel> {
    fun mapFromDTO(dto: DTO):DomainModel

    fun mapToEntity(domainModel: DomainModel) :DTO
}