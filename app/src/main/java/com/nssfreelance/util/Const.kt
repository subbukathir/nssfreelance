package com.nssfreelance.util

import androidx.appcompat.app.AppCompatDelegate
import com.nssfreelance.BuildConfig

object Const {

    const val RC_CAMERA_READ_WRITE = 111
    const val CAPTURE_IMAGE = 10
    const val CHOOSE_IMAGE = 11

    const val FREELANCER = "FREELANCER"
    const val USER_ID = "user_id"
    const val RECOMMENDED_JOB = "recommended_jobs"
    const val TRENDING_JOB = "trending_jobs"
    const val NEARBY_JOB = "nearby_jobs"
    const val LATEST_JOB = "ongoing_jobs"
    const val RECENT_JOB = "recent_jobs"
    const val PAST_JOB="past_jobs"
    const val FAVOURITE_JOB = "favourite_freelancer_list"
    const val FAVOURITE_FREELANCER_JOBS = "favourite_jobs"
    const val APPLIED_JOB = "applied_jobs"
    const val PREFERENCE_NIGHT_MODE_DEF_VAL = AppCompatDelegate.MODE_NIGHT_NO

    const val APPLICATIONS = "view_applications"
    const val OFFERED_JOB = "offered"
    const val REJECTED_JOB = "rejected"
    const val SHORTLISTED_JOB = "shortlisted"
    const val ACTIVE_JOB = "active_jobs"
    const val POSTED_JOBS = "posted_jobs"
    const val COMPLETED_JOB = "completed_jobs"
    const val JOB_DETAILS = "job_details"
    const val JOB_ID = "job_id"

    const val OFFER = "OFFER"
    const val REJECT = "REJECT"
    const val UNDO_REJECT = "UNDO"
    const val COMPLETED = "COMPLETED"
    const val SHORTLIST = "SHORTLISTED"

    const val FAVOURITE_FREELANCER = "make_freelancer_favourite"
    const val INVITE_FREELANCER = "invite_freelancer"

    const val CONTACT_US =
        "${BuildConfig.ADMIN_URL}#/contactus"
    const val ABOUT_US = "${BuildConfig.ADMIN_URL}#/aboutus"
    const val TERMS = "${BuildConfig.ADMIN_URL}#/terms"

}