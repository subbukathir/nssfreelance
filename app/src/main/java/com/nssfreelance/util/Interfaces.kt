package com.nssfreelance.util

import java.util.*

interface DateTimePickerResponse{
    fun okayPressed(date: Date?)
}

interface OnSingleChoiceItemClick{
    fun onSubmit(selectedItem:String, position:Int)
}

interface OnItemClickListener{
    fun onItemClick(searchStr:String, id:String)
}