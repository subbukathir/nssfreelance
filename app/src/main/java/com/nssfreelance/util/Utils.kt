package com.nssfreelance.util

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import com.nssfreelance.R
import com.nssfreelance.data.Job
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


val <T> T.exhaustive: T
    get() = this


fun updatedDateStrToDate(dateStr: String?): Date {
    val date = Date()
    val IP_DATE_FORMAT_PATTERN = "dd-MM-yyyy"
    val inputFormat = SimpleDateFormat(IP_DATE_FORMAT_PATTERN, Locale.getDefault())
    if (dateStr != null && !dateStr.equals("", ignoreCase = true)) {
        var d1: Date? = null
        try {
            d1 = inputFormat.parse(dateStr)
            return d1
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }
    return date
}


fun updatedDOWDateStrToDate(dateStr: String?): Date {
    val date = Date()
    val IP_DATE_FORMAT_PATTERN = "dd MMM yyyy"
    val inputFormat = SimpleDateFormat(IP_DATE_FORMAT_PATTERN, Locale.getDefault())
    if (dateStr != null && !dateStr.equals("", ignoreCase = true)) {
        var d1: Date? = null
        try {
            d1 = inputFormat.parse(dateStr)
            return d1
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }
    return date
}

fun showDobDateFormate(date: Date): String {
    val IP_DATE_FORMAT_PATTERN = "EEE MMM d HH:mm:ss Z yyyy"
    val OP_DATE_FORMAT_PATTERN = "dd-MM-yyyy"
    val outputFormate = SimpleDateFormat(OP_DATE_FORMAT_PATTERN, Locale.getDefault())

    var result: String? = ""
    val readingFormat = SimpleDateFormat(IP_DATE_FORMAT_PATTERN, Locale.getDefault())
    val d1 = readingFormat.format(date)
    result = outputFormate.format(date)
    return result
}

fun serverDateFormate(date: String): String {
    if (date == "") return ""
    val IP_DATE_FORMAT_PATTERN = "dd-MM-yyyy"
    val OP_DATE_FORMAT_PATTERN = "yyyy-MM-dd"
    val outputFormate = SimpleDateFormat(OP_DATE_FORMAT_PATTERN, Locale.getDefault())

    var result = ""
    val readingFormat = SimpleDateFormat(IP_DATE_FORMAT_PATTERN, Locale.getDefault())
    try {
        val d1 = readingFormat.parse(date)
        result = outputFormate.format(d1)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return result
}

fun showDoWDateFormate(date: Date): String {
    val IP_DATE_FORMAT_PATTERN = "EEE MMM d HH:mm:ss Z yyyy"
    val OP_DATE_FORMAT_PATTERN = "yyyy-MM-dd"
    val outputFormate = SimpleDateFormat(OP_DATE_FORMAT_PATTERN, Locale.getDefault())

    var result: String? = ""
    val readingFormat = SimpleDateFormat(IP_DATE_FORMAT_PATTERN, Locale.getDefault())
    val d1 = readingFormat.format(date)
    result = outputFormate.format(date)
    return result
}


fun showDateOfWorkFormate(date: Date): String {
    val IP_DATE_FORMAT_PATTERN = "EEE MMM d HH:mm:ss Z yyyy"
    val OP_DATE_FORMAT_PATTERN = "dd MMM yyyy"
    val outputFormate = SimpleDateFormat(OP_DATE_FORMAT_PATTERN, Locale.getDefault())

    var result: String? = ""
    val readingFormat = SimpleDateFormat(IP_DATE_FORMAT_PATTERN, Locale.getDefault())
    val d1 = readingFormat.format(date)
    result = outputFormate.format(date)
    return result
}


fun commonDateFormate(date: String?): String {
    if (date == null || date == "" ||date.equals("0000-00-00 00:00:00")) return "None"
    val IP_DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    val OP_DATE_FORMAT_PATTERN = "dd MMM yyyy"
    val outputFormate = SimpleDateFormat(OP_DATE_FORMAT_PATTERN, Locale.getDefault())
    outputFormate.timeZone = TimeZone.getDefault()
    var result = ""
    val readingFormat = SimpleDateFormat(IP_DATE_FORMAT_PATTERN, Locale.getDefault())
    readingFormat.timeZone = TimeZone.getTimeZone("UTC")
    try {
        val d1 = readingFormat.parse(date)
        result = outputFormate.format(d1)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return result
}

fun getTimeAgo(activity: Context, date: String?): String {
    if (date.isNullOrEmpty()) return ""
    val IP_DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    val simpleDateFormat = SimpleDateFormat("dd/M/yyyy HH:mm:ss", Locale.getDefault())
    simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC");
    var result = ""
    val readingFormat = SimpleDateFormat(IP_DATE_FORMAT_PATTERN, Locale.getDefault())
    readingFormat.timeZone = TimeZone.getDefault();
    val timeAgo = TimeAgo(activity)
    try {
        val d1 = readingFormat.parse(date)
        val startdata = simpleDateFormat.format(d1)
        val d2 = simpleDateFormat.parse(startdata)
        result = timeAgo.getTimeAgo(d2).toString()
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return result
}


fun convertToCurrency(strValue: String?, context: Context): String? {
    if(strValue==null || strValue=="null") return "$0"
    val df = DecimalFormat("#,###")
    if (strValue == null || strValue == "" || strValue == " " || strValue.equals(
            "0",
            ignoreCase = true
        ) || strValue.equals("0.0", ignoreCase = true)
    ) return String.format(context.getString(R.string.lbl_value_), "0")
    var value = df.format(strValue.toDouble())
    val startsIn = value[0]
    if (startsIn == '.') {
        value = "0$value"
    }
    return String.format(context.getString(R.string.lbl_value_), value)
}

fun openChromeTabs(url: String, context: Context) {
    val colorInt: Int = Color.parseColor("#FFFFFF") //white

    val builder = CustomTabsIntent.Builder()
    builder.setToolbarColor(colorInt)
    builder.setShowTitle(true)
    val customTabsIntent = builder.build()
    customTabsIntent.launchUrl(context, Uri.parse(url))

}

fun shareJobs(shareBody: String, context: Context) {
    val intent = Intent(Intent.ACTION_SEND)
    intent.type = "text/plain"
    intent.putExtra(
        Intent.EXTRA_SUBJECT,
        "job"
    )
    intent.putExtra(Intent.EXTRA_TEXT, shareBody)
    context.startActivity(Intent.createChooser(intent, "Share using"))
}

fun getFreelancerShareBody(job:Job,context: Context):String{
    return "${job.job_title} ${job.country_name} | ${job.city_name} \n Client : ${job.client_user_name} " +
            "${convertToCurrency(job.job_price.toString(), context)} | ${job.job_type} \n" +
            "Posted on ${commonDateFormate(job.job_post_created_on)} | Ends on ${commonDateFormate(job.job_ends_on)}"
}



