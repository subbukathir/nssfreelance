package com.nssfreelance.util

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Rect
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import java.util.*


fun showDatePickerDialog(
    context: Context,
    listener: DateTimePickerResponse,
    selectedDate: Date,
    maxDate: Boolean
) {
    val cal = Calendar.getInstance()
    cal.time = selectedDate

    val dateSetListener =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            listener?.let {
                it.okayPressed(cal.time)
            }
        }

    val datePickerDialog = DatePickerDialog(
        context, dateSetListener,
        cal.get(Calendar.YEAR),
        cal.get(Calendar.MONTH),
        cal.get(Calendar.DAY_OF_MONTH)
    )

    if(maxDate)
    datePickerDialog.datePicker.maxDate = Calendar.getInstance().timeInMillis
    else datePickerDialog.datePicker.minDate = Calendar.getInstance().timeInMillis

    datePickerDialog.show()
}

fun showSingleChoiceDialog(
    context: Context,
    popupTitle: String,
    listItem: ArrayList<String>,
    listener: OnSingleChoiceItemClick
){
    val builder = MaterialAlertDialogBuilder(context)//, R.style.AlertDialogTheme

    // dialog title
    builder.setTitle(popupTitle)

    // set single choice items
    builder.setSingleChoiceItems(
        listItem.toTypedArray(), // array
        -1 // initial selection (-1 none)
    ) { dialog, i -> }

    // alert dialog positive button
    builder.setPositiveButton("Submit") { dialog, which ->
        val position = (dialog as AlertDialog).listView.checkedItemPosition
        // if selected, then get item text
        if (position != -1) {
            val selectedItem = listItem[position]
            listener.onSubmit(selectedItem, position)
        }
    }

    // alert dialog other buttons
    builder.setNegativeButton("Cancel", null)
    //builder.setNeutralButton("Cancel", null)

    // set dialog non cancelable
    builder.setCancelable(false)

    // finally, create the alert dialog and show it
    val dialog = builder.create()
    dialog.show()
}

fun hideKeyboard(activity: Activity) {
    try {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(activity.currentFocus?.windowToken, 0)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun progressBar(progress: ConstraintLayout, show: Boolean){
    progress.visibility = if (show) View.VISIBLE else View.GONE
}


fun displayError(view: View, message: String?) {
    if (message != null) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
    } else {
        Snackbar.make(view, "Unknown error", Snackbar.LENGTH_LONG).show()
    }
}

fun RecyclerView.show(){
    this.visibility = View.VISIBLE
}

fun RecyclerView.hide(){
    this.visibility = View.GONE
}

fun AppCompatButton.show() {
    this.visibility = View.VISIBLE
}

fun AppCompatButton.hide() {
    this.visibility = View.GONE
}

fun TextView.show(){
    this.visibility = View.VISIBLE
}

fun TextView.hide(){
    this.visibility = View.GONE
}

fun ImageView.show(){
    this.visibility = View.VISIBLE
}

fun ImageView.hide(){
    this.visibility = View.GONE
}

fun LinearLayout.show(){
    this.visibility = View.VISIBLE
}

fun LinearLayout.hide(){
    this.visibility = View.GONE
}

fun View.show(){
    this.visibility = View.VISIBLE
}

fun View.hide(){
    this.visibility = View.GONE
}


fun ProgressBar.show(){
    this.visibility = View.VISIBLE
}

fun ProgressBar.hide(){
    this.visibility = View.GONE
}

fun TextInputLayout.show(){
    this.visibility = View.VISIBLE
}

fun TextInputLayout.hide(){
    this.visibility = View.GONE
}

fun RatingBar.hide() { this.visibility=View.GONE}

fun RatingBar.show() { this.visibility=View.VISIBLE}

fun View.setOnSingleClickListener(l: View.OnClickListener) {
    setOnClickListener(OnSingleClickListener(l))
}

fun View.setOnSingleClickListener(l: (View) -> Unit) {
    setOnClickListener(OnSingleClickListener(l))
}

