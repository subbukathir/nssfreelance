package com.nssfreelance.util

import android.content.Context
import android.util.Log
import com.nssfreelance.R
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

private const val TAG = "TimeAgo"
class TimeAgo(context: Context?) {
    var simpleDateFormat: SimpleDateFormat
    var dateFormat: SimpleDateFormat
    var timeFormat: DateFormat
    var dateTimeNow: Date? = null
    var timeFromData: String? = null
    var pastDate: String? = null
    var sDateTimeNow: String
    var context: Context?
    fun getTimeAgo(startDate: Date): String? {
        //  date counting is done till todays date
        val endDate = dateTimeNow

        //  time difference in milli seconds
        val different = endDate!!.time - startDate.time
        return if (context == null) {
            if (different < MINUTE_MILLIS) {
                context!!.resources.getString(R.string.just_now)
            } else if (different < 2 * MINUTE_MILLIS) {
                context!!.resources.getString(R.string.a_min_ago)
            } else if (different < 50 * MINUTE_MILLIS) {
                different.div(MINUTE_MILLIS).toString() + context!!.getString(R.string.mins_ago)
            } else if (different < 90 * MINUTE_MILLIS) {
                context!!.getString(R.string.a_hour_ago)
            } else if (different < 24 * HOUR_MILLIS) {
                timeFromData = timeFormat.format(startDate)
                timeFromData
            } else if (different < 48 * HOUR_MILLIS) {
                context!!.getString(R.string.yesterday)
            } else if (different < 7 * DAY_MILLIS) {
                different .div( DAY_MILLIS).toString() + context!!.getString(R.string.days_ago)
            } else if (different < 2 * WEEKS_MILLIS) {
                different.div(WEEKS_MILLIS).toString() + context!!.getString(R.string.week_ago)
            } else if (different < 3.5 * WEEKS_MILLIS) {
                different.div(WEEKS_MILLIS).toString() + context!!.getString(R.string.weeks_ago)
            } else {
                pastDate = dateFormat.format(startDate)
                pastDate
            }
        } else {
            if (different < MINUTE_MILLIS) {
                context!!.resources.getString(R.string.just_now)
            } else if (different < 2 * MINUTE_MILLIS) {
                context!!.resources.getString(R.string.a_min_ago)
            } else if (different < 50 * MINUTE_MILLIS) {
                different.div(MINUTE_MILLIS).toString() + context!!.getString(R.string.mins_ago)
            } else if (different < 90 * MINUTE_MILLIS) {
                context!!.getString(R.string.a_hour_ago)
            } else if (different < 24 * HOUR_MILLIS) {
                timeFromData = timeFormat.format(startDate)
                timeFromData
            } else if (different < 48 * HOUR_MILLIS) {
                context!!.getString(R.string.yesterday)
            } else if (different < 7 * DAY_MILLIS) {
                different.div(DAY_MILLIS).toString() + context!!.getString(R.string.days_ago)
            } else if (different < 2 * WEEKS_MILLIS) {
                different.div(WEEKS_MILLIS).toString() + context!!.getString(R.string.week_ago)
            } else if (different < 3.5 * WEEKS_MILLIS) {
                different.div(WEEKS_MILLIS).toString() + context!!.getString(R.string.weeks_ago)
            } else {
                pastDate = dateFormat.format(startDate)
                pastDate
            }
        }
    }

    fun getTimeAgoDiff(differentStr: String?): String? {
        val startDate = dateTimeNow
        //  date counting is done till todays date
        //Date endDate = dateTimeNow;
        if (differentStr != null && !differentStr.equals("", ignoreCase = true)) {

            //  time difference in milli seconds
            val different = differentStr.toLong()
            return if (context == null) {
                if (different < MINUTE_MILLIS) {
                    context!!.resources.getString(R.string.just_now)
                } else if (different < 2 * MINUTE_MILLIS) {
                    context!!.resources.getString(R.string.a_min_ago)
                } else if (different < 50 * MINUTE_MILLIS) {
                    different.div(MINUTE_MILLIS).toString() + context!!.getString(R.string.mins)
                } else if (different < 90 * MINUTE_MILLIS) {
                    context!!.getString(R.string.a_hour_ago)
                } else if (different < 24 * HOUR_MILLIS) {
                    timeFromData = timeFormat.format(startDate)
                    timeFromData
                } else if (different < 48 * HOUR_MILLIS) {
                    context!!.getString(R.string.yesterday)
                } else if (different < 7 * DAY_MILLIS) {
                    different.div(DAY_MILLIS).toString() + context!!.getString(R.string.days)
                } else if (different < 2 * WEEKS_MILLIS) {
                    different.div(WEEKS_MILLIS).toString() + context!!.getString(R.string.week)
                } else if (different < 3.5 * WEEKS_MILLIS) {
                    different.div(WEEKS_MILLIS).toString() + context!!.getString(R.string.weeks)
                } else {
                    pastDate = dateFormat.format(startDate)
                    pastDate
                }
            } else {
                if (different < MINUTE_MILLIS) {
                    context!!.resources.getString(R.string.just_now)
                } else if (different < 2 * MINUTE_MILLIS) {
                    context!!.resources.getString(R.string.a_min_ago)
                } else if (different < 50 * MINUTE_MILLIS) {
                    different.div(MINUTE_MILLIS).toString() + context!!.getString(R.string.mins)
                } else if (different < 90 * MINUTE_MILLIS) {
                    context!!.getString(R.string.a_hour_ago)
                } else if (different < 24 * HOUR_MILLIS) {
                    timeFromData = timeFormat.format(startDate)
                    timeFromData
                } else if (different < 48 * HOUR_MILLIS) {
                    context!!.getString(R.string.yesterday)
                } else if (different < 7 * DAY_MILLIS) {
                    different.div(DAY_MILLIS).toString() + context!!.getString(R.string.days)
                } else if (different < 2 * WEEKS_MILLIS) {
                    different.div(WEEKS_MILLIS).toString() + context!!.getString(R.string.week)
                } else if (different < 3.5 * WEEKS_MILLIS) {
                    different.div(WEEKS_MILLIS).toString() + context!!.getString(R.string.weeks)
                } else {
                    pastDate = dateFormat.format(startDate)
                    pastDate
                }
            }
        }
        return ""
    }

    companion object {
        private const val SECOND_MILLIS = 1000
        private const val MINUTE_MILLIS = 60 * SECOND_MILLIS
        private const val HOUR_MILLIS = 60 * MINUTE_MILLIS
        private const val DAY_MILLIS = 24 * HOUR_MILLIS
        private const val WEEKS_MILLIS = 7 * DAY_MILLIS
        private const val MONTHS_MILLIS = 4 * WEEKS_MILLIS
    }

    //private static final int YEARS_MILLIS = 12 * MONTHS_MILLIS;
    init {
        simpleDateFormat = SimpleDateFormat("dd/M/yyyy HH:mm:ss")
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        dateFormat = SimpleDateFormat("dd/MM/yyyy")
        timeFormat = SimpleDateFormat("h:mm aa")
        this.context = context
        val now = Date()
        sDateTimeNow = simpleDateFormat.format(now)
        try {
            dateTimeNow = simpleDateFormat.parse(sDateTimeNow)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }
}