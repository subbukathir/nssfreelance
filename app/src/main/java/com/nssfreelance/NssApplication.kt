package com.nssfreelance

import android.app.Application
import com.stripe.android.PaymentConfiguration
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NssApplication:Application(){
    override fun onCreate() {
        super.onCreate()
        PaymentConfiguration.init(
            applicationContext,
            "pk_live_51HKKKBHZiQCQfedQBmfWro3I1VOpqy4vIA67zOY9Xj8bJTeWOmbNyd3EnZJFfEw3mHJK4vaGkMCoSrEZiNYOoljy00iIhiZnhZ"
        )
    }
}