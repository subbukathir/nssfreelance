package com.nssfreelance.repository

import com.nssfreelance.data.City
import com.nssfreelance.data.Country
import com.nssfreelance.data.RegisterDetails
import com.nssfreelance.data.State
import com.nssfreelance.retrofit.FreelanceService
import com.nssfreelance.retrofit.request.*
import com.nssfreelance.retrofit.response.*
import com.nssfreelance.util.Const
import com.nssfreelance.util.Const.JOB_ID
import com.nssfreelance.util.Const.USER_ID
import com.nssfreelance.util.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(
    private val freelanceService: FreelanceService
) {

    suspend fun getSearchCategory(): Flow<DataState<CategoryListResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.searchCategoryList()
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun updateFcmToken(params: FcmTokenParams): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.updateFcmToken(params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getRegisterDetails(): Flow<DataState<RegisterDetails>> = flow {
        emit(DataState.Loading)

        try {
            val registerDetails = freelanceService.registerDetails()
            emit(DataState.Success(registerDetails.data))
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }

    suspend fun getCountry(): Flow<DataState<List<Country>>> = flow {
        emit(DataState.Loading)
        try {
            val countries = freelanceService.getCountry()
            emit(DataState.Success(countries.data))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getState(country: String): Flow<DataState<List<State>>> = flow {
        emit(DataState.Loading)
        try {
            val states = freelanceService.getState(country)
            emit(DataState.Success(states.data))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getCity(state: String): Flow<DataState<List<City>>> = flow {
        emit(DataState.Loading)
        try {
            val states = freelanceService.getCity(state)
            emit(DataState.Success(states.data))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun login(userType: String, params: LoginParams): Flow<DataState<LoginResponse>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = freelanceService.login(userType, params)
                emit(DataState.Success(response))
            } catch (ex: Exception) {
                emit(DataState.Error(ex))
            }
        }

    suspend fun registerClient(
        userType: String,
        params: RegisterParams
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.register(userType, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun registerWithToken(
        userTypeId: String,
        fbToken: String
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put("token", fbToken)
            val response = freelanceService.registerWithFb(userTypeId, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }


    suspend fun postSkills(params: SkillsParams): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.postSkills(params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getProfile(token: String, userId: String): Flow<DataState<ProfileResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put(USER_ID, userId)
            val response = freelanceService.profile(token, params)
            emit(DataState.Success(response))
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
            try {
                emit(DataState.Error(ex))
            } catch (e: Exception) {
            }
        }
    }


    suspend fun getClientProfile(
        token: String,
        userId: String
    ): Flow<DataState<ProfileClientResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put(USER_ID, userId)
            val response = freelanceService.clientProfile(token, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getAllJobs(token: String, userId: String): Flow<DataState<AllJobsResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put(USER_ID, userId)
            val response = freelanceService.allJobs(token, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getRecommendedJobs(
        token: String,
        userId: String
    ): Flow<DataState<AllJobsResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put(USER_ID, userId)
            val response = freelanceService.recommendedJobs(token, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }


    suspend fun getTrendingJobs(token: String, userId: String): Flow<DataState<AllJobsResponse>> =
        flow {
            emit(DataState.Loading)
            try {
                val params = HashMap<String, String>()
                params.put(USER_ID, userId)
                val response = freelanceService.trendingJobs(token, params)
                emit(DataState.Success(response))
            } catch (ex: Exception) {
                emit(DataState.Error(ex))
            }
        }


    suspend fun getNearByJobs(token: String, userId: String): Flow<DataState<AllJobsResponse>> =
        flow {
            emit(DataState.Loading)
            try {
                val params = HashMap<String, String>()
                params.put(USER_ID, userId)
                val response = freelanceService.nearbyJobs(token, params)
                emit(DataState.Success(response))
            } catch (ex: Exception) {
                emit(DataState.Error(ex))
            }
        }

    suspend fun getLatestJobs(token: String, userId: String): Flow<DataState<AllJobsResponse>> =
        flow {
            emit(DataState.Loading)
            try {
                val params = HashMap<String, String>()
                params.put(USER_ID, userId)
                val response = freelanceService.latestJobs(token, params)
                emit(DataState.Success(response))
            } catch (ex: Exception) {
                emit(DataState.Error(ex))
            }
        }

    suspend fun getJobsByUrl(
        token: String,
        userId: String,
        url: String
    ): Flow<DataState<AllJobsResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put(USER_ID, userId)
            val response = freelanceService.getJobsByUrl(token, url, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getJobDetails(
        userToken: String,
        jobId: String
    ): Flow<DataState<JobDetailsResponse>> = flow {
        emit(DataState.Loading)
        try {

            val response = freelanceService.getJobDetails(userToken, jobId)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            try {
                emit(DataState.Error(ex))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    suspend fun applyFavouriteJob(
        userToken: String,
        params: ApplyJobFavouriteParams
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.applyFavouriteJob(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }


    suspend fun completeJob(
        userToken: String,
        params: CompleteFreelancerJobParams
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.completeJob(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getSearchResult(
        token: String,
        searchString: String
    ): Flow<DataState<SearchJobsResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.getSearchResult(token, searchString)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    //Client jobs

    suspend fun getClientPostedJobs(
        userToken: String,
        userId: String
    ): Flow<DataState<AllJobsResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params["user_id"] = userId
            val response = freelanceService.getClientPostedJobs(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getClientHomeJobs(
        token: String,
        userId: String,
    ): Flow<DataState<AllClientJobResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put(USER_ID, userId)
            val response = freelanceService.getClientHomeJobs(token, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getClientJobsByUrl(
        token: String,
        userId: String,
        url: String
    ): Flow<DataState<AllClientJobResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put(JOB_ID, userId)
            val response = freelanceService.getClientJobsByUrl(token, url, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun createJobPost(
        token: String,
        params: CreateJobPostParams
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.createJobPost(token, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }


    suspend fun getSearchClientResult(
        token: String,
        searchString: String
    ): Flow<DataState<SearchJobsClientResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.getSearchClientResult(token, searchString)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }
    suspend fun getHomeClientJobs(
        token: String,
        userId: String,
        url: String
    ): Flow<DataState<HomeClientJobResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put(USER_ID, userId)
            val response = freelanceService.getHomeClientJobs(token, url, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getClientActiveJobs(
        token: String,
        userId: String,
        url: String
    ): Flow<DataState<AllClientJobResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put(USER_ID, userId)
            val response = freelanceService.getClientActiveJobs(token, url, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getClientCompletedJobs(
        token: String,
        userId: String,
        url: String
    ): Flow<DataState<AllClientJobResponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put(USER_ID, userId)
            val response = freelanceService.getClientActiveJobs(token, url, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }


    suspend fun getClientJobsDetails(
        token: String,
        jobId: String,
        url: String
    ): Flow<DataState<ClientJobDetailReponse>> = flow {
        emit(DataState.Loading)
        try {
            val params = HashMap<String, String>()
            params.put(Const.JOB_ID, jobId)
            val response = freelanceService.getClientJobDetail(token, url, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun updateClientJobStatus(
        userToken: String,
        params: UpdateJobStatusParams
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.updateJobStatus(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun updateFavouriteInviteFreelancer(
        userToken: String,
        url: String,
        params: InviteFavouriteClientParams
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.updateFavouriteInvite(userToken, url, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun getPaymentIntentSecret(
        userToken: String,
        params: PaymentSecretParams
    ): Flow<DataState<PaymentSecretResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.getPaymentSecret(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }


    suspend fun storePaymentSuccess(
        userToken: String,
        params: StorePaymentParams
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.storePaymentSuccess(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun updateProfile(
        userToken: String,
        params: UpdateProfileParams
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.updateProfile(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun forgotPassword(
        params : HashMap<String, String>
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.forgotPassord(params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun verifyOTP(
        params : HashMap<String, String>
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.verifyOtp(params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun resendOTP(
        params : HashMap<String, String>
    ): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.resendOtp(params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun requestContractDetail(
        userToken: String,
        params: ContractRequestParams
    ): Flow<DataState<ContractDetailResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.requestContract(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }

    suspend fun addReview(userToken: String, params: AddReviewParams): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.addReview(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            ex.printStackTrace()
            try {
                emit(DataState.Error(ex))
            } catch (e: Exception) {

            }
        }
    }

    suspend fun addReviewFreelancer(userToken: String, params: AddReviewParams): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.addReviewFreelancer(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            ex.printStackTrace()
            try {
                emit(DataState.Error(ex))
            } catch (e: Exception) {

            }
        }
    }

    suspend fun addRatingFreelancer(userToken: String, params: AddReviewParams): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.addRatingFreelancer(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            ex.printStackTrace()
            try {
                emit(DataState.Error(ex))
            } catch (e: Exception) {

            }
        }
    }


    suspend fun addRatingClient(userToken: String, params: AddReviewParams): Flow<DataState<LoginResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.addRatingClient(userToken, params)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            ex.printStackTrace()
            try {
                emit(DataState.Error(ex))
            } catch (e: Exception) {

            }
        }
    }

    suspend fun getNotification(userToken: String, userId: String): Flow<DataState<NotificationResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = freelanceService.getNotifications(userToken, userId)
            emit(DataState.Success(response))
        } catch (ex: Exception) {
            emit(DataState.Error(ex))
        }
    }
}