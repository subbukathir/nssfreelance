package com.nssfreelance.repository

import androidx.lifecycle.MutableLiveData

object FCMTokenRepository {
    val fcmToken:MutableLiveData<String> = MutableLiveData<String>()
}