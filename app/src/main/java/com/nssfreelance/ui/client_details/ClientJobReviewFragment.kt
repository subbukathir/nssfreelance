package com.nssfreelance.ui.client_details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentClientReviewFreelancerBinding
import com.nssfreelance.ui.job_details.JobDetailsViewmodel
import com.nssfreelance.util.hide
import com.nssfreelance.util.show
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ClientJobReviewFragment : Fragment(R.layout.fragment_client_review_freelancer) {

    private var _binding: FragmentClientReviewFreelancerBinding? = null
    private val binding: FragmentClientReviewFreelancerBinding
        get() = _binding!!

    private val viewmodel: JobDetailsViewmodel by activityViewModels()

    private lateinit var mAdapter: JobReviewsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentClientReviewFreelancerBinding.bind(view)
        mAdapter = JobReviewsAdapter()
        binding.apply {
            recylerviewClientReview.itemAnimator = DefaultItemAnimator()
            recylerviewClientReview.adapter = mAdapter
            val listData = viewmodel.getReviewDetails()
            if (listData != null && listData.size > 0) {
                mAdapter.submitList(listData)
                showDataview(true)
            } else {
                showDataview(false)
            }
        }
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.clientReviewList.observe(viewLifecycleOwner, { data ->
                if (data.size > 0) {
                    mAdapter.submitList(data)
                    showDataview(true)
                } else showDataview(false)
            })
        }
    }

    private fun showDataview(show: Boolean) {
        if (show) {
            binding.apply {
                error.tvError.hide()

                recylerviewClientReview.show()
                viewDividerBottom.show()
                btnJobFavourite.show()
                btnViewClientJobs.show()
            }
        } else {
            binding.apply {
                error.tvError.show()

                recylerviewClientReview.hide()
                viewDividerBottom.hide()
                btnJobFavourite.hide()
                btnViewClientJobs.hide()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}