package com.nssfreelance.ui.client_details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayoutMediator
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentClientDetailsFreelancerBinding
import com.nssfreelance.ui.job_details.JobDetailsViewmodel
import com.nssfreelance.ui.register.ViewpagerAdapter
import com.nssfreelance.util.convertToCurrency
import com.nssfreelance.util.hide
import com.nssfreelance.util.show
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ClientDetailsFragment:Fragment(R.layout.fragment_client_details_freelancer) {
    private var _binding:FragmentClientDetailsFreelancerBinding?=null
    private val binding:FragmentClientDetailsFreelancerBinding
    get() = _binding!!

    private val viewmodel:JobDetailsViewmodel by activityViewModels()

    private lateinit var viewpagerAdapter: ViewpagerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentClientDetailsFreelancerBinding.bind(view)

        viewpagerAdapter =
            ViewpagerAdapter(fragmentManager = requireActivity().supportFragmentManager, lifecycle)
        viewpagerAdapter.addFragment(ClientJobListingsFragment())
        viewpagerAdapter.addFragment(ClientJobReviewFragment())
        viewpagerAdapter.addFragment(AboutMeClientFragment())

        binding.apply {
            viewpagerClientDetails.adapter = viewpagerAdapter
            TabLayoutMediator(tabClientDetails,
            viewpagerClientDetails){tab, position ->
                when(position){
                    0 ->tab.text="Listings"
                    1 ->tab.text="Reviews"
                    2 ->tab.text="About"
                }
            }.attach()

            val data = viewmodel.getClientDetails()
            data?.let {
                Glide.with(requireContext())
                    .load(data.profile_photo_link)
                    .placeholder(R.drawable.ic_placeholder_profile)
                    .into(civClientProfile)

                tvProfileName.text = data.full_name?:""
                tvProfileJobRole.text = data.designation?:""
                tvProfileRating.text = data.rating?:""
                ratingBar.rating = (data.rating?:"0").toFloat()

                tvJobPosted.text = (data.submission_count?:0).toString()
                tvJobsHired.text = (data.completed_job_count?:0).toString()
                tvSpend.text = convertToCurrency((data.money_spent?:0).toString(),requireContext())
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.clientDetails.observe(viewLifecycleOwner,{data->
                binding.apply {
                    if(!data.profile_photo_link.isNullOrEmpty()){
                        Glide.with(requireContext())
                            .load(data.profile_photo_link?:"")
                            .placeholder(R.drawable.ic_placeholder_profile)
                            .into(civClientProfile)
                    }else{
                        Glide.with(requireContext())
                            .load(R.drawable.ic_placeholder_profile)
                            .into(civClientProfile)
                    }


                    tvProfileName.text = data.full_name?:""
                    tvProfileJobRole.text = "${data.city_name} | ${data.country_name}"
                    tvProfileRating.text = "${data.rating_count}"
                    ratingBar.rating = (data.rating?:"0").toFloat()

                    if(data.rating_count == 0) llRating.hide()
                    else llRating.show()

                    tvJobPosted.text = (data.posted_job_count?:0).toString()
                    tvJobsHired.text = (data.completed_job_count?:0).toString()
                    tvSpend.text = convertToCurrency((data.money_spent?:0).toString(),requireContext())
                }
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}