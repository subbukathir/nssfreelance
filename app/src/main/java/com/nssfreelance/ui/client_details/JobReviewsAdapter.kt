package com.nssfreelance.ui.client_details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.Review
import com.nssfreelance.databinding.ItemClientReviewBinding

class JobReviewsAdapter :
    ListAdapter<Review, JobReviewsAdapter.JobReviewViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobReviewViewHolder {
        val binding =
            ItemClientReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return JobReviewViewHolder(binding)
    }

    override fun onBindViewHolder(holder: JobReviewViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class JobReviewViewHolder(private val binding: ItemClientReviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(review: Review) {
            binding.apply {
                tvItemName.text = review.review
                ratingBarReview.rating = 0F
                tvRatingCount.text = "0"
                tvItemReview.text = ""

                Glide.with(binding.root.context)
                    .load(R.drawable.ic_placeholder_profile)
                    .into(ivItemProfile)
            }
        }
    }


    class DiffUtilCallback : DiffUtil.ItemCallback<Review>() {
        override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean {
            return oldItem.client_job_review_id == newItem.client_job_review_id
        }

        override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean {
            return oldItem == newItem
        }
    }
}