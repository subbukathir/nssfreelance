package com.nssfreelance.ui.client_details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.ItemClientJobCardBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency

class JobListingAdapter:ListAdapter<Job, JobListingAdapter.JobListingViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobListingViewHolder {
        val binding = ItemClientJobCardBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return JobListingViewHolder(binding)
    }

    override fun onBindViewHolder(holder: JobListingViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class JobListingViewHolder(private val binding:ItemClientJobCardBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(job:Job){
            binding.apply {
                tvItemJobRole.text = job.job_title
                tvItemName.text = job.client_user_name
                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemRate.text = "${convertToCurrency(job.job_price.toString(),tvItemJobPostedOn.context)} | ${job.job_type}"
                tvItemJobPostedOn.text = commonDateFormate(job?.job_starts_from)
                if(job.favourite==1){
                    Glide.with(binding.root.context)
                        .load(R.drawable.ic_favorite_filled)
                        .into(ivItemFavorite)
                }else{
                    Glide.with(binding.root.context)
                        .load(R.drawable.ic_favorite_border)
                        .into(ivItemFavorite)
                }
            }
        }
    }


    class DiffUtilCallback: DiffUtil.ItemCallback<Job>(){
        override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem == newItem
        }
    }
}