package com.nssfreelance.ui.client_details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentClientJobListingsFreelancerBinding
import com.nssfreelance.ui.job_details.JobDetailsViewmodel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ClientJobListingsFragment:Fragment(R.layout.fragment_client_job_listings_freelancer) {

    private var _binding: FragmentClientJobListingsFreelancerBinding?=null
    private val binding: FragmentClientJobListingsFreelancerBinding
        get() = _binding!!

    private val viewmodel: JobDetailsViewmodel by activityViewModels()

    private lateinit var mAdapter: JobListingAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentClientJobListingsFreelancerBinding.bind(view)
        mAdapter = JobListingAdapter()
        binding.apply {
            recyclerviewJobListings.layoutManager = GridLayoutManager(requireContext(),2,GridLayoutManager.VERTICAL,false)
            recyclerviewJobListings.adapter = mAdapter

            mAdapter.submitList(viewmodel.getJobDetails())

        }
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.clientJobList.observe(viewLifecycleOwner,{data->
                mAdapter.submitList(data)
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}