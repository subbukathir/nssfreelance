package com.nssfreelance.ui.client_details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentAboutClientBinding
import com.nssfreelance.ui.job_details.JobDetailsViewmodel
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.hide
import com.nssfreelance.util.show
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AboutMeClientFragment:Fragment(R.layout.fragment_about_client) {
    private var _binding:FragmentAboutClientBinding?=null
    private val binding:FragmentAboutClientBinding
    get() = _binding!!

    private val viewmodel:JobDetailsViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentAboutClientBinding.bind(view)
        binding.apply {
            val data = viewmodel.getClientDetails()
            val jobDet = viewmodel.jobDet
            if(jobDet?.is_payment_settled!!){
                showUnlockView(true)
            }else{
                showUnlockView(false)
            }
            if(data!=null){
                tvGender.text = "Gender : ${data.gender?:""}"
                tvAge.text = "DOB : ${commonDateFormate(data.date_of_birth!!)}"
                tvAddress.text = "Address : ${data.address?:""}"
                tvContactNumber.text = "Mobile : ${data.mobile_no?:""}"

                if(data.about_me!=null){
                    tvAboutClient.text="${data.about_me}"
                }else{
                    tvAboutClient.text =""
                }
            }
        }
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.clientDetails.observe(viewLifecycleOwner,{data->
                binding.apply {
                    if(data!=null){
                        val jobDet = viewmodel.jobDet
                        if(jobDet?.is_payment_settled!!){
                            showUnlockView(true)
                        }else{
                            showUnlockView(false)
                        }
                        tvGender.text = "Gender : ${data.gender?:""}"
                        tvAge.text = "DOB : ${commonDateFormate(data.date_of_birth!!)}"
                        tvAddress.text = "Address : ${data.city_name?:""} | ${data.country_name}"
                        tvContactNumber.text = "Mobile : ${data.mobile_no?:""}"
                    }
                }
            })
        }
    }

    fun showUnlockView(show:Boolean){
        if(show){
            binding.apply {
                llUnlockView.show()
                llLockView.hide()
                tvAboutClient.show()
            }
        }else{
            binding.apply {
                llLockView.show()
                llUnlockView.hide()
                tvAboutClient.hide()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}