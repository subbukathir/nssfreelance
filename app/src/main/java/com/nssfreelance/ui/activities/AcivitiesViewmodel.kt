package com.nssfreelance.ui.activities

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.request.ApplyJobFavouriteParams
import com.nssfreelance.retrofit.request.CompleteFreelancerJobParams
import com.nssfreelance.retrofit.response.AllJobsResponse
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.Const.APPLIED_JOB
import com.nssfreelance.util.Const.FAVOURITE_FREELANCER_JOBS
import com.nssfreelance.util.Const.FAVOURITE_JOB
import com.nssfreelance.util.Const.LATEST_JOB
import com.nssfreelance.util.Const.PAST_JOB
import com.nssfreelance.util.Const.RECENT_JOB
import com.nssfreelance.util.DataState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class ActivitiesViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val stateHandle: SavedStateHandle,
    private val preferences:PreferencesManager
):ViewModel() {
    val preferencesFlow = preferences.preferencesFlow

    var userId = ""
    var userToken=""
    var triggerCount = 0

    //job lists
    var currentJobList:List<Job> = mutableListOf()
    var pastJobList:List<Job> = mutableListOf()
    var favouriteJobList:List<Job> = mutableListOf()
    var appliedJobList:List<Job> = mutableListOf()

    private val _currentDatastate:MutableLiveData<DataState<AllJobsResponse>> = MutableLiveData()
    val currentDatastate:LiveData<DataState<AllJobsResponse>>
    get() = _currentDatastate


    private val _pastDatastate:MutableLiveData<DataState<AllJobsResponse>> = MutableLiveData()
    val pastDatastate:LiveData<DataState<AllJobsResponse>>
        get() = _pastDatastate


    private val _favouriteDatastate:MutableLiveData<DataState<AllJobsResponse>> = MutableLiveData()
    val favouriteDatastate:LiveData<DataState<AllJobsResponse>>
        get() = _favouriteDatastate


    private val _appliedDatastate:MutableLiveData<DataState<AllJobsResponse>> = MutableLiveData()
    val appliedDatastate:LiveData<DataState<AllJobsResponse>>
        get() = _appliedDatastate

    private val _favouriteJobsState:MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val favouriteJobState:LiveData<DataState<LoginResponse>>
        get() = _favouriteJobsState

    private val _completeJobsState:MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val completeJobState:LiveData<DataState<LoginResponse>>
        get() = _completeJobsState

    fun getCurrentJobs(){
        viewModelScope.launch {
            repository.getJobsByUrl(userToken,userId,LATEST_JOB)
                .onEach { dataState -> _currentDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }


    fun getPastJobs(){
        viewModelScope.launch {
            repository.getJobsByUrl(userToken,userId, PAST_JOB)
                .onEach { dataState -> _pastDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }


    fun getFavouriteJobs(){
        viewModelScope.launch {
            repository.getJobsByUrl(userToken,userId,FAVOURITE_FREELANCER_JOBS)
                .onEach { dataState -> _favouriteDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }


    fun getAppliedJobs(){
        viewModelScope.launch {
            repository.getJobsByUrl(userToken,userId,APPLIED_JOB)
                .onEach { dataState -> _appliedDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    private val activityEventChannel = Channel<ActivityTask> ()
    val activityEventTask = activityEventChannel.receiveAsFlow()

    fun onClickOfItem(job: Job){
        viewModelScope.launch {
            activityEventChannel.send(ActivityTask.NavigateToJobDetails(job))
        }
    }

    fun onFavouriteClicked(job: Job) {
        viewModelScope.launch {
            val params = ApplyJobFavouriteParams(
                request_progress_id = (job.request_progress_id?:0).toString(),
                client_job_post_id = (job.client_job_post_id).toString(),
                freelancer_user_id = userId,
                is_applied = "${job.is_applied}",
                is_favourite = if(job.is_favourite==1)"0" else "1",
                client_user_id = (job.client_user_id?:0).toString(),
                job_bid_price = "0"
            )
            repository.applyFavouriteJob(userToken,params)
                .onEach { dataState -> _favouriteJobsState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun onCompleteFreelancer(job: Job){
        viewModelScope.launch {
            val params =CompleteFreelancerJobParams(
                userId,
                job.client_job_post_id.toString(),
                "1"
            )

            repository.completeJob(userToken,params)
                .onEach{dataState -> _completeJobsState.value = dataState}
                .launchIn(viewModelScope)
        }
    }

    sealed class ActivityTask{
        data class NavigateToJobDetails(val job:Job):ActivityTask()
        object RefreshView:ActivityTask()
    }

}