package com.nssfreelance.ui.activities.past

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.ItemPastActivityCardBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency

class PastJobsAdapter(private val listener:OnItemClickListener):ListAdapter<Job, PastJobsAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding = ItemPastActivityCardBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem =getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding:ItemPastActivityCardBinding):RecyclerView.ViewHolder(binding.root){
        init {

            binding.apply {
                llClientReviewed.setOnClickListener {
                    listener.onClientReviewClick(getItem(adapterPosition))
                }

                root.setOnClickListener{
                    Log.d("Adapter", ": clicked")
                    listener.onItemClick(getItem(adapterPosition))
                }
            }

        }
        fun bind(job:Job){
            binding.apply {

                tvItemJobRole.text = job.job_title
                tvItemName.text = job.client_user_name
                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemRate.text = "Job Price ${convertToCurrency(job.contract_final_amount.toString(),tvItemJobRole.context)} | ${job.job_type}"
                tvItemDateOfWork.text = "Date of work ${commonDateFormate(job.job_starts_from)}"
                tvItemCompletedOnDate.text = commonDateFormate(job.job_completed_on)

                if(job.is_client_reviewed==1){
                    tvLblReviewStatus.text = "Reviewed"
                    llClientReviewed.background =
                        ContextCompat.getDrawable(imageviewJobType.context,
                            R.drawable.bg_shape_yellow_dark_filled)
                    tvLblReviewStatus.setTextColor(ContextCompat.getColor(binding.tvItemDateOfWork.context,R.color.white))
                }else{
                    tvLblReviewStatus.text = "Review Client"
                    llClientReviewed.background =
                        ContextCompat.getDrawable(imageviewJobType.context,
                            R.drawable.bg_shape_black_filled)
                    tvLblReviewStatus.setTextColor(ContextCompat.getColor(binding.tvItemDateOfWork.context,R.color.white))
                }

                if(job.url!=null){
                    Glide.with(binding.root.context)
                        .load(job.url)
                        .placeholder(R.drawable.ic_plug_wire)
                        .into(imageviewJobType)
                }else{
                    Glide.with(binding.root.context)
                        .load(R.drawable.ic_plug_wire)
                        .into(imageviewJobType)
                }
            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(job: Job)
        fun onClientReviewClick(job:Job)
    }

    class DiffUtilCallback: DiffUtil.ItemCallback<Job>(){
        override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem == newItem
        }
    }
}