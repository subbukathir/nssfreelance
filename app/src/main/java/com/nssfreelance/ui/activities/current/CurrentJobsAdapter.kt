package com.nssfreelance.ui.activities.current

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.ItemCurrentActivityCardBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency
import com.nssfreelance.util.hide
import com.nssfreelance.util.show

class CurrentJobsAdapter(private val listener: OnItemClickListener) :
    ListAdapter<Job, CurrentJobsAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding = ItemCurrentActivityCardBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding: ItemCurrentActivityCardBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {

            binding.apply {
                llMarkComplete.setOnClickListener {
                    listener.onMarkCompleteClick(getItem(adapterPosition))
                }

                llMainView.setOnClickListener {
                    listener.onItemClick(getItem(adapterPosition))
                }
            }

        }

        fun bind(job: Job) {
            binding.apply {

                tvItemJobRole.text = job.job_title
                tvItemName.text = job.client_user_name
                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemRate.text = "Job Price ${
                    convertToCurrency(
                        job.bid_price.toString(),
                        tvItemJobRole.context
                    )
                } | ${job.job_type}"

                tvItemAcceptedDate.text = commonDateFormate(job.accepted_date ?: "")
                tvItemShortlistedDate.text = commonDateFormate(job.shortlisted_date ?: "")
                tvItemJobOfferedDate.text = commonDateFormate(job.accepted_date ?: "")

                tvItemAppliedDate.text = commonDateFormate(job.applied_date ?: "")
                tvItemJobStartDate.text = commonDateFormate(job.job_starts_from ?: "")
                tvItemJobEndDate.text = commonDateFormate(job.job_ends_on ?: "")

                tvItemCompletedOnDate.text = commonDateFormate(job.job_ends_on ?: "")

                if (job.is_freelancer_completed != null && job.is_freelancer_completed == 1) {
                    llMarkComplete.hide()
                    tvLblComplete.text = "Completed"

                } else {
                    tvLblComplete.text = "Mark Complete"
                    llMarkComplete.show()
                }




                if (job.url != null) {
                    Glide.with(binding.root.context)
                        .load(job.url)
                        .placeholder(R.drawable.ic_plug_wire)
                        .into(imageviewJobType)
                } else {
                    Glide.with(binding.root.context)
                        .load(R.drawable.ic_plug_wire)
                        .into(imageviewJobType)
                }
            }
        }
    }


    interface OnItemClickListener {
        fun onItemClick(job: Job)
        fun onMarkCompleteClick(job: Job)
    }

    class DiffUtilCallback : DiffUtil.ItemCallback<Job>() {
        override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem == newItem
        }
    }
}