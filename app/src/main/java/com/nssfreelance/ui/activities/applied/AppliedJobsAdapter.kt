package com.nssfreelance.ui.activities.applied

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.ItemAppliedJobActivityCardBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency

class AppliedJobsAdapter(private val listener:OnItemClickListener):ListAdapter<Job, AppliedJobsAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding = ItemAppliedJobActivityCardBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem =getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding:ItemAppliedJobActivityCardBinding):RecyclerView.ViewHolder(binding.root){
        init {

            binding.apply {
                ivItemShare.setOnClickListener {
                    listener.onShareClick(getItem(adapterPosition))
                }

                root.setOnClickListener{
                    listener.onItemClick(getItem(adapterPosition))
                }
            }

        }
        fun bind(job:Job){
            binding.apply {

                tvItemJobRole.text = job.job_title
                tvItemName.text = job.client_user_name
                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemRate.text = "Job Price ${convertToCurrency(job.job_price.toString(),tvItemJobRole.context)} | ${job.job_type}"
                tvItemDateOfWork.text = "Date of work ${commonDateFormate(job.job_starts_from)}"
                tvItemApplicationStatus.text = job.application_status ?:""

                if(job.url!=null){
                    Glide.with(binding.root.context)
                        .load(job.url)
                        .placeholder(R.drawable.ic_plug_wire)
                        .into(imageviewJobType)
                }else{
                    Glide.with(binding.root.context)
                        .load(R.drawable.ic_plug_wire)
                        .into(imageviewJobType)
                }
            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(job: Job)
        fun onFavoriteClick(job:Job)
        fun onShareClick(job: Job)
    }

    class DiffUtilCallback: DiffUtil.ItemCallback<Job>(){
        override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem == newItem
        }
    }
}