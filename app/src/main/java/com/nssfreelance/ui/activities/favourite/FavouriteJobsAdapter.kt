package com.nssfreelance.ui.activities.favourite

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.ItemJobCardBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency

class FavouriteJobsAdapter(private val listener:OnItemClickListener):ListAdapter<Job, FavouriteJobsAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding = ItemJobCardBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem =getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding:ItemJobCardBinding):RecyclerView.ViewHolder(binding.root){
        init {

            binding.apply {
                ivItemFavorite.setOnClickListener {
                    listener.onFavoriteClick(getItem(adapterPosition))
                }
                ivItemShare.setOnClickListener {
                    listener.onShareClick(getItem(adapterPosition))
                }

                root.setOnClickListener{
                    listener.onItemClick(getItem(adapterPosition))
                }
            }

        }
        fun bind(job:Job){
            binding.apply {

                tvItemJobRole.text = job.job_title
                tvItemName.text = job.client_user_name
                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemRate.text = "Job Price ${convertToCurrency(job.bid_price.toString(),tvItemJobRole.context)} | ${job.job_type}"
                tvItemJobPostedOn.text = commonDateFormate(job?.job_starts_from)

                if(job.is_favourite!=null && job.is_favourite==1){
                    Glide.with(binding.root.context)
                        .load(R.drawable.ic_favorite_filled)
                        .into(ivItemFavorite)
                }else{
                    Glide.with(binding.root.context)
                        .load(R.drawable.ic_favorite_border)
                        .into(ivItemFavorite)
                }

            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(job: Job)
        fun onFavoriteClick(job:Job)
        fun onShareClick(job: Job)
    }

    class DiffUtilCallback: DiffUtil.ItemCallback<Job>(){
        override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem == newItem
        }
    }
}