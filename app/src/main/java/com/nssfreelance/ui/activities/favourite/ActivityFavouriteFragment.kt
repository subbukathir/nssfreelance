package com.nssfreelance.ui.activities.favourite

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.FragmentActivitiesBinding
import com.nssfreelance.ui.activities.ActivitiesViewmodel
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "ActivityFavouriteFragme"
@AndroidEntryPoint
class ActivityFavouriteFragment:Fragment(R.layout.fragment_activities), FavouriteJobsAdapter.OnItemClickListener {
    private var _binding: FragmentActivitiesBinding?=null
    private val binding: FragmentActivitiesBinding
        get() = _binding!!

    private val viewmodel: ActivitiesViewmodel by activityViewModels()

    private lateinit var mAdapter: FavouriteJobsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentActivitiesBinding.bind(view)

        mAdapter = FavouriteJobsAdapter(this)

        binding.apply {
            recyclerviewActivity.itemAnimator = DefaultItemAnimator()
            recyclerviewActivity.adapter =mAdapter
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.getFavouriteJobs()

            viewmodel.activityEventTask.collect { event ->
                when(event){
                    is ActivitiesViewmodel.ActivityTask.NavigateToJobDetails ->{

                    }
                    ActivitiesViewmodel.ActivityTask.RefreshView -> {
                        viewmodel.getFavouriteJobs()
                    }
                }.exhaustive
            }
        }
        viewmodel.favouriteDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.triggerCount=0
                        if (dataState.data.data.isNotEmpty()) {
                            viewmodel.favouriteJobList = dataState.data.data
                            mAdapter.submitList(viewmodel.favouriteJobList)
                            binding.error.tvError.hide()
                            binding.recyclerviewActivity.show()
                        } else {
                            binding.error.tvError.show()
                            binding.recyclerviewActivity.hide()
                        }
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    if(dataState.exception.message?.contains("502 Bad Gateway")==true) {
                        displayError(requireView(), "Network error")
                        if(viewmodel.triggerCount<3) {
                            viewmodel.triggerCount++
                            progressBar(binding.progress.progressBar, true)
                            viewmodel.getFavouriteJobs()
                            Thread.sleep(5000)
                        }else{}
                    }else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })


        viewmodel.favouriteJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.getFavouriteJobs()
                    } else {
                        Log.d(TAG, "subscribeObservers: empty list")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${dataState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }

    override fun onItemClick(job: Job) {
        Log.d(TAG, "onItemClick: ")
        viewmodel.onClickOfItem(job)
    }

    override fun onFavoriteClick(job: Job) {
        viewmodel.onFavouriteClicked(job)
    }

    override fun onShareClick(job: Job) {
        val shareBody = getFreelancerShareBody(job,requireContext())
        shareJobs(shareBody,requireContext())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}