package com.nssfreelance.ui.activities.past

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.FragmentActivitiesBinding
import com.nssfreelance.ui.activities.ActivitiesViewmodel
import com.nssfreelance.ui.activities.ActivityTabFragmentDirections
import com.nssfreelance.ui.activities.favourite.FavouriteJobsAdapter
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlin.math.log

private const val TAG = "ActivityPastFragment"
@AndroidEntryPoint
class ActivityPastFragment:Fragment(R.layout.fragment_activities),PastJobsAdapter.OnItemClickListener {
    private var _binding: FragmentActivitiesBinding?=null
    private val binding: FragmentActivitiesBinding
        get() = _binding!!

    private val viewmodel: ActivitiesViewmodel by activityViewModels()

    private lateinit var mAdapter: PastJobsAdapter

    override fun onStart() {
        Log.d(TAG, "onStart: ")
        super.onStart()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView: ")
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onStop() {
        Log.d(TAG, "onStop: ")
        super.onStop()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentActivitiesBinding.bind(view)

        mAdapter = PastJobsAdapter(this)

        binding.apply {
            recyclerviewActivity.itemAnimator = DefaultItemAnimator()
            recyclerviewActivity.adapter =mAdapter
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.getPastJobs()
        }
        viewmodel.pastDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.triggerCount=0
                        if (dataState.data.data.isNotEmpty()) {
                            viewmodel.pastJobList = dataState.data.data
                            mAdapter.submitList(viewmodel.pastJobList)
                            binding.error.tvError.hide()
                            binding.recyclerviewActivity.show()
                        } else {
                            binding.error.tvError.show()
                            binding.recyclerviewActivity.hide()
                        }
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    if(dataState.exception.message?.contains("502 Bad Gateway")==true) {
                        displayError(requireView(), "Network error")
                        if(viewmodel.triggerCount<3) {
                            viewmodel.triggerCount++
                            progressBar(binding.progress.progressBar, true)
                            viewmodel.getPastJobs()
                            Thread.sleep(5000)
                        }else{}
                    }else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }

    override fun onItemClick(job: Job) {
        Log.d(TAG, "onItemClick: ${job.client_job_post_id}")
        viewmodel.onClickOfItem(job)
    }

    override fun onClientReviewClick(job: Job) {
        if(job.is_client_reviewed==0){
            val action = ActivityTabFragmentDirections.actionActivityTabFragmentToCloseContractSuccessDialogFragment(viewmodel.userId,job.client_user_id.toString(),job.client_job_post_id.toString(),true,
                job.client_user_name.toString()
            )
            findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}