package com.nssfreelance.ui.activities.applied

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.FragmentActivitiesBinding
import com.nssfreelance.ui.activities.ActivitiesViewmodel
import com.nssfreelance.ui.activities.current.CurrentJobsAdapter
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ActivityAppliedFragment:Fragment(R.layout.fragment_activities),AppliedJobsAdapter.OnItemClickListener {
    private var _binding: FragmentActivitiesBinding?=null
    private val binding: FragmentActivitiesBinding
        get() = _binding!!

    private val viewmodel: ActivitiesViewmodel by activityViewModels()

    private lateinit var mAdapter: AppliedJobsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentActivitiesBinding.bind(view)

        mAdapter = AppliedJobsAdapter(this)

        binding.apply {
            recyclerviewActivity.itemAnimator = DefaultItemAnimator()
            recyclerviewActivity.adapter =mAdapter
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.getAppliedJobs()
        }
        viewmodel.appliedDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.triggerCount=0
                        if (dataState.data.data.isNotEmpty()) {
                            viewmodel.appliedJobList = dataState.data.data
                            mAdapter.submitList(viewmodel.appliedJobList)
                            binding.error.tvError.hide()
                            binding.recyclerviewActivity.show()
                        } else {
                            binding.error.tvError.show()
                            binding.recyclerviewActivity.hide()
                        }
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    if(dataState.exception.message?.contains("502 Bad Gateway")==true) {
                        displayError(requireView(), "Network error")
                        if(viewmodel.triggerCount<3) {
                            viewmodel.triggerCount++
                            progressBar(binding.progress.progressBar, true)
                            viewmodel.getAppliedJobs()
                            Thread.sleep(5000)
                        }else{}
                    }else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }

    override fun onItemClick(job: Job) {
        viewmodel.onClickOfItem(job)
    }

    override fun onFavoriteClick(job: Job) {
    }

    override fun onShareClick(job: Job) {
        val shareBody = getFreelancerShareBody(job,requireContext())
        shareJobs(shareBody,requireContext())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}