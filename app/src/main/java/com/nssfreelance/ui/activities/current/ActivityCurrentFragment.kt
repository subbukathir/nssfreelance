package com.nssfreelance.ui.activities.current

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.FragmentActivitiesBinding
import com.nssfreelance.ui.activities.ActivitiesViewmodel
import com.nssfreelance.ui.activities.ActivityTabFragmentDirections
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "ActivityCurrentFragment"

@AndroidEntryPoint
class ActivityCurrentFragment : Fragment(R.layout.fragment_activities),
    CurrentJobsAdapter.OnItemClickListener {
    private var _binding: FragmentActivitiesBinding? = null
    private val binding: FragmentActivitiesBinding
        get() = _binding!!

    private val viewmodel: ActivitiesViewmodel by activityViewModels()

    private lateinit var mAdapter: CurrentJobsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentActivitiesBinding.bind(view)

        mAdapter = CurrentJobsAdapter(this)

        binding.apply {
            recyclerviewActivity.itemAnimator = DefaultItemAnimator()
            recyclerviewActivity.adapter = mAdapter
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.getCurrentJobs()

            viewmodel.activityEventTask.collect { event ->
                Log.d(TAG, "subscribeObservers:")
                when (event) {
                    is ActivitiesViewmodel.ActivityTask.NavigateToJobDetails -> {
                        val action =
                            ActivityTabFragmentDirections.actionActivityTabFragmentToJobAndClientDetailsTabFragment(
                                event.job.client_job_post_id.toString(),
                                0
                            )
                        findNavController().navigate(action)
                    }
                    ActivitiesViewmodel.ActivityTask.RefreshView -> {

                    }
                }.exhaustive
            }
        }
        viewmodel.currentDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.triggerCount =0
                        if (dataState.data.data.isNotEmpty()) {
                            viewmodel.currentJobList = dataState.data.data
                            mAdapter.submitList(viewmodel.currentJobList)
                            binding.error.tvError.hide()
                            binding.recyclerviewActivity.show()
                        } else {
                            binding.error.tvError.show()
                            binding.recyclerviewActivity.hide()
                        }
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    if(dataState.exception.message?.contains("502 Bad Gateway")==true) {
                        displayError(requireView(), "Network error")
                        if(viewmodel.triggerCount<3) {
                            viewmodel.triggerCount++
                            progressBar(binding.progress.progressBar, true)
                            viewmodel.getCurrentJobs()
                            Thread.sleep(5000)
                        }else{}
                    }else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.completeJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        displayError(requireView(), "Job completed successfully")
                        viewmodel.getCurrentJobs()
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }

    override fun onItemClick(job: Job) {
        Log.d(TAG, "onItemClick: ")
        viewmodel.onClickOfItem(job)
        val action = ActivityTabFragmentDirections.actionActivityTabFragmentToJobAndClientDetailsTabFragment(job.client_job_post_id.toString(), 0)
        findNavController().navigate(action)
    }

    override fun onMarkCompleteClick(job: Job) {
        if (job.is_freelancer_completed == null || job.is_freelancer_completed == 0)
            viewmodel.onCompleteFreelancer(job)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}