package com.nssfreelance.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.*
import com.nssfreelance.R
import com.nssfreelance.data.HomeJobs
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.ItemHomeJobBinding
import com.nssfreelance.util.Const

class HomeJobsVerticalAdapter(private val listener:OnItemClickListener):ListAdapter<HomeJobs, HomeJobsVerticalAdapter.HomeJobsViewHolder>(DiffCallback()) {
    private lateinit var adapter: JobsHorizontalAdapter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeJobsViewHolder {
        val binding = ItemHomeJobBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return HomeJobsViewHolder(binding = binding)
    }

    override fun onBindViewHolder(holder: HomeJobsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class HomeJobsViewHolder(private val binding:ItemHomeJobBinding):RecyclerView.ViewHolder(binding.root){
        init {
            binding.apply {
                adapter = JobsHorizontalAdapter(object:JobsHorizontalAdapter.OnItemClickListener{
                    override fun onItemClick(job: Job) {
                        listener.onItemClick(job)
                    }

                    override fun onFavoriteClick(job: Job) {
                        listener.onFavoriteClick(job)
                    }
                })
                recylerviewItemJob.layoutManager = LinearLayoutManager(this.root.context,LinearLayoutManager.HORIZONTAL,false)
                recylerviewItemJob.itemAnimator = DefaultItemAnimator()
                recylerviewItemJob.adapter = adapter

                tvItemViewAll.setOnClickListener {
                    listener.onParentClick(getItem(adapterPosition))
                }
            }
        }
        fun bind(jobs: HomeJobs){
            binding.apply {
                tvItemTitle.text = jobs.jobName
                adapter.updateData(jobs.jobs)

                if(jobs.jobNameValue.equals(Const.TRENDING_JOB)){
                    tvItemTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_trending_up,0,0,0)
                }else if(jobs.jobNameValue.equals(Const.NEARBY_JOB)){
                    tvItemTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_location_on,0,0,0)
                }else{
                    tvItemTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,0,0)
                }
            }
        }
    }

    interface OnItemClickListener{
        fun onParentClick(homeJobs:HomeJobs)
        //child clicks
        fun onItemClick(job: Job)
        fun onFavoriteClick(job:Job)

    }

    class DiffCallback: DiffUtil.ItemCallback<HomeJobs>(){
        override fun areItemsTheSame(oldItem: HomeJobs, newItem: HomeJobs): Boolean {
            return oldItem.jobName == newItem.jobName
        }

        override fun areContentsTheSame(oldItem: HomeJobs, newItem: HomeJobs): Boolean {
            return oldItem == newItem
        }
    }
}