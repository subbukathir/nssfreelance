package com.nssfreelance.ui.home

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.HomeJobs
import com.nssfreelance.data.Job
import com.nssfreelance.data.Profile
import com.nssfreelance.data.Review
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.request.ApplyJobFavouriteParams
import com.nssfreelance.retrofit.response.AllJobsResponse
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.retrofit.response.ProfileResponse
import com.nssfreelance.util.DataState
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch


class HomeViewmodel @ViewModelInject constructor(
    private val preferences: PreferencesManager,
    @Assisted private val state: SavedStateHandle,
    private val repository: Repository

) : ViewModel() {

    val preferencesFlow = preferences.preferencesFlow
    var userId = ""
    var userToken = ""
    var userTypeId = 0
    var isDataReceived = false
    var listHomeJobs: MutableLiveData<List<HomeJobs>> = MutableLiveData()
    var mapData: HashMap<String, HomeJobs> = HashMap()

    val profileData: MutableLiveData<Profile> = MutableLiveData()

    val freelancerReview:MutableLiveData<List<Review>> = MutableLiveData()

    private val _profileState: MutableLiveData<DataState<ProfileResponse>> = MutableLiveData()
    val profileState: LiveData<DataState<ProfileResponse>>
        get() = _profileState

    private val _recommendedJobsState: MutableLiveData<DataState<AllJobsResponse>> =
        MutableLiveData()
    val recommendedJobState: LiveData<DataState<AllJobsResponse>>
        get() = _recommendedJobsState

    private val _trendJobsState: MutableLiveData<DataState<AllJobsResponse>> = MutableLiveData()
    val trendJobState: LiveData<DataState<AllJobsResponse>>
        get() = _trendJobsState

    private val _nearJobsState: MutableLiveData<DataState<AllJobsResponse>> = MutableLiveData()
    val nearJobState: LiveData<DataState<AllJobsResponse>>
        get() = _nearJobsState

    private val _latestJobsState: MutableLiveData<DataState<AllJobsResponse>> = MutableLiveData()
    val latestJobState: LiveData<DataState<AllJobsResponse>>
        get() = _latestJobsState

    private val _favouriteJobsState: MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val favouriteJobState: LiveData<DataState<LoginResponse>>
        get() = _favouriteJobsState

    fun getJobs() {
        viewModelScope.launch {
            repository.getRecommendedJobs(token = "Bearer $userToken", userId = userId)
                .onEach { dataState -> _recommendedJobsState.value = dataState }
                .launchIn(viewModelScope)
        }

        viewModelScope.launch {
            repository.getTrendingJobs(token = "Bearer $userToken", userId = userId)
                .onEach { dataState -> _trendJobsState.value = dataState }
                .launchIn(viewModelScope)
        }

        viewModelScope.launch {
            repository.getNearByJobs(token = "Bearer $userToken", userId = userId)
                .onEach { dataState -> _nearJobsState.value = dataState }
                .launchIn(viewModelScope)
        }

        viewModelScope.launch {
            repository.getLatestJobs(token = "Bearer $userToken", userId = userId)
                .onEach { dataState -> _latestJobsState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun getProfileData() {
        viewModelScope.launch {
            repository.getProfile(token = "Bearer $userToken", userId)
                .onEach { dataState -> _profileState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun updateUsername(name: String) {
        viewModelScope.launch {
            if (name.isNotEmpty())
                preferences.updateUsername(name)
        }
    }

    fun updateFavourite(job: Job) {
        viewModelScope.launch {
            val params = ApplyJobFavouriteParams(
                request_progress_id = (job.request_progress_id ?: 0).toString(),
                client_job_post_id = (job.client_job_post_id).toString(),
                freelancer_user_id = userId,
                is_applied = "${job.is_applied}",
                is_favourite = if (job.is_favourite == 1) "0" else "1",
                client_user_id = (job.client_user_id ?: 0).toString(),
                job_bid_price = "0",
                job_id = (job.client_job_post_id).toString()

            )
            repository.applyFavouriteJob("Bearer $userToken", params)
                .onEach { dataState -> _favouriteJobsState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun getReviewDetails():List<Review>{
        return freelancerReview.value!!
    }
}



/**
 * Optimized methode to fetch all the requests at once
 */
/*
suspend fun fetchItems(itemIds: Iterable<Long>): Map<Long, AllJobsResponse> = itemIds
    .asFlow()
    .flatMapMerge(concurrency = 4) { itemId ->

        flow { emit(itemId to repository.getRecommendedJobs(token = "Bearer $userToken", userId = userId)) }
    }
    .toMap()

suspend fun <K, V> Flow<Pair<K, V>>.toMap(): Map<K, V> {
    val result = mutableMapOf<K, V>()
    collect { (k, v) -> result[k] = v }
    return result
}


    inline fun <T> MutableList<T>.mapInPlace(mutator: (T)->T) {
        val iterate = this.listIterator()
        while (iterate.hasNext()) {
            val oldValue = iterate.next()
            val newValue = mutator(oldValue)
            if (newValue !== oldValue) {
                iterate.set(newValue)
            }
        }
    }*/
