package com.nssfreelance.ui.home

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.ItemHomeJobCardBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency

class JobsHorizontalAdapter(private val listener:OnItemClickListener): ListAdapter<Job, JobsHorizontalAdapter.JobHorizontalViewHolder>(DiffUtilCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobHorizontalViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_home_job_card, parent, false)
        view.layoutParams = ViewGroup.LayoutParams((parent.width * 0.7).toInt(),ViewGroup.LayoutParams.MATCH_PARENT)

        val itemBinding = ItemHomeJobCardBinding.inflate(LayoutInflater.from(parent.context),
            parent, false)

        return JobHorizontalViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: JobHorizontalViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    fun updateData(jobs: List<Job>) {
        submitList(jobs)
    }

    inner class JobHorizontalViewHolder(private val binding:ItemHomeJobCardBinding):RecyclerView.ViewHolder(binding.root)
    {
        init {
            binding.ivItemFavorite.setOnClickListener {
                listener.onFavoriteClick(getItem(adapterPosition))
            }

            binding.root.setOnClickListener {
                listener.onItemClick(getItem(adapterPosition))
            }
        }
        fun bind(job: Job){
            binding.apply {
                if(job.isIcon){
                    Glide.with(binding.root.context)
                        .load(job.url)
                        .placeholder(R.drawable.ic_truck)
                        .into(imageviewJobType)
                    imageviewJobType.visibility = View.VISIBLE
                    viewYellowSquare.visibility = View.VISIBLE
                }else{
                    imageviewJobType.visibility = View.GONE
                    viewYellowSquare.visibility = View.GONE
                }

                tvItemJobRole.text = job.job_title
                tvItemName.text = job.client_user_name
                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemRate.text = "${convertToCurrency(job.job_price.toString(),tvItemJobPostedOn.context)} | ${job.job_type}"
                Log.d("TAG", "bind: ${job.job_starts_from}")
                tvItemJobPostedOn.text = commonDateFormate(job.job_starts_from)
                if(job.is_favourite!=null && job.is_favourite==1){
                    Glide.with(binding.root.context)
                        .load(R.drawable.ic_favorite_filled)
                        .into(ivItemFavorite)
                }else{
                    Glide.with(binding.root.context)
                        .load(R.drawable.ic_favorite_border)
                        .into(ivItemFavorite)
                }
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(job: Job)
        fun onFavoriteClick(job:Job)
    }

    class DiffUtilCallback:DiffUtil.ItemCallback<Job>(){
        override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem == newItem
        }
    }
}