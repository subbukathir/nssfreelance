package com.nssfreelance.ui.home

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.HomeJobs
import com.nssfreelance.data.Job
import com.nssfreelance.data.Profile
import com.nssfreelance.databinding.FragmentHomeBinding
import com.nssfreelance.ui.MainActivity
import com.nssfreelance.util.*
import com.nssfreelance.util.Const.LATEST_JOB
import com.nssfreelance.util.Const.NEARBY_JOB
import com.nssfreelance.util.Const.RECOMMENDED_JOB
import com.nssfreelance.util.Const.TRENDING_JOB
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import okhttp3.internal.wait

private const val TAG = "HomeFragment"

@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.fragment_home), HomeJobsVerticalAdapter.OnItemClickListener {

    private var _binding: FragmentHomeBinding? = null
    private val binding: FragmentHomeBinding
        get() = _binding!!
    private lateinit var adapter: HomeJobsVerticalAdapter
    private val viewmodel: HomeViewmodel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated: ")
        _binding = FragmentHomeBinding.bind(view)

        adapter = HomeJobsVerticalAdapter(this)
        binding.apply {


            swipeRefresh.setOnRefreshListener {
                viewmodel.getProfileData()
                viewmodel.getJobs()
            }

            tvTitle.setOnClickListener {
                (activity as MainActivity).requestLocation()
            }

            recyclerviewHomeJobs.itemAnimator = DefaultItemAnimator()
            recyclerviewHomeJobs.adapter = adapter
        }
        subscribeObservers()

        //updateFcmToken
        (requireActivity() as MainActivity).fetchFCMToken()
    }

    private fun moveToMapFragment() {
        val action = HomeFragmentDirections.actionHomeFragmentToMapsFragment()
        findNavController().navigate(action)
    }

    override fun onParentClick(homeJobs: HomeJobs) {
        val action = HomeFragmentDirections.actionHomeFragmentToJobListFragment(homeJobs = homeJobs)
        findNavController().navigate(action)
    }

    override fun onItemClick(job: Job) {
        val action = HomeFragmentDirections.actionHomeFragmentToJobAndClientDetailsTabFragment(
            jobId = "${job.client_job_post_id}",
            0
        )
        findNavController().navigate(action)
    }

    override fun onFavoriteClick(job: Job) {
        viewmodel.updateFavourite(job)
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            Log.d(TAG, "subscribeObservers: ")
            viewmodel.preferencesFlow.collect { flow ->
                viewmodel.userId = flow.userId
                viewmodel.userToken = flow.userToken
                viewmodel.userTypeId = flow.userType
                val locality = flow.locality
                if (locality.isNotEmpty())
                    binding.tvTitle.text = locality
                else {
                    (activity as MainActivity).requestLocation()
                }

                if (!viewmodel.isDataReceived) {
                    viewmodel.getProfileData()
                    viewmodel.getJobs()
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.listHomeJobs.observe(viewLifecycleOwner, { listData ->
                if (listData.isNotEmpty()) {
                    adapter.submitList(ArrayList(listData))
                    binding.tvError.hide()
                    binding.recyclerviewHomeJobs.show()
                } else {
                    binding.tvError.text = "Jobs not found"
                    binding.tvError.show()
                    binding.recyclerviewHomeJobs.hide()
                }
            })
        }


        viewmodel.profileState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    updateProfileView(dataState.data.data.profile)
                }
                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.exception.message?.contains("502 Bad Gateway") == true) {
//                        displayError(requireView(), "Network error")
                    } else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.viewProgress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.favouriteJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.getJobs()
                    } else {
                        Log.d(TAG, "subscribeObservers: Recommended Jobs empty list")
                    }
                }
                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.exception.message?.contains("502 Bad Gateway") == true) {
//                        displayError(requireView(), "Network error")
                    } else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.viewProgress.progressBar, true)
                }
            }.exhaustive
        })


        /**
         * fetch all the jobs and update UI
         */
        viewmodel.recommendedJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    viewmodel.isDataReceived = true
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.data.data.isEmpty()) {
                    } else
                        updateJobList("Recommended Jobs", RECOMMENDED_JOB, dataState.data.data)
                }
                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.exception.message?.contains("502 Bad Gateway") == true) {
                        displayError(requireView(), "Network error")
                        progressBar(binding.viewProgress.progressBar, true)
                        Log.d(TAG, "subscribeObservers: Recommended Jobs called")
                        viewmodel.getProfileData()
                        viewmodel.getJobs()
                        progressBar(binding.viewProgress.progressBar, true)
                        Thread.sleep(5000)
                        progressBar(binding.viewProgress.progressBar, true)
                    } else
                        displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.viewProgress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.trendJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.data.data.isEmpty()) {
                    } else {
                        updateJobList("Trending Jobs", TRENDING_JOB, dataState.data.data)
                    }
                }
                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.exception.message?.contains("502 Bad Gateway") == true) {
//                        displayError(requireView(), "Network error")
                    } else
                        displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.viewProgress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.nearJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.data.data.isEmpty()) {
                    } else {
                        updateJobList("Jobs Near You", NEARBY_JOB, dataState.data.data)
                    }
                }
                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.exception.message?.contains("502 Bad Gateway") == true) {
//                        displayError(requireView(), "Network error")
                    } else
                        displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.viewProgress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.latestJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.data.data.isEmpty()) {
                    } else {
                        updateJobList("Latest Jobs", LATEST_JOB, dataState.data.data)
                    }
                }
                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.exception.message?.contains("502 Bad Gateway") == true) {
//                        displayError(requireView(), "Network error")
                    } else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.viewProgress.progressBar, true)
                }
            }.exhaustive
        })
    }

    private fun updateJobList(jobName: String, jobNameValue: String, listJobs: List<Job>) {

        if (jobNameValue.equals(RECOMMENDED_JOB) || jobNameValue.equals(NEARBY_JOB)) {
            listJobs.forEach { job -> job.isIcon = true }
        }
        viewmodel.mapData[jobNameValue] = HomeJobs(
            jobName = jobName,
            jobNameValue = jobNameValue,
            jobs = listJobs
        )
        val listHomeJobs = arrayListOf<HomeJobs>()
        viewmodel.mapData.forEach { (key, value) ->
            listHomeJobs.add(value)
        }
        Log.d(TAG, "updateJobList: ${listHomeJobs.size}")
        if (listHomeJobs.size > 0) viewmodel.listHomeJobs.value = listHomeJobs
    }

    private fun updateProfileView(data: Profile) {
        binding.apply {
            tvProfileName.text = data.name
            tvUsername.text = data.name
            tvProfileJobRole.text = "${data.country_name} | ${data.city_name}"
            tvRatedCount.text = "${data.review_count}"
            tvProfileRating.text = "${data.rating_count}"
            viewmodel.updateUsername(data.name)
            tvJoinedYrs.text = "Joined on ${commonDateFormate(data.registered_on)}"

            if (data.rating_count == 0) llRating.hide()
            else {
                llRating.show()
                ratingBar.rating = data.rating_count?.toFloat()
            }

            Glide.with(requireContext())
                .load(data.profile_photo_link)
                .placeholder(R.drawable.ic_placeholder_profile)
                .into(civProfile)

            tvTotalEarnings.text =
                convertToCurrency(data.total_earnings.toString(), requireContext())
            tvAmountPending.text =
                convertToCurrency(data.amount_pending.toString(), requireContext())
            tvSubmission.text = "${data.application_submitted}"
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}