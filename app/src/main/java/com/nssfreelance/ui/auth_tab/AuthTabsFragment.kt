package com.nssfreelance.ui.auth_tab

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.Gson
import com.nssfreelance.R
import com.nssfreelance.data.RegisterDetails
import com.nssfreelance.databinding.FragmentAuthTabsBinding
import com.nssfreelance.ui.login.LoginFragment
import com.nssfreelance.ui.login.LoginFragmentDirections
import com.nssfreelance.ui.login.LoginViewmodel
import com.nssfreelance.ui.register.RegisterFragment
import com.nssfreelance.ui.register.RegisterViewmodel
import com.nssfreelance.ui.register.ViewpagerAdapter
import com.nssfreelance.ui.register_choose_skills.ChooseSkillsFragment
import com.nssfreelance.ui.register_choose_skills.ChooseSkillsViewmodel
import com.nssfreelance.util.DataState
import com.nssfreelance.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "AuthTabsFragment"

@AndroidEntryPoint
class AuthTabsFragment : Fragment(R.layout.fragment_auth_tabs) {

    private var _binding: FragmentAuthTabsBinding? = null
    private val binding
        get() = _binding!!

    private val args: AuthTabsFragmentArgs by navArgs()
    private lateinit var viewpagerAdapter: ViewpagerAdapter

    private val viewmodelRegister: RegisterViewmodel by viewModels()
    private val viewmodelLogin: LoginViewmodel by activityViewModels()
    private val viewmodelSkills: ChooseSkillsViewmodel by viewModels()

    private var userId = ""
    private var userTypes:List<RegisterDetails.UserType> = mutableListOf()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentAuthTabsBinding.bind(view)

        viewpagerAdapter =
            ViewpagerAdapter(fragmentManager = requireActivity().supportFragmentManager, lifecycle)
        viewmodelLogin.argsTabPosition = args.tabPosition

        binding.apply {
            tvToolbar.text = args.title
            when (args.title) {
                "Sign Up" -> {
                    viewpagerAdapter.addFragment(RegisterFragment())
                    viewpagerAdapter.addFragment(RegisterFragment())
                }
                "Sign In" -> {
                    viewpagerAdapter.addFragment(LoginFragment())
                    viewpagerAdapter.addFragment(LoginFragment())
                }
                "Choose Minimum 3 Skills" -> {
                    viewpagerAdapter.addFragment(ChooseSkillsFragment())
                    viewpagerAdapter.addFragment(ChooseSkillsFragment())
                }
            }
            viewpagerAuth.adapter = viewpagerAdapter
            TabLayoutMediator(
                tabLayoutAuth, viewpagerAuth
            ) { tab, position ->
                when (args.title) {
                    "Sign Up" -> {
                        when (position) {
                            0 -> tab.text = "Sign Up Freelancer"
                            1 -> tab.text = "Sign Up Client"
                        }
                    }
                    "Sign In" -> {
                        when (position) {
                            0 -> tab.text = "Sign In Freelancer"
                            1 -> tab.text = "Sign In Client"
                        }
                    }
                    "Choose Minimum 3 Skills" -> {
                        when (position) {
                            0 -> tab.text = "Sign Up Freelancer"
                            1 -> tab.text = "Sign Up Client"
                        }
                    }
                }
            }.attach()

            tabLayoutAuth.getTabAt(args.tabPosition)?.select()

            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            tabLayoutAuth.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    Log.d(TAG, "onTabSelected: ${tab?.position}")
                    when (args.title) {
                        "Sign Up" -> {
                            when (tab?.position) {
                                0 -> {
                                    viewmodelRegister.isFreelancer = true
                                    viewmodelRegister.userType = getUserType(0, userTypes).toString()
                                }
                                1 -> {
                                    viewmodelRegister.isFreelancer = false
                                    viewmodelRegister.userType = getUserType(1, userTypes).toString()
                                }
                            }
                        }
                        "Sign In" -> {
                            when (tab?.position) {
                                0 -> {
                                    viewmodelLogin.argsTabPosition = 0
                                    viewmodelLogin.setupUserType(getUserType(0, userTypes).toString(),true)
                                }
                                1 -> {
                                    viewmodelLogin.argsTabPosition = 1
                                    viewmodelLogin.setupUserType(getUserType(1, userTypes).toString(),false)
                                }
                            }
                        }
                        "Choose Minimum 3 Skills" -> {

                        }
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }
            })
        }

        Log.d(TAG, "onViewCreated: ${args.tabPosition}  - ${args.title}")

        getUserDetails()
    }

    private fun getUserDetails() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodelRegister.preferencesFlow.collect { data ->
                Log.d(TAG, "flow user details:")
                val userId = data.userId
                val userDetail = data.userDetails

                if(userDetail.isEmpty()) viewmodelRegister.getRegisterDetails()
                else {
                    userTypes = Gson().fromJson(userDetail,RegisterDetails::class.java).user_type
                    when(args.title){
                        "Sign In" -> {
                            when (viewmodelLogin.argsTabPosition) {
                                0 -> {
                                    viewmodelLogin.setupUserType(getUserType(0, userTypes).toString(),true)
                                }
                                1 -> {
                                    viewmodelLogin.setupUserType(getUserType(1, userTypes).toString(),false)
                                }
                            }
                        }

                    }
                }
            }

            viewmodelLogin.loginEvent.collect { event ->
                when (event) {
                    is LoginViewmodel.LoginTaskEvent.ShowInvalidMsg -> {
                        displayError(event.msg)
                    }
                    LoginViewmodel.LoginTaskEvent.NavigateToRegister -> {
                        Log.d(TAG, "getUserDetails: auth fragment")
                        val action =
                            LoginFragmentDirections.actionLoginFragmentToRegisterLanding()
                        findNavController().navigate(action)
                    }
                    LoginViewmodel.LoginTaskEvent.NavigateToHome -> {
                        /*val action = LoginFragmentDirections.actionLoginFragmentToHomeFragment()
                        findNavController().navigate(action)*/
                    }
                }.exhaustive
            }
        }

        viewmodelRegister.dataState.observe(viewLifecycleOwner, {dataState ->
            when(dataState){
                is DataState.Success<RegisterDetails> -> {
                    displayProgressbar(false)
                    viewmodelRegister.setRegisterDetails(dataState.data)
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(dataState.exception.message)
                }
                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }
        })

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodelLogin.loginEvent.collect { event->
                when(event){
                    is LoginViewmodel.LoginTaskEvent.ShowInvalidMsg -> {}
                    LoginViewmodel.LoginTaskEvent.NavigateToRegister -> {}
                    LoginViewmodel.LoginTaskEvent.NavigateToHome -> {
                        if (viewmodelLogin.userTypeId.value.equals("2")) {
                            Log.d(TAG, "getUserDetails: freelancer")
                            val action =
                                AuthTabsFragmentDirections.actionAuthTabsFragmentToHomeFragment()
                            findNavController().navigate(action)
                        } else {
                            Log.d(TAG, "getUserDetails: client")
                            val action =
                                AuthTabsFragmentDirections.actionAuthTabsFragmentToHomeClientFragment()
                            findNavController().navigate(action)
                        }
                    }
                }.exhaustive
            }
        }
    }

    private fun getUserType(pos:Int, list: List<RegisterDetails.UserType>):Int{
        var itemId = 0
            if(pos==0){
                itemId = list.filter { data-> data.user_type_code.equals("FREELANCER",true) }.single().user_type_id
            }
            else {
                itemId = list.filter { data-> data.user_type_code.equals("CLIENT",true) }.single().user_type_id
            }
        Log.d(TAG, "getUserType: $itemId")
        return itemId
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
        } else {
            Snackbar.make(requireView(), "Unknown error", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun displayProgressbar(isDisplayed: Boolean) {
        binding.progress.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}