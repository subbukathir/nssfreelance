package com.nssfreelance.ui.job_list

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.FragmentJobsFreelancerBinding
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "JobListFragment"

@AndroidEntryPoint
class JobListFragment : Fragment(R.layout.fragment_jobs_freelancer), JobsAdapter.OnItemClickListener {
    private var _binding: FragmentJobsFreelancerBinding? = null
    private val binding: FragmentJobsFreelancerBinding
        get() = _binding!!

    private lateinit var adapter: JobsAdapter
    private val args: JobListFragmentArgs by navArgs()
    private val viewmodel: JobListViewmodel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentJobsFreelancerBinding.bind(view)
        adapter = JobsAdapter(this)
        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            tvTitle.text = args.homeJobs.jobName

            recylerviewJobs.itemAnimator = DefaultItemAnimator()
            recylerviewJobs.adapter = adapter

            swipeRefresh.setOnRefreshListener {
                viewmodel.getJobs(args.homeJobs.jobNameValue)
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.preferencesFlow.collect { flow ->
                viewmodel.userId = flow.userId
                viewmodel.userToken = flow.userToken
                if (args.homeJobs.jobs.size == 0) {
                    viewmodel.getJobs(args.homeJobs.jobNameValue)
                } else {
                    viewmodel.listJobs = args.homeJobs.jobs
                    adapter.submitList(viewmodel.listJobs)
                }
            }
        }

        viewmodel.jobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.data.status) {
                        if (dataState.data.data.isNotEmpty()) {
                            viewmodel.listJobs = dataState.data.data
                            adapter.submitList(viewmodel.listJobs)
                        } else {
                            displayError(requireView(), "Data not found")
                        }
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.viewProgress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.favouriteJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.getJobs(args.homeJobs.jobNameValue)
                    } else {
                        Log.d(TAG, "subscribeObservers: Recommended Jobs empty list")
                    }
                }
                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.viewProgress.progressBar, true)
                }
            }.exhaustive
        })
    }

    override fun onItemClick(job: Job) {
        val action = JobListFragmentDirections.actionJobListFragmentToJobAndClientDetailsTabFragment(jobId = "${job.client_job_post_id}",0)
        findNavController().navigate(action)
    }
    override fun onFavoriteClick(job: Job) {
        Log.d(TAG, "onFavoriteClick: ${job.is_favourite}")
        viewmodel.updateFavourite(job)
    }

    override fun onShareClick(job: Job) {
        Log.d(TAG, "onShareClick: ${job.client_user_name}")
        val shareBody = getFreelancerShareBody(job,requireContext())
        shareJobs(shareBody,requireContext())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}