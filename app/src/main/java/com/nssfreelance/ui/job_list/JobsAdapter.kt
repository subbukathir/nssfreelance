package com.nssfreelance.ui.job_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.ItemJobCardBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency

class JobsAdapter(private val listener:OnItemClickListener):ListAdapter<Job, JobsAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding = ItemJobCardBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem =getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding:ItemJobCardBinding):RecyclerView.ViewHolder(binding.root){
        init {

            binding.apply {
                ivItemFavorite.setOnClickListener {
                    listener.onFavoriteClick(getItem(adapterPosition))
                }
                ivItemShare.setOnClickListener {
                    listener.onShareClick(getItem(adapterPosition))
                }

                root.setOnClickListener{
                    listener.onItemClick(getItem(adapterPosition))
                }
            }

        }
        fun bind(job:Job){
            binding.apply {

                tvItemJobRole.text = job.job_title
                tvItemName.text = job.client_user_name
                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemRate.text = "${convertToCurrency((job.job_price?:0).toString(),binding.root.context)} | ${job.job_type?:""}"
                tvItemJobPostedOn.text = commonDateFormate(job?.job_starts_from)
                if(job.is_favourite==1){
                    Glide.with(binding.root.context)
                        .load(R.drawable.ic_favorite_filled)
                        .into(ivItemFavorite)
                }else{
                    Glide.with(binding.root.context)
                        .load(R.drawable.ic_favorite_border)
                        .into(ivItemFavorite)
                }

                Glide.with(binding.root.context)
                    .load(job.url)
                    .placeholder(R.drawable.ic_plug_wire)
                    .into(imageviewJobType)
                viewYellowSquare.visibility = View.VISIBLE
            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(job: Job)
        fun onFavoriteClick(job:Job)
        fun onShareClick(job: Job)
    }

    class DiffUtilCallback: DiffUtil.ItemCallback<Job>(){
        override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem == newItem
        }
    }
}