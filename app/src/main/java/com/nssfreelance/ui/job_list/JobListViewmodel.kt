package com.nssfreelance.ui.job_list

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.Job
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.request.ApplyJobFavouriteParams
import com.nssfreelance.retrofit.response.AllJobsResponse
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class JobListViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val stateHandle: SavedStateHandle,
    private val prefereces:PreferencesManager
) : ViewModel() {
    val preferencesFlow = prefereces.preferencesFlow
    var userId:String = ""
    var userToken:String =""
    var listJobs: List<Job> = mutableListOf()

    private val _jobsState:MutableLiveData<DataState<AllJobsResponse>> = MutableLiveData()
    val jobState:LiveData<DataState<AllJobsResponse>>
    get() = _jobsState

    private val _favouriteJobsState: MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val favouriteJobState: LiveData<DataState<LoginResponse>>
        get() = _favouriteJobsState

    fun getJobs(jobValue:String){
        viewModelScope.launch {
            repository.getJobsByUrl("Beares $userToken", userId, jobValue)
                .onEach { dataState -> _jobsState.value = dataState }
                .launchIn(viewModelScope)
        }
    }


    fun updateFavourite(job: Job) {
        viewModelScope.launch {
            val params = ApplyJobFavouriteParams(
                request_progress_id = (job.request_progress_id ?: 0).toString(),
                client_job_post_id = (job.client_job_post_id).toString(),
                freelancer_user_id = userId,
                is_applied = "${job.is_applied}",
                is_favourite = if (job.is_favourite == 1) "0" else "1",
                client_user_id = (job.client_user_id ?: 0).toString(),
                job_bid_price = "${job.bid_price}"

            )
            repository.applyFavouriteJob("Bearer $userToken", params)
                .onEach { dataState -> _favouriteJobsState.value = dataState }
                .launchIn(viewModelScope)
        }
    }
}