package com.nssfreelance.ui.maps

import android.location.Location
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nssfreelance.data.preferences.PreferencesManager
import kotlinx.coroutines.launch

class MapsViewmodel @ViewModelInject constructor(
    @Assisted private val stateHandle: SavedStateHandle,
    private val preferences:PreferencesManager
):ViewModel() {
    val preferencesFlow = preferences.preferencesFlow

    fun updateLocation(location:String, locality:String){
        viewModelScope.launch {
            preferences.updateLocation(location = location)
            preferences.updateLocality(locality)
        }
    }
}