package com.nssfreelance.ui.maps

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentMapsBinding
import com.nssfreelance.util.displayError
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*


private const val TAG = "MapsFragment"

@AndroidEntryPoint
class MapsFragment : Fragment(R.layout.fragment_maps), OnMapReadyCallback {

    private var map: GoogleMap? = null
    private var cameraPosition: CameraPosition? = null

    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private val defaultLocation = LatLng(11.1271, 78.6569)
    private var locationPermissionGranted = false
    private var lastKnownLocation: Location? = null

    private var _binding: FragmentMapsBinding? = null
    private val binding: FragmentMapsBinding
        get() = _binding!!

    private val viewmodel: MapsViewmodel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentMapsBinding.bind(view)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(
            requireActivity()
        )

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            btnConfirm.setOnClickListener {
                if (lastKnownLocation != null) {
                    findNavController().popBackStack()
                } else {
                    displayError(requireView(), "Kindly turn on GPS to fetch current location.")
                }
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.preferencesFlow.collect { flow ->
                val location: String = flow.location
                val locality = flow.locality
                val userName = flow.firstname

                if (userName.isNotEmpty()) {
                    binding.tvUsername.text = "Hello! $userName"
                }

                if (location.isNotEmpty() && locality.isNotEmpty()) {
                    val locationStrs = location.split(",")
                    lastKnownLocation = Location(LocationManager.GPS_PROVIDER)
                    lastKnownLocation?.latitude = locationStrs[0].toDouble()
                    lastKnownLocation?.longitude = locationStrs[1].toDouble()

                    moveCamera(locationStrs[0].toDouble(), locationStrs[1].toDouble())

                    binding.tvTitle.text = locality
                } else {
                    getDeviceLocation()
                }
            }
        }

        ActivityCon.activityCon.observe(viewLifecycleOwner) { requestCode ->
            when (requestCode.toInt()) {
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                    Log.d(TAG, "observer: $requestCode")
                    locationPermissionGranted = true
                    getLocationPermission()
                    updateLocationUI()
                    getDeviceLocation()
                    enableLocationSettings()
                }

                REQUEST_CODE_CHECK_SETTINGS -> {
                    Log.d(TAG, "observer $requestCode")
                    locationPermissionGranted = true
                    getLocationPermission()
                    getDeviceLocation()
                    getCurrentCountry(requireContext())
                    updateLocationUI()
                }
            }
        }
    }

    override fun onMapReady(map: GoogleMap?) {
        this.map = map
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // Prompt the user for permission.
            getLocationPermission()
        } else {
            map?.isMyLocationEnabled = true
        }

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI()

        // Get the current location of the device and set the position of the map.
        getDeviceLocation()
    }

    //Getting location
    fun getCurrentCountry(context: Context) {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

        locationRequest = LocationRequest.create()
        locationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest!!.interval = (10 * 1000).toLong() // 10 seconds
        locationRequest!!.fastestInterval = (5 * 1000).toLong() // 5 seconds

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        lastKnownLocation = location
                        Log.d(TAG, "onLocationResult ${location.latitude}, ${location.longitude}")
                        val latLng: LatLng =
                            LatLng(lastKnownLocation!!.latitude, lastKnownLocation!!.longitude)
                        changeCamera(latLng)

                        if (fusedLocationProviderClient != null) {
                            fusedLocationProviderClient!!.removeLocationUpdates(locationCallback!!)
                        }
                    }
                }
            }
        }
    }

    fun changeCamera(latLng: LatLng) {
        with(map) {
            this?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM.toFloat()))
        }
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            moveCamera(
                                lastKnownLocation!!.latitude,
                                lastKnownLocation!!.longitude
                            )
                            val address = getCompleteAddressString(
                                lastKnownLocation!!.latitude,
                                lastKnownLocation!!.longitude
                            )
                            binding.tvTitle.text = "$address"
                            viewmodel.updateLocation(
                                "${lastKnownLocation!!.latitude},${lastKnownLocation!!.longitude}",
                                address ?: ""
                            )
                            Log.d(
                                TAG,
                                "getDeviceLocation: ${lastKnownLocation?.latitude} ${lastKnownLocation?.longitude} : $address"
                            )
                        }
                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.")
                        Log.e(TAG, "Exception: %s", task.exception)
                        moveCamera(defaultLocation.latitude, defaultLocation.longitude)
                        map?.uiSettings?.isMyLocationButtonEnabled = false
                        getLocationPermission()
                        enableLocationSettings()
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    private fun moveCamera(latitude: Double, longitude: Double) {
        map?.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    latitude,
                    longitude
                ), DEFAULT_ZOOM.toFloat()
            )
        )
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
            enableLocationSettings()
            getCurrentCountry(requireContext())
            getDeviceLocation()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private fun updateLocationUI() {
        if (map == null) {
            return
        }
        try {
            if (locationPermissionGranted) {
                map?.isMyLocationEnabled = true
                map?.uiSettings?.isMyLocationButtonEnabled = true
                enableLocationSettings()
                getCurrentCountry(requireContext())
                getDeviceLocation()
            } else {
                map?.isMyLocationEnabled = false
                map?.uiSettings?.isMyLocationButtonEnabled = false
                lastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    private fun getCompleteAddressString(latitude: Double, longitude: Double): String? {
        var strAdd = ""
        val geocoder = Geocoder(requireContext(), Locale.getDefault())
        try {
            val addresses: List<Address>? = geocoder.getFromLocation(latitude, longitude, 1)
            if (addresses != null) {
                val strReturnedAddress = StringBuilder("")
                val cityName = addresses[0].getAddressLine(0)

                strReturnedAddress.append(cityName)
                strAdd = strReturnedAddress.toString()
                Log.w(TAG, strAdd)
            } else {
                Log.w(TAG, "No Address returned!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.w(TAG, "Canont get Address!")
        }
        return strAdd
    }

    protected fun enableLocationSettings() {
        val locationRequest = LocationRequest.create()
            .setInterval(LOCATION_UPDATE_INTERVAL)
            .setFastestInterval(LOCATION_UPDATE_FASTEST_INTERVAL)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
        LocationServices
            .getSettingsClient(requireContext())
            .checkLocationSettings(builder.build())
            .addOnSuccessListener(requireActivity()) { response: LocationSettingsResponse? -> }
            .addOnFailureListener(requireActivity()) { ex ->
                if (ex is ResolvableApiException) {
                    // Location settings are NOT satisfied,  but this can be fixed  by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),  and check the result in onActivityResult().
                        val resolvable = ex as ResolvableApiException
                        resolvable.startResolutionForResult(
                            requireActivity(),
                            REQUEST_CODE_CHECK_SETTINGS
                        )
                    } catch (sendEx: SendIntentException) {
                        Log.e(TAG, "enableLocationSettings: ${sendEx.printStackTrace()}")
                    }
                }
            }
    }

    companion object {
        private const val DEFAULT_ZOOM = 15
        const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
        const val REQUEST_CODE_CHECK_SETTINGS = 2

        // Keys for storing activity state.
        private const val LOCATION_UPDATE_INTERVAL = 10000L
        private const val LOCATION_UPDATE_FASTEST_INTERVAL = 5000L
    }
}