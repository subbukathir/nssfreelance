package com.nssfreelance.ui.settings

import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.request.FcmTokenParams
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.DataState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

private const val TAG = "AccountSettingsViewmode"

class AccountSettingsViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    private val preferences: PreferencesManager
) : ViewModel() {

    var fcmToken: String = ""
    var userId: String = ""
    var userToken: String = ""
    var isTokenUpdated:Boolean = false
    var uid:String =""

    val preferencesFlow = preferences.preferencesFlow
    val userType: MutableLiveData<Int> = MutableLiveData()

    private val _fcmTokenDataState: MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val fcmTokenState: LiveData<DataState<LoginResponse>>
        get() = _fcmTokenDataState

    private val accountEventChannel = Channel<AccountSettingsTask>()
    val accountEventTask = accountEventChannel.receiveAsFlow()

    fun onLogoutClick() {
        viewModelScope.launch {
            accountEventChannel.send(AccountSettingsTask.NavigateToLogoutDialog)
        }
    }

    fun onLogoutSuccessClick() {
        viewModelScope.launch {
            preferences.clearDatastore()
            print("cleared datastore")
            preferences.updateUID(uid)
            //accountEventChannel.send(AccountSettingsTask.NavigateToLogin)
        }
    }

    fun onDarkModeClick(isChecked: Boolean) {
        viewModelScope.launch {
            preferences.updateTheme(if (isChecked) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
        }
    }

    fun sendTokenToServer(token: String) {
        Log.d(TAG, "sendTokenToServer: $token")
        updateFcmToken(token)
        if(!userId.isNullOrEmpty() && !isTokenUpdated){
            val params = FcmTokenParams(
                token = token,
                user_id = userId.toInt(),
            )
            //send FCM token to server
            viewModelScope.launch {
                repository.updateFcmToken(params)
                    .onEach { dataState -> _fcmTokenDataState.value = dataState }
                    .launchIn(viewModelScope)
            }

            isTokenUpdated = true
        }

    }

    fun updateFcmToken(token: String) {
        viewModelScope.launch {
            preferences.updateFcmToken(token)
        }
    }

    fun updateLocation(location:String, locality:String){
        viewModelScope.launch {
            preferences.updateLocation(location = location)
            preferences.updateLocality(locality)
        }
    }

    sealed class AccountSettingsTask {
        object NavigateToLogoutDialog : AccountSettingsTask()
        object NavigateToLogin : AccountSettingsTask()
    }
}