package com.nssfreelance.ui.settings

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.Profile
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.request.UpdateProfileParams
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.retrofit.response.ProfileClientResponse
import com.nssfreelance.retrofit.response.ProfileResponse
import com.nssfreelance.ui.logout.LogoutViewmodel
import com.nssfreelance.util.DataState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

private const val TAG = "SettingsViewmodel"

class SettingsViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    private val preferences: PreferencesManager,
    @Assisted private val state: SavedStateHandle,
) : ViewModel() {

    val preferencesFlow = preferences.preferencesFlow
    var userId = ""
    var userToken = ""
    var isFreelancer = false
    var uid: String = ""
    private var profile: Profile? = null
    var country = "New Zealand"


    private val _profileState: MutableLiveData<DataState<ProfileResponse>> = MutableLiveData()
    val profileFreelancerState: LiveData<DataState<ProfileResponse>>
        get() = _profileState


    private val _updateProfileState: MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val updateProfileState: LiveData<DataState<LoginResponse>>
        get() = _updateProfileState


    private val _profileClienState: MutableLiveData<DataState<ProfileClientResponse>> =
        MutableLiveData()
    val profileClientState: LiveData<DataState<ProfileClientResponse>>
        get() = _profileClienState

    fun getProfile() {
        if (isFreelancer) {
            viewModelScope.launch {
                repository.getProfile(userToken, userId)
                    .onEach { dataState -> _profileState.value = dataState }
                    .launchIn(viewModelScope)
            }
        } else {
            viewModelScope.launch {
                repository.getClientProfile(userToken, userId)
                    .onEach { dataState -> _profileClienState.value = dataState }
                    .launchIn(viewModelScope)
            }
        }
    }

    fun updatePhotoUrl(url: String) {
        viewModelScope.launch {
            preferences.updateProfileUrl(url)
        }
    }

    fun setProfile(data: Profile) {
        this.profile = data
    }

    fun getOfflineProfile(): Profile? {
        return profile
    }

    var firstName = state.get<String>("firstName") ?: ""
        set(value) {
            field = value
            state.set("firstName", value)
        }

    var lastName = state.get<String>("lastName") ?: ""
        set(value) {
            field = value
            state.set("lastName", value)
        }
    var mobileNo = state.get<String>("mobileNo") ?: ""
        set(value) {
            field = value
            state.set("mobileNo", value)
        }

    var email = state.get<String>("email") ?: ""
        set(value) {
            field = value
            state.set("email", value)
        }
    var address = state.get<String>("address") ?: ""
        set(value) {
            field = value
            state.set("address", value)
        }
    var imageBlob = state.get<String>("imageBlob") ?: ""
        set(value) {
            field = value
            state.set("imageBlob", value)
        }
    var aboutMe = state.get<String>("aboutMe") ?: ""
        set(value) {
            field = value
            state.set("aboutMe", value)
        }

    fun updateProfile() {
        if (firstName.isBlank()) {
            showError("FirstName cannot be empty")
            return
        }
        if (lastName.isBlank()) {
            showError("Lastname cannot be empty")
            return
        }
        if (mobileNo.isBlank()) {
            showError("Mobile number cannot be empty")
            return
        }

        /*if (mobileNo.length < 8) {
            showError("Mobile number cannot be less than 8 numbers")
            return
        }*/
        if (email.isBlank()) {
            showError("Email cannot be empty")
            return
        }
        if (imageBlob.isBlank()) {
            showError("Profile picture cannot be empty")
            return
        }

        if(isFreelancer) {
            if(aboutMe.isBlank()){
                aboutMe = "Freelancer"
            }
        }

        if (!isFreelancer) {
            if (aboutMe.isBlank()) {
                showError("About Me cannot be empty")
                return
            }
        }

        val params = UpdateProfileParams(
            uid = uid,
            user_id = userId,
            first_name = firstName,
            last_name = lastName,
            mobileNo,
            email,
            false,
            image = imageBlob,
            profile_photo_link = "",
            about_me = aboutMe
        )

        Log.d(TAG, "updateProfile: $params")

        viewModelScope.launch {
            repository.updateProfile(userToken, params)
                .onEach { dataState -> _updateProfileState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    private fun showError(msg: String) {
        viewModelScope.launch {
            settingsEventChannel.send(SettingsTask.DisplayError(msg))
        }
    }

    private val settingsEventChannel = Channel<SettingsTask>()
    val settingsEventTask = settingsEventChannel.receiveAsFlow()

    fun onLogoutSuccessClick() {
        viewModelScope.launch {
            preferences.clearDatastore()
            print("cleared datastore")
            preferences.updateUID(uid)
            settingsEventChannel.send(SettingsTask.NavigateToRegistration)
        }
    }

    sealed class SettingsTask {
        object NavigateToAccountSettings : SettingsTask()
        object NavigateToRegistration : SettingsTask()
        data class DisplayError(val msg: String) : SettingsTask()
    }

}