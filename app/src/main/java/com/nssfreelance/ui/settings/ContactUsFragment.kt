package com.nssfreelance.ui.settings

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentContactUsBinding
import com.nssfreelance.databinding.FragmentProfileSettingsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ContactUsFragment:Fragment(R.layout.fragment_contact_us) {
    private var _binding: FragmentContactUsBinding?=null
    private val binding: FragmentContactUsBinding
        get() = _binding!!

    private val viewmodel:SettingsViewmodel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentContactUsBinding.bind(view)

        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner
            .lifecycleScope.launchWhenStarted {
                viewmodel.preferencesFlow.collect { prefs ->
                    viewmodel.userId = prefs.userId
                    viewmodel.userToken = "Bearer ${prefs.userToken}"

                    val photoUrl = prefs.profileUrl
                    if (photoUrl != "") {
                        Glide.with(requireContext())
                            .load(photoUrl)
                            .placeholder(R.drawable.ic_placeholder_profile)
                            .into(binding.civProfile)
                    } else {
                        Glide.with(requireContext())
                            .load(R.drawable.ic_placeholder_profile)
                            .into(binding.civProfile)
                    }
                }
            }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}