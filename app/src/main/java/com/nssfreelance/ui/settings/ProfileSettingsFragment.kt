package com.nssfreelance.ui.settings

import android.Manifest
import android.R.attr
import android.R.attr.path
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64.DEFAULT
import android.util.Base64.encodeToString
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import com.nssfreelance.BuildConfig
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentProfileSettingsBinding
import com.nssfreelance.util.*
import com.nssfreelance.util.Const.CAPTURE_IMAGE
import com.nssfreelance.util.Const.CHOOSE_IMAGE
import com.nssfreelance.util.Const.RC_CAMERA_READ_WRITE
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import android.graphics.Bitmap.CompressFormat
import android.graphics.Bitmap.createBitmap
import android.graphics.Canvas

import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toBitmap
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import kotlinx.android.synthetic.main.fragment_profile_settings.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import okhttp3.Dispatcher
import kotlin.math.log
import android.R.attr.resource


private const val TAG = "ProfileSettingsFragment"

@AndroidEntryPoint
class ProfileSettingsFragment : Fragment(R.layout.fragment_profile_settings) {
    private var _binding: FragmentProfileSettingsBinding? = null
    private val binding: FragmentProfileSettingsBinding
        get() = _binding!!

    private val viewmodel: SettingsViewmodel by viewModels()

    private val navArgs: ProfileSettingsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentProfileSettingsBinding.bind(view)

        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            civProfile.setOnClickListener {
                uploadImage()
            }

            tieFirstName.addTextChangedListener {
                viewmodel.firstName = it.toString()
            }

            tieLastName.addTextChangedListener {
                viewmodel.lastName = it.toString()
            }

            tieMobile.addTextChangedListener {
                viewmodel.mobileNo = it.toString()
            }

            tieMobile.addTextChangedListener {
                viewmodel.mobileNo = it.toString()
            }

            tieEmail.addTextChangedListener {
                viewmodel.email = it.toString()
            }
            tieAboutMe.addTextChangedListener {
                viewmodel.aboutMe = it.toString()
            }

            tieAddress.addTextChangedListener {
                viewmodel.address = it.toString()
            }
            btnSaveProfile.setOnClickListener {
                Log.d(TAG, "onViewCreated: btn click")
                viewmodel.updateProfile()
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner
            .lifecycleScope.launchWhenStarted {
                viewmodel.preferencesFlow.collect { prefs ->
                    viewmodel.userId = prefs.userId
                    viewmodel.userToken = "Bearer ${prefs.userToken}"
                    viewmodel.isFreelancer = navArgs.isFreelancer
                    viewmodel.uid = prefs.uid
                    binding.tieAddress.setText(prefs.locality)


                    val photoUrl = prefs.profileUrl
                    if (photoUrl != "") {
                        Glide.with(requireContext())
                            .load(photoUrl)
                            .placeholder(R.drawable.ic_placeholder_profile)
                            .into(binding.civProfile)
                    } else {
                        Glide.with(requireContext())
                            .load(R.drawable.ic_placeholder_profile)
                            .into(binding.civProfile)
                    }
                }
            }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            Log.d(TAG, "subscribeObservers: ")
            val profileData = navArgs.profileData
            profileData?.let { profile ->
                Log.d(TAG, "onViewCreated: ${profile.full_name}")
                profile.full_name?.let {
                    val splits = it.split(" ")
                    if (splits.size > 1) {
                        binding.tieFirstName.setText(splits[0])
                        binding.tieLastName.setText(splits[1])
                    }
                }
                viewmodel.country = profile.country_name ?: "New Zealand"
                profile.first_name?.let {
                    binding.tieFirstName.setText(it)
                }
                profile.last_name?.let {
                    binding.tieLastName.setText(it)
                }
                profile.address?.let {
                    binding.tieAddress.setText(it)
                }
                binding.tieEmail.setText(profile.email_address)

                profile.mobile_no?.let {
                    binding.tieMobile.setText(it)
                }

                profile.about_me?.let {
                    binding.tieAboutMe.setText(profile.about_me ?: "")
                }

                /*if (viewmodel.isFreelancer) {
                    binding.tilAboutMe.hide()
                } else {
                    binding.tilAboutMe.show()
                    binding.tieAboutMe.setText(profile.about_me ?: "")
                }*/
            }

        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.settingsEventTask.collect { event ->
                when (event) {
                    is SettingsViewmodel.SettingsTask.DisplayError -> {
                        displayError(requireView(), event.msg)
                    }
                    SettingsViewmodel.SettingsTask.NavigateToAccountSettings -> TODO()
                    SettingsViewmodel.SettingsTask.NavigateToRegistration -> TODO()
                }.exhaustive
            }
        }

        viewmodel.updateProfileState.observe(viewLifecycleOwner, { data ->
            when (data) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (data.data.status) {
                        displayError(requireView(), "Profile data updated successfully")
                        viewmodel.getProfile()
                    } else displayError(
                        requireView(),
                        "Profile data update: ${data.data.message}"
                    )

                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), data.exception.message)
                }
                DataState.Loading -> {
                    Log.d(TAG, "subscribeObservers: show progressbar")
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }

    //image upload process
    /**--------------------------------------------
     * Capture images for dispached
     *
     * ---------------------------------------------- */
    var permissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    var currentPhotoPath: String = ""
    var mImageUrl: String = ""
    fun uploadImage() {
        Log.d(TAG, "uploadImage")
        if (EasyPermissions.hasPermissions(requireContext(), *permissions)) {
            chooseCaptureImage()
        } else {

            EasyPermissions.requestPermissions(
                this,
                "Need permission of camera and read / write to access data",
                RC_CAMERA_READ_WRITE,
                *permissions
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    @AfterPermissionGranted(RC_CAMERA_READ_WRITE)
    private fun methodForBothPermission() {
        Log.d(TAG, "methodForBothPermission")
        if (EasyPermissions.hasPermissions(requireContext(), *permissions)) {
            chooseCaptureImage()
        } else {
            EasyPermissions.requestPermissions(
                this,
                "Need permission of camera and read / write to access data",
                RC_CAMERA_READ_WRITE,
                *permissions
            )
        }
    }

    //pick or capture image from phone
    private fun chooseCaptureImage() {
        val items =
            arrayOf("Capture Image", "Choose from gallery", "Cancel")
        val builder =
            AlertDialog.Builder(requireContext())
        builder.setItems(items) { dialog, which ->
            if (items[which].equals("Capture image", ignoreCase = true)) {
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePictureIntent.resolveActivity(requireContext().packageManager) != null) {
                    var photoFile: File? = null
                    try {
                        photoFile = createImageFile()
                    } catch (ex: IOException) { // Error occurred while creating the File
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        val photoURI =
                            FileProvider.getUriForFile(
                                requireContext(),
                                BuildConfig.APPLICATION_ID + ".fileprovider",
                                photoFile
                            )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(
                            takePictureIntent,
                            CAPTURE_IMAGE
                        )
                    }
                }
            } else if (items[which].equals("choose from gallery", ignoreCase = true)) {
                val chooseImage = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )
                startActivityForResult(
                    chooseImage,
                    CHOOSE_IMAGE
                )
            } else {
                dialog.cancel()
            }
        }
        builder.show()
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        Log.d(TAG, "onActivityResult: $requestCode resultCode : $resultCode")
        when (requestCode) {
            CAPTURE_IMAGE -> if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG, "onActivity result $currentPhotoPath")
                if (currentPhotoPath != null && currentPhotoPath != "") {
                    val bitmap = BitmapFactory.decodeFile(currentPhotoPath)
                    val bytes = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, bytes)

                    Glide.with(this).load(bitmap)
                        .into(binding.civProfile)

                    uploadImageToCloud()
                }
            }
            CHOOSE_IMAGE -> if (resultCode == Activity.RESULT_OK && data != null) {
                Log.d(TAG, "onActivityResult: $data - resultCode : $resultCode")
                val pickedImageUri = data.data
                val filePathColumn = arrayOf(
                    MediaStore.Images.Media.DATA
                )
                // Get the cursor
                val cursor = requireContext().contentResolver.query(
                    pickedImageUri!!,
                    filePathColumn, null, null, null
                )
                // Move to first row
                cursor!!.moveToFirst()
                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                currentPhotoPath = cursor.getString(columnIndex)
                cursor.close()
                Log.d(TAG, "path : $currentPhotoPath")

                uploadImageToCloud()
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File? { // Create an image file name
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "purchaseDispatch_" + timeStamp + "_"
        val storageDir =
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",  /* suffix */
            storageDir /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.absolutePath
        return image
    }

    @SuppressLint("NewApi")
    private fun uploadImageToCloud() {
        Log.d(TAG, "uploadImageToCloud: $currentPhotoPath")
        var bitmap: Bitmap = BitmapFactory.decodeResource(
            context?.getResources(),
            R.drawable.ic_nss_logo
        )
        Glide.with(requireContext())
            .load(currentPhotoPath)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    Log.d(TAG, "onLoadFailed: ")
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    Log.d(TAG, "onResourceReady: success")
                    try {
                        bitmap = (resource as BitmapDrawable).toBitmap()
                        val bos = ByteArrayOutputStream()
                        bitmap.compress(CompressFormat.PNG, 100, bos)
                        val bb = bos.toByteArray()
                        viewmodel.imageBlob = java.util.Base64.getEncoder().encodeToString(bb)
                        Log.d(TAG, "string:${viewmodel.imageBlob}")

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    return false
                }

            })
            .into(binding.civProfile)


        //new formate from imageview
        try {
            val bos = ByteArrayOutputStream()
            bitmap.compress(CompressFormat.PNG, 100, bos)
            val bb = bos.toByteArray()
            viewmodel.imageBlob = java.util.Base64.getEncoder().encodeToString(bb)
            Log.d(TAG, "string:${viewmodel.imageBlob}")

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}