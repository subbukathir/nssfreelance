package com.nssfreelance.ui.settings

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.play.core.review.ReviewInfo
import com.google.android.play.core.review.ReviewManager
import com.google.android.play.core.review.ReviewManagerFactory
import com.nssfreelance.BuildConfig
import com.nssfreelance.R
import com.nssfreelance.data.Profile
import com.nssfreelance.databinding.FragmentSettingsBinding
import com.nssfreelance.retrofit.FreelanceService.Companion.BASE_URL
import com.nssfreelance.retrofit.response.ProfileResponse
import com.nssfreelance.util.*
import com.nssfreelance.util.Const.CONTACT_US
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "SettingsFragment"

@AndroidEntryPoint
class SettingsFragment : Fragment(R.layout.fragment_settings) {
    private var _binding: FragmentSettingsBinding? = null
    private val binding: FragmentSettingsBinding
        get() = _binding!!

    private lateinit var manager: ReviewManager
    private lateinit var reviewInfo: ReviewInfo

    private val viewmodel: SettingsViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentSettingsBinding.bind(view)

        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            llAccountSettings.setOnClickListener {
                val action =
                    SettingsFragmentDirections.actionSettingsFragmentToAccountSettingsFragment()
                findNavController().navigate(action)
            }

            llProfileSettings.setOnClickListener {
                val action =
                    SettingsFragmentDirections.actionSettingsFragmentToProfileSettingsFragment(viewmodel.getOfflineProfile(),viewmodel.isFreelancer)
                findNavController().navigate(action)
            }

            llContactUs.setOnClickListener {
                openChromeTabs(CONTACT_US, requireContext())
            }

            llPayments.setOnClickListener {
                val action = SettingsFragmentDirections.actionSettingsFragmentToPaymentFragment()
                findNavController().navigate(action)
            }

            llSwitchClient.setOnClickListener {
                viewmodel.onLogoutSuccessClick()
            }

            llAboutNss.setOnClickListener {
                openChromeTabs(Const.ABOUT_US, requireContext())
            }

            llRateUs.setOnClickListener {
                manager = ReviewManagerFactory.create(requireContext())
                val request = manager.requestReviewFlow()
                request.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        reviewInfo = task.result
                        if (reviewInfo != null)
                            launchReviewBox()
                    } else {
                        // There was some problem, log or handle the error code.
                        val reviewErrorCode = (task.exception as Exception).message
                        Log.d(TAG, "onViewCreated: $reviewErrorCode")
                    }
                }

            }

        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.preferencesFlow.collect { prefs ->
                viewmodel.userId = prefs.userId
                viewmodel.userToken = "Bearer ${prefs.userToken}"
                viewmodel.isFreelancer = prefs.userType == 2
                viewmodel.uid = if (prefs.uid != "") prefs.uid else viewmodel.uid

                Log.d(TAG, "subscribeObservers: ${prefs.userType}")

                binding.apply {
                    tvUsername.text = prefs.firstname
                    tvUserAddress.text = prefs.locality

                    /*if (viewmodel.isFreelancer) {
                        tvSwitchToClient.text = "Switch to client side"
                    } else {
                        tvSwitchToClient.text = "Switch to freelancer side"
                    }*/
                }

                viewmodel.getProfile()
            }
        }

        viewmodel.profileFreelancerState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        updateFreelancerProfileView(dataState.data.data.profile)
                    } else {
                        Log.d(TAG, "subscribeObservers: freelancer ${dataState.data.message}")
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    Log.d(TAG, "subscribeObservers error : freelancer ${dataState.exception.message}")
                    if (!viewmodel.isFreelancer) {
                    } else
                        displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.profileClientState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.data != null) {
                        updateFreelancerProfileView(dataState.data.data)
                    } else {
                        displayError(requireView(), "Profile data not found")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    Log.d(TAG, "subscribeObservers error : client ${dataState.exception.message}")
                    if (viewmodel.isFreelancer) {
                    } else
                    {
                        if(dataState.exception.message?.contains("502 Bad Gateway")==true) {
                            displayError(requireView(), "Network error")
                        }else
                        displayError(requireView(), dataState.exception.message)
                    }
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.settingsEventTask.collect { event ->
                when (event) {
                    SettingsViewmodel.SettingsTask.NavigateToAccountSettings -> TODO()
                    SettingsViewmodel.SettingsTask.NavigateToRegistration -> {
                        val action =
                            SettingsFragmentDirections.actionSettingsFragmentToRegisterLanding()
                        findNavController().navigate(action)
                    }
                    is SettingsViewmodel.SettingsTask.DisplayError -> {
                        Log.d(TAG, "subscribeObservers owner : ${event.msg}")
                        displayError(requireView(), event.msg)
                    }
                }.exhaustive
            }
        }
    }

    private fun updateFreelancerProfileView(data: Profile) {
        viewmodel.setProfile(data)
        binding.apply {
            if (viewmodel.isFreelancer)
                tvUsername.text = data.name
            else tvUsername.text = data.full_name
            tvUserAddress.text =
                "${data.city_name}" + if (data.country_name != null) " | ${data.country_name}" else ""

            val photoUrl = data.profile_photo_link ?: ""

            viewmodel.updatePhotoUrl(photoUrl)
            if (photoUrl != "") {
                Glide.with(requireContext())
                    .load(photoUrl)
                    .placeholder(R.drawable.ic_placeholder_profile)
                    .into(civProfile)
            } else {
                Glide.with(requireContext())
                    .load(R.drawable.ic_placeholder_profile)
                    .into(civProfile)
            }
        }
    }

    private fun launchReviewBox() {
        val flow = manager.launchReviewFlow(requireActivity(), reviewInfo)
        flow.addOnCompleteListener { _ ->
            // The flow has finished. The API does not indicate whether the user
            // reviewed or not, or even whether the review dialog was shown. Thus, no
            // matter the result, we continue our app flow.
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}