package com.nssfreelance.ui.settings

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentAccountSettingsBinding
import com.nssfreelance.util.Const
import com.nssfreelance.util.exhaustive
import com.nssfreelance.util.openChromeTabs
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "AccountSettingsFragment"
@AndroidEntryPoint
class AccountSettingsFragment:Fragment(R.layout.fragment_account_settings) {
    private var _binding: FragmentAccountSettingsBinding?=null
    private val binding: FragmentAccountSettingsBinding
        get() = _binding!!

    private val viewmodel:AccountSettingsViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentAccountSettingsBinding.bind(view)

        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            tvLogout.setOnClickListener {
                viewmodel.onLogoutClick()
            }

            tvDarkMode.setOnCheckedChangeListener { _, checked ->
                viewmodel.onDarkModeClick(checked)
            }

            tvTermsAndConditions.setOnClickListener {
                openChromeTabs(Const.TERMS,requireContext())
            }

        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.accountEventTask.collect { event ->
                when(event){
                    AccountSettingsViewmodel.AccountSettingsTask.NavigateToLogoutDialog -> {
                        val action = AccountSettingsFragmentDirections.actionAccountSettingsFragmentToLogoutDialogFragment2()
                        findNavController().navigate(action)
                    }
                    AccountSettingsViewmodel.AccountSettingsTask.NavigateToLogin -> {
                        Log.d(TAG, "subscribeObservers: AccountSettingsTask.NavigateToLogin")
                        try {
                            /*val action =
                                LogoutDialogFragmentDirections.actionLogoutDialogFragmentToRegisterLanding()
                            findNavController().navigate(action)*/
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }.exhaustive
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {

            viewmodel.preferencesFlow.collect { prefs->
                Log.d(TAG, "subscribeObservers: ${prefs.isDarkMode == AppCompatDelegate.MODE_NIGHT_YES}" )
                binding.tvDarkMode.isChecked = prefs.isDarkMode == AppCompatDelegate.MODE_NIGHT_YES

                val photoUrl = prefs.profileUrl
                viewmodel.uid = if(prefs.uid!="") prefs.uid else viewmodel.uid

                if (photoUrl != "") {
                    Glide.with(requireContext())
                        .load(photoUrl)
                        .placeholder(R.drawable.ic_placeholder_profile)
                        .into(binding.civProfile)
                } else {
                    Glide.with(requireContext())
                        .load(R.drawable.ic_placeholder_profile)
                        .into(binding.civProfile)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}