package com.nssfreelance.ui.logout

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nssfreelance.data.preferences.PreferencesManager
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class LogoutViewmodel @ViewModelInject constructor(
    private val preferences:PreferencesManager
):ViewModel() {

    fun onLogoutClick(){
        viewModelScope.launch {
            preferences.clearDatastore()
            print("cleared datastore")
            logoutEventChannel.send(LogoutTask.NavigateToLogin)
        }
    }

    private val logoutEventChannel = Channel<LogoutTask>()
    val logoutEventTask = logoutEventChannel.receiveAsFlow()

    sealed class LogoutTask{
        object NavigateToLogin:LogoutTask()
    }
}