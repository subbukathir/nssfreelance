package com.nssfreelance.ui.logout

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentLogoutDialogBinding
import com.nssfreelance.ui.settings.AccountSettingsViewmodel
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "LogoutDialogFragment"
@AndroidEntryPoint
class LogoutDialogFragment : DialogFragment() {
    private lateinit var binding: FragmentLogoutDialogBinding
    private val viewmodel: AccountSettingsViewmodel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        Log.d(TAG, "Oncreate")

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater;
            // Inflate and set the layout for the dialog

            // Pass null as the parent view because its going in the dialog layout
            binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_logout_dialog, null, false
            )
            setProperties(savedInstanceState)
            builder.setView(binding.root)
            builder.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        binding = FragmentLogoutDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")
        setProperties(savedInstanceState)
    }

    private fun setProperties(savedInstanceState: Bundle?) {
        Log.d(TAG, "setProperties")
        val margin: Int = requireContext().resources.getDimension(R.dimen.dp_20).toInt()
        val back: ColorDrawable = ColorDrawable(Color.TRANSPARENT)
        val inset: InsetDrawable = InsetDrawable(back, margin)
        dialog?.window?.setBackgroundDrawable(inset)

        binding.apply {
            btnCancel.setOnClickListener {
                dialog?.dismiss()
            }
            btnYes.setOnClickListener {
                viewmodel.onLogoutSuccessClick()
                moveToRegistration()
            }
        }

    }

    private fun moveToRegistration() {
        Log.d(TAG, "moveToRegistration: ")
        try {
            findNavController().navigate(R.id.action_logoutDialogFragment_to_registerLanding)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}