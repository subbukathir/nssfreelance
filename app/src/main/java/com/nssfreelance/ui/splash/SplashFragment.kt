package com.nssfreelance.ui.splash

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.nssfreelance.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay

@AndroidEntryPoint
class SplashFragment:Fragment(R.layout.fragment_splash) {

    private val viewmodel:SplashViewmodel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.hide()

        lifecycleScope.launchWhenStarted {
            delay(3000L)

            viewmodel.preferencesFlow.asLiveData().observe(viewLifecycleOwner, {
                var action:NavDirections? = null
                if(it.userToken.isNotEmpty() && it.isSignInCompleted && it.userId.isNotEmpty()){
                    if(it.userType==2)
                    action = SplashFragmentDirections.actionSplashFragmentToHomeFragment()
                    else
                        action = SplashFragmentDirections.actionSplashFragmentToHomeClientFragment()
                }else{
                    action = SplashFragmentDirections.actionSplashFragmentToOnBoardingFragment()
                }
                    findNavController().navigate(action)
            })

        }
    }
}