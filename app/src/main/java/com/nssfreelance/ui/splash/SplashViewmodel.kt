package com.nssfreelance.ui.splash

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.nssfreelance.data.preferences.PreferencesManager

class SplashViewmodel @ViewModelInject constructor(
    private val preferences:PreferencesManager
):ViewModel() {
    val preferencesFlow = preferences.preferencesFlow
}