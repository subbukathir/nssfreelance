package com.nssfreelance.ui.home_client

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.HomeClientJob
import com.nssfreelance.data.Profile
import com.nssfreelance.databinding.FragmentHomeClientBinding
import com.nssfreelance.ui.MainActivity
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "HomeClientFragment"

@AndroidEntryPoint
class HomeClientFragment : Fragment(R.layout.fragment_home_client),
    ClientPostedJobsAdapter.OnItemClickListener {

    private var _binding: FragmentHomeClientBinding? = null
    private val binding: FragmentHomeClientBinding
        get() = _binding!!
    private lateinit var mAdapter: ClientPostedJobsAdapter
    private val viewmodel: HomeClientViewmodel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentHomeClientBinding.bind(view)

        mAdapter = ClientPostedJobsAdapter(this)

        binding.apply {
            recyclerviewHomeJobs.layoutManager = LinearLayoutManager(requireContext())
            recyclerviewHomeJobs.itemAnimator = DefaultItemAnimator()

            recyclerviewHomeJobs.adapter = mAdapter

            fabPostJob.setOnClickListener {
                Log.d(TAG, "onViewCreated: on btn click")
                viewmodel.onClickOfPostJob()
            }

            tvTitle.setOnClickListener {
                (activity as MainActivity).requestLocation()
            }
            imageviewLocation.setOnClickListener {
                (activity as MainActivity).requestLocation()
            }

            swipeRefresh.setOnRefreshListener {
                viewmodel.getProfileAndJobsData()
            }
        }
        subscribeObservers()
        //updateFcmToken
        (requireActivity() as MainActivity).fetchFCMToken()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.preferencesFlow.collect { flow ->
                viewmodel.userId = flow.userId
                viewmodel.userToken = "Bearer ${flow.userToken}"

                val locality = flow.locality
                if (locality.isNotEmpty()) {
                    Log.d(TAG, "subscribeObservers: $locality")
                    binding.tvTitle.text = locality
                } else {
                    Log.d(TAG, "Address not found: $locality")
                    //viewmodel.onLocationClick()
                    (activity as MainActivity).requestLocation()
                }

                viewmodel.getProfileAndJobsData()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.homeTaskEvent.collect() { event ->
                when (event) {
                    HomeClientViewmodel.HomeClientTask.NavigateToCreateJob -> {
                        Log.d(
                            TAG,
                            "subscribeObservers: navigate job"
                        )
                        val action =
                            HomeClientFragmentDirections.actionHomeClientFragmentToPostJobSelectCategoryFragment()
                        findNavController().navigate(action)
                    }
                    HomeClientViewmodel.HomeClientTask.NavigateToMaps -> {
                        Log.d(
                            TAG,
                            "subscribeObservers: navigate map"

                        )
                        val action =
                            HomeClientFragmentDirections.actionHomeClientFragmentToMapsFragment()
                        findNavController().navigate(action)
                    }
                    is HomeClientViewmodel.HomeClientTask.NavigateToApplication -> {
                        val action =
                            HomeClientFragmentDirections.actionHomeClientFragmentToJobDetailsClientTabFragment(
                                event.jobId,
                                event.jobType
                            )
                        findNavController().navigate(action)
                    }
                }.exhaustive
            }
        }

        viewmodel.profileState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.data.data != null) {
                        updateProfileView(dataState.data.data)
                        binding.cardviewProfileHome.visibility = View.VISIBLE
                    } else {
                        displayError(requireView(), "Profile data not found")
                    }
                }
                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.viewProgress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.homeClientJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    if (dataState.data.status) {
                        if (dataState.data.data.isNotEmpty()) {
                            updateList(dataState.data.data)
                            binding.error.tvError.hide()
                        } else binding.error.tvError.show()
                    } else {
                        Log.d(TAG, "subscribeObservers: home Jobs empty list")
                    }
                }

                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.viewProgress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.viewProgress.progressBar, true)
                }
            }.exhaustive
        })
    }

    private fun updateProfileView(data: Profile) {
        binding.apply {
            tvProfileName.text = data.full_name
            tvProfileLocation.text = "${data.city_name ?: ""} | ${data.country_name ?: ""}"

            tvUsername.text = data.full_name

            viewmodel.updateProfile(data)

            tvProfileRating.text = "${data.rating_count}"
            tvJoinedYrs.text = "Joined on " + commonDateFormate(date = "${data.joined_date}")

            if (data.rating_count == 0) llRating.hide()
            else {
                llRating.show()
                ratingBar.rating = data.rating_count?.toFloat()!!
            }

            Glide.with(requireContext())
                .load(data.profile_photo_link)
                .placeholder(R.drawable.ic_placeholder_profile)
                .into(civProfile)
        }
    }

    private fun navigateToMapsScreen() {
        val action = HomeClientFragmentDirections.actionHomeClientFragmentToMapsFragment()
        findNavController().navigate(action)
    }

    private fun updateList(data: List<HomeClientJob>) {
        if (data.isNotEmpty()) {
            mAdapter.submitList(data)
            binding.error.tvError.hide()
            binding.recyclerviewHomeJobs.show()
        } else {
            binding.error.tvError.show()
            binding.recyclerviewHomeJobs.hide()
        }
    }

    override fun onItemClick(job: HomeClientJob) {
        Log.d(TAG, "onItemClick: ${job.job_title}")
        viewmodel.onJobClicked(job.client_job_post_id, job.job_type)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}