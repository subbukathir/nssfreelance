package com.nssfreelance.ui.home_client

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nssfreelance.R
import com.nssfreelance.data.HomeClientJob
import com.nssfreelance.databinding.ItemClientHomeJobsBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency

class ClientPostedJobsAdapter(private val listener:OnItemClickListener):ListAdapter<HomeClientJob, ClientPostedJobsAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding = ItemClientHomeJobsBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem =getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding:ItemClientHomeJobsBinding):RecyclerView.ViewHolder(binding.root){
        init {
            binding.apply {
                root.setOnClickListener{
                    listener.onItemClick(getItem(adapterPosition))
                }
            }
        }
        fun bind(job:HomeClientJob){
            binding.apply {

                tvItemPostTitle.text = "${job.job_title}"
                tvItemPostLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemPostPaymentType.text = "Payment ${convertToCurrency(job.job_price?:"0",tvItemPostTitle.context)} | ${job.job_type}"
                tvItemApplied.text = "${job.applied_count}"
                tvItemRejected.text = "${job.rejected_count}"
                tvItemPostedTime.text = "Posted on "+ commonDateFormate(job?.job_post_created_on)
                tvItemShortlisted.text ="${job.shortlisted_count}"

                if(job.is_payment_settled){
                    tvItemPostPaymentStatus.text ="Paid"
                    tvItemPostPaymentStatus.background = ResourcesCompat.getDrawable(tvItemApplied.context.resources,
                        R.drawable.bg_green_status,null)
                }else{
                    tvItemPostPaymentStatus.text ="Unpaid"
                    tvItemPostPaymentStatus.background = ResourcesCompat.getDrawable(tvItemApplied.context.resources,
                        R.drawable.bg_red_status,null)
                }
            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(job: HomeClientJob)
    }

    class DiffUtilCallback: DiffUtil.ItemCallback<HomeClientJob>(){
        override fun areItemsTheSame(oldItem: HomeClientJob, newItem: HomeClientJob): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: HomeClientJob, newItem: HomeClientJob): Boolean {
            return oldItem == newItem
        }
    }
}