package com.nssfreelance.ui.home_client

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nssfreelance.data.Profile
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.response.*
import com.nssfreelance.util.Const
import com.nssfreelance.util.Const.POSTED_JOBS
import com.nssfreelance.util.DataState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

private const val TAG = "HomeClientViewmodel"

class HomeClientViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    private val preferences: PreferencesManager
) : ViewModel() {

    val preferencesFlow = preferences.preferencesFlow
    var userId = ""
    var userToken = ""

    private val _profileState: MutableLiveData<DataState<ProfileClientResponse>> = MutableLiveData()
    val profileState: LiveData<DataState<ProfileClientResponse>>
        get() = _profileState

    private val _homeClientJobsState: MutableLiveData<DataState<HomeClientJobResponse>> =
        MutableLiveData()
    val homeClientJobState: LiveData<DataState<HomeClientJobResponse>>
        get() = _homeClientJobsState

    fun getProfileAndJobsData() {
        Log.d(TAG, "getProfileAndJobsData: $userId - $userToken")
        viewModelScope.launch {
            repository.getClientProfile(token = userToken, userId)
                .onEach { dataState -> _profileState.value = dataState }
                .launchIn(viewModelScope)
        }

        viewModelScope.launch {
            repository.getHomeClientJobs(userToken, userId, POSTED_JOBS)
                .onEach { dataState -> _homeClientJobsState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun updateProfile(data: Profile) {
        viewModelScope.launch {
            preferences.updateUsername(data.full_name)
            preferences.updateProfileUrl(data.profile_photo_link ?: "")
            preferences.updateIsPaid(data.is_paid==1)
        }
    }

    private val _homeTaskEventChannel = Channel<HomeClientTask>()
    val homeTaskEvent = _homeTaskEventChannel.receiveAsFlow()

    fun onClickOfPostJob() {
        viewModelScope.launch {
            _homeTaskEventChannel.send(HomeClientTask.NavigateToCreateJob)
        }
    }

    fun onLocationClick() {
        viewModelScope.launch {
            _homeTaskEventChannel.send(HomeClientTask.NavigateToMaps)
        }
    }

    fun onJobClicked(jobId: Int, jobType:String) {
        viewModelScope.launch {
            _homeTaskEventChannel.send(HomeClientTask.NavigateToApplication(jobId, jobType))
        }
    }

    sealed class HomeClientTask {
        object NavigateToCreateJob : HomeClientTask()
        object NavigateToMaps : HomeClientTask()
        data class NavigateToApplication(val jobId: Int, val jobType: String) : HomeClientTask()
    }
}