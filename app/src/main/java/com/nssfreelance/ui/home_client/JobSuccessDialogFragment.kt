package com.nssfreelance.ui.home_client

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentPostJobSuccessBinding
import com.nssfreelance.ui.post_new_job.PostNewJobViewmodel

private const val TAG = "JobSuccessDialogFragmen"
class JobSuccessDialogFragment : DialogFragment() {
    private lateinit var binding: FragmentPostJobSuccessBinding

    private var _currentPosition = 0

    private val viewmodel: PostNewJobViewmodel by activityViewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "Oncreate")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        Log.d(TAG, "Oncreate")

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater;
            // Inflate and set the layout for the dialog

            // Pass null as the parent view because its going in the dialog layout
            binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_post_job_success, null, false
            )
            setProperties(savedInstanceState)
            builder.setView(binding.root)
            builder.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        binding = FragmentPostJobSuccessBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")
        setProperties(savedInstanceState)
    }

    private fun setProperties(savedInstanceState: Bundle?) {
        Log.d(TAG, "setProperties")
        val margin: Int = requireContext().resources.getDimension(R.dimen.dp_20).toInt()
        val back: ColorDrawable = ColorDrawable(Color.TRANSPARENT);
        val inset: InsetDrawable = InsetDrawable(back, margin);
        dialog?.window?.setBackgroundDrawable(inset)

        if (savedInstanceState != null) {
            _currentPosition = savedInstanceState.getInt("position")
        }

        binding.apply {
            btnHome.setOnClickListener {
                Log.d(TAG, "setProperties: home btn click")
                viewmodel.clearInputDetails()
                val action = JobSuccessDialogFragmentDirections.actionJobSuccessDialogFragmentToHomeClientFragment()
                findNavController().navigate(action)
            }

            btnViewPost.setOnClickListener {
                Log.d(TAG, "setProperties: viewpost btn click")
                val action =
                    JobSuccessDialogFragmentDirections.actionJobSuccessDialogFragmentToViewPostedJobFragment()
                findNavController().navigate(action)
            }
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("position", _currentPosition)
    }
}