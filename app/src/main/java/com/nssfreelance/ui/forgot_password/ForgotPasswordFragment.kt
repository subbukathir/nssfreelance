package com.nssfreelance.ui.forgot_password

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentForgotPasswordBinding
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.DataState
import com.nssfreelance.util.exhaustive
import com.nssfreelance.util.hideKeyboard
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ForgotPasswordFragment:Fragment(R.layout.fragment_forgot_password) {
    private var _binding:FragmentForgotPasswordBinding?=null
    private val binding
        get() = _binding!!

    private val viewmodel:ForgotPasswordViewmodel by viewModels()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentForgotPasswordBinding.bind(view)

        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            etEmailId.addTextChangedListener {
                viewmodel.emailId = it.toString()
            }

            btnForgotPassword.setOnClickListener {
                viewmodel.onSubmitClick()
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewmodel.forgotPwdState.observe(viewLifecycleOwner, Observer { dataState ->
            when (dataState) {
                is DataState.Success<LoginResponse> -> {
                    displayProgressbar(false)
                    if (dataState.data.status) {
                        displayError(dataState.data.message)
                    } else {
                        displayError(dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(dataState.exception.message)
                }
                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }
        })

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.preferencesFlow.collect { prefs ->
                viewmodel.uid = prefs.uid
                viewmodel.userToken = "Bearer ${prefs.userToken}"
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.forgotPwdEvent.collect { event ->
                when(event){
                    is ForgotPasswordViewmodel.ForgotPwdTaskEvent.ShowInvalidMsg -> {
                        displayError(event.msg)
                    }
                }.exhaustive
            }
        }

    }


    private fun displayError(message: String?) {
        hideKeyboard(requireActivity())
        if (message != null) {
            Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
        } else {
            Snackbar.make(requireView(), "Unknown error", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun displayProgressbar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}