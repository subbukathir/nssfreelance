package com.nssfreelance.ui.forgot_password

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.DataState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class ForgotPasswordViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val state:SavedStateHandle,
    private val preferences:PreferencesManager
):ViewModel() {

    val preferencesFlow = preferences.preferencesFlow
    var uid:String=""
    var userToken: String =""

    private val _forgotPwdState:MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val forgotPwdState:LiveData<DataState<LoginResponse>>
    get() = _forgotPwdState

    var emailId = state.get<String>("emailId") ?:""
    set(value) {
        field = value
        state.set("emailId", value)
    }

    private val _forgotPwdChannel = Channel<ForgotPwdTaskEvent>()
    val forgotPwdEvent = _forgotPwdChannel.receiveAsFlow()

    fun onSubmitClick(){
        if(emailId.isNullOrBlank()){
            showInvalidMessage("Email ID should not be empty")
            return
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches()) {
            showInvalidMessage("Email ID should be valid")
            return
        }

        val params = HashMap<String, String>()
        params["email"] = emailId

        viewModelScope.launch {
            repository.forgotPassword(params)
                .onEach { dataState -> _forgotPwdState.value = dataState }
                .launchIn(viewModelScope)
        }

    }

    private fun showInvalidMessage(message:String) = viewModelScope.launch {
        _forgotPwdChannel.send(ForgotPwdTaskEvent.ShowInvalidMsg(message))
    }

    sealed class ForgotPwdTaskEvent{
        data class ShowInvalidMsg(val msg: String) : ForgotPwdTaskEvent()
    }

}