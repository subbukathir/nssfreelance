package com.nssfreelance.ui.post_new_job.post_success

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentPostJobSuccessBinding
import com.nssfreelance.ui.home_client.JobSuccessDialogFragmentDirections
import com.nssfreelance.ui.post_new_job.PostNewJobViewmodel
import com.nssfreelance.util.displayError
import com.nssfreelance.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "SuccessJobPostFragment"

@AndroidEntryPoint
class SuccessJobPostFragment : DialogFragment() {
    private var _binding: FragmentPostJobSuccessBinding? = null
    private val binding: FragmentPostJobSuccessBinding
        get() = _binding!!

    private val viewmodel: PostNewJobViewmodel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        Log.d(TAG, "onCreateDialog")

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater;
            // Inflate and set the layout for the dialog

            // Pass null as the parent view because its going in the dialog layout
            _binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_post_job_success, null, false)
            setProperties(savedInstanceState)
            builder.setView(_binding?.root)
            builder.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        _binding = FragmentPostJobSuccessBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setProperties(savedInstanceState)
    }

    private fun setProperties(savedInstanceState: Bundle?) {
        Log.d(TAG, "setProperties: ")
        val margin:Int = requireContext().resources.getDimension(R.dimen.dp_20).toInt()
        val back: ColorDrawable = ColorDrawable(Color.TRANSPARENT)
        val inset: InsetDrawable = InsetDrawable(back, margin)
        dialog?.window?.setBackgroundDrawable(inset)

        binding.apply {
            btnHome.setOnClickListener {
                Log.d(TAG, "setProperties: home btn click")
                viewmodel.clearInputDetails()
                val action = JobSuccessDialogFragmentDirections.actionJobSuccessDialogFragmentToHomeClientFragment()
                findNavController().navigate(action)
            }

            btnViewPost.setOnClickListener {
                Log.d(TAG, "setProperties: viewpost btn click")
                val action =
                    JobSuccessDialogFragmentDirections.actionJobSuccessDialogFragmentToViewPostedJobFragment()
                findNavController().navigate(action)
            }
        }
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.postjobTaskEvent.collect() { event ->
                when (event) {
                    is PostNewJobViewmodel.PostJobTask.DisplayMessage -> {
                        displayError(binding.btnViewPost, event.msg)
                    }
                    is PostNewJobViewmodel.PostJobTask.NavigateToAddDetails -> {

                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToReviewScreen -> {

                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToSuccessScreen -> {

                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToViewJobApplication -> {
                        val action =
                            JobSuccessDialogFragmentDirections.actionJobSuccessDialogFragmentToViewPostedJobFragment()
                        findNavController().navigate(action)
                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToHome -> {
                        viewmodel.clearInputDetails()
                        val action = JobSuccessDialogFragmentDirections.actionJobSuccessDialogFragmentToHomeClientFragment()
                        findNavController().navigate(action)
                    }
                    PostNewJobViewmodel.PostJobTask.ShowJobType -> TODO()
                }.exhaustive
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}