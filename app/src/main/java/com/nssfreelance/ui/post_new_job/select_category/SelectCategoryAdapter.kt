package com.nssfreelance.ui.post_new_job.select_category

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.SearchByCategory
import com.nssfreelance.databinding.ItemSearchByCategoryBinding
import com.nssfreelance.util.OnItemClickListener

class SelectCategoryAdapter(private val listener: OnItemClickListener):ListAdapter<SearchByCategory, SelectCategoryAdapter.SearchCategoryViewHolder>(DiffUtilCallback()) {
    private var selectedPos =-1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchCategoryViewHolder {
        val binding = ItemSearchByCategoryBinding.inflate(LayoutInflater.from(parent.context), parent,false)
        return SearchCategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchCategoryViewHolder, position: Int) {
        val currentItem= getItem(position)
        holder.bind(currentItem)
    }

    inner class SearchCategoryViewHolder(private val binding:ItemSearchByCategoryBinding):RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                listener.onItemClick(getItem(adapterPosition).job_category, getItem(adapterPosition).job_category_id.toString())
                selectedPos = adapterPosition
                notifyDataSetChanged()
            }
        }
        fun bind(data:SearchByCategory){
            binding.apply {
                tvItemName.text = data.job_category

                Glide.with(binding.root.context)
                    .load(data.asset_link)
                    .into(ivItemIcon)

                if(selectedPos == adapterPosition) {
                    root.setBackgroundResource(R.drawable.bg_shape_yellow_with_outline_black)
                    tvItemName.setBackgroundResource(R.drawable.bg_shape_transparent_80_outline_black)
                    tvItemName.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.ic_tick_black,0)
                }else{
                    root.setBackgroundResource(R.drawable.bg_shape_yellow_dark_filled)
                    tvItemName.setBackgroundResource(R.drawable.bg_shape_transparent_80)
                    tvItemName.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,0,0)
                }

            }
        }
    }

    class DiffUtilCallback:DiffUtil.ItemCallback<SearchByCategory>(){
        override fun areItemsTheSame(
            oldItem: SearchByCategory,
            newItem: SearchByCategory
        ): Boolean = oldItem.job_category == newItem.job_category

        override fun areContentsTheSame(
            oldItem: SearchByCategory,
            newItem: SearchByCategory
        ): Boolean = oldItem==newItem
    }
}