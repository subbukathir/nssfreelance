package com.nssfreelance.ui.post_new_job

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.*
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.request.CreateJobPostParams
import com.nssfreelance.retrofit.response.CategoryListResponse
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.retrofit.response.ProfileClientResponse
import com.nssfreelance.util.DataState
import com.nssfreelance.util.showDateOfWorkFormate
import com.nssfreelance.util.showDoWDateFormate
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.util.*
import kotlin.math.log

private const val TAG = "PostNewJobViewmodel"
class PostNewJobViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    private val preferences:PreferencesManager,
    @Assisted private val state:SavedStateHandle
):ViewModel() {
    val preferencesFlow = preferences.preferencesFlow
    lateinit var jobTypes: List<RegisterDetails.JobType>

    var userId= ""
    var userToken = ""
    var userName =""

    var listCategory:MutableList<SearchByCategory> = mutableListOf()
    var profile:Profile? = null

    private val _createJobPostDataState:MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val createJobpostDataState:LiveData<DataState<LoginResponse>> = _createJobPostDataState


    private val _profileDataState:MutableLiveData<DataState<ProfileClientResponse>> = MutableLiveData()
    val prfileDataState:LiveData<DataState<ProfileClientResponse>> = _profileDataState

    private val _categoryState:MutableLiveData<DataState<CategoryListResponse>> = MutableLiveData()
    val categoryState:LiveData<DataState<CategoryListResponse>>
        get() = _categoryState

    private val _countryState: MutableLiveData<DataState<List<Country>>> = MutableLiveData()
    val countryState: LiveData<DataState<List<Country>>>
        get() = _countryState

    private val _stateState: MutableLiveData<DataState<List<State>>> = MutableLiveData()
    val stateState: LiveData<DataState<List<State>>>
        get() = _stateState

    private val _cityState: MutableLiveData<DataState<List<City>>> = MutableLiveData()
    val cityState: LiveData<DataState<List<City>>>
        get() = _cityState



    var category:String = state.get<String>("category")?:""
        set(value) {
            field = value
            state.set("category", value)
        }

    var categoryId:String = state.get<String>("categoryId")?:""
        set(value) {
            field = value
            state.set("categoryId", value)
        }

    //add job details
    var jobTitle:String = state.get<String>("jobTitle")?:""
    set(value) {
        field = value
        state.set("jobTitle",value)
    }


    var country = state.get<String>("country") ?: ""
        set(value) {
            field = value
            state.set("country", value)
        }

    var countryId = state.get<String>("countryId") ?: ""
        set(value) {
            field = value
            state.set("countryId", value)
        }

    var stateStr = state.get<String>("stateStr") ?: ""
        set(value) {
            field = value
            state.set("stateStr", value)
        }
    var stateId = state.get<String>("stateId") ?: ""
        set(value) {
            field = value
            state.set("stateId", value)
        }

    var cityStr = state.get<String>("cityStr") ?: ""
        set(value) {
            field = value
            state.set("cityStr", value)
        }
    var cityId = state.get<String>("cityId") ?: ""
        set(value) {
            field = value
            state.set("cityId", value)
        }


    var jobType = state.get<String>("jobType") ?: ""
        set(value) {
            field = value
            state.set("jobType", value)
        }
    var jobTypeId = state.get<String>("jobTypeId") ?: ""
        set(value) {
            field = value
            state.set("jobTypeId", value)
        }

    var addressLineOne:String = state.get<String>("addressLineOne")?:""
        set(value) {
            field = value
            state.set("addressLineOne",value)
        }

    var addressLineTwo:String = state.get<String>("addressLineTwo")?:""
        set(value) {
            field = value
            state.set("addressLineTwo",value)
        }

    var cost:String = state.get<String>("jobCost")?:""
    set(value) {
        field = value
        state.set("jobCost",value)
    }

    var overview:String = state.get<String>("jobOverview")?:""
        set(value) {
            field = value
            state.set("jobOverview",value)
        }

    var requirements:String = state.get<String>("jobRequirements")?:""
        set(value) {
            field = value
            state.set("jobRequirements",value)
        }

    var qualification:String = state.get<String>("jobQualification")?:""
        set(value) {
            field = value
            state.set("jobQualification",value)
        }


    var jobDate:String = state.get<String>("jobDate")?:""
        set(value) {
            field = value
            state.set("jobDate",value)
        }
    var expiryDate:String = state.get<String>("jobExpiryDate")?:""
        set(value) {
            field = value
            state.set("jobExpiryDate",value)
        }

    var params:CreateJobPostParams? = null


    fun getCountry() {
        viewModelScope.launch {
            repository.getCountry()
                .onEach { dataState ->
                    _countryState.value = dataState
                }
                .launchIn(viewModelScope)
        }
    }

    fun getState(country: String) {
        Log.d(TAG, "getState: $country")
        viewModelScope.launch {
            repository.getState(country)
                .onEach { _stateState.value = it }
                .launchIn(viewModelScope)
        }
    }

    fun getCity(state: String) {
        Log.d(TAG, "getCity: $state")
        viewModelScope.launch {
            repository.getCity(state)
                .onEach { _cityState.value = it }
                .launchIn(viewModelScope)
        }
    }

    fun getListByCategory(){
        viewModelScope.launch {
            repository.getSearchCategory()
                .onEach { dataState -> _categoryState.value = dataState }
                .launchIn(viewModelScope)
        }
    }


    fun updateListCategory(list:List<SearchByCategory>){
        listCategory = list.toMutableList()
    }

    fun getProfileDetails(){
        viewModelScope.launch {
            repository.getClientProfile(userToken,userId)
                .onEach { dataState -> _profileDataState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    private val _postjobChannel = Channel<PostJobTask>()
    val postjobTaskEvent = _postjobChannel.receiveAsFlow()

    fun onNextClickOfSelectCategory() {
        if(category.isNotEmpty()){
            viewModelScope.launch {
                _postjobChannel.send(PostJobTask.NavigateToAddDetails(category))
            }
        }else{
            postError("Please select category")
        }
    }

    fun onJobTypeClicked(){
        viewModelScope.launch {
            _postjobChannel.send(PostJobTask.ShowJobType)
        }
    }

    fun clearInputDetails(){
        jobTitle = ""
        cityStr = ""
        country =""
        category=""
        categoryId=""
        addressLineOne =""
        addressLineTwo=""
        cost=""
        jobType=""
        jobTypeId=""
        overview = ""
        requirements =""
        qualification=""
        expiryDate=""
        jobDate=""
    }


    fun onNextClickOfAddSkills() {
        Log.d(TAG, "onNextClickOfAddSkills")
        if(jobTitle.isEmpty()){
            postError("Enter job title")
            return
        }

        if(cityStr.isEmpty()){
            postError("Select city")
            return
        }

        if(addressLineOne.isEmpty()){
            postError("Enter address")
            return
        }
        if(cost.isEmpty()){
            postError("Enter cost")
            return
        }

        if(jobTypeId.isEmpty()){
            postError("Select job type")
            return
        }

        if(overview.isEmpty()){
            postError("Enter job overview")
            return
        }
        if(requirements.isEmpty()){
            postError("Enter job requirements")
            return
        }
        if(qualification.isEmpty()){
            postError("Enter job qualifications")
            return
        }

        if(expiryDate.isEmpty()){
            postError("Enter job expiry rate")
            return
        }
        val skills = listOf("1","4")
        params = CreateJobPostParams(
            job_title = jobTitle,
            job_city_id = cityId,
            job_starts_from = jobDate,
            job_ends_on = expiryDate,
            job_highlights = "SPOT PAYMENT",
            job_overview = overview,
            job_requirement = requirements,
            job_qualification = qualification,
            job_type_id = jobTypeId,
            job_price = cost.replace("$","").replace(",",""),
            client_user_id = userId,
            skills = skills,
            job_category_id = categoryId,
            address_line_1 = addressLineOne,
            address_line_2 = addressLineTwo
        )
        viewModelScope.launch {
            _postjobChannel.send(PostJobTask.NavigateToReviewScreen)
        }
    }

    fun updateExpiryDate(date:Date){
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.MONTH,1)
        expiryDate = showDateOfWorkFormate(calendar.time)
        Log.d(TAG, "updateExpiryDate: $expiryDate")
    }

    fun postJobPost(){
        params?.let { param->
            viewModelScope.launch {
                repository.createJobPost(userToken, param)
                    .onEach { dataState -> _createJobPostDataState.value = dataState }
                    .launchIn(viewModelScope)
            }
        }

    }

    fun onSuccessJobCreation(){
        viewModelScope.launch {
            _postjobChannel.send(PostJobTask.NavigateToSuccessScreen)
        }
    }

    fun onSubmitClickReview() {
        postJobPost()
    }

    fun onViewPostClick() {
        viewModelScope.launch {
            _postjobChannel.send(PostJobTask.NavigateToViewJobApplication)
        }
    }
    fun onViewApplicationClick(){
        viewModelScope.launch {
            _postjobChannel.send(PostJobTask.NavigateToHome)
        }
    }

    private fun postError(error:String){
        viewModelScope.launch {
            _postjobChannel.send(PostJobTask.DisplayMessage(error))
        }
    }
    sealed class PostJobTask{
        data class NavigateToAddDetails(val jobCategory:String):PostJobTask()
        object NavigateToReviewScreen:PostJobTask()
        object NavigateToSuccessScreen:PostJobTask()
        object NavigateToViewJobApplication:PostJobTask()
        object NavigateToHome:PostJobTask()
        object ShowJobType:PostJobTask()
        data class DisplayMessage(val msg:String) :PostJobTask()
    }
}