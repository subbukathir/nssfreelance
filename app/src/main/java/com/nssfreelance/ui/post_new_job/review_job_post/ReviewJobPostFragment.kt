package com.nssfreelance.ui.post_new_job.review_job_post

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentPostJobReviewAndSubmitBinding
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.ui.post_new_job.PostNewJobViewmodel
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.util.*

private const val TAG = "ReviewJobPostFragment"
@AndroidEntryPoint
class ReviewJobPostFragment : Fragment(R.layout.fragment_post_job_review_and_submit) {
    private var _binding: FragmentPostJobReviewAndSubmitBinding? = null
    private val binding: FragmentPostJobReviewAndSubmitBinding
        get() = _binding!!

    private val viewmodel: PostNewJobViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentPostJobReviewAndSubmitBinding.bind(view)

        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            btnNext.setOnClickListener {
                viewmodel.onSubmitClickReview()
            }

            ivJobDetailsEdit.setOnClickListener {
                findNavController().popBackStack()
            }

            tvItemJobRole.text = viewmodel.params?.job_title
            tvItemName.text = viewmodel.userName
            tvItemLocation.text = "${viewmodel.cityStr} | ${viewmodel.country}"
            tvDateOfWork.text = "Date of Work ${viewmodel.jobDate}"
            tvPostExpireDate.text = "Post Expiry Date ${viewmodel.expiryDate}"
            tvJobPrice.text = "Price ${viewmodel.cost} | ${viewmodel.jobType}"
            tvJobOverview.text = viewmodel.overview
            tvJobRequirement.text = viewmodel.requirements
            tvJobQualification.text = viewmodel.qualification
        }
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.postjobTaskEvent.collect() { event ->
                when (event) {
                    is PostNewJobViewmodel.PostJobTask.DisplayMessage -> {
                        displayError(binding.btnNext, event.msg)
                    }
                    is PostNewJobViewmodel.PostJobTask.NavigateToAddDetails -> {

                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToReviewScreen -> {

                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToSuccessScreen -> {
                        val action =
                            ReviewJobPostFragmentDirections.actionReviewJobPostFragmentToJobSuccessDialogFragment()
                        findNavController().navigate(action)
                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToViewJobApplication -> {

                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToHome -> TODO()
                    PostNewJobViewmodel.PostJobTask.ShowJobType -> TODO()
                }.exhaustive
            }
        }

        viewmodel.createJobpostDataState.observe(viewLifecycleOwner) { dataState ->
            when (dataState) {
                is DataState.Success<LoginResponse> -> {
                    displayProgressbar(false)
                    if (dataState.data.status)
                    {

                        viewmodel.onSuccessJobCreation()
                    }
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(binding.btnNext, dataState.exception.message)
                }
                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }
        }

        viewmodel.prfileDataState.observe(viewLifecycleOwner) { dataState ->
            when (dataState) {
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(binding.btnNext, dataState.exception.message)
                }
                DataState.Loading -> displayProgressbar(true)
                is DataState.Success -> {
                    displayProgressbar(false)

                    if (dataState.data.data!=null)
                    {
                        viewmodel.profile = dataState.data.data
                    }
                    else Log.d(TAG, "subscribeObservers: empty profile")
                }
            }.exhaustive
        }
    }

    private fun displayProgressbar(show: Boolean) {
        binding.progress.progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}