package com.nssfreelance.ui.post_new_job.select_category

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.SearchByCategory
import com.nssfreelance.databinding.FragmentPostJobSelectCategoryBinding
import com.nssfreelance.ui.post_new_job.PostNewJobViewmodel
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "PostJobSelectCategoryFr"

@AndroidEntryPoint
class PostJobSelectCategoryFragment:Fragment(R.layout.fragment_post_job_select_category),OnItemClickListener {
    private var _binding:FragmentPostJobSelectCategoryBinding?=null
    private val binding:FragmentPostJobSelectCategoryBinding
    get() = _binding!!

    private lateinit var mAdapter:SelectCategoryAdapter

    private val viewmodel: PostNewJobViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentPostJobSelectCategoryBinding.bind(view)

        mAdapter = SelectCategoryAdapter(this)

        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            recylerviewSelectCategory.itemAnimator = DefaultItemAnimator()
            recylerviewSelectCategory.adapter = mAdapter

            btnNext.setOnClickListener {
                viewmodel.onNextClickOfSelectCategory()
            }
        }
        viewmodel.getListByCategory()
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.postjobTaskEvent.collect(){event ->
                when(event){
                    is PostNewJobViewmodel.PostJobTask.DisplayMessage -> {
                        displayError(binding.btnNext,event.msg)
                    }
                    is PostNewJobViewmodel.PostJobTask.NavigateToAddDetails -> {
                        Log.d(TAG, "subscribeObservers: navigate")
                        val action = PostJobSelectCategoryFragmentDirections.actionPostJobSelectCategoryFragmentToAddJobDetailsFragment()
                        findNavController().navigate(action)
                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToReviewScreen -> {}
                    PostNewJobViewmodel.PostJobTask.NavigateToSuccessScreen -> {}
                    PostNewJobViewmodel.PostJobTask.NavigateToViewJobApplication -> {

                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToHome -> TODO()
                    PostNewJobViewmodel.PostJobTask.ShowJobType -> TODO()
                }.exhaustive

            }
        }

        viewmodel.categoryState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        updateCategory(dataState.data.data)
                    } else {
                        Log.d(TAG, "subscribeObservers: empty list")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${dataState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }

    private fun updateCategory(listCategory:List<SearchByCategory>) {
        mAdapter.submitList(listCategory)
        viewmodel.updateListCategory(listCategory)
    }

    override fun onItemClick(searchStr: String, id:String) {
        viewmodel.category = searchStr
        viewmodel.categoryId = id
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding =null
    }
}