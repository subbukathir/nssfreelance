package com.nssfreelance.ui.post_new_job.view_posted_job

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentViewPostedJobLikeFreelancerBinding
import com.nssfreelance.ui.post_new_job.PostNewJobViewmodel
import com.nssfreelance.util.exhaustive
import com.nssfreelance.util.showDateOfWorkFormate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.util.*

@AndroidEntryPoint
class ViewPostedJobFragment:Fragment(R.layout.fragment_view_posted_job_like_freelancer) {
    private var _binding:FragmentViewPostedJobLikeFreelancerBinding?=null
    private val binding:FragmentViewPostedJobLikeFreelancerBinding
    get() = _binding!!

    private val viewmodel:PostNewJobViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentViewPostedJobLikeFreelancerBinding.bind(view)

        binding.apply {
            imageviewBack.setOnClickListener {
                viewmodel.clearInputDetails()
                findNavController().popBackStack()
            }

            btnViewApplication.setOnClickListener {
                viewmodel.onViewApplicationClick()
            }
            tvItemJobRole.text = viewmodel.params?.job_title
            tvItemName.text = viewmodel.profile?.full_name?:""
            tvItemLocation.text = viewmodel.cityStr + " | "+viewmodel.country
            tvDateOfWork.text = "Date of Work ${viewmodel.jobDate}"
            tvPostExpireDate.text = "Post Expiry Date ${viewmodel.expiryDate}"
            tvJobPrice.text = "Price ${viewmodel.cost} | ${viewmodel.jobType}"
            tvJobOverview.text = viewmodel.overview
            tvJobRequirement.text = viewmodel.requirements
            tvJobQualification.text = viewmodel.qualification
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.postjobTaskEvent.collect { event ->
                when(event){
                    is PostNewJobViewmodel.PostJobTask.DisplayMessage -> TODO()
                    is PostNewJobViewmodel.PostJobTask.NavigateToAddDetails -> TODO()
                    PostNewJobViewmodel.PostJobTask.NavigateToHome -> {
                        viewmodel.clearInputDetails()
                        val action = ViewPostedJobFragmentDirections.actionViewPostedJobFragmentToHomeClientFragment()
                        findNavController().navigate(action)
                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToReviewScreen -> TODO()
                    PostNewJobViewmodel.PostJobTask.NavigateToSuccessScreen -> TODO()
                    PostNewJobViewmodel.PostJobTask.NavigateToViewJobApplication -> TODO()
                    PostNewJobViewmodel.PostJobTask.ShowJobType -> TODO()
                }.exhaustive
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}