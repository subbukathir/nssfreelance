package com.nssfreelance.ui.post_new_job.add_job_details

import `in`.galaxyofandroid.spinerdialog.SpinnerDialog
import android.os.Bundle
import android.text.format.DateUtils
import android.util.Log
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.nssfreelance.R
import com.nssfreelance.data.City
import com.nssfreelance.data.Country
import com.nssfreelance.data.RegisterDetails
import com.nssfreelance.data.State
import com.nssfreelance.databinding.FragmentPostJobAddDetailsBinding
import com.nssfreelance.ui.post_new_job.PostNewJobViewmodel
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_register_freelancer.*
import kotlinx.coroutines.flow.collect
import java.lang.Exception
import java.util.*

private const val TAG = "AddJobDetailsFragment"
@AndroidEntryPoint
class AddJobDetailsFragment:Fragment(R.layout.fragment_post_job_add_details) {
    private var _binding:FragmentPostJobAddDetailsBinding?=null
    private val binding:FragmentPostJobAddDetailsBinding
    get() = _binding!!

    private val viewmodel:PostNewJobViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentPostJobAddDetailsBinding.bind(view)

        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            btnNext.setOnClickListener {
                viewmodel.onNextClickOfAddSkills()
            }

            //store entered values to state
            etJobTitle.addTextChangedListener {
                viewmodel.jobTitle = it.toString()
            }

            etState.setOnClickListener {
                if (viewmodel.countryId.isNotBlank() && viewmodel.stateId.isNullOrBlank()) {
                    viewmodel.getState(viewmodel.countryId)
                }else{
                    displayError(requireView(),"Kindly select country")
                }
            }

            etCity.setOnClickListener {
                if (viewmodel.stateId.isNotBlank() && viewmodel.cityId.isNullOrBlank()) {
                    viewmodel.getCity(viewmodel.stateId)
                }else{
                    displayError(requireView(),"Kindly select state")
                }
            }

            etAddressLineOne.addTextChangedListener {
                viewmodel.addressLineOne = it.toString()
            }

            etAddressLineTwo.addTextChangedListener {
                viewmodel.addressLineTwo = it.toString()
            }

            etJobCost.addTextChangedListener {
                viewmodel.cost = it.toString()
                try {
                    val text: String = viewmodel.cost
                    val textLength: Int = viewmodel.cost.length
                    if (text.endsWith("$") || text.endsWith(" ")) return@addTextChangedListener
                    if (textLength == 1) {
                        if (!text.contains("$")) {
                            etMobileNo.setText(
                                StringBuilder(text).insert(text.length - 1, "$").toString()
                            )
                            etJobCost.setSelection(viewmodel.cost.length)
                        }
                    }
                }catch (e:Exception){
                    e.printStackTrace()
                }
            }

            etJobCost.setOnFocusChangeListener { v, hasFocus ->
                if(!hasFocus){
                    if(etJobCost.text.toString().isNotEmpty()){
                        etJobCost.setText(convertToCurrency(etJobCost.text.toString().replace("$",""),requireContext()))
                    }
                }
            }

            etJobOverview.addTextChangedListener {
                viewmodel.overview = it.toString()
            }

            etJobRequirement.addTextChangedListener {
                viewmodel.requirements = it.toString()
            }

            etJobQualification.addTextChangedListener {
                viewmodel.qualification = it.toString()
            }


            etJobType.setOnClickListener {
                viewmodel.onJobTypeClicked()
            }

            etJobDate.setOnClickListener {
                val calendar = Calendar.getInstance()
                val selectedDate: Date
                val strDate: String = etJobDate.text.toString().trim()
                if (strDate.equals("")) {
                    selectedDate = calendar.time
                } else {
                    // convert string to date
                    selectedDate = updatedDOWDateStrToDate(strDate)
                }
                showDatePickerDialog(requireContext(),object : DateTimePickerResponse{
                    override fun okayPressed(date: Date?) {
                        etJobDate.setText(date?.let { it1 -> showDateOfWorkFormate(it1) })
                        viewmodel.jobDate = date?.let { it1 -> showDateOfWorkFormate(it1) }.toString()
                        date?.let { it1 -> viewmodel.updateExpiryDate(it1) }
                    }
                },selectedDate,false)
            }

            //set 30 days from current date
            val calendar = Calendar.getInstance()
            val jobDate = calendar.time

            if(viewmodel.jobDate.isEmpty() || viewmodel.jobDate.isNullOrBlank()) {
                etJobDate.setText(jobDate?.let { it1 -> showDateOfWorkFormate(it1) })
                viewmodel.jobDate = jobDate?.let { dateJob-> showDateOfWorkFormate(dateJob) }.toString()
                    calendar.add(Calendar.MONTH,1)
                    val plusDate = calendar.time
                viewmodel.expiryDate =
                    plusDate?.let { it1 -> showDateOfWorkFormate(it1) }.toString()
                Log.d(TAG, "onViewCreated: ${viewmodel.jobDate} , expiry ${viewmodel.expiryDate}")
            }

            //set values to edit text
            etJobTitle.setText(viewmodel.jobTitle)
            etCountry.setText(viewmodel.country)
            etState.setText(viewmodel.stateStr)
            etCity.setText(viewmodel.cityStr)
            etAddressLineOne.setText(viewmodel.addressLineOne)
            etAddressLineTwo.setText(viewmodel.addressLineTwo)
            etJobCost.setText(viewmodel.cost)
            etJobType.setText(viewmodel.jobType)
            etJobOverview.setText(viewmodel.overview)
            etJobRequirement.setText(viewmodel.requirements)
            etJobQualification.setText(viewmodel.qualification)
            etJobDate.setText(viewmodel.jobDate)

        }
        subscribeObservers()
        viewmodel.getCountry()



    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.preferencesFlow.collect { prefs ->
                viewmodel.userId = prefs.userId
                viewmodel.userToken = "Bearer ${prefs.userToken}"
                viewmodel.userName = prefs.firstname
                val registerDetails = Gson().fromJson(prefs.userDetails,RegisterDetails::class.java)
                viewmodel.jobTypes = registerDetails.job_type
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.postjobTaskEvent.collect(){event ->
                when(event){
                    is PostNewJobViewmodel.PostJobTask.DisplayMessage -> {
                        displayError(binding.btnNext,event.msg)
                    }
                    is PostNewJobViewmodel.PostJobTask.NavigateToAddDetails -> {

                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToReviewScreen -> {
                        val  action = AddJobDetailsFragmentDirections.actionAddJobDetailsFragmentToReviewJobPostFragment()
                        findNavController().navigate(action)
                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToSuccessScreen -> {}
                    PostNewJobViewmodel.PostJobTask.NavigateToViewJobApplication -> {

                    }
                    PostNewJobViewmodel.PostJobTask.NavigateToHome -> TODO()
                    PostNewJobViewmodel.PostJobTask.ShowJobType -> {
                        if (viewmodel.jobTypes.isNotEmpty()) {
                            val genderLis: ArrayList<String> = arrayListOf()
                            for (data in viewmodel.jobTypes) {
                                genderLis.add(data.job_type)
                            }
                            showSingleChoiceDialog(
                                requireContext(),
                                "Choose Job Type",
                                genderLis,
                                object : OnSingleChoiceItemClick {
                                    override fun onSubmit(selectedItem: String, position: Int) {
                                        binding.etJobType.setText(selectedItem)
                                        viewmodel.jobType = selectedItem
                                        viewmodel.jobTypeId =
                                            "${viewmodel.jobTypes[position].job_type_id}"
                                    }
                                })

                        } else {
                            displayError(requireView(),"Job type is empty")
                        }
                    }
                }.exhaustive
            }
        }


        viewmodel.countryState.observe(viewLifecycleOwner, { countryState ->
            when (countryState) {
                is DataState.Success<List<Country>> -> {
                    displayProgressbar(false)
                    appendCountry(countryState.data)
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(requireView(),countryState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${countryState.exception.printStackTrace()}")
                }
                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }.exhaustive
        })

        viewmodel.stateState.observe(viewLifecycleOwner, { stateState ->
            when (stateState) {
                is DataState.Success<List<State>> -> {
                    displayProgressbar(false)
                    appendState(stateState.data)
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(requireView(),stateState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${stateState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    displayProgressbar(true)
                }
            }.exhaustive
        })

        viewmodel.cityState.observe(viewLifecycleOwner, { stateState ->
            when (stateState) {
                is DataState.Success<List<City>> -> {
                    displayProgressbar(false)
                    appendCity(stateState.data)
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(requireView(),stateState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${stateState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    displayProgressbar(true)
                }
            }.exhaustive
        })

    }


    private fun appendCity(data: List<City>) {
        var spinnerDialog: SpinnerDialog? = null
        if (data.isNotEmpty()) {
            val cities = arrayListOf<String>()
            for (item in data) {
                cities.add(item.city_name)
            }
            spinnerDialog = SpinnerDialog(
                requireActivity(),
                cities,
                "Choose City",
                R.style.DialogAnimations_SmileWindow
            )
            spinnerDialog.setCancellable(false)
            spinnerDialog.setShowKeyboard(false)
            spinnerDialog.setUseContainsFilter(true)
            spinnerDialog.bindOnSpinerListener { item, position ->
                etCity.setText(item)
                viewmodel.cityStr = item
                viewmodel.cityId = "${data[position].city_id}"
            }
            //spinnerDialog.showSpinerDialog()
        }
        binding.etCity.setOnClickListener {
            spinnerDialog?.let {
                it.showSpinerDialog()
            }
        }
    }

    private fun appendState(data: List<State>) {
        var spinnerDialog: SpinnerDialog? = null
        if (data.isNotEmpty()) {
            val states = arrayListOf<String>()
            for (item in data) {
                states.add(item.state_name)
            }
            spinnerDialog = SpinnerDialog(
                requireActivity(),
                states,
                "Choose State",
                R.style.DialogAnimations_SmileWindow
            )
            spinnerDialog.setCancellable(false)
            spinnerDialog.setShowKeyboard(false)
            spinnerDialog.setUseContainsFilter(true)
            spinnerDialog.bindOnSpinerListener { item, position ->
                etState.setText(item)
                viewmodel.stateStr = item
                viewmodel.stateId = "${data[position].state_id}"
                viewmodel.getCity(viewmodel.stateId)
            }
            //spinnerDialog.showSpinerDialog()
        }
        binding.etState.setOnClickListener {
            spinnerDialog?.let {
                it.showSpinerDialog()
            }
        }

    }


    private fun appendCountry(data: List<Country>) {
        binding.etCountry.setOnClickListener {
            if (data.isNotEmpty()) {
                val countries = arrayListOf<String>()
                for (item in data) {
                    countries.add(item.country_name)
                }
                val spinnerDialog = SpinnerDialog(
                    requireActivity(),
                    countries,
                    "Choose Country",
                    R.style.DialogAnimations_SmileWindow
                )
                spinnerDialog.setCancellable(false)
                spinnerDialog.setShowKeyboard(false)
                spinnerDialog.setUseContainsFilter(true)
                spinnerDialog.bindOnSpinerListener { item, position ->
                    etCountry.setText(item)
                    viewmodel.country = item
                    viewmodel.countryId = "${data[position].country_id}"
                    Log.d(TAG, "appendCountry: ${data[position].country_id}")
                    viewmodel.getCity("${data[position].country_id}")
                }
                spinnerDialog.showSpinerDialog()
            }
        }
    }

    private fun displayProgressbar(show: Boolean) {
        if(show) binding.progress.progressBar.show()
        else binding.progress.progressBar.hide()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}