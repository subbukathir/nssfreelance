package com.nssfreelance.ui.notification

import android.os.Build
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.Notification
import com.nssfreelance.databinding.ItemNotificationBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency
import com.nssfreelance.util.getTimeAgo

private const val TAG = "NotificationAdapter"
class NotificationAdapter(val isFreelancer:Boolean):ListAdapter<Notification, NotificationAdapter.NotificationViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val viewBinding = ItemNotificationBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return NotificationViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        val currentItem =getItem(position)
        holder.bind(currentItem)
    }

    inner class NotificationViewHolder(private val binding:ItemNotificationBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(notify:Notification){
            Log.d(TAG, "bind: $isFreelancer")
            binding.apply {
                var content ="${notify.descriptions}"
                if(isFreelancer){
                    //content = "<font color='#DAA520'>${notify.client_name?:""}</font> Marked project as ${notify.job_title?:""}"
                    if(notify.client_profile_url!=null){
                        Glide.with(binding.root.context)
                            .load(notify.client_profile_url)
                            .placeholder(R.drawable.ic_placeholder_profile)
                            .into(civProfile)
                    }else{
                        Glide.with(binding.root.context)
                            .load(R.drawable.ic_placeholder_profile)
                            .into(civProfile)
                    }
                }else{
                    //content = "<font color='#DAA520'>${notify.freelancer_name?:""}</font> Marked project as ${notify.job_title?:""}"
                    if(notify.freelancer_profile_url!=null){
                        Glide.with(binding.root.context)
                            .load(notify.freelancer_profile_url)
                            .placeholder(R.drawable.ic_placeholder_profile)
                            .into(civProfile)
                    }else{
                        Glide.with(binding.root.context)
                            .load(R.drawable.ic_placeholder_profile)
                            .into(civProfile)
                    }
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    tvItemContent.text = Html.fromHtml(content,Html.FROM_HTML_MODE_LEGACY)
                }else{
                    tvItemContent.text = Html.fromHtml(content)
                }
                tvItemPostedTime.text = if(notify.notification_on!=null) commonDateFormate(notify?.notification_on) else ""

            }
        }
    }

    class DiffUtilCallback: DiffUtil.ItemCallback<Notification>(){
        override fun areItemsTheSame(oldItem: Notification, newItem: Notification): Boolean {
            return oldItem.notification_id == newItem.notification_id
        }

        override fun areContentsTheSame(oldItem: Notification, newItem: Notification): Boolean {
            return oldItem == newItem
        }
    }
}