package com.nssfreelance.ui.notification

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.response.NotificationResponse
import com.nssfreelance.retrofit.response.ProfileClientResponse
import com.nssfreelance.util.Const
import com.nssfreelance.util.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class NotificationViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    private val preferences:PreferencesManager
):ViewModel() {
    val preferencesFlow = preferences.preferencesFlow

    var userId = ""
    var userToken =""
    var profileUrl = ""
    var isFreelancer: Boolean=false


    private val _notificationState: MutableLiveData<DataState<NotificationResponse>> = MutableLiveData()
    val notificationState: LiveData<DataState<NotificationResponse>>
        get() = _notificationState

    fun getNotifications() {
        viewModelScope.launch {
            repository.getNotification(userToken, userId)
                .onEach { dataState -> _notificationState.value = dataState }
                .launchIn(viewModelScope)
        }
    }
}