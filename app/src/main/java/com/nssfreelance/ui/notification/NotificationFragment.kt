package com.nssfreelance.ui.notification

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Transformations
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentNotificationBinding
import com.nssfreelance.util.DataState
import com.nssfreelance.util.exhaustive
import com.nssfreelance.util.hide
import com.nssfreelance.util.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "NotificationFragment"
@AndroidEntryPoint
class NotificationFragment:Fragment(R.layout.fragment_notification) {
    private var _binding:FragmentNotificationBinding?=null
    private val binding:FragmentNotificationBinding
    get() = _binding!!

    private val viewmodel:NotificationViewmodel by viewModels()
    private lateinit var adapter: NotificationAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentNotificationBinding.bind(view)


        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            recyclerviewNotification.itemAnimator = DefaultItemAnimator()


            error.tvError.hide()
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.preferencesFlow.collect { prefs->
                viewmodel.userId = prefs.userId
                viewmodel.userToken = "Bearer ${prefs.userToken}"
                viewmodel.profileUrl = prefs.profileUrl
                viewmodel.isFreelancer = prefs.userType == 2

                Log.d(TAG, "subscribeObservers: ${viewmodel.isFreelancer}")
                adapter = NotificationAdapter(viewmodel.isFreelancer)
                binding.recyclerviewNotification.adapter = adapter
                Glide.with(requireContext())
                    .load(viewmodel.profileUrl)
                    .placeholder(R.drawable.ic_placeholder_profile)
                    .into(binding.civProfile)
             viewmodel.getNotifications()
            }
        }

        viewmodel.notificationState.observe(viewLifecycleOwner,{dataState->
            when(dataState){
                is DataState.Error -> {
                    binding.progress.progressBar.hide()
                }
                DataState.Loading -> binding.progress.progressBar.show()
                is DataState.Success -> {
                    binding.progress.progressBar.hide()
                    if(dataState.data.status){
                        binding.error.tvError.hide()
                        if (adapter == null) {
                        } else adapter.submitList(dataState.data.data.filter { notify-> notify.notification_on!=null })
                    }else{
                        binding.error.tvError.show()
                    }
                }
            }.exhaustive
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding =null
    }
}