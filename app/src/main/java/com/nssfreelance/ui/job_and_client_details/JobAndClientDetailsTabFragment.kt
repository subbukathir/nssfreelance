package com.nssfreelance.ui.job_and_client_details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.tabs.TabLayoutMediator
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentJobAndClientDetailsFreelancerBinding
import com.nssfreelance.ui.client_details.ClientDetailsFragment
import com.nssfreelance.ui.job_details.JobDetailsFragment
import com.nssfreelance.ui.job_details.JobDetailsViewmodel
import com.nssfreelance.ui.register.ViewpagerAdapter
import com.nssfreelance.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "JobAndClientDetailsTabF"
@AndroidEntryPoint
class JobAndClientDetailsTabFragment:Fragment(R.layout.fragment_job_and_client_details_freelancer) {

    private var _binding:FragmentJobAndClientDetailsFreelancerBinding?=null
    private val binding:FragmentJobAndClientDetailsFreelancerBinding
    get() = _binding!!

    private val args:JobAndClientDetailsTabFragmentArgs by navArgs()
    private val viewmodel:JobDetailsViewmodel by activityViewModels()

    private lateinit var viewpagerAdapter:ViewpagerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentJobAndClientDetailsFreelancerBinding.bind(view)

        viewpagerAdapter =
            ViewpagerAdapter(fragmentManager = requireActivity().supportFragmentManager, lifecycle)

        viewpagerAdapter.addFragment(JobDetailsFragment())
        viewpagerAdapter.addFragment(ClientDetailsFragment())
        binding.apply {

            viewpagerJobClientDetails.adapter = viewpagerAdapter
            TabLayoutMediator(tabLayoutJobClient,viewpagerJobClientDetails) { tab, position ->
                when (position) {
                    0 -> tab.text = "Job Details"
                    1 -> tab.text = "Client Details"
                }
            }.attach()

            tabLayoutJobClient.getTabAt(args.tabPosition)?.select()

            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.preferencesFlow.collect { prefs ->
                viewmodel.userToken = "Bearer ${prefs.userToken}"
                viewmodel.jobId = args.jobId?:""
                viewmodel.userId = prefs.userId
                viewmodel.userName = prefs.firstname
            }

            viewmodel.jobDetailsEvent.collect { event->
                when(event){

                    is JobDetailsViewmodel.JobDetailsTaskEvent.NavigateToSuccessApply -> {

                    }
                    is JobDetailsViewmodel.JobDetailsTaskEvent.ShowErrorMsg -> {
                    }
                    is JobDetailsViewmodel.JobDetailsTaskEvent.NavigateToApplyJob -> {

                    }
                }.exhaustive
            }
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}