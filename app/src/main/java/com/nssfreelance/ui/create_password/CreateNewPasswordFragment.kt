package com.nssfreelance.ui.create_password

import androidx.fragment.app.Fragment
import com.nssfreelance.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreateNewPasswordFragment:Fragment(R.layout.fragment_create_new_password) {
}