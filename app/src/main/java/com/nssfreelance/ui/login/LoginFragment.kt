package com.nssfreelance.ui.login

import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.facebook.*
import com.facebook.login.LoginResult
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.nssfreelance.BuildConfig
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentLoginBinding
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.ui.auth_tab.AuthTabsFragmentDirections
import com.nssfreelance.util.DataState
import com.nssfreelance.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.util.*

private const val TAG = "LoginFragment"

@AndroidEntryPoint
class LoginFragment : Fragment(R.layout.fragment_login) {
    private val EMAIL: String = "email"
    private val viewmodel: LoginViewmodel by activityViewModels()
    private var _binding: FragmentLoginBinding? = null
    private val binding
        get() = _binding!!

    private lateinit var callbackManager: CallbackManager
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var authStateListener: FirebaseAuth.AuthStateListener
    private lateinit var accessTokenTracker: AccessTokenTracker

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentLoginBinding.bind(view)

        callbackManager = CallbackManager.Factory.create();
        firebaseAuth = FirebaseAuth.getInstance()
        FacebookSdk.sdkInitialize(requireContext())

        binding.facebookLogin.setFragment(this)


        binding.apply {

            tvForgotPassword.setOnClickListener {
                val action = AuthTabsFragmentDirections.actionAuthTabsFragmentToForgotPasswordFragment()
                findNavController().navigate(action)
            }

            btnLogin.setOnClickListener {
                hideKeyboard()
                viewmodel.onLoginClick()
            }

            //input validation
            etEmailId.addTextChangedListener {
                viewmodel.username = it.toString()
            }

            etPassword.addTextChangedListener {
                viewmodel.password = it.toString()
            }

            /*if(BuildConfig.DEBUG){
                etEmailId.setText("subbukathir@email.com")
                etPassword.setText("test123456")
            }*/

            //facebook login
            ivFbLogin.setOnClickListener {
                if (!viewmodel.userTypeId.value.isNullOrBlank()) {
                    val accesToken = AccessToken.getCurrentAccessToken()
                    if (accesToken != null) {
                        viewmodel.setFbToken(accesToken.token)
                        viewmodel.onFbAccessTokenReceived()
                    } else
                        facebookLogin.performClick()
                } else displayError("Kindly choose user type")
            }

            facebookLogin.setReadPermissions(Arrays.asList(EMAIL))

            facebookLogin.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    Log.d(TAG, "onSuccess: $result")
                    result?.let {
                        Log.d(TAG, "handleFacebookToken: ${it.accessToken.token}")
                        it.accessToken?.let { token ->
                            viewmodel.setFbToken(token = token.token.toString())
                            viewmodel.onFbAccessTokenReceived()
                        }
                        handleFacebookToken(it.accessToken)
                    }
                }

                override fun onCancel() {
                    Log.d(TAG, "onCancel: ")
                }

                override fun onError(error: FacebookException?) {
                    Log.d(TAG, "onError: ${error?.printStackTrace()}")
                }
            })

            authStateListener = FirebaseAuth.AuthStateListener {
                val user = it.currentUser

            }

            accessTokenTracker = object : AccessTokenTracker() {
                override fun onCurrentAccessTokenChanged(
                    oldAccessToken: AccessToken?,
                    currentAccessToken: AccessToken?
                ) {
                    if (currentAccessToken == null) firebaseAuth.signOut()
                    else {
                        viewmodel.setFbToken(currentAccessToken.token)
                        viewmodel.onFbAccessTokenReceived()
                    }
                }
            }
        }

        setSpannableSignInStr()
        subscribeObservers()
    }

    override fun onStart() {
        super.onStart()
        firebaseAuth.addAuthStateListener(authStateListener)
    }

    override fun onStop() {
        super.onStop()
        firebaseAuth.removeAuthStateListener(authStateListener)
    }

    private fun handleFacebookToken(result: AccessToken) {
        val credentials = FacebookAuthProvider.getCredential(result.token)
        firebaseAuth.signInWithCredential(credentials).addOnCompleteListener {
            if (it.isSuccessful) {
                Log.d(TAG, "handleFacebookToken: successfull")
                val user = firebaseAuth.currentUser

            } else {
                Log.d(TAG, "handleFacebookToken: failure")
            }
        }
    }

    private fun subscribeObservers() {

        viewmodel.loginSate.observe(viewLifecycleOwner, Observer { dataState ->
            when (dataState) {
                is DataState.Success<LoginResponse> -> {
                    displayProgressbar(false)
                    if (dataState.data.status) {
                        Log.d(TAG, "subscribeObservers: ${viewmodel.userTypeId}")
                        viewmodel.onSuccessLoggedIn(dataState.data.user_id, dataState.data.token, dataState.data.uid)
                    } else {
                        displayError(dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(dataState.exception.message)
                }
                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }
        })

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.loginEvent.collect { event ->
                when (event) {
                    is LoginViewmodel.LoginTaskEvent.ShowInvalidMsg -> {
                        displayError(event.msg)
                    }
                    LoginViewmodel.LoginTaskEvent.NavigateToRegister -> {
                        Log.d(TAG, "subscribeObservers: login fragment")

                        val action =
                            AuthTabsFragmentDirections.actionAuthTabsFragmentToRegisterLanding()
                        findNavController().navigate(action)
                    }
                    LoginViewmodel.LoginTaskEvent.NavigateToHome -> {
                        /*val action = LoginFragmentDirections.actionLoginFragmentToHomeFragment()
                        findNavController().navigate(action)*/
                    }
                }.exhaustive
            }
        }

        //observe preferences flow of datastore values
        viewmodel.preferencesFlow.asLiveData().observe(viewLifecycleOwner, { preferences ->
            val userTypes = preferences.userTypes
        })
    }

    private fun setSpannableSignInStr() {
        val register = getString(R.string.don_t_have_an_account_register)

        val spannableString = SpannableString(register)
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                viewmodel.onRegisterClick()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                    ds.underlineColor = ContextCompat.getColor(requireContext(), R.color.text_color)
                }
                ds.color = ContextCompat.getColor(requireContext(), R.color.text_color)
            }
        }

        spannableString.setSpan(clickableSpan, 23, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding.tvDontHaveAccountRegister.text = spannableString
        binding.tvDontHaveAccountRegister.movementMethod = LinkMovementMethod.getInstance()
    }


    private fun displayError(message: String?) {
        hideKeyboard()
        if (message != null) {
            Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
        } else {
            Snackbar.make(requireView(), "Unknown error", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun displayProgressbar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun hideKeyboard() {
        try {
            val imm = requireContext().getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.let {

                imm.hideSoftInputFromWindow(requireActivity().currentFocus?.windowToken, 0)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}