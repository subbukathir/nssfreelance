package com.nssfreelance.ui.login

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.retrofit.request.LoginParams
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.DataState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

private const val TAG = "LoginViewmodel"

class LoginViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val stateHandle: SavedStateHandle,
    private val preferencesManager: PreferencesManager
) : ViewModel() {
    val preferencesFlow = preferencesManager.preferencesFlow

    var argsTabPosition:Int = 0
    val userTypes: MutableLiveData<String> = MutableLiveData()
    private val _loginState: MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val loginSate: LiveData<DataState<LoginResponse>>
        get() = _loginState

    private val loginEventChannel = Channel<LoginTaskEvent>()
    val loginEvent = loginEventChannel.receiveAsFlow()

    private var fbToken = ""
    var userTypeId:MutableLiveData<String> = MutableLiveData()
    var isFreelancer:MutableLiveData<Boolean> = MutableLiveData()

    fun setFbToken(token: String) {
        fbToken = token
    }

    fun setupUserType(userType:String, isFreelance:Boolean){
        Log.d(TAG, "setupUserType: $userType $isFreelance")
        userTypeId.value = userType
        isFreelancer.value = isFreelance
    }


    var username = stateHandle.get<String>("username") ?: ""
        set(value) {
            field = value
            stateHandle.set("username", value)
        }

    var userType = stateHandle.get<String>("userType") ?: ""
        set(value) {
            field = value
            stateHandle.set("userType", value)
        }


    var password = stateHandle.get<String>("password") ?: ""
        set(value) {
            field = value
            stateHandle.set("password", value)
        }

    fun onLoginClick() {
        Log.d(TAG, "onLoginClick: ${isFreelancer.value} ${userTypeId.value}")
        if (username.isBlank()) {
            showInvalidMsg("Email ID should not be blank")
            return
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(username).matches()) {
            showInvalidMsg("Email ID should be valid")
            return
        }
        if (password.isBlank()) {
            showInvalidMsg("Password should not be blank")
            return
        }

        val params = LoginParams(username, password)
        viewModelScope.launch {
            repository.login(userTypeId.value!!, params)
                .onEach { dataState -> _loginState.value = dataState }
                .launchIn(viewModelScope)

            Log.d(TAG, "onSuccessLoggedIn: ${userTypeId.value!!.toInt()}")
            preferencesManager.updateUserType(userTypeId.value!!.toInt())
        }

    }

    fun onFbAccessTokenReceived() {
        Log.d(TAG, "onFbAccessTokenReceived: $userTypeId $fbToken")
        viewModelScope.launch {
            if (userTypeId.value?.isNotEmpty()!!) {
                repository.registerWithToken(userTypeId.value!!, fbToken)
                    .onEach { dataState -> _loginState.value = dataState }
                    .launchIn(viewModelScope)
            } else showInvalidMsg("Kindly choose user type")
        }
    }

    private fun showInvalidMsg(msg: String) = viewModelScope.launch {
        loginEventChannel.send(LoginTaskEvent.ShowInvalidMsg(msg))
    }

    fun onRegisterClick() {
        viewModelScope.launch {
            loginEventChannel.send(LoginTaskEvent.NavigateToRegister)
        }
    }

    fun storeUserInfo(token: String) {
        viewModelScope.launch {
            preferencesManager.updateUserToken(token)

        }
    }

    fun onSuccessLoggedIn(userId: Int, token: String, uid:String) {
        viewModelScope.launch {
            preferencesManager.updateUserId(userId.toString())
            preferencesManager.updateUserToken(token)
            preferencesManager.updateSignIn(true)
            preferencesManager.updateUID(uid)

            loginEventChannel.send(LoginTaskEvent.NavigateToHome)
        }
    }

    sealed class LoginTaskEvent {
        data class ShowInvalidMsg(val msg: String) : LoginTaskEvent()
        object NavigateToRegister : LoginTaskEvent()
        object NavigateToHome:LoginTaskEvent()
    }

}