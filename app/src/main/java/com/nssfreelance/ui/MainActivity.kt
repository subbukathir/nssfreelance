package com.nssfreelance.ui

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.*
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.nssfreelance.R
import com.nssfreelance.repository.FCMTokenRepository
import com.nssfreelance.ui.maps.ActivityCon
import com.nssfreelance.ui.maps.MapsFragment
import com.nssfreelance.ui.settings.AccountSettingsViewmodel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.flow.collect
import pub.devrel.easypermissions.EasyPermissions
import java.util.*

private const val TAG = "MainActivity"

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private val listener =
        NavController.OnDestinationChangedListener { controller, destination, arguments ->
            // react on change
            if (destination.id == R.id.splashFragment ||
                destination.id == R.id.onBoardingFragment ||
                destination.id == R.id.registerLanding ||
                destination.id == R.id.passwordAccountSuccessFragment ||
                destination.id == R.id.loginFragment ||
                destination.id == R.id.forgotPasswordFragment ||
                destination.id == R.id.registerFragment ||
                destination.id == R.id.chooseSkillsFragment ||
                destination.id == R.id.authTabsFragment
            ) {
                bottom_nav.visibility = View.GONE
            } else {
                bottom_nav.visibility = View.VISIBLE
            }
        }
    private val viewmodel: AccountSettingsViewmodel by viewModels()

    //location

    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private val defaultLocation = LatLng(11.1271, 78.6569)
    private var locationPermissionGranted = false
    private var lastKnownLocation: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG, "onCreate")

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        navController = navHostFragment.findNavController()

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.homeFragment,
                R.id.searchFragment,
                R.id.settingsFragment,
                R.id.activityTabFragment,
                R.id.notificationFragment
            )
        )

        bottom_nav.setupWithNavController(navController)
        setupActionBarWithNavController(navController, appBarConfiguration)

        supportActionBar?.hide()

        subscribeObservers()

        requestLocation()
    }

    private fun subscribeObservers() {
        lifecycleScope.launchWhenStarted {
            viewmodel.preferencesFlow.collect { prefs ->
                prefs.isDarkMode?.let {
                    delegate.localNightMode = it
                }
                Log.d(TAG, "onCreate: launched ${prefs.userType}")

                viewmodel.userId = prefs.userId
                viewmodel.userType.value = prefs.userType
                viewmodel.fcmToken = prefs.fcmToken
            }
        }
        viewmodel.userType.observe(this) { user ->
            Log.d(TAG, "onCreate: observer $user")
            if (user == 2) {
                bottom_nav.menu.clear()
                bottom_nav.inflateMenu(R.menu.nav_bottom_bar)
            } else {
                bottom_nav.menu.clear()
                bottom_nav.inflateMenu(R.menu.nav_bottom_bar_client)
            }
        }

        FCMTokenRepository.fcmToken.observe(this) { token ->
            Log.d(TAG, "subscribeObservers: $token")
            viewmodel.updateFcmToken(token)
            if (viewmodel.fcmToken.isNullOrEmpty() || viewmodel.fcmToken != token) {
                viewmodel.sendTokenToServer(token)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        navController.addOnDestinationChangedListener(listener)

    }

    override fun onPause() {
        navController.removeOnDestinationChangedListener(listener)
        super.onPause()
    }

    override fun onSupportNavigateUp(): Boolean {

        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun fetchFCMToken() {
        Log.d(TAG, "fetchFCMToken")
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val token = task.result?.token
                if (token != null) {
                    viewmodel.sendTokenToServer(token)
                }
            })
    }

    /**
     * Handles the result of the request for location permissions.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
        Log.d(TAG, "onRequestPermissionsResult: $requestCode")
        when (requestCode) {
            MapsFragment.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                Log.d(TAG, "onRequestPermissionsResult: $requestCode")
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCon.activityCon.value = "${MapsFragment.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION}"
                }
            }

            MapsFragment.REQUEST_CODE_CHECK_SETTINGS -> {
                Log.d(TAG, "onRequestPermissionsResult $requestCode")
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ActivityCon.activityCon.value = "${MapsFragment.REQUEST_CODE_CHECK_SETTINGS}"
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "onActivityResult: $requestCode")
        if(requestCode == MapsFragment.REQUEST_CODE_CHECK_SETTINGS && resultCode == Activity.RESULT_OK){
            ActivityCon.activityCon.value = "${MapsFragment.REQUEST_CODE_CHECK_SETTINGS}"
        }
    }

    /**
     * Location
     */

    public fun requestLocation(){
        Log.d(TAG,"Location fetching...")
        getCurrentCountry(this@MainActivity)
        ActivityCon.activityCon.observe(this) { requestCode ->
            when (requestCode.toInt()) {
                MapsFragment.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                    Log.d(TAG, "observer: $requestCode")
                    locationPermissionGranted = true
                    getLocationPermission()
                    updateLocationUI()
                    getDeviceLocation()
                }

                MapsFragment.REQUEST_CODE_CHECK_SETTINGS -> {
                    Log.d(TAG, "observer $requestCode")
                    locationPermissionGranted = true
                    getLocationPermission()
                    getDeviceLocation()
                    updateLocationUI()
                }
            }
        }

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // Prompt the user for permission.
            getLocationPermission()
        }
        updateLocationUI()
        // Get the current location of the device and set the position of the map.
        getDeviceLocation()
    }
    //Getting location
    fun getCurrentCountry(context: Context) {
        Log.d(TAG, "getCurrentCountry: ")
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

        locationRequest = LocationRequest.create()
        locationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest!!.interval = (5 * 1000).toLong() // 10 seconds
        locationRequest!!.fastestInterval = (5 * 1000).toLong() // 5 seconds

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                Log.d(TAG, "onLocationResult: "+ locationResult)
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        lastKnownLocation = location
                        Log.d(TAG, "onLocationResult ${location.latitude}, ${location.longitude}")

                        if (fusedLocationProviderClient != null) {
                            fusedLocationProviderClient!!.removeLocationUpdates(locationCallback!!)
                        }
                    }
                }
            }
        }
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "getDeviceLocation: ")
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            Log.d(TAG, "getDeviceLocation: not null ${lastKnownLocation!!.latitude}")
                            val address = getCompleteAddressString(
                                lastKnownLocation!!.latitude,
                                lastKnownLocation!!.longitude
                            )
                            viewmodel.updateLocation(
                                "${lastKnownLocation!!.latitude},${lastKnownLocation!!.longitude}",
                                address ?: ""
                            )
                            Log.d(
                                TAG,
                                "getDeviceLocation: ${lastKnownLocation?.latitude} ${lastKnownLocation?.longitude} : $address"
                            )
                        }
                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.")
                        Log.e(TAG, "Exception: %s", task.exception)
                        getLocationPermission()
                        enableLocationSettings()
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
            enableLocationSettings()
            getDeviceLocation()
        } else {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private fun updateLocationUI() {
        try {
            if (locationPermissionGranted) {
                enableLocationSettings()
                getDeviceLocation()
            } else {
                lastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    private fun getCompleteAddressString(latitude: Double, longitude: Double): String? {
        var strAdd = ""
        val geocoder = Geocoder(this, Locale.getDefault())
        try {
            val addresses: List<Address>? = geocoder.getFromLocation(latitude, longitude, 1)
            if (addresses != null) {
                val strReturnedAddress = StringBuilder("")
                val cityName = addresses[0].getAddressLine(0)

                strReturnedAddress.append(cityName)
                strAdd = strReturnedAddress.toString()
                Log.w(TAG, strAdd)
            } else {
                Log.w(TAG, "No Address returned!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.w(TAG, "Canont get Address!")
        }
        return strAdd
    }

    protected fun enableLocationSettings() {
        val locationRequest = LocationRequest.create()
            .setInterval(LOCATION_UPDATE_INTERVAL)
            .setFastestInterval(LOCATION_UPDATE_FASTEST_INTERVAL)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
        LocationServices
            .getSettingsClient(this)
            .checkLocationSettings(builder.build())
            .addOnSuccessListener(this) { response: LocationSettingsResponse? ->
                Log.d(TAG, "enableLocationSettings: Success GPS ")

                getDeviceLocation()
            }
            .addOnFailureListener(this) { ex ->
                if (ex is ResolvableApiException) {
                    // Location settings are NOT satisfied,  but this can be fixed  by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),  and check the result in onActivityResult().
                        val resolvable = ex as ResolvableApiException
                        resolvable.startResolutionForResult(
                            this,
                            REQUEST_CODE_CHECK_SETTINGS
                        )
                    } catch (sendEx: IntentSender.SendIntentException) {
                        Log.e(TAG, "enableLocationSettings exception: ${sendEx.printStackTrace()}")
                    }
                }
            }
    }

    companion object {
        private const val DEFAULT_ZOOM = 15
        const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
        const val REQUEST_CODE_CHECK_SETTINGS = 2

        // Keys for storing activity state.
        private const val LOCATION_UPDATE_INTERVAL = 10000L
        private const val LOCATION_UPDATE_FASTEST_INTERVAL = 5000L
    }
}