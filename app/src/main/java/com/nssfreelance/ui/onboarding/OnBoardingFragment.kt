package com.nssfreelance.ui.onboarding

import android.os.Bundle
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.nssfreelance.R
import com.nssfreelance.data.Onboarding
import com.nssfreelance.databinding.FragmentOnboardingBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_onboarding.*

@AndroidEntryPoint
class OnBoardingFragment : Fragment(R.layout.fragment_onboarding) {

    private var _currentPosition = 0
    private lateinit var onboardingAdapter: OnboardingAdapter
    private var _binding:FragmentOnboardingBinding?=null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(savedInstanceState!=null){
            _currentPosition = savedInstanceState.getInt("position")
        }
        _binding = FragmentOnboardingBinding.bind(view)

        setOnboardingAdapter()

        binding.apply {

            onboardingViewpager.adapter = onboardingAdapter
            onboardingViewpager.registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)

                    //btn text and background

                    if(position == onboardingAdapter.itemCount-1){
                        buttonDrawable(false)
                    }else{
                        buttonDrawable(true)
                    }
                    setCurrentIndicator(indicatorContainer, position)
                }
            })

            if(_currentPosition==onboardingAdapter.itemCount-1){
                buttonDrawable(false)
            }else buttonDrawable(true)
            setupIndicators(indicatorContainer)

            setCurrentIndicator(indicatorContainer, 0)

            btnNext.setOnClickListener {
                if (onboardingViewpager.currentItem + 1 < onboardingAdapter.itemCount) {
                    onboardingViewpager.currentItem += 1
                }else{
                    navigateToRegisterFragment()
                }
            }

            imageviewBack.setOnClickListener {
                activity?.finish()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("position",_currentPosition)
    }


    private fun buttonDrawable(showNext:Boolean){
        if(showNext){
            //btnNext view on item 0
            binding.btnNext.text = "Next"
            binding.btnNext.setTextColor(ContextCompat.getColor(requireContext(), R.color.text_color))
            binding.btnNext.background = ContextCompat.getDrawable(requireContext(),R.drawable.bg_shape_white_with_outline_black)
        }else{
            binding.btnNext.text = "Get Started"
            binding.btnNext.setTextColor(ContextCompat.getColor(requireContext(), R.color.text_white_color))
            binding.btnNext.background = ContextCompat.getDrawable(requireContext(),R.drawable.bg_shape_black_filled)
        }
    }

    private fun setOnboardingAdapter() {
        val listItem = listOf(
            Onboarding(R.drawable.onboarding_one, "Post jobs or find jobs based on Location"),
            Onboarding(
                R.drawable.onboarding_two,
                "Over 1000 Jobs posted and 1000 Skilled Professionals"
            ),
            Onboarding(R.drawable.onboarding_three, "Enjoy secure transactions through NSS")
        )

        onboardingAdapter = OnboardingAdapter(listItem)
    }

    private fun setupIndicators(indicatorContainer: LinearLayout) {
        val indicators = arrayOfNulls<ImageView>(onboardingAdapter.itemCount)
        val layoutparams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)

        layoutparams.setMargins(8, 0, 8, 0)

        for (i in indicators.indices) {
            indicators[i] = ImageView(requireContext())
            indicators[i]?.let {
                it.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.bg_indicator_inactive
                    )
                )
                it.layoutParams = layoutparams
                indicatorContainer.addView(it)
            }
        }
    }

    private fun setCurrentIndicator(indicatorContainer: LinearLayout, currentPos: Int) {
        val childCount = indicatorContainer.childCount
        for (pos in 0 until childCount) {
            val imageView = indicatorContainer.getChildAt(pos) as ImageView
            if (pos == currentPos) {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_indicator_active)
                )
            } else {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_indicator_inactive)
                )
            }
        }
    }

    private fun navigateToRegisterFragment(){
        val action = OnBoardingFragmentDirections.actionOnBoardingFragmentToRegisterLanding()
        findNavController().navigate(action)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}