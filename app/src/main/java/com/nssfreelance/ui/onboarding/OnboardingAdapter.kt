package com.nssfreelance.ui.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nssfreelance.data.Onboarding
import com.nssfreelance.databinding.OnboardingItemContainerBinding

class OnboardingAdapter(private val listOnboarding:List<Onboarding>):RecyclerView.Adapter<OnboardingAdapter.OnBoardingViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnBoardingViewHolder {
        val binding = OnboardingItemContainerBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return OnBoardingViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OnBoardingViewHolder, position: Int) {
        val currentItem = listOnboarding[position]
        holder.bind(currentItem)
    }

    override fun getItemCount(): Int {
        return listOnboarding.size
    }

    inner class OnBoardingViewHolder(private val binding:OnboardingItemContainerBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(onboardingModel: Onboarding){
            binding.apply {
                imageOnboarding.setImageResource(onboardingModel.onboardingImg)
                tvOnboardingMsg.text = onboardingModel.onboardingMsg
            }
        }
    }

}