package com.nssfreelance.ui.register

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.google.gson.Gson
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import com.nssfreelance.data.City
import com.nssfreelance.data.Country
import com.nssfreelance.data.RegisterDetails
import com.nssfreelance.data.State
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.request.RegisterParams
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.DataState
import com.nssfreelance.util.serverDateFormate
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch


private const val TAG = "RegisterViewmodel"

class RegisterViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val state: SavedStateHandle,
    private val preferences: PreferencesManager
) : ViewModel() {
    val preferencesFlow = preferences.preferencesFlow

    var userTypes: List<RegisterDetails.UserType> = mutableListOf()

    private val _dataState: MutableLiveData<DataState<RegisterDetails>> = MutableLiveData()
    val dataState: LiveData<DataState<RegisterDetails>>
        get() = _dataState

    private val _countryState: MutableLiveData<DataState<List<Country>>> = MutableLiveData()
    val countryState: LiveData<DataState<List<Country>>>
        get() = _countryState

    private val _stateState: MutableLiveData<DataState<List<State>>> = MutableLiveData()
    val stateState: LiveData<DataState<List<State>>>
        get() = _stateState

    private val _cityState: MutableLiveData<DataState<List<City>>> = MutableLiveData()
    val cityState: LiveData<DataState<List<City>>>
        get() = _cityState

    private val _registerState: MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val registerState: LiveData<DataState<LoginResponse>>
        get() = _registerState

    private val _genderList: MutableLiveData<List<RegisterDetails.Gender>> = MutableLiveData()
    private val _registerDetails: MutableLiveData<RegisterDetails> = MutableLiveData()

    private var params: RegisterParams? = null

    //setter
    fun updateUserTypeDatastore() {
        if (_registerDetails.value != null) {
            userTypes = _registerDetails.value!!.user_type
            if (userTypes.isNotEmpty()) {
                val userTypeString = Gson().toJson(userTypes).toString()
                viewModelScope.launch {
                    preferences.updateUserTypes(userTypeString)
                }
            }

            val userDetailsString: String = Gson().toJson(_registerDetails.value).toString()
            viewModelScope.launch {
                preferences.updateUserDetails(userDetailsString)
            }
        } else {
            //showInvalidInputMessage("User type not available")
        }
    }

    fun updateUserId(userId: String) {
        viewModelScope.launch {
            preferences.updateUserId(userId)
        }
    }

    fun setGenderList(genderList: List<RegisterDetails.Gender>) {
        Log.d(TAG, "setGenderList: ${genderList}")
        _genderList.value = genderList
    }

    fun setRegisterDetails(regDetails: RegisterDetails) {
        _registerDetails.value = regDetails
        updateUserTypeDatastore()
    }


    //getter
    fun getRegisterDetails() {
        viewModelScope.launch {
            repository.getRegisterDetails()
                .onEach { dataState ->
                    _dataState.value = dataState
                }
                .launchIn(viewModelScope)
        }
    }

    fun getCountry() {
        viewModelScope.launch {
            repository.getCountry()
                .onEach { dataState ->
                    _countryState.value = dataState
                }
                .launchIn(viewModelScope)
        }
    }

    fun getState(country: String) {
        Log.d(TAG, "getState: $country")
        viewModelScope.launch {
            repository.getState(country)
                .onEach { _stateState.value = it }
                .launchIn(viewModelScope)
        }
    }

    fun getCity(state: String) {
        Log.d(TAG, "getCity: $state")
        viewModelScope.launch {
            repository.getCity(state)
                .onEach { _cityState.value = it }
                .launchIn(viewModelScope)
        }
    }

    fun getUserType(freelancer: Boolean): Int {
        var itemId = 0
        if (freelancer) {
            itemId = userTypes.filter { data -> data.user_type_code.equals("FREELANCER", true) }
                .single().user_type_id
        } else {
            itemId = userTypes.filter { data -> data.user_type_code.equals("CLIENT", true) }
                .single().user_type_id
        }
        Log.d(TAG, "getUserType: $itemId")
        return itemId
    }

    var isFreelancer = state.get<Boolean>("isFreelancer") ?: false
        set(value) {
            field = value
            state.set("isFreelancer", value)
        }

    var userType = state.get<String>("userType") ?: ""
        set(value) {
            field = value
            state.set("userType", value)
        }

    var firstName = state.get<String>("firstName") ?: ""
        set(value) {
            field = value
            state.set("firstName", value)
        }

    var lastName = state.get<String>("lastName") ?: ""
        set(value) {
            field = value
            state.set("lastName", value)
        }

    var emailId = state.get<String>("emailId") ?: ""
        set(value) {
            field = value
            state.set("emailId", value)
        }

    var password = state.get<String>("password") ?: ""
        set(value) {
            field = value
            state.set("password", value)
        }

    var country = state.get<String>("country") ?: ""
        set(value) {
            field = value
            state.set("country", value)
        }

    var countryId = state.get<String>("countryId") ?: ""
        set(value) {
            field = value
            state.set("countryId", value)
        }

    var stateStr = state.get<String>("stateStr") ?: ""
        set(value) {
            field = value
            state.set("stateStr", value)
        }
    var stateId = state.get<String>("stateId") ?: ""
        set(value) {
            field = value
            state.set("stateId", value)
        }

    var cityStr = state.get<String>("cityStr") ?: ""
        set(value) {
            field = value
            state.set("cityStr", value)
        }
    var cityId = state.get<String>("cityId") ?: ""
        set(value) {
            field = value
            state.set("cityId", value)
        }

    var gender = state.get<String>("gender") ?: ""
        set(value) {
            field = value
            state.set("gender", value)
        }
    var genderId = state.get<String>("genderId") ?: ""
        set(value) {
            field = value
            state.set("genderId", value)
        }

    var dob = state.get<String>("dob") ?: ""
        set(value) {
            field = value
            state.set("dob", value)
        }

    var mobile = state.get<String>("mobile") ?: ""
        set(value) {
            field = value
            state.set("mobile", value)
        }

    var countryCodeDisplay = state.get<String>("country_code_display") ?: ""
        set(value) {
            field = value
            state.set("country_code_display", value)
        }

    var countryCode = state.get<String>("country_code") ?: ""
        set(value) {
            field = value
            state.set("country_code", value)
        }

    private val registerTaskEventChannel = Channel<RegisterTaskEvent>()
    val registerTaskEvent = registerTaskEventChannel.receiveAsFlow()

    fun onGenderClick() = viewModelScope.launch {
        registerTaskEventChannel.send(
            RegisterTaskEvent.ShowGenderPopup(
                genderList = _genderList.value!!
            )
        )
    }

    fun onNextClick() {
        Log.d(TAG, "onNextClick: $isFreelancer")
        if (firstName.isBlank()) {
            showInvalidInputMessage("FirstName cannot be empty")
            return
        }
        if (lastName.isBlank()) {
            showInvalidInputMessage("LastName cannot be empty")
            return
        }
        if (emailId.isBlank()) {
            showInvalidInputMessage("Email ID cannot be empty")
            return
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches()) {
            showInvalidInputMessage("Email ID should be valid")
            return
        }
        if (password.isBlank()) {
            showInvalidInputMessage("Password cannot be empty")
            return
        }
        if (country.isBlank()) {
            showInvalidInputMessage("Country cannot be empty")
            return
        }
        if (cityStr.isBlank()) {
            showInvalidInputMessage("City cannot be empty")
            return
        }
        if (gender.isBlank()) {
            showInvalidInputMessage("Gender cannot be empty")
            return
        }
        if (dob.isBlank()) {
            showInvalidInputMessage("Date of birth cannot be empty")
            return
        }

        if (mobile.isBlank()) {
            showInvalidInputMessage("Mobile cannot be empty")
            return
        }

        if(mobile.length<8){
            showInvalidInputMessage("Mobile number cannot be less than 8 numbers")
            return
        }


        params = RegisterParams(
            firstName = firstName,
            lastName = lastName,
            email = emailId,
            password = password,
            county_id = countryId,
            gender_id = genderId,
            dob = serverDateFormate(dob),
            mobile_no = countryCode + mobile.replace(" ",""),
            city_id = cityId
        )

        Log.d(TAG, "onNextClick: $params")

        register(params!!)
    }

    private fun register(params: RegisterParams) {
        viewModelScope.launch {
            repository.registerClient(userType, params)
                .onEach { dataState ->
                    _registerState.value = dataState
                }
                .launchIn(viewModelScope)
        }
    }

    private fun sendDataToSkills() = viewModelScope.launch {
        registerTaskEventChannel.send(RegisterTaskEvent.NavigateToSkillsWithResult(if (isFreelancer) 0 else 1))
    }

    private fun showInvalidInputMessage(msg: String) = viewModelScope.launch {
        registerTaskEventChannel.send(RegisterTaskEvent.ShowInvalidInputMessage(msg))
    }

    //view click from register landing screen
    fun onLoginClicked() = viewModelScope.launch {
        registerTaskEventChannel.send(RegisterTaskEvent.NavigateToLogin)

    }

    fun moveToChooseSkillsScreen(userId: String) {
        viewModelScope.launch {
            preferences.updateUserId(userId)
            if (firstName.isNotEmpty())
                preferences.updateUsername(firstName)
        }
        sendDataToSkills()
    }

    sealed class RegisterTaskEvent {
        data class ShowInvalidInputMessage(val msg: String) : RegisterTaskEvent()
        data class NavigateToSkillsWithResult(val tabPosition: Int) :
            RegisterTaskEvent()

        data class ShowGenderPopup(val genderList: List<RegisterDetails.Gender>) :
            RegisterTaskEvent()

        object NavigateToLogin : RegisterTaskEvent()
    }

}