package com.nssfreelance.ui.register

import `in`.galaxyofandroid.spinerdialog.SpinnerDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.nssfreelance.R
import com.nssfreelance.data.City
import com.nssfreelance.data.Country
import com.nssfreelance.data.RegisterDetails
import com.nssfreelance.data.State
import com.nssfreelance.databinding.FragmentRegisterFreelancerBinding
import com.nssfreelance.ui.register.RegisterViewmodel.RegisterTaskEvent
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_register_freelancer.*
import kotlinx.coroutines.flow.collect
import java.util.*

private const val TAG = "RegisterFragment"

@AndroidEntryPoint
class RegisterFragment : Fragment(R.layout.fragment_register_freelancer) {

    private val viewmodel: RegisterViewmodel by viewModels()

    private var _binding: FragmentRegisterFreelancerBinding? = null
    private val binding get() = _binding!!

    private val args: RegisterFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentRegisterFreelancerBinding.bind(view)

        viewmodel.userType = args.userTypeId.toString()

        binding.apply {
            //setvalues
            etFirstName.setText(viewmodel.firstName)
            etLastName.setText(viewmodel.lastName)
            etEmailId.setText(viewmodel.emailId)
            etPassword.setText(viewmodel.password)
            etCountry.setText(viewmodel.country)
            etState.setText(viewmodel.stateStr)
            etCity.setText(viewmodel.cityStr)
            etGender.setText(viewmodel.gender)
            etDateOfBirth.setText(viewmodel.dob)
            etMobileNo.setText(viewmodel.mobile)
            tvLblCountryCode.setText(viewmodel.countryCodeDisplay)
            tvToolbar.text = args.title


            etFirstName.addTextChangedListener {
                viewmodel.firstName = it.toString()
            }

            etLastName.addTextChangedListener {
                viewmodel.lastName = it.toString()
            }

            etEmailId.addTextChangedListener {
                viewmodel.emailId = it.toString()
            }

            etPassword.addTextChangedListener {
                viewmodel.password = it.toString()
            }

            etMobileNo.addTextChangedListener {
                viewmodel.mobile = it.toString()
            }

            etState.setOnClickListener {
                if (viewmodel.countryId.isNotBlank() && viewmodel.stateId.isNullOrBlank()) {
                    viewmodel.getState(viewmodel.countryId)
                } else {
                    displayError(requireView(), "Kindly select country")
                }
            }

            etCity.setOnClickListener {
                if (viewmodel.stateId.isNotBlank() && viewmodel.cityId.isNullOrBlank()) {
                    viewmodel.getCity(viewmodel.stateId)
                } else {
                    displayError(requireView(), "Kindly select state")
                }
            }

            etGender.setOnClickListener {
                viewmodel.onGenderClick()
            }

            etDateOfBirth.setOnClickListener {
                val calendar = Calendar.getInstance()
                val selectedDate: Date
                val strDate: String = etDateOfBirth.text.toString().trim()
                if (strDate.equals("")) {
                    calendar.add(Calendar.YEAR, -20)
                    selectedDate = calendar.time
                } else {
                    // convert string to date
                    selectedDate = updatedDateStrToDate(strDate)
                }
                showDatePickerDialog(
                    requireContext(),
                    object : DateTimePickerResponse {
                        override fun okayPressed(date: Date?) {
                            date?.let { date1 ->
                                etDateOfBirth.setText(showDobDateFormate(date1))
                                viewmodel.dob = showDobDateFormate(date1)
                            }
                        }

                    }, selectedDate, true
                )
            }

            btnNextRegister.setOnClickListener {
                Log.d(TAG, "onViewCreated: ${viewmodel.mobile}")
                viewmodel.onNextClick()
            }

            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.registerTaskEvent.collect { event ->
                when (event) {
                    is RegisterTaskEvent.ShowInvalidInputMessage -> {
                        Snackbar.make(requireView(), event.msg, Snackbar.LENGTH_LONG).show()
                    }
                    is RegisterTaskEvent.NavigateToSkillsWithResult -> {
                        val action: NavDirections =
                            RegisterFragmentDirections.actionRegisterFragmentToOTPFragment(
                                viewmodel.userType,
                                "${viewmodel.countryCode} ${viewmodel.mobile}"
                            )
                        findNavController().navigate(action)
                    }
                    is RegisterTaskEvent.ShowGenderPopup -> {
                        if (event.genderList.isNotEmpty()) {
                            val genderLis: ArrayList<String> = arrayListOf()
                            for (data in event.genderList) {
                                genderLis.add(data.gender)
                            }
                            showSingleChoiceDialog(
                                requireContext(),
                                "Choose Gender",
                                genderLis,
                                object : OnSingleChoiceItemClick {
                                    override fun onSubmit(selectedItem: String, position: Int) {
                                        binding.etGender.setText(selectedItem)
                                        viewmodel.gender = selectedItem
                                        viewmodel.genderId =
                                            "${event.genderList[position].gender_id}"
                                    }
                                })

                        } else {
                            displayError("Gender list is empty")
                        }
                    }
                    is RegisterTaskEvent.NavigateToLogin -> {
                    }
                }.exhaustive
            }
        }

        subscribeObservers()
        viewmodel.getRegisterDetails()
        viewmodel.getCountry()

    }


    private fun subscribeObservers() {
        viewmodel.dataState.observe(viewLifecycleOwner, Observer { dataState ->
            when (dataState) {
                is DataState.Success<RegisterDetails> -> {
                    displayProgressbar(false)
                    appendToView(dataState.data)
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(dataState.exception.message)
                }
                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }
        })

        viewmodel.countryState.observe(viewLifecycleOwner, { countryState ->
            when (countryState) {
                is DataState.Success<List<Country>> -> {
                    displayProgressbar(false)
                    appendCountry(countryState.data)
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(countryState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${countryState.exception.printStackTrace()}")
                }
                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }.exhaustive
        })

        viewmodel.stateState.observe(viewLifecycleOwner, { stateState ->
            when (stateState) {
                is DataState.Success<List<State>> -> {
                    displayProgressbar(false)
                    appendState(stateState.data)
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(stateState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${stateState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    displayProgressbar(true)
                }
            }.exhaustive
        })

        viewmodel.cityState.observe(viewLifecycleOwner, { stateState ->
            when (stateState) {
                is DataState.Success<List<City>> -> {
                    displayProgressbar(false)
                    appendCity(stateState.data)
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(stateState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${stateState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    displayProgressbar(true)
                }
            }.exhaustive
        })

        viewmodel.registerState.observe(viewLifecycleOwner, { registerState ->
            when (registerState) {
                is DataState.Success -> {
                    displayProgressbar(false)
                    displayError(registerState.data.message)
                    if (registerState.data.status) {
                        viewmodel.moveToChooseSkillsScreen(registerState.data.user_id.toString())
                    } else {
                        //something went wrong
                        displayError(registerState.data.message)
                    }
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(registerState.exception.message)
                }
                DataState.Loading -> {
                    displayProgressbar(true)
                }
            }.exhaustive
        })
    }

    private fun appendCity(data: List<City>) {
        var spinnerDialog: SpinnerDialog? = null
        if (data.isNotEmpty()) {
            val cities = arrayListOf<String>()
            for (item in data) {
                cities.add(item.city_name)
            }
            spinnerDialog = SpinnerDialog(
                requireActivity(),
                cities,
                "Choose City",
                R.style.DialogAnimations_SmileWindow
            )
            spinnerDialog.setCancellable(false)
            spinnerDialog.setShowKeyboard(false)
            spinnerDialog.setUseContainsFilter(true)
            spinnerDialog.bindOnSpinerListener { item, position ->
                etCity.setText(item)
                viewmodel.cityStr = item
                viewmodel.cityId = "${data[position].city_id}"
            }
            //spinnerDialog.showSpinerDialog()
        }
        binding.etCity.setOnClickListener {
            spinnerDialog?.let {
                it.showSpinerDialog()
            }
        }
    }

    private fun appendState(data: List<State>) {
        var spinnerDialog: SpinnerDialog? = null
        if (data.isNotEmpty()) {
            val states = arrayListOf<String>()
            for (item in data) {
                states.add(item.state_name)
            }
            spinnerDialog = SpinnerDialog(
                requireActivity(),
                states,
                "Choose State",
                R.style.DialogAnimations_SmileWindow
            )
            spinnerDialog.setCancellable(false)
            spinnerDialog.setShowKeyboard(false)
            spinnerDialog.setUseContainsFilter(true)
            spinnerDialog.bindOnSpinerListener { item, position ->
                etState.setText(item)
                viewmodel.stateStr = item
                viewmodel.stateId = "${data[position].state_id}"
                viewmodel.getCity(viewmodel.stateId)
            }
            //spinnerDialog.showSpinerDialog()
        }
        binding.etState.setOnClickListener {
            spinnerDialog?.let {
                it.showSpinerDialog()
            }
        }

    }


    private fun appendCountry(data: List<Country>) {
        binding.etCountry.setOnClickListener {
            if (data.isNotEmpty()) {
                val countries = arrayListOf<String>()
                for (item in data) {
                    countries.add(item.country_name)
                }
                val spinnerDialog = SpinnerDialog(
                    requireActivity(),
                    countries,
                    "Choose Country",
                    R.style.DialogAnimations_SmileWindow
                )
                spinnerDialog.setCancellable(false)
                spinnerDialog.setShowKeyboard(false)
                spinnerDialog.setUseContainsFilter(true)
                spinnerDialog.bindOnSpinerListener { item, position ->
                    etCountry.setText(item)
                    viewmodel.country = item
                    viewmodel.countryId = "${data[position].country_id}"
                    val countryCodes = data[position].country_code.split("@")
                    binding.tvLblCountryCode.text = countryCodes[0]
                    viewmodel.countryCodeDisplay = countryCodes[0]
                    viewmodel.countryCode = countryCodes[1]
                    Log.d(TAG, "appendCountry: ${data[position].country_id}  mobile -- ${viewmodel.mobile}")
                    viewmodel.getCity("${data[position].country_id}")
                }
                spinnerDialog.showSpinerDialog()
            }
        }
    }

    private fun appendToView(data: RegisterDetails) {
        viewmodel.setGenderList(data.gender)
        viewmodel.setRegisterDetails(data)
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
        } else {
            Snackbar.make(requireView(), "Unknown error", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun displayProgressbar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}