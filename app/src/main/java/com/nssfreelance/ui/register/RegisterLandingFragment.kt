package com.nssfreelance.ui.register

import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.nssfreelance.R
import com.nssfreelance.data.RegisterDetails
import com.nssfreelance.databinding.FragmentRegisterLandingBinding
import com.nssfreelance.util.DataState
import com.nssfreelance.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_register_landing.*
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class RegisterLandingFragment : Fragment(R.layout.fragment_register_landing) {

    private var _binding: FragmentRegisterLandingBinding? = null
    private val binding get() = _binding!!

    private val viewmodel: RegisterViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentRegisterLandingBinding.bind(view)

        //click operations
        binding.apply {
            btnFreelanceSignup.setOnClickListener {
                val action = RegisterLandingFragmentDirections.actionRegisterLandingToRegisterFragment(viewmodel.getUserType(true),getString(R.string.registering_as_freelancer))
                findNavController().navigate(action)
            }

            btnClientSignup.setOnClickListener {
                val action = RegisterLandingFragmentDirections.actionRegisterLandingToRegisterFragment(viewmodel.getUserType(false),getString(R.string.registering_as_client))
                findNavController().navigate(action)
            }


            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

        }

        setSpannableSignInStr()

        subscribeObservers()
        viewmodel.getRegisterDetails()
    }

    private fun subscribeObservers() {
        viewmodel.dataState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success<RegisterDetails> -> {
                    displayProgressbar(false)
                    viewmodel.setRegisterDetails(dataState.data)
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(dataState.exception.message)
                }
                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }
        })


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.registerTaskEvent.collect { event ->
                when (event) {
                    is RegisterViewmodel.RegisterTaskEvent.NavigateToLogin -> {
                        val action = RegisterLandingFragmentDirections.actionGlobalAuthTabsFragment("Sign In")
                        findNavController().navigate(action)
                    }
                    is RegisterViewmodel.RegisterTaskEvent.ShowInvalidInputMessage -> {

                    }
                    is RegisterViewmodel.RegisterTaskEvent.NavigateToSkillsWithResult -> {

                    }
                    is RegisterViewmodel.RegisterTaskEvent.ShowGenderPopup -> {


                    }
                }.exhaustive
            }
        }
    }

    private fun setSpannableSignInStr() {
        val signin = getString(R.string.already_registered_sign_in)

        val spannableString = SpannableString(signin)
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                viewmodel.onLoginClicked()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                    ds.underlineColor =
                        ContextCompat.getColor(requireContext(), R.color.text_white_color)
                }
                ds.color = ContextCompat.getColor(requireContext(), R.color.text_white_color)
            }
        }

        spannableString.setSpan(clickableSpan, 20, 27, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        tvLblAlreadyRegisteredSignIn.text = spannableString
        tvLblAlreadyRegisteredSignIn.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
        } else {
            Snackbar.make(requireView(), "Unknown error", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun displayProgressbar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}