package com.nssfreelance.ui.job_details_client

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentUnlockFreelancerBinding
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "UnlockFreelancerDialogF"

@AndroidEntryPoint
class UnlockFreelancerDialogFragment : DialogFragment() {
    private lateinit var binding: FragmentUnlockFreelancerBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        Log.d(TAG, "Oncreate")

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater

            binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_unlock_freelancer, null, false
            )
            setProperties(savedInstanceState)
            builder.setView(binding.root)
            builder.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        binding = FragmentUnlockFreelancerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")
        setProperties(savedInstanceState)
    }

    private fun setProperties(savedInstanceState: Bundle?) {
        Log.d(TAG, "setProperties")
        val margin: Int = requireContext().resources.getDimension(R.dimen.dp_20).toInt()
        val back: ColorDrawable = ColorDrawable(Color.TRANSPARENT)
        val inset: InsetDrawable = InsetDrawable(back, margin)
        dialog?.window?.setBackgroundDrawable(inset)

        binding.apply {
            ivClose.setOnClickListener {
                dialog?.dismiss()
            }
            btnMakePayment.setOnClickListener {
                val action =
                    UnlockFreelancerDialogFragmentDirections.actionUnlockFreelancerDialogFragment2ToCheckoutPaymentFragment()
                findNavController().navigate(action)
            }
        }
    }
}