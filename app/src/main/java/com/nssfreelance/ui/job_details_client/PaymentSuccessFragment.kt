package com.nssfreelance.ui.job_details_client

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentShowSuccessMessageBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PaymentSuccessFragment:Fragment(R.layout.fragment_show_success_message) {
    private var _binding: FragmentShowSuccessMessageBinding?=null
    private val binding
        get() = _binding!!

    private val viewmodel:JobDetailsClientViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentShowSuccessMessageBinding.bind(view)

        binding.apply {
            tvSuccessTitle.text = "Payment Successful!"
            tvSuccessContent.text = "Your payment is secured! we will release the payment only after job is completed."
            btnSuccess.text="Done"
            btnSuccess.setOnClickListener {
                val action = PaymentSuccessFragmentDirections.actionPaymentSuccessFragmentToJobDetailsClientTabFragment(viewmodel.jobId, viewmodel.clientJobType)
                findNavController().navigate(action)
            }

            imageviewBack.setOnClickListener {
                val action = PaymentSuccessFragmentDirections.actionPaymentSuccessFragmentToJobDetailsClientTabFragment(viewmodel.jobId, viewmodel.clientJobType)
                findNavController().navigate(action)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }
}