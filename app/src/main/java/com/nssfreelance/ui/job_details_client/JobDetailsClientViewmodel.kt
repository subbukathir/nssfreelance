package com.nssfreelance.ui.job_details_client

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.ClientJob
import com.nssfreelance.data.Country
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.request.ContractRequestParams
import com.nssfreelance.retrofit.request.PaymentSecretParams
import com.nssfreelance.retrofit.request.StorePaymentParams
import com.nssfreelance.retrofit.request.UpdateJobStatusParams
import com.nssfreelance.retrofit.response.*
import com.nssfreelance.util.Const
import com.nssfreelance.util.DataState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class JobDetailsClientViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    private val preferences: PreferencesManager,
    @Assisted private val state: SavedStateHandle,
) : ViewModel() {

    val preferencesFlow = preferences.preferencesFlow

    var userId = ""
    var userToken = ""
    var jobId: Int = 0
    var isPaid: Boolean = false
    var updateJobStatus: UpdateJobStatusParams? = null
    var clientJob: ClientJob? = null
    var paymentIntentSecret = ""
    var freelancerId = ""
    var clientJobType =""
    var isHired: Boolean = false
    var triggerCount = 0

    //job lists
    var appliedJobList: List<ClientJob> = mutableListOf()
    var rejectedJobList: List<ClientJob> = mutableListOf()
    var shortlistedJobList: List<ClientJob> = mutableListOf()
    val payment: MutableLiveData<Boolean> = MutableLiveData()

    private val _appliedDatastate: MutableLiveData<DataState<AllClientJobResponse>> =
        MutableLiveData()
    val appliedDatastate: LiveData<DataState<AllClientJobResponse>>
        get() = _appliedDatastate

    private val _rejectedDatastate: MutableLiveData<DataState<AllClientJobResponse>> =
        MutableLiveData()
    val rejectedDatastate: LiveData<DataState<AllClientJobResponse>>
        get() = _rejectedDatastate


    private val _shortlistedDatastate: MutableLiveData<DataState<AllClientJobResponse>> =
        MutableLiveData()
    val shortlistedDatastate: LiveData<DataState<AllClientJobResponse>>
        get() = _shortlistedDatastate


    private val _jobDetailsDatastate: MutableLiveData<DataState<ClientJobDetailReponse>> =
        MutableLiveData()
    val jobDetailsDatastate: LiveData<DataState<ClientJobDetailReponse>>
        get() = _jobDetailsDatastate

    private val _updateJobStatusDatastate: MutableLiveData<DataState<LoginResponse>> =
        MutableLiveData()
    val updateJobStatusDatastate: LiveData<DataState<LoginResponse>>
        get() = _updateJobStatusDatastate


    private val _paymentSuccessDatastate: MutableLiveData<DataState<LoginResponse>> =
        MutableLiveData()
    val paymentSuccessDatastate: LiveData<DataState<LoginResponse>>
        get() = _paymentSuccessDatastate


    private val _paymentSecretDatastate: MutableLiveData<DataState<PaymentSecretResponse>> =
        MutableLiveData()
    val paymentSecretDatastate: LiveData<DataState<PaymentSecretResponse>>
        get() = _paymentSecretDatastate

    private val _countryState: MutableLiveData<DataState<List<Country>>> = MutableLiveData()
    val countryState: LiveData<DataState<List<Country>>>
        get() = _countryState

    //Contract details
    private val _updateContractDatastate: MutableLiveData<DataState<ContractDetailResponse>> =
        MutableLiveData()
    val updateContractDatastate: LiveData<DataState<ContractDetailResponse>>
        get() = _updateContractDatastate

    fun getAppliedJobs() {
        viewModelScope.launch {
            repository.getClientJobsByUrl(userToken, jobId.toString(), Const.APPLICATIONS)
                .onEach { dataState -> _appliedDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun getRejectedJobs() {
        viewModelScope.launch {
            repository.getClientJobsByUrl(userToken, jobId.toString(), Const.REJECTED_JOB)
                .onEach { dataState -> _rejectedDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun getShortlistedJobs() {
        viewModelScope.launch {
            repository.getClientJobsByUrl(userToken, jobId.toString(), Const.SHORTLISTED_JOB)
                .onEach { dataState -> _shortlistedDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun getJobDetails() {
        viewModelScope.launch {
            repository.getClientJobsDetails(userToken, jobId = jobId.toString(), Const.JOB_DETAILS)
                .onEach { dataState -> _jobDetailsDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun getContractDetails(params: ContractRequestParams) {
        viewModelScope.launch {
            repository.requestContractDetail(userToken, params)
                .onEach { dataState -> _updateContractDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun getCountry() {
        viewModelScope.launch {
            repository.getCountry()
                .onEach { dataState ->
                    _countryState.value = dataState
                }
                .launchIn(viewModelScope)
        }
    }

    var jobType = state.get<String>("jobType") ?: ""
        set(value) {
            field = value
            state.set("jobType", value)
        }

    var jobFinalPrice = state.get<String>("jobFinalPrice") ?: ""
        set(value) {
            field = value
            state.set("jobFinalPrice", value)
        }

    //checkout params

    var country = state.get<String>("country") ?: ""
        set(value) {
            field = value
            state.set("country", value)
        }

    fun onCreateContractClick(): Boolean {
        if (jobType.equals("Choose Type", true)) {
            displayErrorMsg("Choose job type")
            return false
        } else if (jobFinalPrice.isNullOrEmpty()) {
            displayErrorMsg("Enter offer price")
            return false
        } else if (jobFinalPrice.replace("$", "").toInt() == 0) {
            displayErrorMsg("Enter proper offer price")
            return false
        }
        return true
    }

    private val contractTaskChannel = Channel<JobContractTask>()
    val jobContractEvent = contractTaskChannel.receiveAsFlow()

    private fun displayErrorMsg(msg: String) {
        viewModelScope.launch {
            contractTaskChannel.send(JobContractTask.ShowErrorMessage(msg))
        }
    }

    fun updateStatus(params: UpdateJobStatusParams) {
        viewModelScope.launch {
            repository.updateClientJobStatus(userToken, params)
                .onEach { dataState -> _updateJobStatusDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }


    fun getPaymentSecret() {
        val items = PaymentSecretParams.PaymentParams(
            "USD",
            5
        )
        val params = PaymentSecretParams(items)
        viewModelScope.launch {
            repository.getPaymentIntentSecret(userToken, params)
                .onEach { dataState -> _paymentSecretDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun updatePaymentSuccess(params: StorePaymentParams) {
        viewModelScope.launch {
            repository.storePaymentSuccess(userToken, params)
                .onEach { dataState -> _paymentSuccessDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun updateIsPaid(isPaid: Boolean) {
        viewModelScope.launch {
            preferences.updateIsPaid(isPaid)
        }
    }

    private val jobDetailsTaskChannel = Channel<JobDetailsDataTask>()
    val jobDetailsEvent = jobDetailsTaskChannel.receiveAsFlow()

    //payment process channel
    private val paymentTaskChannel = Channel<PaymentDataTask>()
    val paymentEvent = paymentTaskChannel.receiveAsFlow()

    fun onHireClicked() {
        viewModelScope.launch {
            jobDetailsTaskChannel.send(JobDetailsDataTask.NavigateToCreateContract(clientJob!!,clientJobType))
        }
    }

    fun onContactFreelancerClicked() {
        viewModelScope.launch {
            jobDetailsTaskChannel.send(JobDetailsDataTask.NavigateToMakePayment)
        }
    }

    fun onViewFreelancer() {
        viewModelScope.launch {
            jobDetailsTaskChannel.send(JobDetailsDataTask.NavigateToFreelancer)
        }
    }

    fun onPaymentSuccess() {
        viewModelScope.launch {
            paymentTaskChannel.send(PaymentDataTask.NavigateToPaymentSuccess)
        }
    }

    fun onPaymentFailed(msg: String) {
        viewModelScope.launch {
            paymentTaskChannel.send(PaymentDataTask.ShowErrorMessage(msg))
        }
    }

    fun getJobTypeId(): Int {
        if (jobType == "") return 10
        else {
            if (jobType == "Fixed") return 10
            else if (jobType == "Per Hour") return 11
            else return 12
        }
    }


    sealed class JobDetailsDataTask {
        object NavigateToMakePayment : JobDetailsDataTask()
        data class NavigateToCreateContract(val clientJob: ClientJob, val bidPrice:String) : JobDetailsDataTask()
        object NavigateToFreelancer : JobDetailsDataTask()
    }

    sealed class PaymentDataTask {
        object NavigateToPaymentSuccess : PaymentDataTask()
        data class ShowErrorMessage(val msg: String) : PaymentDataTask()
    }

    sealed class JobContractTask {
        data class ShowErrorMessage(val msg: String) : JobContractTask()
    }

}