package com.nssfreelance.ui.job_details_client

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentCreateContractSuccessBinding
import com.nssfreelance.util.displayError
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "ContractCreatedSuccessf"
@AndroidEntryPoint
class ContractCreatedSuccessfullyFragment : DialogFragment() {
    private lateinit var binding: FragmentCreateContractSuccessBinding

    private val viewmodel:JobDetailsClientViewmodel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        Log.d(TAG, "Oncreate")

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater

            binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_create_contract_success, null, false
            )
            setProperties(savedInstanceState)
            builder.setView(binding.root)
            builder.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        binding = FragmentCreateContractSuccessBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")
        setProperties(savedInstanceState)
    }

    private fun setProperties(savedInstanceState: Bundle?) {
        Log.d(TAG, "setProperties")
        val margin: Int = requireContext().resources.getDimension(R.dimen.dp_20).toInt()
        val back: ColorDrawable = ColorDrawable(Color.TRANSPARENT)
        val inset: InsetDrawable = InsetDrawable(back, margin)
        dialog?.window?.setBackgroundDrawable(inset)

        binding.apply {
            btnBack.setOnClickListener {
                findNavController().popBackStack()
            }

            btnView.setOnClickListener {
                val action = ContractCreatedSuccessfullyFragmentDirections.actionContractCreatedSuccessfullyFragmentToCloseContractFragment(
                    viewmodel.clientJob!!
                )
                findNavController().navigate(action)
            }
        }
    }
}