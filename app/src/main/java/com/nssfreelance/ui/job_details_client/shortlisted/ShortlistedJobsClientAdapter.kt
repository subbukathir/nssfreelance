package com.nssfreelance.ui.job_details_client.shortlisted

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.ClientJob
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.ItemJobAppliedClientBinding
import com.nssfreelance.databinding.ItemJobShortlistedClientBinding
import com.nssfreelance.ui.job_details_client.JobDetailsClientViewmodel
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency
import com.nssfreelance.util.hide
import com.nssfreelance.util.show

class ShortlistedJobsClientAdapter(
    private val listener: OnItemClickListener,
    private val viewmodel: JobDetailsClientViewmodel,
    private val owner: LifecycleOwner
) :
    ListAdapter<ClientJob, ShortlistedJobsClientAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding =
            ItemJobShortlistedClientBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding: ItemJobShortlistedClientBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.apply {
                llTextJobRole.setOnClickListener {
//                    listener.onFreelancerDetailsClick(getItem(adapterPosition))
                }
                tvItemContactFreelancer.setOnClickListener {
                    listener.onContactFreelancerClick(getItem(adapterPosition))
                }

                llFreelancerDetail.setOnClickListener {
                    listener.onFreelancerDetailsClick(getItem(adapterPosition))
                }

                tvItemHire.setOnClickListener {
                    listener.onHireClick(getItem(adapterPosition))
                }

                tvItemReject.setOnClickListener {
                    listener.onRejectClick(getItem(adapterPosition))
                }
            }

        }

        fun bind(job: ClientJob) {
            binding.apply {
                tvItemJobPrice.text =
                    convertToCurrency("${job.bid_price ?: "0"}", binding.root.context)
                tvItemJobType.text = "Freelancer Price(${job.job_type})"
                tvItemJoinedDate.text = "${commonDateFormate(job.joined_on)}"

                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemJobRate.text =
                    convertToCurrency("${job.bid_price ?: "0"}", binding.root.context)
                tvItemAppliedDate.text = "${commonDateFormate(job.applied_on)}"
                tvItemSuccessfulJobs.text = "${job.completed_job_count}"
                tvProfileRating.text = "${job.rating_count}"

                if (job.rating_count == 0) llRating.hide()
                else {
                    llRating.show()
                    if(job.rating!=null) ratingBar.rating = job.rating?.toFloat()!!
                }

                    Glide.with(root.context)
                        .load(job.profile_url)
                        .placeholder(R.drawable.ic_placeholder)
                        .error(R.drawable.ic_placeholder)
                        .into(imageviewJobType)

                viewmodel.payment.observe(owner, Observer { data ->
                    if (data) {
                        tvItemContactFreelancer.show()
                        llFreelancerDetail.hide()
                        tvItemName.text = "${job.first_name} ${job.last_name}"
                    } else {
                        llFreelancerDetail.show()
                        tvItemContactFreelancer.hide()
                        tvItemName.text = "${job.first_name} ${job.last_name.first()}"
                        Glide.with(root.context)
                            .load(R.drawable.ic_placeholder)
                            .into(imageviewJobType)
                    }
                })


            }
        }
    }


    interface OnItemClickListener {
        fun onFreelancerDetailsClick(job: ClientJob)
        fun onContactFreelancerClick(job: ClientJob)
        fun onRejectClick(job: ClientJob)
        fun onHireClick(job: ClientJob)
    }

    class DiffUtilCallback : DiffUtil.ItemCallback<ClientJob>() {
        override fun areItemsTheSame(oldItem: ClientJob, newItem: ClientJob): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: ClientJob, newItem: ClientJob): Boolean {
            return oldItem == newItem
        }
    }
}