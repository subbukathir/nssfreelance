package com.nssfreelance.ui.job_details_client.rejected

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.ClientJob
import com.nssfreelance.databinding.ItemJobRejectedClientBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency
import com.nssfreelance.util.hide
import com.nssfreelance.util.show

class RejectedJobsClientAdapter(private val listener: OnItemClickListener) :
    ListAdapter<ClientJob, RejectedJobsClientAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding =
            ItemJobRejectedClientBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding: ItemJobRejectedClientBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.apply {
                tvItemUndoRejection.setOnClickListener {
                    listener.onUndoRejectClick(getItem(adapterPosition))
                }
            }
        }

        fun bind(job: ClientJob) {
            binding.apply {
                tvItemJobPrice.text =
                    convertToCurrency("${job.job_price ?: "0"}", binding.root.context)
                tvItemName.text = "${job.first_name} ${job.last_name}"
                tvItemLocation.text = "${job.city_name} | ${job.country}"
                tvItemJobRate.text =
                    convertToCurrency("${job.job_price ?: "0"}", binding.root.context)
                tvItemAppliedDate.text = "${commonDateFormate(job.applied_on)}"
                tvItemJobPostedOn.text = "${commonDateFormate(job.job_post_created_on)}"
                tvItemSuccessfulJobs.text = "${job.completed_job_count}"
                tvProfileRating.text = "${job.rating_count}"

                if (job.rating_count == 0) llRating.hide()
                else {
                    llRating.show()
                    ratingBar.rating = if (job.rating != null) job.rating.toFloat() else 0f
                }

                Glide.with(root.context)
                    .load(job.url)
                    .placeholder(R.drawable.ic_placeholder)
                    .into(imageviewJobType)

            }
        }
    }


    interface OnItemClickListener {
        fun onUndoRejectClick(job: ClientJob)
    }

    class DiffUtilCallback : DiffUtil.ItemCallback<ClientJob>() {
        override fun areItemsTheSame(oldItem: ClientJob, newItem: ClientJob): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: ClientJob, newItem: ClientJob): Boolean {
            return oldItem == newItem
        }
    }
}