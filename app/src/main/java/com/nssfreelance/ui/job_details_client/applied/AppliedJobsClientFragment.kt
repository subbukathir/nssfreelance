package com.nssfreelance.ui.job_details_client.applied

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.ClientJob
import com.nssfreelance.databinding.FragmentJobListingsClientBinding
import com.nssfreelance.retrofit.request.UpdateJobStatusParams
import com.nssfreelance.ui.job_details_client.JobDetailsClientViewmodel
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AppliedJobsClientFragment:Fragment(R.layout.fragment_job_listings_client),AppliedJobsClientAdapter.OnItemClickListener {
    private var _binding: FragmentJobListingsClientBinding?=null
    private val binding: FragmentJobListingsClientBinding
        get() = _binding!!

    private val viewmodel: JobDetailsClientViewmodel by activityViewModels()

    private lateinit var mAdapter: AppliedJobsClientAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentJobListingsClientBinding.bind(view)

        mAdapter = AppliedJobsClientAdapter(this,viewmodel,this)

        binding.apply {
            recyclerviewJobListings.itemAnimator = DefaultItemAnimator()
            recyclerviewJobListings.adapter =mAdapter
            swipeRefresh.setOnRefreshListener {
                viewmodel.getAppliedJobs()
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.getAppliedJobs()
        }
        viewmodel.appliedDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    hideProgress()
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.triggerCount=0
                        if (dataState.data.data.isNotEmpty()) {
                            viewmodel.appliedJobList = dataState.data.data
                            mAdapter.submitList(viewmodel.appliedJobList)
                            binding.error.tvError.hide()
                            binding.recyclerviewJobListings.visibility = View.VISIBLE
                        } else {
                            binding.recyclerviewJobListings.visibility = View.GONE
                            binding.error.tvError.show()
                        }
                    } else {
                        displayError(requireView(), "Data not found")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    if(dataState.exception.message?.contains("502 Bad Gateway")==true){
                        displayError(requireView(), "Network error")
                        if (viewmodel.triggerCount >= 3) {
                        } else {
                            viewmodel.triggerCount++
                            progressBar(binding.progress.progressBar, true)
                            viewmodel.getAppliedJobs()
                            Thread.sleep(5000)
                        }
                    }else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.updateJobStatusDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.getAppliedJobs()
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    if(dataState.exception.message?.contains("502 Bad Gateway")==true){
                        displayError(requireView(), "Network error")
                        if (viewmodel.triggerCount >= 3) {
                        } else {
                            viewmodel.triggerCount++
                            progressBar(binding.progress.progressBar, true)
                            viewmodel.getAppliedJobs()
                            Thread.sleep(5000)
                        }
                    }else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }


    private fun hideProgress() {
        if(binding.swipeRefresh.isRefreshing)
            binding.swipeRefresh.isRefreshing = false
    }

    override fun onFreelancerDetailsClick(job: ClientJob) {
        viewmodel.onContactFreelancerClicked()
    }

    override fun onContactFreelancerClick(job: ClientJob) {
//        viewmodel.onContactFreelancerClicked()
        viewmodel.freelancerId = "${job.freelancer_user_id}"
        viewmodel.onViewFreelancer()
//        displayError(requireView(),"Feature is updating")
    }

    override fun onRejectClick(job: ClientJob) {
        val params = UpdateJobStatusParams(
            Const.REJECT,
            client_job_post_id = job.client_job_post_id,
            request_progress_id = job.request_progress_id,
            freelancer_user_id = job.freelancer_user_id,
            client_user_id = viewmodel.userId.toInt(),
            contract_final_amount = 0,
            contract_job_type_id = 0
        )
        viewmodel.updateStatus(params)
    }

    override fun onShortlistClick(job: ClientJob) {
        val params = UpdateJobStatusParams(
            Const.SHORTLIST,
            client_job_post_id = job.client_job_post_id,
            request_progress_id = job.request_progress_id,
            freelancer_user_id = job.freelancer_user_id,
            client_user_id = viewmodel.userId.toInt()
        )
        viewmodel.updateStatus(params)
    }

    override fun onHireClick(job: ClientJob) {
        //show create contract screen
        if(viewmodel.isPaid){
            viewmodel.clientJob = job
            viewmodel.onHireClicked()
            viewmodel.isHired = true
        }else{
            displayError(requireView(),"Need to pay for hire")
            viewmodel.onContactFreelancerClicked()
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}