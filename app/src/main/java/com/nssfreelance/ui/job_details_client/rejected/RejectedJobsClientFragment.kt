package com.nssfreelance.ui.job_details_client.rejected

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.ClientJob
import com.nssfreelance.databinding.FragmentJobListingsClientBinding
import com.nssfreelance.retrofit.request.UpdateJobStatusParams
import com.nssfreelance.ui.job_details_client.JobDetailsClientViewmodel
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RejectedJobsClientFragment:Fragment(R.layout.fragment_job_listings_client),RejectedJobsClientAdapter.OnItemClickListener {
    private var _binding: FragmentJobListingsClientBinding?=null
    private val binding: FragmentJobListingsClientBinding
        get() = _binding!!

    private val viewmodel: JobDetailsClientViewmodel by activityViewModels()

    private lateinit var mAdapter: RejectedJobsClientAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentJobListingsClientBinding.bind(view)

        mAdapter = RejectedJobsClientAdapter(this)

        binding.apply {
            recyclerviewJobListings.itemAnimator = DefaultItemAnimator()
            recyclerviewJobListings.adapter =mAdapter

            swipeRefresh.setOnRefreshListener {
                viewmodel.getRejectedJobs()
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.getRejectedJobs()
        }
        viewmodel.rejectedDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    hideProgress()
                    if (dataState.data.status) {
                        if (dataState.data.data.isNotEmpty()) {
                            viewmodel.rejectedJobList = dataState.data.data
                            mAdapter.submitList(viewmodel.rejectedJobList)
                            binding.error.tvError.hide()
                            binding.recyclerviewJobListings.visibility = View.VISIBLE
                        } else {
                            binding.recyclerviewJobListings.visibility = View.GONE
                            binding.error.tvError.show()
                        }
                    } else {
                        displayError(requireView(), "No data found")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.updateJobStatusDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.getRejectedJobs()
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }

    private fun hideProgress() {
        if(binding.swipeRefresh.isRefreshing)
            binding.swipeRefresh.isRefreshing = false
    }

    override fun onUndoRejectClick(job: ClientJob) {
        val params = UpdateJobStatusParams(
            Const.UNDO_REJECT,
            client_job_post_id = job.client_job_post_id,
            request_progress_id = job.request_progress_id,
            freelancer_user_id = job.freelancer_user_id,
            client_user_id = viewmodel.userId.toInt(),
            contract_job_type_id = job.contract_job_type_id,
            contract_final_amount = job.contract_final_amount
        )
        viewmodel.updateStatus(params)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}