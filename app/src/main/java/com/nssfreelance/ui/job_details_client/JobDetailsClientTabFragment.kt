package com.nssfreelance.ui.job_details_client

import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavArgs
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.tabs.TabLayoutMediator
import com.nssfreelance.R
import com.nssfreelance.data.Job
import com.nssfreelance.data.JobDetails
import com.nssfreelance.databinding.FragmentClientJobDetailsBinding
import com.nssfreelance.ui.job_details_client.JobDetailsClientViewmodel.JobDetailsDataTask.NavigateToCreateContract
import com.nssfreelance.ui.job_details_client.applied.AppliedJobsClientFragment
import com.nssfreelance.ui.job_details_client.rejected.RejectedJobsClientFragment
import com.nssfreelance.ui.job_details_client.shortlisted.ShortlistedJobsClientFragment
import com.nssfreelance.ui.register.ViewpagerAdapter
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class JobDetailsClientTabFragment : Fragment(R.layout.fragment_client_job_details) {
    private var _binding: FragmentClientJobDetailsBinding? = null
    private val binding
        get() = _binding!!

    private lateinit var viewpagerAdapter: ViewpagerAdapter
    private val args: JobDetailsClientTabFragmentArgs by navArgs()

    private val viewmodel: JobDetailsClientViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentClientJobDetailsBinding.bind(view)

        viewmodel.jobId = args.jobId

        viewpagerAdapter =
            ViewpagerAdapter(fragmentManager = requireActivity().supportFragmentManager, lifecycle)
        viewpagerAdapter.addFragment(AppliedJobsClientFragment())
        viewpagerAdapter.addFragment(ShortlistedJobsClientFragment())
        viewpagerAdapter.addFragment(RejectedJobsClientFragment())

        binding.apply {
            viewpagerClientJobs.adapter = viewpagerAdapter

            TabLayoutMediator(tabLayoutClientJobDetails, viewpagerClientJobs) { tab, position ->
                when (position) {
                    0 -> tab.text = "Applied"
                    1 -> tab.text = "Shortlisted"
                    2 -> tab.text = "Rejected"
                }
            }.attach()

            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.preferencesFlow.collect { prefs ->
                viewmodel.userId = prefs.userId
                viewmodel.userToken = "Bearer ${prefs.userToken}"
                viewmodel.clientJobType = args.jobType
                viewmodel.getJobDetails()
                //If the user clicks hired button and return to same screen refresh the jobs
                if(viewmodel.isHired)
                {
                    viewmodel.getAppliedJobs()
                    viewmodel.getShortlistedJobs()
                    viewmodel.isHired = false
                }
            }
        }

        viewmodel.jobDetailsDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        appendJobDetails(dataState.data.data)
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.jobDetailsEvent.collect { event ->
                when (event) {
                    JobDetailsClientViewmodel.JobDetailsDataTask.NavigateToMakePayment -> {
                        val action =
                            JobDetailsClientTabFragmentDirections.actionJobDetailsClientTabFragmentToUnlockFreelancerDialogFragment2()
                        findNavController().navigate(action)
                    }

                    JobDetailsClientViewmodel.JobDetailsDataTask.NavigateToFreelancer -> {
                        val action =
                            JobDetailsClientTabFragmentDirections.actionJobDetailsClientTabFragmentToFreelancerProfileFragment(viewmodel.freelancerId)
                        findNavController().navigate(action)
                    }
                    is NavigateToCreateContract -> {
                    val action =
                        JobDetailsClientTabFragmentDirections.actionJobDetailsClientTabFragmentToCreateContractFragment(event.clientJob, event.bidPrice)
                    findNavController().navigate(action)
                }
                }.exhaustive
            }
        }
    }

    private fun appendJobDetails(data: JobDetails) {
        binding.apply {

            tvPostedDate.text = commonDateFormate(data.job_post_created_on)
            tvFixedPrice.text = convertToCurrency(data.job_price.toString(), requireContext())
            tvJobType.text = "Client Price(${args.jobType})"
            tvJobDate.text = commonDateFormate(data.job_starts_from)
            tvJobLocation.text = "${data.city_name} | ${data.country_name}"
            tvJobTitle.text = data.job_title

            viewmodel.payment.value = data.is_payment_settled
            viewmodel.isPaid = data.is_payment_settled
            viewmodel.clientJobType = args.jobType
            if(data.is_payment_settled){
                tvPaymentStatus.text = "Paid"
                tvPaymentStatus.background = ResourcesCompat.getDrawable(resources,R.drawable.bg_green_status,null)
            }else{
                tvPaymentStatus.text = "Unpaid"
                tvPaymentStatus.background = ResourcesCompat.getDrawable(resources,R.drawable.bg_red_status,null)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}