package com.nssfreelance.ui.job_details_client

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentCreateContractBinding
import com.nssfreelance.databinding.FragmentJobApplySuccessBinding
import com.nssfreelance.databinding.FragmentUnlockFreelancerBinding
import com.nssfreelance.retrofit.request.UpdateJobStatusParams
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "CreateContractFragment"
@AndroidEntryPoint
class CreateContractFragment:Fragment(R.layout.fragment_create_contract) {
    private var _binding: FragmentCreateContractBinding?=null
    private val binding
        get() = _binding!!

    val viewmodel:JobDetailsClientViewmodel by viewModels()
    val args:CreateContractFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentCreateContractBinding.bind(view)

        Log.d(TAG, "onViewCreated: ")
        binding.apply {
            btnCreateContract.setOnClickListener {
                if(viewmodel.onCreateContractClick())
                {
                    val params = UpdateJobStatusParams(
                        Const.OFFER,
                        client_job_post_id = viewmodel.clientJob?.client_job_post_id!!,
                        request_progress_id = viewmodel.clientJob?.request_progress_id!!,
                        freelancer_user_id = viewmodel.clientJob?.freelancer_user_id!!,
                        client_user_id = viewmodel.userId.toInt(),
                        contract_final_amount = viewmodel.clientJob?.bid_price!!,
                        contract_job_type_id = viewmodel.getJobTypeId()
                    )
                    Log.d(TAG, "onViewCreated: $params")
                    viewmodel.updateStatus(params)
                }
            }

            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }
            spinnerJobType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?,
                    view: View,
                    i: Int,
                    l: Long
                ) {
                    viewmodel.jobType = spinnerJobType.selectedItem.toString()
                }

                override fun onNothingSelected(adapterView: AdapterView<*>?) {
                    return
                }
            }
            etOfferPrice.addTextChangedListener {
                viewmodel.jobFinalPrice = it.toString()

                it?.let { editable ->
                    if(editable.toString() != ""){
                        val value = editable.toString().replace("$","")
                        tvOfferedPrice.text = "Job Price ${convertToCurrency(value,requireContext())}"
                    }
                }
            }

            args.clientJob?.let { data->
                tvOriginalPrice.text ="Job Price ${convertToCurrency(data.job_price?:"0",requireContext())} | ${args.clientJobType}"
                tvOfferedPrice.text = "Job Price ${convertToCurrency(data.bid_price.toString()?:"0",requireContext())} | ${data.job_type}"
//                etOfferPrice.setText("${convertToCurrency(data.job_price?:"0",requireContext())}")

                tvFreelancerName.text = "${data.first_name} ${data.last_name}"
                tvFreelancerLocation.text = "${data.city_name} | ${data.country_name}"

                tvClientLocation.text = "${data.city_name} | ${data.country_name}"
                viewmodel.jobFinalPrice=data.bid_price.toString()?:"0"
            }

        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        Log.d(TAG, "subscribeObservers: ")
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.clientJob = args.clientJob
            viewmodel.preferencesFlow.collect { prefs->
                viewmodel.userId = prefs.userId
                viewmodel.userToken = "Bearer ${prefs.userToken}"

                binding.apply {
                    tvClientName.text = prefs.firstname

                    viewmodel.userId = prefs.userId

                    spinnerJobType.setSelection(1,true)
                    viewmodel.jobType = spinnerJobType.selectedItem.toString()

                    Glide.with(root.context)
                        .load(prefs.profileUrl)
                        .placeholder(R.drawable.ic_placeholder_profile)
                        .into(ivClientProfile)

                    Glide.with(root.context)
                        .load(args.clientJob.profile_url)
                        .placeholder(R.drawable.ic_placeholder_profile)
                        .into(ivFreelancerProfile)
                }
            }
        }

        viewmodel.updateJobStatusDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        /*val action = CreateContractFragmentDirections.actionCreateContractFragmentToContractCreatedSuccessfullyFragment()
                        findNavController().navigate(action)*/
                        findNavController().popBackStack()
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {

        viewmodel.jobContractEvent.collect {event->
            when(event){
                is JobDetailsClientViewmodel.JobContractTask.ShowErrorMessage -> displayError(requireView(),event.msg)
            }.exhaustive
        }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }
}