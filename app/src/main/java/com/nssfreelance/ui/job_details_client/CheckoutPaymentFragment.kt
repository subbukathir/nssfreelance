package com.nssfreelance.ui.job_details_client

import `in`.galaxyofandroid.spinerdialog.SpinnerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.nssfreelance.R
import com.nssfreelance.data.Country
import com.nssfreelance.databinding.FragmentCheckoutPaymentBinding
import com.nssfreelance.retrofit.request.StorePaymentParams
import com.nssfreelance.util.DataState
import com.nssfreelance.util.displayError
import com.nssfreelance.util.exhaustive
import com.stripe.android.ApiResultCallback
import com.stripe.android.PaymentConfiguration
import com.stripe.android.PaymentIntentResult
import com.stripe.android.Stripe
import com.stripe.android.model.ConfirmPaymentIntentParams
import com.stripe.android.model.PaymentMethodCreateParams
import com.stripe.android.model.StripeIntent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_register_freelancer.*
import kotlinx.coroutines.flow.collect


private const val TAG = "CheckoutPaymentFragment"

@AndroidEntryPoint
class CheckoutPaymentFragment : Fragment(R.layout.fragment_checkout_payment) {
    private var _binding: FragmentCheckoutPaymentBinding? = null
    private val binding
        get() = _binding!!
    lateinit var stripe: Stripe

    val viewmodel: JobDetailsClientViewmodel by activityViewModels()
    var progressDialog: AlertDialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentCheckoutPaymentBinding.bind(view)

        binding.apply {
            btnPay.setOnClickListener {
                if (viewmodel.paymentIntentSecret.isNullOrEmpty()) {
                    displayError(requireView(), "Payment secret is missing")
                    viewmodel.getPaymentSecret()
                } else {
                    val params = cardInputWidget.paymentMethodCreateParams
                    if (params != null) {
                        showProgress(true)
                        checkoutPayment(params)
                    } else {
                        displayError(requireView(), "Payment secret is missing")
                    }
                }
            }

            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            etName.addTextChangedListener {

            }
        }

        setProgressDialog()

        subcribeObservers()
        viewmodel.getCountry()
    }

    private fun setProgressDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
        builder.setCancelable(false) // if you want user to wait for some process to finish,

        val view = LayoutInflater.from(requireContext()).inflate(R.layout.view_progress, null)
        val progressBar = view.findViewById<ConstraintLayout>(R.id.progress_bar)
        progressBar.visibility = View.VISIBLE

        builder.setView(view)
        progressDialog = builder.create()
    }

    private fun checkoutPayment(params: PaymentMethodCreateParams) {
        val confirmParams = ConfirmPaymentIntentParams
            .createWithPaymentMethodCreateParams(params, viewmodel.paymentIntentSecret)
        stripe = Stripe(
            requireContext(),
            PaymentConfiguration.getInstance(requireContext()).publishableKey
        )
        stripe.confirmPayment(this, confirmParams)
    }

    private fun subcribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.getPaymentSecret()
            viewmodel.getCountry()

            viewmodel.paymentEvent.collect { event ->
                when(event){
                    JobDetailsClientViewmodel.PaymentDataTask.NavigateToPaymentSuccess -> {
                        viewmodel.getAppliedJobs()
                        viewmodel.getShortlistedJobs()
                        val action = CheckoutPaymentFragmentDirections.actionCheckoutPaymentFragmentToPaymentSuccessFragment()
                        findNavController().navigate(action)

                    }
                    is JobDetailsClientViewmodel.PaymentDataTask.ShowErrorMessage -> {
                        displayError(requireView(),event.msg)
                    }
                }.exhaustive
            }
        }

        viewmodel.paymentSecretDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    showProgress(false)
                    if (dataState.data.status) {
                        viewmodel.paymentIntentSecret = dataState.data.clientSecret
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    showProgress(false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    showProgress(true)
                }
            }.exhaustive
        })

        viewmodel.paymentSuccessDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    showProgress(false)
                    if (dataState.data.status) {
                        viewmodel.isPaid = true
                        viewmodel.updateIsPaid(true)
                        viewmodel.onPaymentSuccess()
                    } else {
                        displayError(requireView(), "Something went wrong on payment success")
                    }
                }
                is DataState.Error -> {
                    showProgress(false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    showProgress(true)
                }
            }.exhaustive
        })

        viewmodel.countryState.observe(viewLifecycleOwner, { countryState ->
            when (countryState) {
                is DataState.Success<List<Country>> -> {
                    showProgress(false)
                    appendCountry(countryState.data)
                }
                is DataState.Error -> {
                    showProgress(false)
                    displayError(requireView(),countryState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${countryState.exception.printStackTrace()}")
                }
                is DataState.Loading -> {
                    showProgress(true)
                }
            }.exhaustive
        })

    }

    private fun appendCountry(data: List<Country>) {
        binding.etCountry.setOnClickListener {
            if (data.isNotEmpty()) {
                val countries = arrayListOf<String>()
                for (item in data) {
                    countries.add(item.country_name)
                }
                val spinnerDialog = SpinnerDialog(
                    requireActivity(),
                    countries,
                    "Choose Country",
                    R.style.DialogAnimations_SmileWindow
                )
                spinnerDialog.setCancellable(false)
                spinnerDialog.setShowKeyboard(false)
                spinnerDialog.setUseContainsFilter(true)
                spinnerDialog.bindOnSpinerListener { item, position ->
                    etCountry.setText(item)
                    viewmodel.country = item
                    Log.d(TAG, "appendCountry: ${data[position].country_id}")
                }
                spinnerDialog.showSpinerDialog()
            }
        }
    }

    private fun showProgress(show: Boolean) {
        if (show) {
            progressDialog?.show()
        } else {
            if (progressDialog?.isShowing!!) {
                progressDialog?.dismiss()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        showProgress(false)
        Log.d(TAG, "onActivityResult: ${requestCode} ${resultCode}")
        // Handle the result of stripe.confirmPayment
        stripe.onPaymentResult(requestCode, data, object : ApiResultCallback<PaymentIntentResult> {
            override fun onSuccess(result: PaymentIntentResult) {
                val paymentIntent = result.intent
                val status = paymentIntent.status
                if (status == StripeIntent.Status.Succeeded) {
                    val gson = GsonBuilder().setPrettyPrinting().create()
                    Log.d(TAG, "onSuccess: ${gson.toJson(paymentIntent)}")
                    val paymentParams = StorePaymentParams(
                        client_user_id = viewmodel.userId,
                        viewmodel.jobId.toString(),
                        data = Gson().fromJson(gson.toJson(paymentIntent),StorePaymentParams.PaymentResponse::class.java)
                    )
                    Log.d(TAG, "onSuccess: params --> $paymentParams")

                    viewmodel.updatePaymentSuccess(paymentParams)

                } else {
                    Log.d(
                        TAG,
                        "onSuccess: Payment failed ${paymentIntent.lastPaymentError?.message ?: ""}"
                    )
                    viewmodel.onPaymentFailed("Payment failed : ${paymentIntent.lastPaymentError?.message ?: ""}")
                }
            }

            override fun onError(e: Exception) {
                Log.d(TAG, "onError: Payment failed --> $e")
                viewmodel.onPaymentFailed("Payment failed, something went wrong try again later.")
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}