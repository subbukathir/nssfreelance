package com.nssfreelance.ui.activities_client.close_contract

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentCloseContractSuccessBinding
import com.nssfreelance.retrofit.request.AddReviewParams
import com.nssfreelance.ui.activities.ActivitiesViewmodel
import com.nssfreelance.ui.activities_client.ActivitiesClientViewmodel
import com.nssfreelance.util.DataState
import com.nssfreelance.util.displayError
import com.nssfreelance.util.exhaustive
import com.nssfreelance.util.progressBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "CloseContractSuccessDia"

@AndroidEntryPoint
class CloseContractSuccessDialogFragment : DialogFragment() {
    private lateinit var binding: FragmentCloseContractSuccessBinding

    private var _currentPosition = 0

    private val args: CloseContractSuccessDialogFragmentArgs by navArgs()
    private val viewmodel: ActivitiesClientViewmodel by activityViewModels()
    private val viewmodelFreelancer: ActivitiesViewmodel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        Log.d(TAG, "onCreateDialog")

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater;
            // Inflate and set the layout for the dialog

            // Pass null as the parent view because its going in the dialog layout
            binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_close_contract_success, null, false
            )
            setProperties(savedInstanceState)
            builder.setView(binding.root)
            builder.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        binding = FragmentCloseContractSuccessBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")
        setProperties(savedInstanceState)
        observers()
    }

    private fun setProperties(savedInstanceState: Bundle?) {
        Log.d(TAG, "setProperties")
        val margin: Int = requireContext().resources.getDimension(R.dimen.dp_20).toInt()
        val back: ColorDrawable = ColorDrawable(Color.TRANSPARENT);
        val inset: InsetDrawable = InsetDrawable(back, margin);
        dialog?.window?.setBackgroundDrawable(inset)

        if (savedInstanceState != null) {
            _currentPosition = savedInstanceState.getInt("position")
        }

        binding.apply {
            btnSubmit.setOnClickListener {
                val rating = ratingBar.rating
                val review = etAboutFreelancer.text.toString()
                if (review.isNullOrBlank()) {
                    displayError(dialog?.window?.decorView!!, "Review should not be empty")
                    return@setOnClickListener
                }
                val params = AddReviewParams(
                    args.freelancerId.toInt(),
                    args.clientId.toInt(),
                    args.postId.toInt(),
                    rating,
                    review
                )
                if (args.isFreelancer)
                    viewmodel.onAddReviewRatingClient(params)
                else viewmodel.onAddReviewRatingFreelancer(params)
            }

            binding.apply {
                tvSuccessTitle.text = "Please review the ${if(args.isFreelancer) "freelance" else "client"} so other users can benefit!!!"
                tvRateFreelancer.text = "How do you rate ${args.userName}?"
                tvAskService.text = "How was ${args.userName.trim()}'s service overall?"
            }
        }


        if (view != null)
            observers()

    }

    private fun observers() {
        Log.d(TAG, "observers: ")
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.preferencesFlow.collect { prefs ->
                viewmodel.userId = prefs.userId
                viewmodel.userToken = "Bearer ${prefs.userToken}"
            }
        }
        viewmodel.updateJobStatusDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.count--
                        if (viewmodel.count == 0) {
                            if (!args.isFreelancer)
                                viewmodel.getPastJobs()
                            else viewmodelFreelancer.getPastJobs()
                            dialog?.dismiss()
                        } else Log.d(TAG, "observers: ")
                    } else {
                        displayError(dialog?.window?.decorView!!, dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(dialog?.window?.decorView!!, dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("position", _currentPosition)
    }
}