package com.nssfreelance.ui.activities_client.past

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.ClientJob
import com.nssfreelance.databinding.ItemPastActivityClientBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency

class PastClientJobsAdapter(private val listener:OnItemClickListener):ListAdapter<ClientJob, PastClientJobsAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding = ItemPastActivityClientBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem =getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding:ItemPastActivityClientBinding):RecyclerView.ViewHolder(binding.root){
        init {

            binding.apply {
                llItemReviewFreelancer.setOnClickListener {
                    listener.onClientReviewClick(getItem(adapterPosition))
                }

                root.setOnClickListener{
                    listener.onItemClick(getItem(adapterPosition))
                }
            }

        }
        fun bind(job:ClientJob){
            binding.apply {

                tvItemJobRole.text = "${job.job_title?.get(0)?.toUpperCase()!!.plus(job.job_title!!.substring(1)) }"
                tvItemName.text = job.freelancer_name?:"NA"
                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemRate.text = "${convertToCurrency(job.contract_final_amount.toString() ?: "0", tvItemJobRole.context)} | ${job.job_type}"
                tvItemDateOfWork.text = "Date of Work ${commonDateFormate(job.job_starts_from)}"
                if(job.is_freelancer_reviewed==1){
                    tvLblStatus.text = "Reviewed"
                    llItemReviewFreelancer.background =
                        ContextCompat.getDrawable(imageviewJobType.context,
                            R.drawable.bg_shape_yellow_dark_filled)
                    tvLblStatus.setTextColor(ContextCompat.getColor(binding.tvItemDateOfWork.context,R.color.white))
                }else{
                    tvLblStatus.text = "Review Freelancer"
                    llItemReviewFreelancer.background =
                        ContextCompat.getDrawable(imageviewJobType.context,
                            R.drawable.bg_shape_white_with_outline_black)
                    tvLblStatus.setTextColor(ContextCompat.getColor(binding.tvItemDateOfWork.context,R.color.text_color))
                }
                if(job.url!=null){
                    Glide.with(tvItemJobRole.context)
                        .load(job.url)
                        .placeholder(R.drawable.ic_plug_wire)
                        .into(imageviewJobType)
                }else{
                    Glide.with(tvItemJobRole.context)
                        .load(R.drawable.ic_plug_wire)
                        .into(imageviewJobType)
                }
            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(job: ClientJob)
        fun onClientReviewClick(job:ClientJob)
    }

    class DiffUtilCallback: DiffUtil.ItemCallback<ClientJob>(){
        override fun areItemsTheSame(oldItem: ClientJob, newItem: ClientJob): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: ClientJob, newItem: ClientJob): Boolean {
            return oldItem == newItem
        }
    }
}