package com.nssfreelance.ui.activities_client.past

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.ClientJob
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.FragmentActivitiesBinding
import com.nssfreelance.ui.activities.ActivitiesViewmodel
import com.nssfreelance.ui.activities.ActivityTabFragmentDirections
import com.nssfreelance.ui.activities.favourite.FavouriteJobsAdapter
import com.nssfreelance.ui.activities_client.ActivitiesClientViewmodel
import com.nssfreelance.ui.activities_client.ActivityClientTabFragmentDirections
import com.nssfreelance.ui.activities_client.close_contract.CloseContractSuccessDialogFragment
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ActivityPastClientFragment:Fragment(R.layout.fragment_activities),PastClientJobsAdapter.OnItemClickListener {
    private var _binding: FragmentActivitiesBinding?=null
    private val binding: FragmentActivitiesBinding
        get() = _binding!!

    private val viewmodel: ActivitiesClientViewmodel by activityViewModels()

    private lateinit var mAdapter: PastClientJobsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentActivitiesBinding.bind(view)

        mAdapter = PastClientJobsAdapter(this)

        binding.apply {
            recyclerviewActivity.itemAnimator = DefaultItemAnimator()
            recyclerviewActivity.adapter =mAdapter
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.getPastJobs()
        }
        viewmodel.pastDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.triggerCount = 0
                        if (dataState.data.data.isNotEmpty()) {
                            viewmodel.pastJobList = dataState.data.data
                            mAdapter.submitList(viewmodel.pastJobList)
                            binding.error.tvError.hide()
                            binding.recyclerviewActivity.show()
                        } else {
                            binding.error.tvError.show()
                            binding.recyclerviewActivity.hide()
                        }
                    } else {
                        displayError(requireView(), "No Data found")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    if(dataState.exception.message?.contains("502 Bad Gateway")==true){
                        displayError(requireView(), "Network error")
                        if (viewmodel.triggerCount >= 3) {
                        } else {
                            viewmodel.triggerCount++
                            progressBar(binding.progress.progressBar, true)
                            viewmodel.getPastJobs()
                            Thread.sleep(5000)
                        }
                    }else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }

    override fun onItemClick(job: ClientJob) {
        val action = ActivityClientTabFragmentDirections.actionActivityClientTabFragmentToJobDetailsClientTabFragment(jobId = job.client_job_post_id,jobType = job.job_type.toString())
        findNavController().navigate(action)
    }

    override fun onClientReviewClick(job: ClientJob) {

        if(job.is_freelancer_reviewed==0){
            viewmodel.isRate = true
            val action = ActivityClientTabFragmentDirections.actionActivityClientTabFragmentToCloseContractSuccessDialogFragment(job.freelancer_user_id.toString(),viewmodel.userId,job.client_job_post_id.toString(),false,
            job.freelancer_name.toString())
            findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}