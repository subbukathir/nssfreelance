package com.nssfreelance.ui.activities_client.close_contract

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentCloseContractBinding
import com.nssfreelance.retrofit.request.ContractRequestParams
import com.nssfreelance.retrofit.request.UpdateJobStatusParams
import com.nssfreelance.ui.activities_client.ActivitiesClientViewmodel
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "CloseContractFragment"

@AndroidEntryPoint
class CloseContractFragment : Fragment(R.layout.fragment_close_contract) {
    private var _binding: FragmentCloseContractBinding? = null
    private val binding
        get() = _binding!!

    private val viewmodel: ActivitiesClientViewmodel by viewModels()
    private val args: CloseContractFragmentArgs by navArgs()
    private lateinit var params: ContractRequestParams

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentCloseContractBinding.bind(view)
        Log.d(TAG, "onViewCreated: ${args.clientJob.freelancer_user_id}")

        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            btnReleasePayment.setOnClickListener {
                val params = UpdateJobStatusParams(
                    Const.COMPLETED,
                    client_job_post_id = viewmodel.contractDetail.client_job_post_id,
                    request_progress_id = args.clientJob.request_progress_id,
                    freelancer_user_id = args.clientJob.freelancer_user_id,
                    client_user_id = viewmodel.userId.toInt(),
                    contract_final_amount = viewmodel.contractDetail.bid_price,
                    contract_job_type_id = if(viewmodel.contractDetail.freelancer_job_type_id!=null) viewmodel.contractDetail.freelancer_job_type_id?.toInt()!! else viewmodel.contractDetail.job_type_id
                )
                viewmodel.onConfirmCompletion(params)
            }

            //set values
            Glide.with(requireContext())
                .load(args.clientJob.url)
                .placeholder(R.drawable.ic_placeholder_profile)
                .into(ivClientProfile)

            tvClientName.setText(args.clientJob.first_name + " " + args.clientJob.last_name)
            tvClientLocation.setText("${args.clientJob.city_name} | ${args.clientJob.country_name}")
            ratingBar.rating =
                if (args.clientJob.rating != null) args.clientJob.rating!!.toFloat() else 0.0F
            tvOriginalPrice.text =
                "Job Price ${convertToCurrency(args.clientJob.job_price, requireContext())} | ${args.clientJob.job_type}"
            tvOfferedPrice.text =
                "Job Price ${convertToCurrency(args.clientJob.job_price, requireContext())} | ${args.clientJob.job_type}"

            Glide.with(requireContext())
                .load(args.clientJob.url)
                .placeholder(R.drawable.ic_plug_wire)
                .into(ivFinalPrice)

            tvJobName.text = "${args.clientJob.job_title}"
            tvJobLocation.text = "${args.clientJob.city_name} | ${args.clientJob.country_name}"
            tvFinalCost.text =
                "Final Cost ${convertToCurrency(args.clientJob.job_price, requireContext())} | ${args.clientJob.job_type}"

            swipeRefresh.setOnRefreshListener {
                viewmodel.getContractDetails(params)
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.preferencesFlow.collect { prefs->
                viewmodel.userId = prefs.userId
                viewmodel.userToken = "Bearer ${prefs.userToken}"
            }
        }
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {

            params = ContractRequestParams(
                args.clientJob.freelancer_user_id,
                viewmodel.userId.toInt(),
                args.clientJob.client_job_post_id
            )
            viewmodel.getContractDetails(params)
        }
        viewmodel.updateContractDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    hideProgress()
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.triggerCount=0
                        //handle data
                        val contractDetail = dataState.data.result[0]
                        viewmodel.contractDetail = contractDetail
                        binding.apply {
                            tvJobName.text = if(contractDetail.job_title!= null) contractDetail.job_title else "NA"
                            tvOriginalPrice.text =
                                "Job Price ${convertToCurrency(contractDetail.job_price, requireContext())} | ${contractDetail.job_type}"
                            tvOfferedPrice.text =
                                if (contractDetail.freelancer_amount != null) "Job Price ${
                                    convertToCurrency(
                                        contractDetail.freelancer_amount,
                                        requireContext()
                                    )
                                }  ${if(contractDetail.freelancer_job_type!=null)" | ${contractDetail.freelancer_job_type}" else ""}" else "Job Price ${
                                    convertToCurrency(
                                        args.clientJob.job_price,
                                        requireContext()
                                    )
                                }  ${if(contractDetail.freelancer_job_type!=null)" | ${contractDetail.freelancer_job_type}" else ""}"
                            tvFreelancerName.text =
                                if (contractDetail.freelancer_name != null) contractDetail.freelancer_name else ""
                            tvFinalCost.text =
                                if (contractDetail.bid_price != null) "Final Cost ${
                                    convertToCurrency(
                                        contractDetail.bid_price.toString(),
                                        requireContext()
                                    )
                                }  ${if(contractDetail.freelancer_job_type!=null)" | ${contractDetail.freelancer_job_type}" else ""}" else "Final Cost ${
                                    convertToCurrency(
                                        contractDetail.bid_price,
                                        requireContext()
                                    )
                                } ${if(contractDetail.freelancer_job_type!=null)" | ${contractDetail.freelancer_job_type}" else ""}"
                            tvFreelancerLocation.text ="${contractDetail.freelancer_city} | ${contractDetail.freelancer_state}"
                            tvClientLocation.text = "${contractDetail.client_city} | ${contractDetail.client_state}"
                            tvProfileRatingClient.text = "${contractDetail.client_review_count ?:0}"
                            tvFreelancerRating.text ="${contractDetail.freelancer_review_count?:0}"
                            ratingBarFreelancer.rating =
                                if (contractDetail.freelancer_rating != null) contractDetail.freelancer_rating.toFloat() else 0f
                            ratingBar.rating=if (contractDetail.client_rating != null) contractDetail.client_rating.toFloat() else 0f

                            if(contractDetail.contract_final_amount.isNullOrEmpty()) btnReleasePayment.hide()
                            else btnReleasePayment.show()

                            if(contractDetail.freelancer_rating!=null) llFreelancerRating.show()
                            else llFreelancerRating.hide()

                            if(contractDetail.client_rating!=null) llClientRating.show()
                            else llClientRating.hide()

                            Glide.with(requireContext())
                                .load(contractDetail.photo_url_client)
                                .placeholder(R.drawable.ic_placeholder_profile)
                                .into(ivClientProfile)

                            Glide.with(requireContext())
                                .load(contractDetail.photo_url_freelancer)
                                .placeholder(R.drawable.ic_placeholder_profile)
                                .into(ivFreelancerProfile)
                        }
                    } else {
                        displayError(requireView(), "No Data found")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    if(dataState.exception.message?.contains("502 Bad Gateway")==true){
                        displayError(requireView(), "Network error")
                        if (viewmodel.triggerCount >= 3) {
                        } else {
                            viewmodel.triggerCount++
                            progressBar(binding.progress.progressBar, true)
                            params = ContractRequestParams(
                                args.clientJob.freelancer_user_id,
                                viewmodel.userId.toInt(),
                                args.clientJob.client_job_post_id
                            )
                            viewmodel.getContractDetails(params)
                            Thread.sleep(5000)
                            progressBar(binding.progress.progressBar, true)
                        }
                    }else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.updateJobStatusDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    hideProgress()
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        displayError(requireView(), "Contract closed successfully")
                        findNavController().popBackStack()
                    } else {
                        displayError(requireView(), "Error: ${dataState.data.message}")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    if(dataState.exception.message?.contains("502 Bad Gateway")==true) {
                        displayError(requireView(), "Network error")
                    }else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }


    private fun hideProgress() {
        if (binding.swipeRefresh.isRefreshing)
            binding.swipeRefresh.isRefreshing = false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}