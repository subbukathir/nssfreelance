package com.nssfreelance.ui.activities_client.favourite

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.ClientJob
import com.nssfreelance.databinding.ItemFavouriteActivityClientBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.hide
import com.nssfreelance.util.show

class FavouriteClientJobsAdapter(private val listener:OnItemClickListener):ListAdapter<ClientJob, FavouriteClientJobsAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding = ItemFavouriteActivityClientBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem =getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding:ItemFavouriteActivityClientBinding):RecyclerView.ViewHolder(binding.root){
        init {

            binding.apply {
                tvItemFavourite.setOnClickListener {
                    listener.onFavoriteClick(getItem(adapterPosition))
                }
                tvItemInvite.setOnClickListener {
                    listener.onShareClick(getItem(adapterPosition))
                }

                root.setOnClickListener{
                    listener.onItemClick(getItem(adapterPosition))
                }
            }

        }
        fun bind(job:ClientJob){
            binding.apply {

                tvItemName.text = job.freelancer_name
                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemJobPostedOn.text = commonDateFormate(job?.joined_since)
                tvItemNumberOfSuccessfulJob.text = "Completed jobs ${job.completed_job_count}"

                if(job.rating_count!= null && job.rating_count==0){
                    llRating.hide()
                }else{
                    llRating.show()
                    tvProfileRating.text = "${job.rating_count}"
                    ratingBar.rating = if(job.rating!=null) job.rating.toFloat() else 0f
                }

                if(job.is_invited==1) tvItemInvite.text = "Invited"
                else tvItemInvite.text = "Invite"

                Glide.with(tvItemJobRole.context)
                    .load(job.profile_photo_url)
                    .placeholder(R.drawable.ic_placeholder)
                    .into(imageviewJobType)

            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(job: ClientJob)
        fun onFavoriteClick(job:ClientJob)
        fun onShareClick(job: ClientJob)
    }

    class DiffUtilCallback: DiffUtil.ItemCallback<ClientJob>(){
        override fun areItemsTheSame(oldItem: ClientJob, newItem: ClientJob): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: ClientJob, newItem: ClientJob): Boolean {
            return oldItem == newItem
        }
    }
}