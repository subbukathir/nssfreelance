package com.nssfreelance.ui.activities_client.current

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.ClientJob
import com.nssfreelance.data.Job
import com.nssfreelance.databinding.FragmentActivitiesBinding
import com.nssfreelance.ui.activities.ActivitiesViewmodel
import com.nssfreelance.ui.activities.ActivityTabFragmentDirections
import com.nssfreelance.ui.activities_client.ActivitiesClientViewmodel
import com.nssfreelance.ui.activities_client.ActivityClientTabFragmentDirections
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ActivityCurrentClientFragment:Fragment(R.layout.fragment_activities) , CurrentClientJobsAdapter.OnItemClickListener {
    private var _binding:FragmentActivitiesBinding?=null
    private val binding:FragmentActivitiesBinding
    get() = _binding!!

    private val viewmodel: ActivitiesClientViewmodel by activityViewModels()

    private lateinit var mAdapter:CurrentClientJobsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentActivitiesBinding.bind(view)

        mAdapter = CurrentClientJobsAdapter(this)

        binding.apply {
            recyclerviewActivity.itemAnimator = DefaultItemAnimator()
            recyclerviewActivity.adapter =mAdapter
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.getCurrentJobs()
        }
        viewmodel.currentDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    viewmodel.isCompleteClicked  = false
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.triggerCount = 0
                        if (dataState.data.data.isNotEmpty()) {
                            viewmodel.currentJobList = dataState.data.data
                            mAdapter.submitList(viewmodel.currentJobList)
                            binding.error.tvError.hide()
                            binding.recyclerviewActivity.show()
                        } else {
                            binding.error.tvError.show()
                            binding.recyclerviewActivity.hide()
                        }
                    } else {
                        displayError(requireView(), "No Data found")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    if(dataState.exception.message?.contains("502 Bad Gateway")==true){
                        displayError(requireView(), "Network error")
                        if (viewmodel.triggerCount >= 3) {
                        } else {
                            viewmodel.triggerCount++
                            progressBar(binding.progress.progressBar, true)
                            viewmodel.getCurrentJobs()
                            Thread.sleep(5000)
                        }
                    }else
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }

    override fun onItemClick(job: ClientJob) {
        val action = ActivityClientTabFragmentDirections.actionActivityClientTabFragmentToJobDetailsClientTabFragment(jobId = job.client_job_post_id,jobType = job.job_type.toString())
        findNavController().navigate(action)
    }

    override fun onMarkCompleteClick(job: ClientJob) {
        viewmodel.isCompleteClicked = true
        val action =
            ActivityClientTabFragmentDirections.actionActivityClientTabFragmentToCloseContractFragment(
                job
            )
        findNavController().navigate(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}