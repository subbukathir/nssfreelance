package com.nssfreelance.ui.activities_client.current

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.ClientJob
import com.nssfreelance.databinding.ItemCurrentActivityClientCardBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency
import com.nssfreelance.util.hide
import com.nssfreelance.util.show

class CurrentClientJobsAdapter(private val listener: OnItemClickListener) :
    ListAdapter<ClientJob, CurrentClientJobsAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding = ItemCurrentActivityClientCardBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding: ItemCurrentActivityClientCardBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {

            binding.apply {
                llMarkComplete.setOnClickListener {
                        listener.onMarkCompleteClick(getItem(adapterPosition))
                }

                root.setOnClickListener {
                    listener.onItemClick(getItem(adapterPosition))
                }
            }

        }

        fun bind(job: ClientJob) {
            binding.apply {

                tvItemJobRole.text = job.job_title
                tvItemName.text = "${job.first_name} ${job.last_name}"
                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemRate.text =
                    "${convertToCurrency(job.contract_final_amount.toString() ?: "0", tvItemJobRole.context)} | ${job.job_type}"
                tvItemDateOfWork.text = "Date of Work ${commonDateFormate(job.job_starts_from)}"
                if (job.is_freelancer_completed == null || job.is_freelancer_completed == 0) {
                    llMarkComplete.show()
                    tvItemContractStatus.text = "Pending"
                    tvLblStatus.text = "Confirm Completion"
                    tvItemContractStatus.setTextColor(
                        ContextCompat.getColor(
                            imageviewJobType.context,
                            R.color.yellow_dark
                        )
                    )
                    tvItemContractStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        R.drawable.ic_ellipse_yellow,
                        0,
                        0,
                        0
                    )
                } else {
                    llMarkComplete.hide()
                    tvItemContractStatus.text = "Completed"
                    tvLblStatus.text = "Confirm Completion"
                    tvItemContractStatus.setTextColor(
                        ContextCompat.getColor(
                            imageviewJobType.context,
                            R.color.error
                        )
                    )
                    tvItemContractStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        R.drawable.ic_ellipse_red,
                        0,
                        0,
                        0
                    )
                }

                if (job.url != null) {
                    Glide.with(tvItemJobRole.context)
                        .load(job.url)
                        .placeholder(R.drawable.ic_plug_wire)
                        .into(imageviewJobType)
                } else {
                    Glide.with(tvItemJobRole.context)
                        .load(R.drawable.ic_plug_wire)
                        .into(imageviewJobType)
                }
            }
        }
    }


    interface OnItemClickListener {
        fun onItemClick(job: ClientJob)
        fun onMarkCompleteClick(job: ClientJob)
    }

    class DiffUtilCallback : DiffUtil.ItemCallback<ClientJob>() {
        override fun areItemsTheSame(oldItem: ClientJob, newItem: ClientJob): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: ClientJob, newItem: ClientJob): Boolean {
            return oldItem == newItem
        }
    }
}