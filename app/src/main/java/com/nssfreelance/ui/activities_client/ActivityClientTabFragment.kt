package com.nssfreelance.ui.activities_client

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayoutMediator
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentActivitiesTabsBinding
import com.nssfreelance.ui.activities.current.ActivityCurrentFragment
import com.nssfreelance.ui.activities.favourite.ActivityFavouriteFragment
import com.nssfreelance.ui.activities.past.ActivityPastFragment
import com.nssfreelance.ui.activities_client.current.ActivityCurrentClientFragment
import com.nssfreelance.ui.activities_client.favourite.ActivityFavouriteClientFragment
import com.nssfreelance.ui.activities_client.past.ActivityPastClientFragment
import com.nssfreelance.ui.register.ViewpagerAdapter
import com.nssfreelance.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "ActivityClientTabFragme"
@AndroidEntryPoint
class ActivityClientTabFragment:Fragment(R.layout.fragment_activities_tabs) {
    private var _binding: FragmentActivitiesTabsBinding? = null
    private val binding
        get() = _binding!!

    private lateinit var viewpagerAdapter: ViewpagerAdapter

    private val viewmodel:ActivitiesClientViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated: ")
        _binding = FragmentActivitiesTabsBinding.bind(view)

        viewpagerAdapter =
            ViewpagerAdapter(fragmentManager = requireActivity().supportFragmentManager, lifecycle)
        viewpagerAdapter.addFragment(ActivityCurrentClientFragment())
        viewpagerAdapter.addFragment(ActivityPastClientFragment())
        viewpagerAdapter.addFragment(ActivityFavouriteClientFragment())

        binding.apply {
            viewpagerActivity.adapter = viewpagerAdapter

            TabLayoutMediator(tabLayoutActivity, viewpagerActivity){tab,position ->
                when(position){
                    0 -> tab.text = "Current"
                    1 -> tab.text = "Past"
                    2 -> tab.text = "Favourites"
                }
            }.attach()

            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.preferencesFlow.collect { prefs->
                viewmodel.userId = prefs.userId
                viewmodel.userToken = "Bearer ${prefs.userToken}"

                if(viewmodel.isCompleteClicked)
                {
                    viewmodel.getCurrentJobs()
                    viewmodel.getPastJobs()
                }
                if(viewmodel.isRate)
                    viewmodel.getPastJobs()
            }

            viewmodel.activityEventTask.collect { event->
                when(event){
                    is ActivitiesClientViewmodel.ActivityTask.NavigateToJobDetails -> {
                        val action = ActivityClientTabFragmentDirections.actionActivityClientTabFragmentToJobDetailsClientTabFragment(jobId = event.job.client_job_post_id,jobType = event.job.job_type.toString())
                        findNavController().navigate(action)
                    }
                    ActivitiesClientViewmodel.ActivityTask.RefreshView -> {

                    }
                }.exhaustive
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}