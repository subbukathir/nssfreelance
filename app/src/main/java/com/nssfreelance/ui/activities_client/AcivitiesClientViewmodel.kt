package com.nssfreelance.ui.activities_client

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.ClientJob
import com.nssfreelance.data.ContractDetail
import com.nssfreelance.data.Job
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.request.AddReviewParams
import com.nssfreelance.retrofit.request.ApplyJobFavouriteParams
import com.nssfreelance.retrofit.request.ContractRequestParams
import com.nssfreelance.retrofit.request.UpdateJobStatusParams
import com.nssfreelance.retrofit.response.AllClientJobResponse
import com.nssfreelance.retrofit.response.AllJobsResponse
import com.nssfreelance.retrofit.response.ContractDetailResponse
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.Const
import com.nssfreelance.util.Const.ACTIVE_JOB
import com.nssfreelance.util.Const.APPLIED_JOB
import com.nssfreelance.util.Const.COMPLETED_JOB
import com.nssfreelance.util.Const.FAVOURITE_JOB
import com.nssfreelance.util.DataState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class ActivitiesClientViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val stateHandle: SavedStateHandle,
    private val preferences: PreferencesManager
) : ViewModel() {
    val preferencesFlow = preferences.preferencesFlow

    var userId = ""
    var userToken = ""
    var count:Int =2
    var isCompleteClicked:Boolean = false
    var isRate:Boolean = false
    var triggerCount = 0

    lateinit var contractDetail:ContractDetail

    //job lists
    var currentJobList: List<ClientJob> = mutableListOf()
    var pastJobList: List<ClientJob> = mutableListOf()
    var favouriteJobList: List<ClientJob> = mutableListOf()
    var appliedJobList: List<Job> = mutableListOf()

    private val _currentDatastate: MutableLiveData<DataState<AllClientJobResponse>> =
        MutableLiveData()
    val currentDatastate: LiveData<DataState<AllClientJobResponse>>
        get() = _currentDatastate


    private val _pastDatastate: MutableLiveData<DataState<AllClientJobResponse>> = MutableLiveData()
    val pastDatastate: LiveData<DataState<AllClientJobResponse>>
        get() = _pastDatastate


    private val _favouriteDatastate: MutableLiveData<DataState<AllClientJobResponse>> =
        MutableLiveData()
    val favouriteDatastate: LiveData<DataState<AllClientJobResponse>>
        get() = _favouriteDatastate


    private val _appliedDatastate: MutableLiveData<DataState<AllJobsResponse>> = MutableLiveData()
    val appliedDatastate: LiveData<DataState<AllJobsResponse>>
        get() = _appliedDatastate

    private val _favouriteJobsState: MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val favouriteJobState: LiveData<DataState<LoginResponse>>
        get() = _favouriteJobsState

    private val _updateJobStatusDatastate: MutableLiveData<DataState<LoginResponse>> =
        MutableLiveData()
    val updateJobStatusDatastate: LiveData<DataState<LoginResponse>>
        get() = _updateJobStatusDatastate

    //Contract details
    private val _updateContractDatastate: MutableLiveData<DataState<ContractDetailResponse>> =
        MutableLiveData()
    val updateContractDatastate: LiveData<DataState<ContractDetailResponse>>
        get() = _updateContractDatastate

    private val _addReviewDatastate: MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val addReviewDatastate: LiveData<DataState<LoginResponse>>
        get() = _addReviewDatastate

    fun getCurrentJobs() {
        viewModelScope.launch {
            repository.getClientActiveJobs(userToken, userId, ACTIVE_JOB)
                .onEach { dataState -> _currentDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }


    fun getPastJobs() {
        viewModelScope.launch {
            repository.getClientCompletedJobs(userToken, userId, COMPLETED_JOB)
                .onEach { dataState -> _pastDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }


    fun getFavouriteJobs() {
        viewModelScope.launch {
            repository.getClientCompletedJobs(userToken, userId, FAVOURITE_JOB)
                .onEach { dataState -> _favouriteDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun getAppliedJobs() {
        viewModelScope.launch {
            repository.getJobsByUrl(userToken, userId, APPLIED_JOB)
                .onEach { dataState -> _appliedDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun getContractDetails(params: ContractRequestParams) {
        viewModelScope.launch {
            repository.requestContractDetail(userToken, params)
                .onEach { dataState -> _updateContractDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    private val activityEventChannel = Channel<ActivityTask>()
    val activityEventTask = activityEventChannel.receiveAsFlow()

    fun onClickOfItem(job: ClientJob) {
        viewModelScope.launch {
            activityEventChannel.send(ActivityTask.NavigateToJobDetails(job))
        }
    }

    fun onFavouriteClicked(job: ClientJob) {
        /*viewModelScope.launch {
            val params = ApplyJobFavouriteParams(
                request_progress_id = (job.request_progress_id?:0).toString(),
                client_job_post_id = (job.client_job_post_id).toString(),
                freelancer_user_id = userId,
                is_applied = "${job.is_applied}",
                is_favourite = if(job.is_favourite==1)"0" else "1",
                client_user_id = (job.client_user_id?:0).toString()
            )
            repository.applyFavouriteJob(userToken,params)
                .onEach { dataState -> _favouriteJobsState.value = dataState }
                .launchIn(viewModelScope)
        }*/
    }

    fun onConfirmCompletion(params: UpdateJobStatusParams) {
        viewModelScope.launch {
            repository.updateClientJobStatus(userToken, params)
                .onEach { dataState -> _updateJobStatusDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun onAddReviewRatingFreelancer(params: AddReviewParams) {
        viewModelScope.launch {
            repository.addReviewFreelancer(userToken, params)
                .onEach { dataState -> _updateJobStatusDatastate.value = dataState }
                .launchIn(viewModelScope)
            repository.addRatingFreelancer(userToken, params)
                .onEach { dataState -> _updateJobStatusDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun onAddReviewRatingClient(params: AddReviewParams) {
        viewModelScope.launch {
            repository.addRatingClient(userToken, params)
                .onEach { dataState -> _updateJobStatusDatastate.value = dataState }
                .launchIn(viewModelScope)
            repository.addReview(userToken, params)
                .onEach { dataState -> _updateJobStatusDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    sealed class ActivityTask {
        data class NavigateToJobDetails(val job: ClientJob) : ActivityTask()
        object RefreshView : ActivityTask()
    }

}