package com.nssfreelance.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.SearchByCategory
import com.nssfreelance.databinding.ItemSearchByCategoryBinding
import com.nssfreelance.util.OnItemClickListener

class SearchByCategoryAdapter(private val listener: OnItemClickListener):ListAdapter<SearchByCategory, SearchByCategoryAdapter.SearchCategoryViewHolder>(DiffUtilCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchCategoryViewHolder {
        val binding = ItemSearchByCategoryBinding.inflate(LayoutInflater.from(parent.context), parent,false)
        return SearchCategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchCategoryViewHolder, position: Int) {
        val currentItem= getItem(position)
        holder.bind(currentItem)
    }

    inner class SearchCategoryViewHolder(private val binding:ItemSearchByCategoryBinding):RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                listener.onItemClick(getItem(adapterPosition).job_category,getItem(adapterPosition).job_category_id.toString())
            }
        }
        fun bind(data:SearchByCategory){
            binding.apply {
                tvItemName.text = data.job_category

                Glide.with(binding.root.context)
                    .load(data.asset_link)
                    .placeholder(R.drawable.ic_truck)
                    .into(ivItemIcon)
            }
        }
    }

    class DiffUtilCallback:DiffUtil.ItemCallback<SearchByCategory>(){
        override fun areItemsTheSame(
            oldItem: SearchByCategory,
            newItem: SearchByCategory
        ): Boolean = oldItem.job_category_id == newItem.job_category_id

        override fun areContentsTheSame(
            oldItem: SearchByCategory,
            newItem: SearchByCategory
        ): Boolean = oldItem==newItem
    }
}