package com.nssfreelance.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nssfreelance.data.SearchByCategory
import com.nssfreelance.databinding.ItemSearchHistoryBinding
import com.nssfreelance.util.OnItemClickListener

class SearchHistoryAdapter(private val listener: OnItemClickListener):ListAdapter<SearchByCategory, SearchHistoryAdapter.SearchCategoryViewHolder>(DiffUtilCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchCategoryViewHolder {
        val binding = ItemSearchHistoryBinding.inflate(LayoutInflater.from(parent.context), parent,false)
        return SearchCategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchCategoryViewHolder, position: Int) {
        val currentItem= getItem(position)
        holder.bind(currentItem)
    }

    inner class SearchCategoryViewHolder(private val binding:ItemSearchHistoryBinding):RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                listener.onItemClick(getItem(adapterPosition).job_category, getItem(adapterPosition).job_category_id.toString())
            }
        }
        fun bind(data:SearchByCategory){
            binding.apply {
                tvSearchHistory.text = data.job_category
            }
        }
    }

    class DiffUtilCallback:DiffUtil.ItemCallback<SearchByCategory>(){
        override fun areItemsTheSame(
            oldItem: SearchByCategory,
            newItem: SearchByCategory
        ): Boolean = oldItem.job_category == newItem.job_category

        override fun areContentsTheSame(
            oldItem: SearchByCategory,
            newItem: SearchByCategory
        ): Boolean = oldItem==newItem
    }
}