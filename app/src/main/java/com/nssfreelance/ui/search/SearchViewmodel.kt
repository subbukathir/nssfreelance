package com.nssfreelance.ui.search

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.*
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.request.ApplyJobFavouriteParams
import com.nssfreelance.retrofit.request.InviteFavouriteClientParams
import com.nssfreelance.retrofit.response.*
import com.nssfreelance.util.Const
import com.nssfreelance.util.Const.FAVOURITE_FREELANCER
import com.nssfreelance.util.Const.INVITE_FREELANCER
import com.nssfreelance.util.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class SearchViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val stateHandle: SavedStateHandle,
    private val preferences:PreferencesManager
):ViewModel() {
    val preferencesFlow = preferences.preferencesFlow
    var userToken = ""
    var userId =""
    var userType = 0

    var isFirst:Boolean = true

    var listSearchByCategory:MutableList<SearchByCategory> = mutableListOf()
    var listPopularSearches:MutableList<SearchByCategory> = mutableListOf()

    val listSearchResult:MutableLiveData<List<SearchJobFreelancer>> = MutableLiveData()

    val listClientSearchResult:MutableLiveData<List<SearchJobClient>> = MutableLiveData()
    //freelancer invite
    val inviteFreelancerJob:MutableLiveData<HomeClientJob> = MutableLiveData()

    private val _favouriteJobsState:MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val favouriteJobState:LiveData<DataState<LoginResponse>>
        get() = _favouriteJobsState

    private val _favouriteInviteFreelancerState:MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val favouriteInviteFreelancertate:LiveData<DataState<LoginResponse>>
        get() = _favouriteInviteFreelancerState

    private val _categoryState:MutableLiveData<DataState<CategoryListResponse>> = MutableLiveData()
    val categoryState:LiveData<DataState<CategoryListResponse>>
        get() = _categoryState

    private val _postedClientJobsState: MutableLiveData<DataState<HomeClientJobResponse>> =
        MutableLiveData()
    val postedClientJobState: LiveData<DataState<HomeClientJobResponse>>
        get() = _postedClientJobsState

    fun updateListCategory(listCategory:List<SearchByCategory>){
        listSearchByCategory = listCategory.toMutableList()
    }


    fun getPopularSearchList():List<SearchByCategory>{
        if(listPopularSearches==null || listPopularSearches.size==0){
            listPopularSearches.add(
                SearchByCategory(
                2,
                "Plumber",
                "",
                false
            )
            )
            listPopularSearches.add(SearchByCategory(
                3,
                "Electrician",
                "",
                false
            ))
            listPopularSearches.add(SearchByCategory(
                4,
                "FanFix",
                "",
                false
            ))
            listPopularSearches.add(SearchByCategory(
                5,
                "MoveHouse",
                "",
                false
            ))
            listPopularSearches.add(SearchByCategory(
                6,
                "LeakFix",
                "",
                false
            ))
            listPopularSearches.add(SearchByCategory(
                7,
                "Install",
                "",
                false
            ))
        }

        return listPopularSearches
    }

    fun getSearchHistoryList():List<SearchByCategory>{
        return listSearchByCategory
    }

    var searchString = stateHandle.get<String>("search_string")?:""
        set(value) {
            field = value
            stateHandle.set("search_string", value)
        }

    private val _searchResultDatastate:MutableLiveData<DataState<SearchJobsResponse>> = MutableLiveData()
    val searchResultDatastate:LiveData<DataState<SearchJobsResponse>>
    get() = _searchResultDatastate


    private val _searchClientDatastate:MutableLiveData<DataState<SearchJobsClientResponse>> = MutableLiveData()
    val searchClientDatastate:LiveData<DataState<SearchJobsClientResponse>>
        get() = _searchClientDatastate

    fun getSearchResult(){
        viewModelScope.launch {
            repository.getSearchResult(userToken, searchString)
                .onEach { dataState -> _searchResultDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun getSearchClientResult(){
        viewModelScope.launch {
            repository.getSearchClientResult(userToken, searchString)
                .onEach { dataState -> _searchClientDatastate.value = dataState }
                .launchIn(viewModelScope)
        }
    }


    fun getCategoryList(){
        viewModelScope.launch {
            repository.getSearchCategory()
                .onEach { dataState -> _categoryState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun getPostedJobList(){
        viewModelScope.launch {
            repository.getHomeClientJobs(userToken, userId, Const.POSTED_JOBS)
                .onEach { dataState -> _postedClientJobsState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun onFavouriteClicked(job: SearchJobFreelancer) {
        viewModelScope.launch {
            val params = ApplyJobFavouriteParams(
                request_progress_id = "0",
                client_job_post_id = (job.client_job_post_id).toString(),
                freelancer_user_id = userId,
                is_applied = "0",
                is_favourite = if(job.is_favourite==1)"0" else "1",
                client_user_id = "0",
                job_bid_price = "0"
            )
            repository.applyFavouriteJob(userToken,params)
                .onEach { dataState -> _favouriteJobsState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun onFavouriteClientClicked(job: SearchJobClient) {
        viewModelScope.launch {
            val params = InviteFavouriteClientParams(
                client_job_post_id = 0,
                freelancer_user_id = job.user_id,
                client_user_id = userId.toInt()
            )
            repository.updateFavouriteInviteFreelancer(userToken,FAVOURITE_FREELANCER,params)
                .onEach { dataState -> _favouriteInviteFreelancerState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun onInviteClientClicked(params: InviteFavouriteClientParams) {
        viewModelScope.launch {
            repository.updateFavouriteInviteFreelancer(userToken, INVITE_FREELANCER,params)
                .onEach { dataState -> _favouriteInviteFreelancerState.value = dataState }
                .launchIn(viewModelScope)
        }
    }


}