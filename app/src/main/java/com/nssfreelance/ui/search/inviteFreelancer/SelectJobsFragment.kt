package com.nssfreelance.ui.search.inviteFreelancer

import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.ClientJob
import com.nssfreelance.data.HomeClientJob
import com.nssfreelance.data.SearchJobFreelancer
import com.nssfreelance.databinding.FragmentSearchResultBinding
import com.nssfreelance.databinding.FragmentSelectJobBinding
import com.nssfreelance.retrofit.response.SearchJobsResponse
import com.nssfreelance.ui.search.SearchViewmodel
import com.nssfreelance.util.*

private const val TAG = "SelectJobsFragment"
class SelectJobsFragment:Fragment(R.layout.fragment_select_job), InviteFreelancerAdapter.OnItemClickListener {
    private var _binding: FragmentSelectJobBinding? = null
    private val binding: FragmentSelectJobBinding
        get() = _binding!!

    private lateinit var adapter: InviteFreelancerAdapter
    private val viewmodel: SearchViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentSelectJobBinding.bind(view)

        adapter =  InviteFreelancerAdapter(this)

        binding.apply {
            imageviewClose.setOnClickListener {
                findNavController().popBackStack()
            }

            //set recylerview
            recyclerviewInviteFreelancer.itemAnimator = DefaultItemAnimator()
            recyclerviewInviteFreelancer.adapter = adapter
        }
        subscribeObservers()
    }

    override fun onItemClick(job: HomeClientJob) {
        Log.d(TAG, "onItemClick: ${job.job_title}")
        viewmodel.inviteFreelancerJob.value = job
        findNavController().popBackStack()
    }

    private fun subscribeObservers(){
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                viewmodel.getPostedJobList()
        }

        viewmodel.postedClientJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        if(dataState.data.data.isNotEmpty())
                        {
                            adapter.submitList(dataState.data.data)
                            binding.error.tvError.hide()
                        }
                        else binding.error.tvError.show()
                    } else {
                        Log.d(TAG, "subscribeObservers: home Jobs empty list")
                    }
                }

                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding =null
    }
}