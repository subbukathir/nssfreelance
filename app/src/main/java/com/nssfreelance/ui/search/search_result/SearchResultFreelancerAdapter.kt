package com.nssfreelance.ui.search.search_result

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.SearchJobFreelancer
import com.nssfreelance.databinding.ItemSearchResultFreelancerBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency

class SearchResultFreelancerAdapter(private val listener:OnItemClickListener):ListAdapter<SearchJobFreelancer, SearchResultFreelancerAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding = ItemSearchResultFreelancerBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem =getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding:ItemSearchResultFreelancerBinding):RecyclerView.ViewHolder(binding.root){
        init {

            binding.apply {
                ivItemFavorite.setOnClickListener {
                    listener.onFavoriteClick(getItem(adapterPosition))
                }
                ivItemShare.setOnClickListener {
                    listener.onShareClick(getItem(adapterPosition))
                }

                root.setOnClickListener{
                    listener.onItemClick(getItem(adapterPosition))
                }
            }

        }
        fun bind(job:SearchJobFreelancer){
            binding.apply {

                tvItemJobRole.text = job.job_title
                tvItemName.text = "${job.first_name?:""} ${job.last_name}"
                tvItemLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemRate.text = "${convertToCurrency((job.job_price?:0).toString(),binding.root.context)}"
                tvItemJobPostedOn.text = commonDateFormate(job.posted_on)
                if(job.is_favourite!=null && job.is_favourite==1){
                    ivItemFavorite.setImageDrawable(ContextCompat.getDrawable(binding.root.context,R.drawable.ic_favorite_filled))
                }else{
                    ivItemFavorite.setImageDrawable(ContextCompat.getDrawable(binding.root.context,R.drawable.ic_favorite_border))
                }

                Glide.with(binding.root.context)
                    .load(job.additional_param_2)
                    .placeholder(R.drawable.ic_plug_wire)
                    .into(imageviewJobType)
                viewYellowSquare.visibility = View.VISIBLE
            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(job: SearchJobFreelancer)
        fun onFavoriteClick(job:SearchJobFreelancer)
        fun onShareClick(job: SearchJobFreelancer)
    }

    class DiffUtilCallback: DiffUtil.ItemCallback<SearchJobFreelancer>(){
        override fun areItemsTheSame(oldItem: SearchJobFreelancer, newItem: SearchJobFreelancer): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: SearchJobFreelancer, newItem: SearchJobFreelancer): Boolean {
            return oldItem == newItem
        }
    }
}