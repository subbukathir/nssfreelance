package com.nssfreelance.ui.search.search_result

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.SearchJobClient
import com.nssfreelance.databinding.ItemSearchResultClientBinding
import com.nssfreelance.util.*

class SearchJobsClientAdapter(private val listener:OnItemClickListener):ListAdapter<SearchJobClient, SearchJobsClientAdapter.JobsViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobsViewHolder {
        val viewBinding = ItemSearchResultClientBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return JobsViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: JobsViewHolder, position: Int) {
        val currentItem =getItem(position)
        holder.bind(currentItem)
    }

    inner class JobsViewHolder(private val binding:ItemSearchResultClientBinding):RecyclerView.ViewHolder(binding.root){
        init {

            binding.apply {
                btnItemFavourite.setOnClickListener {
                    listener.onFavoriteClick(getItem(adapterPosition))
                }
                btnInvite.setOnClickListener {
                    listener.onInviteClick(getItem(adapterPosition))
                }

                root.setOnClickListener{
                    listener.onItemClick(getItem(adapterPosition))
                }
            }

        }
        fun bind(job:SearchJobClient){
            binding.apply {

                tvItemName.text = job.full_name?:""
                tvItemJoined.text = "${commonDateFormate(job.joined_since)}"
                tvItemLocation.text = "${job.city_name?:""} | ${job.country?:""}"
                tvItemJobRate.text = "Rate ${convertToCurrency((job.bid_price?:0).toString(),binding.root.context)}"
                tvItemSuccessfulJobs.text = "Number of successful jobs ${job.completed_job_count}"

                if(job.is_favourite==1){
                    btnItemFavourite.text = "Favourited"
                }else btnItemFavourite.text = "Favourite"

                if(job.is_invited ==1) btnInvite.text ="Invited"
                else btnInvite.text ="Invite"

                if(job.rating!=null){
                    ratingBar.rating = job.rating.toFloat()
                    ratingBar.show()

                }else{
                    ratingBar.hide()
                }
                if(job.photo_profile_url!=null){
                    Glide.with(root.context)
                        .load(job.photo_profile_url)
                        .placeholder(R.drawable.ic_placeholder)
                        .into(ivItemFreelancer)
                }else{
                    Glide.with(root.context)
                        .load(R.drawable.ic_placeholder)
                        .into(ivItemFreelancer)
                }

            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(job: SearchJobClient)
        fun onFavoriteClick(job:SearchJobClient)
        fun onInviteClick(job: SearchJobClient)
    }

    class DiffUtilCallback: DiffUtil.ItemCallback<SearchJobClient>(){
        override fun areItemsTheSame(oldItem: SearchJobClient, newItem: SearchJobClient): Boolean {
            return oldItem.user_id == newItem.user_id
        }

        override fun areContentsTheSame(oldItem: SearchJobClient, newItem: SearchJobClient): Boolean {
            return oldItem == newItem
        }
    }
}