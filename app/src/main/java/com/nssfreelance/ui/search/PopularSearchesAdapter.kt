package com.nssfreelance.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nssfreelance.data.SearchByCategory
import com.nssfreelance.databinding.ItemPopularSearchBinding
import com.nssfreelance.util.OnItemClickListener

class PopularSearchesAdapter(private val listener: OnItemClickListener):ListAdapter<SearchByCategory, PopularSearchesAdapter.SearchCategoryViewHolder>(DiffUtilCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchCategoryViewHolder {
        val binding = ItemPopularSearchBinding.inflate(LayoutInflater.from(parent.context), parent,false)
        return SearchCategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchCategoryViewHolder, position: Int) {
        val currentItem= getItem(position)
        holder.bind(currentItem)
    }

    inner class SearchCategoryViewHolder(private val binding:ItemPopularSearchBinding):RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                listener.onItemClick(getItem(adapterPosition).job_category,getItem(adapterPosition).toString())
            }
        }
        fun bind(data:SearchByCategory){
            binding.apply {
                tvPopularSearch.text = data.job_category
            }
        }
    }

    class DiffUtilCallback:DiffUtil.ItemCallback<SearchByCategory>(){
        override fun areItemsTheSame(
            oldItem: SearchByCategory,
            newItem: SearchByCategory
        ): Boolean = oldItem.job_category == newItem.job_category

        override fun areContentsTheSame(
            oldItem: SearchByCategory,
            newItem: SearchByCategory
        ): Boolean = oldItem==newItem
    }
}