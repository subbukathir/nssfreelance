package com.nssfreelance.ui.search.inviteFreelancer

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nssfreelance.data.ClientJob
import com.nssfreelance.data.HomeClientJob
import com.nssfreelance.databinding.ItemInviteFreelancerBinding
import com.nssfreelance.util.commonDateFormate

class InviteFreelancerAdapter(private val listener:OnItemClickListener):ListAdapter<HomeClientJob, InviteFreelancerAdapter.InviteViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InviteViewHolder {
        val viewBinding = ItemInviteFreelancerBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return InviteViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: InviteViewHolder, position: Int) {
        val currentItem =getItem(position)
        holder.bind(currentItem)
    }

    inner class InviteViewHolder(private val binding:ItemInviteFreelancerBinding):RecyclerView.ViewHolder(binding.root){
        init {
            binding.apply {
                root.setOnClickListener{
                    listener.onItemClick(getItem(adapterPosition))
                }
            }
        }
        fun bind(job:HomeClientJob){
            binding.apply {

                tvItemPostTitle.text = "${job.job_title}"
                tvItemPostLocation.text = "${job.city_name} | ${job.country_name}"
                tvItemPostPaymentType.text = "Payment ${job.job_price?:"0"} | ${job.job_type}"
                tvItemPostedTime.text = "Posted on "+ commonDateFormate(job?.job_post_created_on)
            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(job: HomeClientJob)
    }

    class DiffUtilCallback: DiffUtil.ItemCallback<HomeClientJob>(){
        override fun areItemsTheSame(oldItem: HomeClientJob, newItem: HomeClientJob): Boolean {
            return oldItem.client_job_post_id == newItem.client_job_post_id
        }

        override fun areContentsTheSame(oldItem: HomeClientJob, newItem: HomeClientJob): Boolean {
            return oldItem == newItem
        }
    }
}