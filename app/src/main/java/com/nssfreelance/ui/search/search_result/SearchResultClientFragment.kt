package com.nssfreelance.ui.search.search_result

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.SearchJobClient
import com.nssfreelance.databinding.FragmentSearchClientResultBinding
import com.nssfreelance.retrofit.request.InviteFavouriteClientParams
import com.nssfreelance.retrofit.response.SearchJobsClientResponse
import com.nssfreelance.ui.search.SearchViewmodel
import com.nssfreelance.util.*

private const val TAG = "SearchResultFragment"

class SearchResultClientFragment : Fragment(R.layout.fragment_search_client_result),
    SearchJobsClientAdapter.OnItemClickListener {
    private var _binding: FragmentSearchClientResultBinding? = null
    private val binding: FragmentSearchClientResultBinding
        get() = _binding!!

    private lateinit var adapter: SearchJobsClientAdapter
    private val viewmodel: SearchViewmodel by activityViewModels()
    private lateinit var inviteParams: InviteFavouriteClientParams
    private var isModified:Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentSearchClientResultBinding.bind(view)

        adapter = SearchJobsClientAdapter(this)

        binding.apply {

            toolbar.apply {
                imageviewBack.setOnClickListener {
                    findNavController().popBackStack()
                }
                imageviewFilter.hide()
                tvToolbar.text = "Search Results"
            }
            etSearch.setText(viewmodel.searchString)
            ivClearText.setOnClickListener {
                etSearch.setText("")
                viewmodel.searchString = ""
            }

            etSearch.addTextChangedListener {
                it?.let {
                    if (it.toString().trim().isNotEmpty()) {
                        viewmodel.searchString = it.toString().trim()
                    }
                }
            }

            etSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Your piece of code on keyboard search click
                    if (viewmodel.searchString.isNotEmpty()) {
                        Log.d(TAG, "onViewCreated: ${viewmodel.searchString}")
                        hideKeyboard(requireActivity())
                        viewmodel.getSearchClientResult()
                    }
                    return@OnEditorActionListener true
                }
                false
            })

            //set recylerview
            recyclerviewSearchResult.itemAnimator = DefaultItemAnimator()
            recyclerviewSearchResult.adapter = adapter
        }
        subscribeObservers()
    }

    override fun onItemClick(job: SearchJobClient) {
        //move to freelancer view
    }

    override fun onFavoriteClick(job: SearchJobClient) {
        viewmodel.onFavouriteClientClicked(job)
    }

    override fun onInviteClick(job: SearchJobClient) {
        isModified = true
        inviteParams = InviteFavouriteClientParams()
        inviteParams.freelancer_user_id = job.user_id
        inviteParams.client_user_id = viewmodel.userId.toInt()
        findNavController().navigate(R.id.action_searchResultClientFragment_to_selectJobsFragment)
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            if (viewmodel.searchString.isNotEmpty()) {// && viewmodel.isFirst
                viewmodel.getSearchClientResult()
                viewmodel.isFirst = false
            }
        }

        viewmodel.searchClientDatastate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success<SearchJobsClientResponse> -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        if (dataState.data.data.isNotEmpty()) {
                            viewmodel.listClientSearchResult.value = dataState.data.data
                            binding.error.tvError.hide()
                            binding.recyclerviewSearchResult.show()
                        } else {
                            binding.error.tvError.show()
                            binding.recyclerviewSearchResult.hide()
                        }
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                is DataState.Loading -> {
                    progressBar(binding.progress.progressBar, false)
                }
            }
        })


        viewmodel.favouriteInviteFreelancertate.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.getSearchClientResult()
//                        displayError(requireView(), "Status updated")
                    } else {
                        Log.d(TAG, "subscribeObservers: empty list")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${dataState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.listClientSearchResult.observe(viewLifecycleOwner, { data ->
            adapter.submitList(data)
        })

        viewmodel.inviteFreelancerJob.observe(viewLifecycleOwner, { data ->
            if(isModified){
                inviteParams.client_job_post_id = data.client_job_post_id
                Log.d(TAG, "subscribeObservers: $inviteParams")
                viewmodel.onInviteClientClicked(inviteParams)
                isModified = false
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}