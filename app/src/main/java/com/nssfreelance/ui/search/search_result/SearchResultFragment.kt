package com.nssfreelance.ui.search.search_result

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.data.SearchJobFreelancer
import com.nssfreelance.databinding.FragmentSearchResultBinding
import com.nssfreelance.retrofit.response.SearchJobsResponse
import com.nssfreelance.ui.search.SearchViewmodel
import com.nssfreelance.util.*

private const val TAG = "SearchResultFragment"
class SearchResultFragment:Fragment(R.layout.fragment_search_result), SearchResultFreelancerAdapter.OnItemClickListener {
    private var _binding: FragmentSearchResultBinding? = null
    private val binding: FragmentSearchResultBinding
        get() = _binding!!

    private lateinit var adapter: SearchResultFreelancerAdapter
    private val viewmodel: SearchViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentSearchResultBinding.bind(view)

        adapter =  SearchResultFreelancerAdapter(this)

        binding.apply {

            toolbar.apply {
                imageviewBack.setOnClickListener {
                    findNavController().popBackStack()
                }
                imageviewFilter.hide()
                tvToolbar.text = "Search Results"
            }
            etSearch.setText(viewmodel.searchString)
            ivClearText.setOnClickListener {
                etSearch.setText("")
                viewmodel.searchString=""
            }

            etSearch.addTextChangedListener {
                it?.let {
                    if(it.toString().trim().isNotEmpty())
                    {
                        viewmodel.searchString = it.toString().trim()
                    }
                }
            }

            etSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Your piece of code on keyboard search click
                    if (viewmodel.searchString.isNotEmpty()) {
                        Log.d(TAG, "onViewCreated: ${viewmodel.searchString}")
                        hideKeyboard(requireActivity())
                        viewmodel.getSearchResult()
                    }
                    return@OnEditorActionListener true
                }
                false
            })

            //set recylerview
            recyclerviewSearchResult.itemAnimator = DefaultItemAnimator()
            recyclerviewSearchResult.adapter = adapter
        }
        subscribeObservers()
    }

    override fun onItemClick(job: SearchJobFreelancer) {
        val action = SearchResultFragmentDirections.actionSearchResultFragmentToJobAndClientDetailsTabFragment(job.client_job_post_id.toString(),0)
        findNavController().navigate(action)
    }

    override fun onFavoriteClick(job: SearchJobFreelancer) {
        viewmodel.onFavouriteClicked(job)
    }

    override fun onShareClick(job: SearchJobFreelancer) {
        val shareBody = "${job.job_title} ${job.country_name} | ${job.city_name} \n " +
                "${convertToCurrency(job.job_price.toString(), requireContext())} \n" +
                "Posted on ${commonDateFormate(job.posted_on)}"
        shareJobs(shareBody,requireContext())
    }

    private fun subscribeObservers(){
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            if(viewmodel.searchString.isNotEmpty())
                viewmodel.getSearchResult()
        }

        viewmodel.searchResultDatastate.observe(viewLifecycleOwner, {dataState ->
            when(dataState){
                is DataState.Success<SearchJobsResponse> -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        if (dataState.data.data.isNotEmpty()) {
                            viewmodel.listSearchResult.value = dataState.data.data
                            binding.error.tvError.hide()
                            binding.recyclerviewSearchResult.show()
                        } else {
                            binding.error.tvError.show()
                            binding.recyclerviewSearchResult.hide()
                        }
                    } else {
                        displayError(requireView(), dataState.data.message)
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                is DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }
        })


        viewmodel.favouriteJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.getSearchResult()
                    } else {
                        Log.d(TAG, "subscribeObservers: empty list")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${dataState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.listSearchResult.observe(viewLifecycleOwner,{data->
            adapter.submitList(data)
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding =null
    }
}