package com.nssfreelance.ui.search

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.gson.Gson
import com.nssfreelance.R
import com.nssfreelance.data.RegisterDetails
import com.nssfreelance.data.SearchByCategory
import com.nssfreelance.databinding.FragmentSearchBinding
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect


private const val TAG = "SearchFragment"

@AndroidEntryPoint
class SearchFragment : Fragment(R.layout.fragment_search), OnItemClickListener {
    private var _binding: FragmentSearchBinding? = null
    private val binding: FragmentSearchBinding
        get() = _binding!!

    private lateinit var adapterCategory: SearchByCategoryAdapter
    private lateinit var adapterPopularSearch: PopularSearchesAdapter
    private lateinit var adapterSearchHistoryAdapter: SearchHistoryAdapter

    private val viewmodel: SearchViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentSearchBinding.bind(view)

        adapterCategory = SearchByCategoryAdapter(this)
        adapterPopularSearch = PopularSearchesAdapter(this)
        adapterSearchHistoryAdapter = SearchHistoryAdapter(this)

        binding.apply {
            recylerviewSearchCategory.layoutManager = GridLayoutManager(requireContext(), 2)
            recylerviewSearchCategory.itemAnimator = DefaultItemAnimator()
            recylerviewSearchCategory.adapter = adapterCategory
            recylerviewSearchCategory.isNestedScrollingEnabled = false
            recylerviewSearchCategory.addItemDecoration(GridSpacingItemDecoration(2,16,true))

            recylerviewPopularSearch.layoutManager = StaggeredGridLayoutManager(
                3,
                StaggeredGridLayoutManager.VERTICAL
            )
            recylerviewPopularSearch.itemAnimator = DefaultItemAnimator()
            recylerviewPopularSearch.adapter = adapterPopularSearch
            recylerviewPopularSearch.isNestedScrollingEnabled = false

            recylerviewSearchHistory.layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            )
            recylerviewSearchHistory.itemAnimator = DefaultItemAnimator()
            recylerviewSearchHistory.adapter = adapterSearchHistoryAdapter
            recylerviewSearchHistory.isNestedScrollingEnabled = false

                //adapterCategory.submitList(viewmodel.getListByCategory())
                adapterPopularSearch.submitList(viewmodel.getPopularSearchList())
                adapterSearchHistoryAdapter.submitList(viewmodel.getSearchHistoryList())

            //show initially
            llSearchCategory.show()
            llSearchHistory.hide()

            etSearch.addTextChangedListener {
                it?.let {
                    if(it.toString().trim().isNotEmpty())
                    {
                        viewmodel.searchString = it.toString().trim()
                        llSearchCategory.hide()
                        llSearchHistory.show()
                    }else{
                        llSearchCategory.show()
                        llSearchHistory.hide()
                    }
                }
            }

            etSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Your piece of code on keyboard search click
                    if (viewmodel.searchString.isNotEmpty()) {
                        Log.d(TAG, "onViewCreated: ${viewmodel.searchString}")
                        hideKeyboard(requireActivity())

                        moveToSearchResultFragment()
                    }
                    return@OnEditorActionListener true
                }
                false
            })

            ivClearText.setOnClickListener {
                etSearch.setText("")
                viewmodel.searchString=""
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            viewmodel.preferencesFlow.collect { prefs ->
                viewmodel.userToken = "Bearer ${prefs.userToken}"
                viewmodel.userId = prefs.userId
                viewmodel.userType = prefs.userType
                binding.apply {
                    tvTitle.text = prefs.locality
                }


                viewmodel.getCategoryList()
            }
        }

        viewmodel.categoryState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        updateCategory(dataState.data.data)
                    } else {
                        Log.d(TAG, "subscribeObservers: empty list")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${dataState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })
    }

    private fun updateCategory(listCategory:List<SearchByCategory>) {
        adapterCategory.submitList(listCategory)
        adapterSearchHistoryAdapter.submitList(listCategory)
        viewmodel.updateListCategory(listCategory)
    }

    override fun onItemClick(searchStr: String,id:String) {
        Log.d(TAG, "onItemClick: $searchStr")
        viewmodel.searchString = searchStr
        moveToSearchResultFragment()
    }

    private fun moveToSearchResultFragment(){
        val action:NavDirections = if(viewmodel.userType == 2){
            SearchFragmentDirections.actionSearchFragmentToSearchResultFragment()
        }else{
            SearchFragmentDirections.actionSearchFragmentToSearchResultClientFragment()
        }
        findNavController().navigate(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}