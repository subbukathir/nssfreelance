package com.nssfreelance.ui.pwd_account_create_success

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentShowSuccessMessageBinding
import com.nssfreelance.ui.register.RegisterLandingFragmentDirections
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PasswordAccountSuccessFragment : Fragment(R.layout.fragment_show_success_message) {
    private var _binding: FragmentShowSuccessMessageBinding? = null
    private val binding
        get() = _binding!!

    private val args: PasswordAccountSuccessFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentShowSuccessMessageBinding.bind(view)

        binding.apply {
            imageviewBack.setOnClickListener {
                moveToFurther()
            }

            tvSuccessTitle.text = args.title
            if (args.moveToLogin) {
                tvSuccessContent.text = getString(R.string.msg_password_change_content)
                btnSuccess.text = getString(R.string.login_with_password)
            } else {
                tvSuccessContent.text = getString(R.string.msg_success_content)
                btnSuccess.text = getString(R.string.proceed_to_login_page)
            }

            btnSuccess.setOnClickListener {
                moveToFurther()
            }
        }
    }

    private fun moveToFurther() {

        findNavController().popBackStack(R.id.action_global_passwordAccountSuccessFragment, true)

        val action = PasswordAccountSuccessFragmentDirections.actionGlobalAuthTabsFragment("Sign In")
        findNavController().navigate(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}