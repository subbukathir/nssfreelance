package com.nssfreelance.ui.register_choose_skills

import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.nssfreelance.R
import com.nssfreelance.data.RegisterDetails
import com.nssfreelance.databinding.FragmentRegisterFreelancerSkillsBinding
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.DataState
import com.nssfreelance.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "ChooseSkillsFragment"
@AndroidEntryPoint
class ChooseSkillsFragment:Fragment(R.layout.fragment_register_freelancer_skills) {

    private val viewmodel:ChooseSkillsViewmodel by viewModels()
    private var _binding:FragmentRegisterFreelancerSkillsBinding?=null
    private val binding:FragmentRegisterFreelancerSkillsBinding
    get() = _binding!!

    private lateinit var skillAdapter:ChooseSkillAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentRegisterFreelancerSkillsBinding.bind(view)
        skillAdapter = ChooseSkillAdapter()
        binding.apply {

            recyclerviewSkills.setHasFixedSize(true)
            recyclerviewSkills.layoutManager = StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL)
            recyclerviewSkills.adapter = skillAdapter

            skillAdapter.submitList(viewmodel.getSkills()?: listOf())

            btnSubmitSkills.setOnClickListener {
                viewmodel.onRegisterClick(skillAdapter.currentList)
            }
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }
        }
        setSpannableSignInStr()

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.skillEvent.collect { event->
                when(event){
                    is ChooseSkillsViewmodel.ChooseSkillsTaskEvent.ShowInvalideMessage -> {
                        Snackbar.make(requireView(),event.msg,Snackbar.LENGTH_LONG).show()
                    }
                }.exhaustive
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        //observe preferences flow of datastore values
        viewmodel.preferencesFlow.asLiveData().observe(viewLifecycleOwner, { preferences ->
            val userDetailsStr = preferences.userDetails
            val userId = preferences.userId
            Log.d(TAG, "subscribeObservers: $userDetailsStr $userId")
            if(userId.isNotEmpty()){
                viewmodel.userId =userId.toInt()
            }
            if (userDetailsStr.isNotEmpty()) {
                val userDetails = Gson().fromJson(userDetailsStr, RegisterDetails::class.java)
                if (userDetails?.skills != null) {
                    viewmodel.setRegisterParamsDetails(userDetails)
                    skillAdapter.submitList(viewmodel.getSkills())
                }
            }
        })

        viewmodel.postSkillsState.observe(viewLifecycleOwner,{dataState->
            when (dataState) {
                is DataState.Success<LoginResponse> -> {
                    displayProgressbar(false)

                    val action = ChooseSkillsFragmentDirections.actionGlobalPasswordAccountSuccessFragment("Account Creation Completed",
                        false)
                    findNavController().navigate(action)
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(dataState.exception.message)
                }
                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
        } else {
            Snackbar.make(requireView(), "Unknown error", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun displayProgressbar(isDisplayed: Boolean) {
        binding.progress.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }


    private fun setSpannableSignInStr() {
        val termsConditions = getString(R.string.lbl_terms_condition)

        val spannableString = SpannableString(termsConditions)
        val clickableSpan = object: ClickableSpan(){
            override fun onClick(widget: View) {
                Snackbar.make(requireView(),"Terms & Conditions",Snackbar.LENGTH_SHORT).show()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                ds.color = Color.parseColor("#DAA520")
            }
        }

        spannableString.setSpan(clickableSpan, 45,63, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding.tvTermsNCondition.text=spannableString
        binding.tvTermsNCondition.movementMethod = LinkMovementMethod.getInstance()
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}