package com.nssfreelance.ui.register_choose_skills

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.RegisterDetails
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.retrofit.request.SkillsParams
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.DataState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class ChooseSkillsViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val stateHandle: SavedStateHandle,
    private val preferences: PreferencesManager
) : ViewModel() {

//    private val _dataState = MutableLiveData<DataState<>>

    private val _registerDetails: MutableLiveData<RegisterDetails> = MutableLiveData()

    private val _postSkillsState: MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val postSkillsState: LiveData<DataState<LoginResponse>>
        get() = _postSkillsState

    val preferencesFlow = preferences.preferencesFlow

    fun setRegisterParamsDetails(details: RegisterDetails) {
        _registerDetails.value = details
    }

    fun getSkills(): List<RegisterDetails.Skills>? {
        return _registerDetails.value?.skills
    }

    fun postSkills(skillsParams:SkillsParams) {
        viewModelScope.launch {
            repository.postSkills(skillsParams)
                .onEach { dataState ->
                    _postSkillsState.value = dataState
                }
                .launchIn(viewModelScope)
        }
    }

    private val registerEventChannel = Channel<ChooseSkillsTaskEvent>()
    val skillEvent = registerEventChannel.receiveAsFlow()

    private var _tabPosition: Int = 0

    fun setTabPosition(tabPos: Int) {
        _tabPosition = tabPos
    }

    var isFreelancer = stateHandle.get<Boolean>("isFreelancer") ?: false
        set(value) {
            field = value
            stateHandle.set("isFreelancer", value)
        }
    var userId = stateHandle.get<Int>("userId") ?: 0
        set(value) {
            field = value
            stateHandle.set("userId", value)
        }

    fun onRegisterClick(currentList: List<RegisterDetails.Skills>) {
        var count: Int = 0
        if (currentList.isNotEmpty()) {
            for (item in currentList) {
                if (item.isChecked) count += 1
            }

            if (count < 3) {
                showMinSkillsSelection("Kindly choose at least 3 skills")
                return
            } else {
                val listSkillset: MutableList<SkillsParams.SkillSet> = mutableListOf()
                for (item in currentList) {
                    if(item.isChecked)
                    listSkillset.add(SkillsParams.SkillSet(item.skill_id))
                }
                val skillsParams= SkillsParams(userId,listSkillset)
                postSkills(skillsParams = skillsParams)
            }

        }
    }

    private fun showMinSkillsSelection(msg: String) = viewModelScope.launch {
        registerEventChannel.send(ChooseSkillsTaskEvent.ShowInvalideMessage(msg))
    }

    sealed class ChooseSkillsTaskEvent {
        data class ShowInvalideMessage(val msg: String) : ChooseSkillsTaskEvent()
    }


}