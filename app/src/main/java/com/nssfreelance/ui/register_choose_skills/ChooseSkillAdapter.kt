package com.nssfreelance.ui.register_choose_skills

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nssfreelance.data.RegisterDetails
import com.nssfreelance.databinding.ItemFreelancerSkillsBinding

class ChooseSkillAdapter() :
    ListAdapter<RegisterDetails.Skills, ChooseSkillAdapter.ChooseSkillsViewHolder>(DiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChooseSkillsViewHolder {
        val binding = ItemFreelancerSkillsBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return ChooseSkillsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ChooseSkillsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class ChooseSkillsViewHolder(private val binding: ItemFreelancerSkillsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val position= adapterPosition
                if(position !=RecyclerView.NO_POSITION){
                    val item = getItem(position)
                    item.isChecked = !item.isChecked
                    notifyDataSetChanged()
                }

            }
        }
        fun bind(data:RegisterDetails.Skills){
            binding.apply {
                tvSkill.text = data.skill
                tvSkill.isChecked = data.isChecked
            }
        }
    }

    class DiffCallback: DiffUtil.ItemCallback<RegisterDetails.Skills>(){
        override fun areItemsTheSame(
            oldItem: RegisterDetails.Skills,
            newItem: RegisterDetails.Skills
        ): Boolean =
            oldItem.skill_id == newItem.skill_id


        override fun areContentsTheSame(
            oldItem: RegisterDetails.Skills,
            newItem: RegisterDetails.Skills
        ): Boolean =
            oldItem == newItem

    }

}


