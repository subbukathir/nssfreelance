package com.nssfreelance.ui.otp

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentOtpBinding
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.ui.otp.OTPViewmodel.ForgotPwdTaskEvent.ShowInvalidMsg
import com.nssfreelance.ui.register.RegisterFragmentArgs
import com.nssfreelance.ui.register.RegisterFragmentDirections
import com.nssfreelance.util.DataState
import com.nssfreelance.util.exhaustive
import com.nssfreelance.util.hideKeyboard
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import android.os.CountDownTimer


@AndroidEntryPoint
class OTPFragment : Fragment(R.layout.fragment_otp) {
    private var _binding: FragmentOtpBinding? = null
    private val binding
        get() = _binding!!
    private lateinit var cTimer: CountDownTimer

    private val viewmodel: OTPViewmodel by viewModels()
    private val args: OTPFragmentArgs by navArgs()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentOtpBinding.bind(view)

        viewmodel.userType = args.userTypeId

        binding.apply {
            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }

            btnVerify.setOnClickListener {
                viewmodel.otpValue = otpPinEntry.value.toString()
                viewmodel.onSubmitClick()
                cancelTimer()
            }

            tvVerifyAccount.text =
                "Please enter the one-time password(OTP) that we have sent to your phone number: 0${
                    args.userPhoneNumber.drop(
                        3
                    )
                }"
            enablePinview(true)
            startTimer()
            tvResendOtp.setOnClickListener {
                cancelTimer()
                tvCountdownTime.visibility = View.INVISIBLE
                if (otpPinEntry.value.isNullOrEmpty())
                    otpPinEntry.value = "1234"
                otpPinEntry?.clearValue()
                viewmodel.otpValue = ""
                enablePinview(false)
                viewmodel.onResendOtp(args.userPhoneNumber.replace("+", ""))
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewmodel.otpState.observe(viewLifecycleOwner, Observer { dataState ->
            when (dataState) {
                is DataState.Success<LoginResponse> -> {
                    displayProgressbar(false)
                    if (dataState.data.status) {
                        displayError(dataState.data.message)

                        val action: NavDirections
                        if (viewmodel.userType.equals("3")) {
                            action =
                                OTPFragmentDirections.actionGlobalPasswordAccountSuccessFragment(
                                    "Account Creation Completed",
                                    false
                                )
                        } else {
                            action =
                                OTPFragmentDirections.actionOTPFragmentToChooseSkillsFragment()
                        }
                        findNavController().navigate(action)
                    } else {
                        displayError("OTP failure")
                    }
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(dataState.exception.message)
                }
                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }
        })
        viewmodel.resendOtpState.observe(viewLifecycleOwner, Observer { dataState ->
            when (dataState) {
                is DataState.Success<LoginResponse> -> {
                    displayProgressbar(false)
                    if (dataState.data.status) {
                        binding.apply {
                            startTimer()
                            enablePinview(true)
                            tvCountdownTime.visibility = View.VISIBLE
                        }
                    } else {
                        displayError("ResendOTP failure")
                    }
                }
                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(dataState.exception.message)
                }
                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }
        })

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.preferencesFlow.collect { prefs ->
                viewmodel.uid = prefs.uid
                viewmodel.userToken = "Bearer ${prefs.userToken}"
                viewmodel.userId = prefs.userId
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.forgotPwdEvent.collect { event ->
                when (event) {
                    is ShowInvalidMsg -> {
                        displayError(event.msg)
                    }
                }.exhaustive
            }
        }

    }

    //start timer function
    fun startTimer() {
        cTimer = object : CountDownTimer(120000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.tvCountdownTime.text =
                    "OTP is valid for ${millisUntilFinished / 1000} seconds"
            }

            override fun onFinish() {
                binding.apply {
                    if (otpPinEntry.value.toString() != null && !otpPinEntry.value.toString()
                            .equals("")
                    ) {
                        tvCountdownTime.visibility = View.INVISIBLE
                    } else {
                        enablePinview(false)
                    }
                }
            }
        }
        cTimer.start()
    }


    //cancel timer
    fun cancelTimer() {
        if (cTimer != null) cTimer.cancel()
    }

    private fun enablePinview(enable: Boolean) {
        if (enable) {
            binding.apply {
                otpPinEntry.isEnabled = true
                tvResendOtp.isEnabled = false
            }
        } else {
            binding.apply {
                otpPinEntry.isEnabled = false
                tvResendOtp.isEnabled = true
            }
        }
    }

    private fun displayError(message: String?) {
        hideKeyboard(requireActivity())
        if (message != null) {
            Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
        } else {
            Snackbar.make(requireView(), "Unknown error", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun displayProgressbar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}