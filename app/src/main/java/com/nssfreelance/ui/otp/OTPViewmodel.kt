package com.nssfreelance.ui.otp

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.DataState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class OTPViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val state:SavedStateHandle,
    private val preferences:PreferencesManager
):ViewModel() {

    val preferencesFlow = preferences.preferencesFlow
    var uid:String=""
    var userId:String = ""
    var userToken: String =""

    private val _otpState:MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val otpState:LiveData<DataState<LoginResponse>>
    get() = _otpState

    private val _resendOtpState:MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val resendOtpState:LiveData<DataState<LoginResponse>>
        get() = _resendOtpState

    var otpValue = state.get<String>("otpValue") ?:""
    set(value) {
        field = value
        state.set("otpValue", value)
    }

    var userType = state.get<String>("userType") ?: ""
        set(value) {
            field = value
            state.set("userType", value)
        }

    private val _forgotPwdChannel = Channel<ForgotPwdTaskEvent>()
    val forgotPwdEvent = _forgotPwdChannel.receiveAsFlow()

    fun onSubmitClick(){
        if(otpValue.isNullOrBlank()){
            showInvalidMessage("OTP should not be empty")
            return
        }

        val params = HashMap<String, String>()
        params["otp_no"] = otpValue
        params["user_id"] = userId

        viewModelScope.launch {
            repository.verifyOTP(params)
                .onEach { dataState -> _otpState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun onResendOtp(mobileNo:String){
        val params = HashMap<String, String>()
        params["user_Id"] = userId
        params["mobile_no"] = mobileNo

        viewModelScope.launch {
            repository.resendOTP(params)
                .onEach { dataState -> _resendOtpState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    private fun showInvalidMessage(message:String) = viewModelScope.launch {
        _forgotPwdChannel.send(ForgotPwdTaskEvent.ShowInvalidMsg(message))
    }

    sealed class ForgotPwdTaskEvent{
        data class ShowInvalidMsg(val msg: String) : ForgotPwdTaskEvent()
    }

}