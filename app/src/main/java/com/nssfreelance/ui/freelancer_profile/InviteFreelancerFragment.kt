package com.nssfreelance.ui.freelancer_profile

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentInviteFreelancerClientBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InviteFreelancerFragment : Fragment(R.layout.fragment_invite_freelancer_client) {
    private var _binding: FragmentInviteFreelancerClientBinding? = null
    private val binding
        get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentInviteFreelancerClientBinding.bind(view)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}