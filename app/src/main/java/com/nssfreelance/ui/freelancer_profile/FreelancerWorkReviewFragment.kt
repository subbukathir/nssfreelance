package com.nssfreelance.ui.freelancer_profile

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentFreelancerReviewsClientBinding
import com.nssfreelance.ui.home.HomeViewmodel
import com.nssfreelance.ui.job_details.JobDetailsViewmodel
import com.nssfreelance.util.hide
import com.nssfreelance.util.show
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "FreelancerWorkReviewFra"
@AndroidEntryPoint
class FreelancerWorkReviewFragment : Fragment(R.layout.fragment_freelancer_reviews_client) {

    private var _binding: FragmentFreelancerReviewsClientBinding? = null
    private val binding: FragmentFreelancerReviewsClientBinding
        get() = _binding!!

    private val viewmodel: HomeViewmodel by activityViewModels()

    private lateinit var mAdapter: WorkReviewsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentFreelancerReviewsClientBinding.bind(view)
        mAdapter = WorkReviewsAdapter()
        binding.apply {
            recyclerviewWorkReview.itemAnimator = DefaultItemAnimator()
            recyclerviewWorkReview.adapter = mAdapter
            val listData = viewmodel.getReviewDetails()
            if (listData != null && listData.size > 0) {
                mAdapter.submitList(listData)
                showDataview(true)
            } else {
                showDataview(false)
            }
        }
        subscribeObservers()
    }

    private fun subscribeObservers() {
        Log.d(TAG, "subscribeObservers: ")
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.freelancerReview.observe(viewLifecycleOwner, { data ->
                if (data.size > 0) {
                    mAdapter.submitList(data)
                    showDataview(true)
                } else showDataview(false)
            })
        }
    }

    private fun showDataview(show: Boolean) {
        if (show) {
            binding.apply {
                error.tvError.hide()
                recyclerviewWorkReview.show()

            }
        } else {
            binding.apply {
                error.tvError.show()
                recyclerviewWorkReview.hide()
            }
        }
        binding.apply {
            btnFavourite.hide()
            btnInvite.hide()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}