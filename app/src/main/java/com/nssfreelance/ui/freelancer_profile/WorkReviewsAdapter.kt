package com.nssfreelance.ui.freelancer_profile

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.data.Review
import com.nssfreelance.databinding.ItemClientReviewBinding
import com.nssfreelance.databinding.ItemWorkReviewsBinding
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.convertToCurrency

class WorkReviewsAdapter :
    ListAdapter<Review, WorkReviewsAdapter.WorkReviewViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkReviewViewHolder {
        val binding =
            ItemWorkReviewsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WorkReviewViewHolder(binding)
    }

    override fun onBindViewHolder(holder: WorkReviewViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class WorkReviewViewHolder(private val binding: ItemWorkReviewsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(review: Review) {
            binding.apply {
                tvItemJobName.text = "${review.job_title}"
                tvItemClientName.text = review.name
                tvItemJobDate.text = "Date of Work ${commonDateFormate(review.job_starts_from)}"
                tvItemJobRate.text = "Cost of Project ${convertToCurrency(review.contract_final_amount.toString(),tvItemJobName.context)}"
                tvItemRating.text = "4.2"
                tvItemPostedTime.text = ""
                tvItemReview.text = "${review.review}"
            }
        }
    }


    class DiffUtilCallback : DiffUtil.ItemCallback<Review>() {
        override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean {
            return oldItem.client_job_review_id == newItem.client_job_review_id
        }

        override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean {
            return oldItem == newItem
        }
    }
}