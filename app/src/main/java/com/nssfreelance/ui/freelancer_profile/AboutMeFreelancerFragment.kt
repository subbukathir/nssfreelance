package com.nssfreelance.ui.freelancer_profile

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentAboutClientBinding
import com.nssfreelance.ui.home.HomeViewmodel
import com.nssfreelance.ui.job_details.JobDetailsViewmodel
import com.nssfreelance.ui.job_details_client.JobDetailsClientViewmodel
import com.nssfreelance.util.commonDateFormate
import com.nssfreelance.util.hide
import com.nssfreelance.util.show
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AboutMeFreelancerFragment:Fragment(R.layout.fragment_about_client) {
    private var _binding:FragmentAboutClientBinding?=null
    private val binding:FragmentAboutClientBinding
    get() = _binding!!

    private val viewmodel:HomeViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentAboutClientBinding.bind(view)
        binding.apply {
            llLockView.hide()
            llUnlockView.show()
            llAboutMe.hide()
            viewDividerBottom.hide()
            btnJobFavourite.hide()
            btnViewClientJobs.hide()
        }
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.profileData.observe(viewLifecycleOwner,{data->
                binding.apply {
                    if(data!=null){
                        tvAge.text = "Phone Number : ${data.mobile_no}"
                        tvGender.text = "Email ID : ${data.email_address} ${data.gender?:""}"
                        tvContactNumber.text = "Address : ${data.city_name?:""} | ${data.country_name?:""}"
                        tvAddress.text = "Skills : ${data.skills?:""}\n\nRegistered On : ${commonDateFormate(data.registered_on!!)}"

                        tvAboutClient.text = "${data.about_me}"
                    }
                }
            })
        }
    }

    fun showUnlockView(show:Boolean){
        if(show){
            binding.apply {
                llUnlockView.show()
                llLockView.hide()
            }
        }else{
            binding.apply {
                llLockView.show()
                llUnlockView.hide()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}