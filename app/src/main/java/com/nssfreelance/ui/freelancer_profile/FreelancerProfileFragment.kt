package com.nssfreelance.ui.freelancer_profile

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayoutMediator
import com.nssfreelance.R
import com.nssfreelance.data.Profile
import com.nssfreelance.databinding.FragmentClientDetailsFreelancerBinding
import com.nssfreelance.databinding.FragmentClientFreelancerProfileBinding
import com.nssfreelance.ui.home.HomeViewmodel
import com.nssfreelance.ui.job_details.JobDetailsViewmodel
import com.nssfreelance.ui.job_details_client.JobDetailsClientTabFragmentArgs
import com.nssfreelance.ui.job_details_client.JobDetailsClientViewmodel
import com.nssfreelance.ui.register.ViewpagerAdapter
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "FreelancerProfileFragme"
@AndroidEntryPoint
class FreelancerProfileFragment : Fragment(R.layout.fragment_client_freelancer_profile) {
    private var _binding: FragmentClientFreelancerProfileBinding? = null
    private val binding: FragmentClientFreelancerProfileBinding
        get() = _binding!!

    private val viewmodel: HomeViewmodel by activityViewModels()
    private val args: FreelancerProfileFragmentArgs by navArgs()
    private lateinit var viewpagerAdapter: ViewpagerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentClientFreelancerProfileBinding.bind(view)

        viewpagerAdapter =
            ViewpagerAdapter(fragmentManager = requireActivity().supportFragmentManager, lifecycle)
        viewpagerAdapter.addFragment(AboutMeFreelancerFragment())
        viewpagerAdapter.addFragment(FreelancerWorkReviewFragment())

        binding.apply {
            viewpagerClientDetails.adapter = viewpagerAdapter
            TabLayoutMediator(
                tabClientDetails,
                viewpagerClientDetails
            ) { tab, position ->
                when (position) {
                    0 -> tab.text = "About"
                    1 -> tab.text = "Work Reviews"
                }
            }.attach()

            imageviewBack.setOnClickListener {
                findNavController().popBackStack()
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.preferencesFlow.collect { flow->
                viewmodel.userId = args.userId
                viewmodel.userToken = flow.userToken
                viewmodel.userTypeId = flow.userType

                viewmodel.getProfileData()
            }
        }

        viewmodel.profileState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    Log.d(TAG, "subscribeObservers: success ${dataState.data.data}")
                    progressBar(binding.progress.progressBar, false)
                    updateProfileView(dataState.data.data.profile)
                    viewmodel.freelancerReview.value = dataState.data.data.review
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

    }

    private fun updateProfileView(data: Profile) {
        binding.apply {
            viewmodel.profileData.value = data
            if (!data.profile_photo_link.isNullOrEmpty()) {
                Glide.with(requireContext())
                    .load(data.profile_photo_link ?: "")
                    .placeholder(R.drawable.ic_placeholder_profile)
                    .into(civClientProfile)
            } else {
                Glide.with(requireContext())
                    .load(R.drawable.ic_placeholder_profile)
                    .into(civClientProfile)
            }


            tvProfileName.text = data.name ?: ""
            tvProfileJobRole.text =  "${data.city_name} | ${data.country_name}"
            tvProfileRating.text = "${data.rating_count}"


            if(data.rating_count==0) llRating.hide()
            else {
                llRating.show()
                ratingBar.rating = if(data.rating!=null) data.rating.toFloat() else 0f
            }

            tvJobCompleted.text = convertToCurrency((data.total_earnings ?: 0).toString(), requireContext())
            tvJobsBid.text = (data.application_submitted ?: 0).toString()
            tvDeclined.text =
                convertToCurrency((data.amount_pending ?: 0).toString(), requireContext())

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}