package com.nssfreelance.ui.apply_job

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavArgs
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentJobApplySuccessBinding
import com.nssfreelance.util.hide

class JobAppliedSuccessFragment:Fragment(R.layout.fragment_job_apply_success) {
    private var _binding: FragmentJobApplySuccessBinding?=null
    private val binding
        get() = _binding!!
    val args:JobAppliedSuccessFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentJobApplySuccessBinding.bind(view)

        binding.apply {
            btnBrowseJobs.setOnClickListener {
                val action = JobAppliedSuccessFragmentDirections.actionJobAppliedSuccessFragmentToHomeFragment()
                findNavController().navigate(action)
            }

            btnViewStatus.setOnClickListener {

            }

            btnViewStatus.hide()
            tvSuccessTitle.text = "Congratulations ${args.username}!"
            tvSuccessContent.text = "Application for ${args.jobTitle} has been submitted! \n" +
                    "\n" +
                    "We have notified the client about the details."
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }
}