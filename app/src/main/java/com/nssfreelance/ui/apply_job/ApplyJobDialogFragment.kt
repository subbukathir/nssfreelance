package com.nssfreelance.ui.apply_job

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.nssfreelance.R
import com.nssfreelance.databinding.FragmentApplyJobBinding
import com.nssfreelance.ui.job_details.JobDetailsViewmodel
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

private const val TAG = "JobSuccessDialogFragmen"

@AndroidEntryPoint
class ApplyJobDialogFragment : DialogFragment() {
    private lateinit var binding: FragmentApplyJobBinding

    private val viewmodel: JobDetailsViewmodel by viewModels()

    private val args: ApplyJobDialogFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "Oncreate")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        Log.d(TAG, "onCreateDialog")

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater;
            // Inflate and set the layout for the dialog

            // Pass null as the parent view because its going in the dialog layout
            binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_apply_job, null, false
            )
            setProperties(savedInstanceState)
            builder.setView(binding.root)
            builder.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.d(TAG, "onCreateView")
        binding = FragmentApplyJobBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")
        setProperties(savedInstanceState)
    }

    private fun setProperties(savedInstanceState: Bundle?) {
        Log.d(TAG, "setProperties")
        val margin: Int = requireContext().resources.getDimension(R.dimen.dp_20).toInt()
        val back: ColorDrawable = ColorDrawable(Color.TRANSPARENT)
        val inset: InsetDrawable = InsetDrawable(back, margin)
        dialog?.window?.setBackgroundDrawable(inset)

        dialog?.window?.attributes?.alpha = 0.9f


        binding.apply {
            spinnerJobType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?,
                    view: View,
                    i: Int,
                    l: Long
                ) {
                    viewmodel.jobType = spinnerJobType.selectedItem.toString()
                }

                override fun onNothingSelected(adapterView: AdapterView<*>?) {
                    return
                }
            }

            etOfferPrice.addTextChangedListener {
                viewmodel.jobPrice = it.toString()
                try {
                    val text: String = viewmodel.jobPrice
                    val textLength: Int = viewmodel.jobPrice.length
                    if (text.endsWith("$") || text.endsWith(" ")) return@addTextChangedListener
                    if (textLength == 1) {
                        if (!text.contains("$")) {
                            etOfferPrice.setText(
                                StringBuilder(text).insert(text.length - 1, "$").toString()
                            )
                            etOfferPrice.setSelection(viewmodel.jobPrice.length)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            btnSubmit.setOnClickListener {
                viewmodel.onJobApplyClicked()
            }

            args.jobDetail.let { data ->
                Log.d(TAG, "setProperties: ${data.job_type}")
                    tvFixedPrice.text = "Job Price ${
                        convertToCurrency(
                            data.job_price.toString(),
                            requireContext()
                        )
                    } | ${data.job_type}"

                    tvFreelancerLocation.text = "${data.city_name}${if(data.country_name!=null) " | ${data.country_name}" else ""}"
                    tvClientLocation.text = "${args.clientCity} | ${args.clientCountry}"

                    tvClientName.text = data.client_user_name
                tvFreelancerName.text = "${args.freelancerName}"

                    tvRatedCountClient.text = "0"
                    tvProfileRatingClient.text = "0"
                    Glide.with(requireContext())
                        .load(R.drawable.ic_placeholder_profile)
                        .into(ivClientProfile)

                    Glide.with(requireContext())
                        .load(R.drawable.ic_placeholder_profile)
                        .into(ivFreelancerProfile)

                    //hiding completely
                    llRating.hide()
                    llFreelancerRating.hide()
            }
        }
        if (view != null)
            subscribeObservers()
    }

    private fun subscribeObservers() {
        Log.d(TAG, "subscribeObservers: ")
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewmodel.preferencesFlow.collect { prefs ->
                Log.d(TAG, "subscribeObservers: preferences")
                binding.tvFreelancerName.text = prefs.firstname
                viewmodel.userName = prefs.firstname

                viewmodel.userId = prefs.userId
                viewmodel.userToken = "Bearer ${prefs.userToken}"
                viewmodel.jobDet = args.jobDetail
            }
        }

        viewmodel.applyJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.onSuccessApplyJob()
                    } else {
                        Log.d(TAG, "subscribeObservers: empty list")
                    }
                }
                is DataState.Error -> {
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${dataState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.applyJobDetailsEvent.collect { event ->
                when (event) {

                    is JobDetailsViewmodel.JobDetailsTaskEvent.ShowErrorMsg -> {
                        Log.d(TAG, "subscribeObservers: ${event.msg}")
                        displayError(dialog?.window?.decorView!!, event.msg)
                    }
                    is JobDetailsViewmodel.JobDetailsTaskEvent.NavigateToSuccessApply -> {
                        Log.d(TAG, "subscribeObservers: NavigateToSuccessApply")
                        val action =
                            ApplyJobDialogFragmentDirections.actionApplyJobDialogFragmentToJobAppliedSuccessFragment(
                                event.username,
                                event.jobTitle
                            )
                        findNavController().navigate(action)
                    }
                    is JobDetailsViewmodel.JobDetailsTaskEvent.NavigateToApplyJob -> {
                    }
                }.exhaustive
            }
        }
    }

}