package com.nssfreelance.ui.job_details

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.nssfreelance.data.Client
import com.nssfreelance.data.Job
import com.nssfreelance.data.JobDetails
import com.nssfreelance.data.Review
import com.nssfreelance.data.preferences.PreferencesManager
import com.nssfreelance.repository.Repository
import com.nssfreelance.retrofit.request.ApplyJobFavouriteParams
import com.nssfreelance.retrofit.response.JobDetailsResponse
import com.nssfreelance.retrofit.response.LoginResponse
import com.nssfreelance.util.DataState
import com.nssfreelance.util.displayError
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

private const val TAG = "JobDetailsViewmodel"

class JobDetailsViewmodel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val stateHandle: SavedStateHandle,
    private val preferences: PreferencesManager
) : ViewModel() {
    val preferencesFlow = preferences.preferencesFlow

    var userToken = ""
    var userId = ""
    var jobId = ""
    var clientName: String? = ""
    var userName: String = ""
    var jobDet: JobDetails? = null
    var clientDet: Client? = null

    var jobType = stateHandle.get<String>("jobType")?:""
        set(value) {
            field = value
            stateHandle.set("jobType", value)
        }

    var jobPrice = stateHandle.get<String>("jobPrice")?:""
        set(value) {
            field = value
            stateHandle.set("jobPrice", value)
        }

    private val _jobDetailstate: MutableLiveData<DataState<JobDetailsResponse>> = MutableLiveData()
    val jobDetailsState: LiveData<DataState<JobDetailsResponse>>
        get() = _jobDetailstate


    private val _favouriteJobsState: MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val favouriteJobState: LiveData<DataState<LoginResponse>>
        get() = _favouriteJobsState


    private val _applyJobsState: MutableLiveData<DataState<LoginResponse>> = MutableLiveData()
    val applyJobState: LiveData<DataState<LoginResponse>>
        get() = _applyJobsState


    val jobDetails: MutableLiveData<JobDetails> = MutableLiveData()
    val clientJobList: MutableLiveData<List<Job>> = MutableLiveData()
    val clientReviewList: MutableLiveData<List<Review>> = MutableLiveData()
    val clientDetails: MutableLiveData<Client> = MutableLiveData()

    fun getDetails() {
        viewModelScope.launch {
            repository.getJobDetails(userToken, jobId)
                .onEach { dataState -> _jobDetailstate.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun updateData(data: JobDetailsResponse.JobDetailGroup) {
        Log.d(TAG, "updateData")
        jobDetails.value = data.job_details
        clientJobList.value = data.client_posted_jobs
        clientDetails.value = if(data.about_client!=null&&data.about_client.size>0) data.about_client[0] else Client()
        clientDet = if(data.about_client!=null&&data.about_client.size>0) data.about_client[0] else Client()
        clientReviewList.value = data.review_about_client

        clientName = clientDetails.value!!.full_name
    }

    private val jobDetailsTaskEventChannel = Channel<JobDetailsTaskEvent>()
    val jobDetailsEvent = jobDetailsTaskEventChannel.receiveAsFlow()

    private val applyJobTaskEventChannel = Channel<JobDetailsTaskEvent>()
    val applyJobDetailsEvent = applyJobTaskEventChannel.receiveAsFlow()


    fun onClickOfJobApply() {
        viewModelScope.launch {
            jobDetailsTaskEventChannel.send(JobDetailsTaskEvent.NavigateToApplyJob(jobDet!!))
        }
    }

    fun getClientDetails(): Client? {
        return clientDetails.value
    }

    fun getJobDetails(): MutableList<Job>? {
        return clientJobList.value as MutableList<Job>?
    }

    fun getReviewDetails(): MutableList<Review>? {
        return clientReviewList.value as MutableList<Review>?
    }

    fun onFavouriteClicked() {

        Log.d(TAG, "onFavouriteClicked: ${jobDet?.is_favourite}")
        viewModelScope.launch {
            val params = ApplyJobFavouriteParams(
                request_progress_id = (jobDet?.request_progress_id ?: 0).toString(),
                client_job_post_id = (jobDet?.client_job_post_id).toString(),
                freelancer_user_id = userId,
                is_applied = "${jobDet?.is_applied}",
                is_favourite = if (jobDet?.is_favourite == 1) "0" else "1",
                client_user_id = (jobDet?.client_user_id ?: 0).toString(),
                job_id = (jobDet?.client_job_post_id).toString(),
            )
            repository.applyFavouriteJob(userToken, params)
                .onEach { dataState -> _favouriteJobsState.value = dataState }
                .launchIn(viewModelScope)
        }
    }

    fun onJobApplyClicked() {
        if(isValid()){
            viewModelScope.launch {
                val params = ApplyJobFavouriteParams(
                    request_progress_id = (jobDet?.request_progress_id ?: 0).toString(),
                    client_job_post_id = (jobDet?.client_job_post_id).toString(),
                    freelancer_user_id = userId,
                    is_applied = "1",
                    is_favourite = "${jobDet?.is_favourite}",
                    client_user_id = (jobDet?.client_user_id ?: 0).toString(),
                    job_bid_price = "${jobPrice.replace("$","")}",
                    job_id = (jobDet?.client_job_post_id).toString(),
                    job_type_id = getJobTypeId()
                )
                repository.applyFavouriteJob(userToken, params)
                    .onEach { dataState -> _applyJobsState.value = dataState }
                    .launchIn(viewModelScope)
            }
        }
    }

    private fun getJobTypeId(): String {
        if(jobType=="Fixed") return "10"
        else if(jobType=="Per Hour") return "11"
        else return "12"
    }

    private fun isValid(): Boolean {
        if (jobType.isNullOrEmpty() || jobType == "Choose Type") {
            showErrorMessage("Kindly select any one job type")
            return false
        }

        if (jobPrice.isNullOrBlank()) {
            showErrorMessage( "Kindly enter job price")
            return false
        }
        if(jobPrice.length==1){
            showErrorMessage( "Kindly enter job price")
            return false
        }
        if(jobPrice!!.replace("$","").toInt() <=0){
            showErrorMessage( "Job price should not be zero or less")
            return false
        }
        return true
    }

    private fun showErrorMessage(msg: String) = viewModelScope.launch {
        Log.d(TAG, "showErrorMessage: $msg")
        applyJobTaskEventChannel.send(JobDetailsTaskEvent.ShowErrorMsg(msg))
    }


    fun onSuccessApplyJob() {
        viewModelScope.launch {
            jobDet?.job_title?.let {
                JobDetailsTaskEvent.NavigateToSuccessApply(
                    userName,
                    it
                )
            }?.let { applyJobTaskEventChannel.send(it) }
        }

    }

    sealed class JobDetailsTaskEvent {
        data class NavigateToApplyJob(val jobDetail:JobDetails) : JobDetailsTaskEvent()
        data class NavigateToSuccessApply(val username: String, val jobTitle: String) :
            JobDetailsTaskEvent()

        data class ShowErrorMsg(val msg: String) : JobDetailsTaskEvent()
    }


}