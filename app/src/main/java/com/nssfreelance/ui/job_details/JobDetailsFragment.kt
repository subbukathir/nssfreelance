package com.nssfreelance.ui.job_details

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.nssfreelance.R
import com.nssfreelance.data.JobDetails
import com.nssfreelance.databinding.FragmentJobDetailsFreelancerBinding
import com.nssfreelance.ui.job_and_client_details.JobAndClientDetailsTabFragmentDirections
import com.nssfreelance.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlin.math.log

private const val TAG = "JobDetailsFragment"

@AndroidEntryPoint
class JobDetailsFragment : Fragment(R.layout.fragment_job_details_freelancer) {
    private var _binding: FragmentJobDetailsFreelancerBinding? = null
    private val binding: FragmentJobDetailsFreelancerBinding
        get() = _binding!!

    private val viewmodel: JobDetailsViewmodel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentJobDetailsFreelancerBinding.bind(view)

        //Initially set null for clear all old data
        viewmodel.jobDetails.value = JobDetails()
        binding.apply {
            btnJobApply.setOnClickListener {
                if (viewmodel.jobDet?.is_applied == 0)
                {
                    //viewmodel.onClickOfJobApply()
                    val action = JobAndClientDetailsTabFragmentDirections.actionJobAndClientDetailsTabFragmentToApplyJobDialogFragment(
                        viewmodel.jobDet!!,
                        viewmodel.clientDet?.country_name!!,
                        viewmodel.clientDet?.city_name!!,
                        viewmodel.userName
                    )
                    findNavController().navigate(action)
                }
                else {
                    val snackbar =
                        Snackbar.make(requireView(), "Job already applied", Snackbar.LENGTH_LONG)

                    snackbar.show()
                }


            }

            btnJobFavourite.setOnClickListener {
                viewmodel.onFavouriteClicked()
            }

            swipeRefresh.setOnRefreshListener {
                viewmodel.getDetails()
            }
        }

        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.getDetails()
        }

        viewmodel.jobDetailsState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        Log.d(TAG, "Data received")
                        viewmodel.updateData(dataState.data.data)
                    } else {
                        Log.d(TAG, "subscribeObservers: empty list")
                    }
                }
                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${dataState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.favouriteJobState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.progress.progressBar, false)
                    if (dataState.data.status) {
                        viewmodel.getDetails()
                    } else {
                        Log.d(TAG, "subscribeObservers: empty list")
                    }
                }
                is DataState.Error -> {
                    binding.swipeRefresh.isRefreshing = false
                    progressBar(binding.progress.progressBar, false)
                    displayError(requireView(), dataState.exception.message)
                    Log.e(TAG, "subscribeObservers: ${dataState.exception.printStackTrace()}")
                }
                DataState.Loading -> {
                    progressBar(binding.progress.progressBar, true)
                }
            }.exhaustive
        })

        viewmodel.jobDetails.observe(viewLifecycleOwner, { data ->
            progressBar(binding.progress.progressBar, false)
            Log.d(TAG, "subscribeObservers: $data")
            binding.apply {
                viewmodel.jobDet = data
                tvItemJobRole.text = data.job_title ?: ""
                tvItemName.text = data.client_user_name
                tvItemLocation.text = "${data.city_name} ${if(data.country_name!=null) " | ${data.country_name}" else ""}"
                tvItemRate.text = "${
                    convertToCurrency(
                        data.job_price.toString(),
                        requireContext()
                    )
                } | ${data.job_type}"

                tvPostedDate.text = commonDateFormate(date = data.job_post_created_on)
                tvPostExpiryDate.text = commonDateFormate(date = data.job_ends_on)
                tvJobDate.text = commonDateFormate(date = data.job_starts_from)

                tvJobOverview.text = data.job_overview
                tvJobQualification.text = data.job_qualification ?: "NA"
                tvJobRequirement.text = data.job_requirement

                /*if (data.is_favourite != null && data.is_favourite == 1) {
                    btnJobFavourite.text = "Favourited"
                    btnJobFavourite.background = ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.bg_shape_white_with_outline_yellow
                    )
                    btnJobFavourite.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        R.drawable.ic_favorite_border,
                        0,
                        0,
                        0
                    )
                } else {
                    btnJobFavourite.text = "Favourite"
                    btnJobFavourite.background = ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.bg_shape_white_with_outline_black
                    )
                    btnJobFavourite.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0)
                }*/

                if (data.is_favourite != null && data.is_favourite == 1) {
                    btnJobFavourite.text = "Favourited"
                    btnJobFavourite.background = ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.bg_shape_yellow_dark_filled
                    )
                    btnJobFavourite.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                } else {
                    btnJobFavourite.text = "Favourite"
                    btnJobFavourite.background = ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.bg_shape_white_with_outline_black
                    )
                    btnJobFavourite.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.black
                        )
                    )
                }

                if (data.is_applied != null && data.is_applied == 1) {
                    btnJobApply.text = "Applied"
                    btnJobApply.background = ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.bg_shape_yellow_dark_filled
                    )
                    btnJobApply.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                } else {
                    btnJobApply.text = "Apply"
                    btnJobApply.background = ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.bg_shape_white_with_outline_black
                    )
                    btnJobApply.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.black
                        )
                    )
                }

                ivJobDetailsShare.setOnClickListener {
                    val shareBody =
                        "${data.job_title} ${data.country_name} ${data.city_name} \n Client : ${data.client_user_name} " +
                                "${
                                    convertToCurrency(
                                        data.job_price.toString(),
                                        requireContext()
                                    )
                                } | ${data.job_type} \n" +
                                "Posted on ${commonDateFormate(data.job_post_created_on)} | Ends on ${
                                    commonDateFormate(
                                        data.job_ends_on
                                    )
                                }"
                    shareJobs(shareBody, requireContext())
                }

            }
        })

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewmodel.jobDetailsEvent.collect { event ->
                when (event) {
                    is JobDetailsViewmodel.JobDetailsTaskEvent.ShowErrorMsg -> {
                        Log.d(TAG, "subscribeObservers: ${event.msg}")
                        displayError(requireView(), event.msg)
                    }
                    is JobDetailsViewmodel.JobDetailsTaskEvent.NavigateToSuccessApply -> {

                    }
                    is JobDetailsViewmodel.JobDetailsTaskEvent.NavigateToApplyJob -> {

                    }
                }.exhaustive
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}